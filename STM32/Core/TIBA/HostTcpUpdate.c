


#include "main.h"
#include <string.h>

#define VER_STR         "Ver  10.0"

#define FLASH_MAGIC     (uint32_t)(0x081FFF00)


#define WAIT_QSPI_IDLE           0
#define WAIT_QSPI_READ           1
#define WAIT_QSPI_WRITE          2
#define WAIT_QSPI_PROG           3

#define MAX_BF_SIZE_HOST     0x600

static uint8_t   RxB     [MAX_BF_SIZE_HOST]      ;
static uint8_t   RxT     [0x100]                 ;
static uint8_t   UseBf   [MAX_BF_SIZE_HOST / 2] ;
static uint8_t   RxHex   [MAX_BF_SIZE_HOST / 2] ;
static uint16_t  RxHexPoi                       ;
static uint16_t  RxBpoi , TxBpoi                ;
static uint8_t   RxFlag                         ;
static uint16_t  HostHoldLen                    ;
static uint8_t   HostState = WAIT_QSPI_IDLE      ;
static uint8_t   TimeOut   = 0                  ;
static uint8_t  *PtrB                            ;
static uint8_t   TxCrc                           ;
static uint32_t  magicP                          ;
#ifdef __PRG_30__
extern uint8_t   Global_ProgFlash               ;
uint32_t   Global_SendFld             ;
#endif

void DoorContSetCM(byte cm);

uint8_t SST25ReadByte(uint32_t address)
{
  return 0 ;
}

void SST25WriteByte(uint8_t data, uint32_t address)
{
}

uint8_t Hex2Char(uint8_t hex)
{
    uint8_t    h;
    h = hex & 0x0f;

    // From 0xa to 0xf
    if(h > 9)
        return (h + 55);
    else
        return (h + 48);
}

void PutTxChar(uint8_t ch)
{
  TxCrc += ch ;
  if (TxBpoi < sizeof(RxT)) RxT[TxBpoi++] = ch ;
}

void PutTxByte(uint8_t Data)
{
    PutTxChar(Hex2Char(Data >> 4));
    PutTxChar(Hex2Char(Data & 0x0f));
}

void HostBuildTx(uint8_t x)
{
   TxBpoi = 0 ;
   TxCrc  = 0 ;
   PutTxChar(':');
   PutTxChar(x);
}

void HostStartTx(uint8_t x)
{
   if (x) PutTxByte(TxCrc) ;
   PutTxChar(0x0d);
   ReplayRemoteTcp( RxT , TxBpoi);
}

void HostAnsNak(void)
{
   HostBuildTx('_');
   PutTxChar('*');
   HostStartTx(0);
}


void HostCommand_E(void)
{
  if (RxHex[0]==0x55)
     {
#ifndef __PRG_30__
       if (QSPI_CommandEraseFromToSector(QSECTOR_MEM_FILE_START , QSECTOR_MEM_FILE_END)==RET_OK)
#endif
          {
            HostBuildTx('E');
            PutTxChar(VER_STR[5]);
            PutTxChar(VER_STR[6]);
            PutTxChar(VER_STR[7]);
            PutTxChar(VER_STR[8]);
            HostStartTx(0);
          }
#ifndef __PRG_30__
         else
          {
            HostAnsNak();
          }
#endif
     }
 }


#ifndef __MP_30__
void HostCommand_R(void)
{
  uint32_t Addr   ;
  uint16_t i,Len  ;
  uint8_t *p      ;
  if (RxHexPoi>3)
     {
       Addr  = RxHex[0] ;
       Addr  <<= 8     ;
       Addr |= RxHex[1] ;
       Addr  <<= 8     ;
       Addr |= RxHex[2] ;
       Addr  <<= 8     ;
       Addr |= RxHex[3] ;
       Len   = RxHex[4] ;
       p       = (void *)(STM_UPLOAD_PROG + Addr)  ;
       TxBpoi = 0 ;
       PutTxChar(':');
       PutTxChar('R');
       for (i=0; i<Len; i++) PutTxByte(*p++);
       PutTxChar(0x0d);
       ReplayRemoteTcp( RxT , TxBpoi);
       RxFlag = 0 ;
       HostState = WAIT_QSPI_IDLE ;
     }
}
  #else
void HostCommand_R(void)
{
  uint32_t Addr ;
  uint16_t Len  ;
  if (RxHexPoi>3)
     {
       Addr  = RxHex[0] ;
       Addr  <<= 8     ;
       Addr |= RxHex[1] ;
       Addr  <<= 8     ;
       Addr |= RxHex[2] ;
       Addr  <<= 8     ;
       Addr |= RxHex[3] ;
       Len   = RxHex[4] ;
       Addr += QSECTOR_MEM_FILE_BASE ;
       if (QSPI_CommandReadBuffer(Addr , Len)==RET_OK)
          {
            HostHoldLen = Len            ;
            HostState   = WAIT_QSPI_READ ;
            TimeOut     = 40             ;  // 1 Sec
          }
         else
          {
            HostAnsNak();
          }
     }
}
#endif

void HostCommand_W(void)
{
  uint32_t Addr    ;
  uint16_t i , j   ;
  uint8_t  x , *p  ;
  x = '1' ;
  if (RxHexPoi>5)
     {
       Addr    = RxHex[0] ;
       Addr    <<= 8      ;
       Addr   |= RxHex[1] ;
       Addr    <<= 8      ;
       Addr   |= RxHex[2] ;
       Addr    <<= 8      ;
       Addr   |= RxHex[3] ;
       p       = (void *)(QUAD_MEM_SEC_1 + Addr)  ;
       Addr   += QSECTOR_MEM_FILE_BASE ;
       i = 4 ;
       j = 0 ;
       while ((i<RxHexPoi-1) && (j < sizeof(UseBf)))
       {
         UseBf[j++] = RxHex[i];
         *p++ = RxHex[i];
         i++;
       }
       if (QSPI_CommandWriteBuffer(Addr , UseBf , j) == RET_OK)
          {
            HostState   = WAIT_QSPI_WRITE ;
            TimeOut     = 40              ;  // 1 Sec
            return ;
          }
       x = '2' ;
     }
 HostBuildTx('*');
 PutTxChar(x);
 HostStartTx(0);
}

void HostCommand_X(void)
{
  uint32_t Addr ;
  uint16_t Len  ;
  if (RxHexPoi>3)
     {
       Addr  = RxHex[0] ;
       Addr  <<= 8     ;
       Addr |= RxHex[1] ;
       Addr  <<= 8     ;
       Addr |= RxHex[2] ;
       Addr  <<= 8     ;
       Addr |= RxHex[3] ;
       Len   = RxHex[4] ;
       if (QSPI_CommandReadBuffer(Addr , Len)==RET_OK)
          {
            HostHoldLen = Len            ;
            HostState   = WAIT_QSPI_READ ;
            TimeOut     = 40             ;  // 1 Sec
          }
         else
          {
            HostAnsNak();
          }
     }
}

void HostCommand_C(void)  // ;C55 PP aabb ccdd eeff gghh <CRC>
{
 uint8_t  f , Ccm ;
 uint16_t Pg1 , Pg2 ;
  f = 0 ;
  if (RxHex[0] == 0x55)
     {
       Ccm = RxHex[1] ;
       switch (Ccm)
       {
         case 0x01 : // Erase PageStart .. PageEnd
                     Pg1 = RxHex[2]+RxHex[3]*0x100 ;
                     Pg2 = RxHex[4]+RxHex[5]*0x100 ;
                     if (Pg2>Pg1)
                        {
                           f = 1 ;
                           if (QSPI_CommandEraseFromToSector(Pg1 , Pg2)==RET_OK)
                              {
                                f = 2 ;
                              }
                        }
                     break ;
       }
     }
  HostBuildTx('C');
  PutTxByte(f);
  PutTxByte(0);
  HostStartTx(1);   // Add CRC
}

void HostCommand_N(void)
{
 uint8_t  *Pb , i , b ;
  Pb = (uint8_t *)(FLASH_MAGIC + 32) ;
  HostBuildTx('N');
  if (RxHex[0] == 0)
     {
       PutTxChar('0');
       PutTxChar('0');
       i = 0 ;
       b = 1 ;
       while ((i<64) && (b))
       {
         b = *Pb++ ;
         if (b)
            {
              PutTxChar(b);
            }
         i++;
       }
     }
  HostStartTx(1);   // Add CRC
}

void HostCommand_P(void)  // ;P AABBCCDD d1..dn <CRC>    32bit Address
{
 static uint32_t  Addr   ;
 static uint8_t  *p , f  ;
 static uint8_t  b1 , b2 ;
 static uint16_t i       ;
  f = 1 ;
  if (RxHex[0] == 0x00)
     {
       Addr  = RxHex[1] ;
       Addr  <<= 8      ;
       Addr |= RxHex[2] ;
       Addr  <<= 8      ;
       Addr |= RxHex[3] ;
       Addr  <<= 8      ;
       Addr |= RxHex[4] ;
       p       = (void *)(STM_UPLOAD_PROG + Addr)  ;
       i = 5 ;
       while (i<RxHexPoi-1)
       {
         *p++ = RxHex[i];
         i++;
       }
       if (Addr)
          { // Padding 0xFF
            i = 0 ;
            while (i<0x80) { *p++ = 0xff ; i++; }
          }
       p       = (void *)(STM_UPLOAD_PROG + Addr)  ;
       i = 5 ;
       f = 0 ;
       while (i<RxHexPoi-1)
       {
         b1 = *p++;
         b2 = RxHex[i];
         if (b1 != b2)
            {
              f = 1 ;
            }
         i++;
       }
     }
  if (f==0)
     {
       TxBpoi = 0 ;
       PutTxChar(':');
       PutTxChar('P');
       PutTxChar('0');
       PutTxChar('1');
       PutTxChar(0x0d);
       ReplayRemoteTcp( RxT , TxBpoi);
#ifdef __PRG_30__
       if (Addr == 0) Global_ProgFlash = 0x55 ;
    #else
       if (Addr == 0)
          {
            magicP = *((uint32_t *)(STM_UPLOAD_PROG)) ;
            if (magicP == 0xa6967854)
               {
                 HAL_NVIC_SystemReset()  ;
               }
#ifdef __MP_30__
            if (magicP == 0x28678543)
               {
                  DoorContSetCM(1);
               }
#endif
          }
#endif
     }
   else
     {
       HostAnsNak();
     }
}

void HostCommand_SetUp(void)
{
  uint8_t *Pb , i , j ;
  uint32_t Dev , Fld  ;
  if ((RxB[0] == '#') && (RxB[1] == '#') && (RxB[5] == '_') && (RxB[9] == '_') && (RxBpoi > 9))
     {
       Dev = (RxB[2] & 0x0f)*100 + (RxB[3] & 0x0f)*10 + (RxB[4] & 0x0f) ;
       Fld = (RxB[6] & 0x0f)*100 + (RxB[7] & 0x0f)*10 + (RxB[8] & 0x0f) ;
       if (Dev == Global_No_OnScr)
          {
            if (Fld < SETUP_FIELDS)
               {
                   *(uint32_t *)BACKUP_BASE_ADDR = 0xaa99a55a ;
                   Pb = (uint8_t *)(BACKUP_BASE_ADDR_SETUP + Fld * SETUP_FIELDS_WIDTH_MAX) ;
                   memset((uint8_t *)Pb , 0 , SETUP_FIELDS_WIDTH_MAX );
                   i = 10 ;
                   j = 0 ;
                   while ((i<RxBpoi - 2) && (j<(SETUP_FIELDS_WIDTH_MAX  - 1)))
                   {
                     Pb[j] = RxB[i] ;
                     j++;
                     i++;
                   }
                   Pb[SETUP_FIELDS_WIDTH_MAX-1] = 0 ;
               }
              else
               {
                  Global_SendFld = Fld ;
               }
          }
     }
  HostBuildTx('U');
  PutTxChar('U');
  HostStartTx(1);   // Add CRC
}

void HostCommand(void)
{

  uint8_t  Cm,Ck,CkCal,b1,b2 ;
  uint16_t  i                ;
  Cm = RxB[1] ;
  Ck = 0      ;
  for (i=0; i<RxBpoi-2; i++) Ck += RxB[i] ;
  i        = 2 ;
  RxHexPoi = 0 ;
  while ((i<RxBpoi) && (RxHexPoi<sizeof(RxHex)))
  {
    b1 = RxB[i++] ;
    b2 = RxB[i++] ;
    if (b1>'9') b1 -= 7 ;
    if (b2>'9') b2 -= 7 ;
    RxHex[RxHexPoi++] = ((b1 & 0x0f)*0x10) + (b2 & 0x0f) ;
  }
  b1 = RxB[RxBpoi-2] ;
  b2 = RxB[RxBpoi-1] ;
  if (b1>'9') b1 -= 7 ;
  if (b2>'9') b2 -= 7 ;
  CkCal = ((b1 & 0x0f)*0x10) + (b2 & 0x0f) ;
  if ((Ck != CkCal) && (Cm != 'x')) return ;
  if ((Cm>='a') && (Cm<='z')) Cm -= 0x20 ;
  TxBpoi = 0 ;
  switch (Cm)
  {
    case 'P' : HostCommand_P();
               break ;
    case 'R' : HostCommand_R();
               break ;
#ifndef __PRG_30__
    case 'C' : HostCommand_C();
               break ;
#endif
    case 'E' : HostCommand_E();
               break ;
    case 'N' : HostCommand_N();
               break ;
#ifndef __PRG_30__
    case 'W' : HostCommand_W();
               break ;
    case 'X' : HostCommand_X();
               break ;
    case '#' : HostCommand_SetUp();
               break ;
#endif
    default  :
               HostAnsNak();
               break ;
  }
}


void HostTcpRx(uint8_t data)
{
  if (RxFlag != 1)
  {
    if (data>=' ')
       {
         if (RxBpoi < sizeof(RxB)) RxB[RxBpoi++] = data ;
       }
      else
       {
         if (data==0x0d) RxFlag = 1 ;
         if (RxBpoi<3)
            {
              RxBpoi = 0 ;
              RxFlag = 0 ;
            }
       }
  }
}

void HostTcpUpdate_RealTime(void)
{
 static uint8_t OldTmZ  ;
#ifndef __PRG_30__
 static uint16_t i      ;
#endif
  if (OldTmZ != Global_Tm)
     {
       OldTmZ = Global_Tm;
       if (TimeOut) TimeOut-- ;
     }
  if (TimeOut == 0) HostState = 0 ;
  switch (HostState)
  {
#ifndef __PRG_30__
    case WAIT_QSPI_READ  : if (QSPI_CommandTestReady() == RET_OK)
                              {
                                TxBpoi = 0 ;
                                PutTxChar(':');
                                PutTxChar('R');
                                PtrB = QSPI_GetTstBfrPtr();
                                for (i=0; i<HostHoldLen; i++) PutTxByte(*PtrB++);
                                PutTxChar(0x0d);
                                ReplayRemoteTcp( RxT , TxBpoi);
                                RxFlag = 0 ;
                                HostState = WAIT_QSPI_IDLE ;
                              }
                           break ;
    case WAIT_QSPI_WRITE : if (QSPI_CommandTestReady() == RET_OK)
                              {
                                TxBpoi = 0 ;
                                PutTxChar(':');
                                PutTxChar('W');
                                PutTxChar(0x0d);
                                ReplayRemoteTcp( RxT , TxBpoi);
                                HostState = WAIT_QSPI_IDLE ;
                              }
                           break ;
    case WAIT_QSPI_PROG  : if (QSPI_CommandTestReady() == RET_OK)
                              {
                                TxBpoi = 0 ;
                                PutTxChar(':');
                                PutTxChar('P');
                                PutTxChar('0');
                                PutTxChar('1');
                                PutTxChar(0x0d);
                                ReplayRemoteTcp( RxT , TxBpoi);
                                HostState = WAIT_QSPI_IDLE ;
                              }
                          break ;

    case WAIT_QSPI_IDLE  :
#endif
    default              :
                           if (RxFlag == 1)
                              {
                                HostCommand();
                                RxFlag = 0 ;
                                RxBpoi = 0 ;
                              }
                           break ;
  }
}




