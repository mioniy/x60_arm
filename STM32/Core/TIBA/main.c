
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "ethernetif.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "app_ethernet.h"

#include <string.h>


//    0   "IP addr"
//    1   "Subnet"
//    2   "gateway"
//    3   "UDP port"
//    4   "Dirc No."
//    5   "SIP Srvr"
//    6   "Username"
//    7   "Password"
//    8   "Ext Name"
//    9   "Call ex1"
//   10   "Call ex2"
//   11   " - "

/*

DevSetUp.txt

#081,000,192.168.4.75
#081,001,255.255.255.0
#081,002,192.168.4.1
#081,003,3000
#081,004,106
#081,005,192.168.4.80
#081,006,106
#081,007,1234
#081,008,106
#081,009,102
#081,010,100

*/

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//byte  Global_LinkStat;  // STM32\Core\Ethernet\app_ethernet.c
//byte  Global_PacketIn;  // STM32\Core\Ethernet\ethernetif.c



///* SIP Aor */
//#define SIP_AOR "sip:582@192.115.163.196"
///* SIP Registar */
//#define SIP_REGISTRAR "sip:192.115.163.196"
///* SIP User Name */
//#define SIP_USER_NAME "310582"
///* SIP Password */
//#define SIP_PASSWORD "Ti40q*Ba"
///* SIP Display Name */
//#define SIP_DISPLAY_NAME "582"
//
///* SIP Callee Extension AOR */
//#define SIP_CALLEE_AOR "sip:0522451926@192.115.163.196"
//
//
///* SIP Aor */
//#define SIP_AOR "sip:580@192.115.163.196"
///* SIP Registar */
//#define SIP_REGISTRAR "sip:192.115.163.196"
///* SIP User Name */
//#define SIP_USER_NAME "310580"
///* SIP Password */
//#define SIP_PASSWORD "se40g*Sr"
///* SIP Display Name */
//#define SIP_DISPLAY_NAME "580"
//
///* SIP Callee Extension AOR */
//#define SIP_CALLEE_AOR "sip:201@192.115.163.196"

///* SIP Aor */
//#define SIP_AOR "sip:106@192.168.4.80"
///* SIP Registar */
//#define SIP_REGISTRAR "sip:192.168.4.80"
///* SIP User Name */
//#define SIP_USER_NAME "106"
///* SIP Password */
//#define SIP_PASSWORD "1234"
///* SIP Display Name */
//#define SIP_DISPLAY_NAME "106"
//
/* SIP Callee Extension AOR */
//#define SIP_CALLEE_AOR "sip:100@192.168.4.80"

static char SIP_AOR                 [80]={0};
static char SIP_REGISTRAR           [80]={0};
static char SIP_USER_NAME           [80]={0};
static char SIP_PASSWORD            [80]={0};
static char SIP_DISPLAY_NAME        [80]={0};
static char SIP_CALLEE_AOR          [80]={0};
static char SIP_CALLEE_AOR_SERVER   [80]={0};
static char SIP_CALLEE_AOR2         [80]={0};


/* Auto Answer feature */
#define SIP_AUTO_REGISTER           1   // ON: 1   , OFF:0
/* Auto Answer feature */
#define SIP_AUTO_ANSWER_ENABLE        1   // ON: 1   , OFF:0
/* SIP Callee Extension AOR */
#define SIP_CALL_TRANSPORT_TCP_UDP     0   // TCP:1    , UDP:0
/* Type of Call. Automatically enable/disable the registration according to the type */
#define SIP_TYPE_OF_CALL_SERVER_P2P     1   // SERVER:1 , P2P:0
/* Registration time. Min value: 1 sec, Max value: 65535 sec. The value set could be limited by registration minimum and maximum expiries of server */
#define SIP_REG_RENEWAL_TIME_SEC          1200 //20 mins 600   //10 mins

/* Enable or Disable some codecs */
#define BD_CODEC_G722   1
#define BD_CODEC_G711A   1
#define BD_CODEC_G711U   1
#define BD_CODEC_L16   1

S2C_mem_t S2C_mem;



struct netif gnetif; /* network interface structure */

ADC_HandleTypeDef hadc3;

DCMI_HandleTypeDef hdcmi;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c3;

LTDC_HandleTypeDef hltdc;

QSPI_HandleTypeDef hqspi;

RTC_HandleTypeDef hrtc;

SAI_HandleTypeDef hsai_BlockA2;
SAI_HandleTypeDef hsai_BlockB2;

SD_HandleTypeDef hsd1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

SDRAM_HandleTypeDef hsdram1;

osThreadId AppMainHandle;

static bdCallStatus sipCallStatus;
static xTaskHandle mainTask    = NULL ;

static byte  statisIP_DHCP = 0 ; // 0 - DHCP, 1 - Static

#pragma data_alignment=4
#pragma location=0x30020000
//__no_init bdSipUa_mem_t sipUa;
bdSipUa_mem_t sipUa;

#pragma location=0x30028000
__no_init audioInterfaceAEC_t audioInterfaceAEC;


void *DHCP_sem;
void *SIP_sem;
void *LINK_sem;
void *tb_sip_thread(void *arg);
void *tb_sip_action_thread(void *arg);
static bdAccount_t account;
static TaskHandle_t sipActionHandler;


void USB_Thread(void const *argument);
extern osMessageQId  AppliEvent;


extern TaskHandle_t T_FileHandler;
void *Task_FilesHandler_Main(void *arg);
/* USER CODE END PV */
uint8_t FW_Update_Thread_Mem[5000];

/* Private function prototypes -----------------------------------------------*/
static void MX_GPIO_Init(void);
static void MX_DCMI_Init(void);
static void MX_FMC_Init(void);
static void MX_I2C1_Init(void);
static void MX_LTDC_Init(void);
static void MX_SAI2_Init(void);
static void MX_SDMMC1_SD_Init(void);
//static void MX_UART4_Init(void);
//static void MX_UART5_Init(void);
//static void MX_UART8_Init(void);
//static void MX_USART2_UART_Init(void);
//static void MX_USART3_UART_Init(void);
//static void MX_USART6_UART_Init(void);
//static void MX_USB_OTG_FS_USB_Init(void);
//static void MX_USB_OTG_HS_USB_Init(void);
static void MX_QUADSPI_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
void SDRAM_Initialization_Sequence(SDRAM_HandleTypeDef *hsdram);
void BRD208_Init(void);
void BR_SystemClock_Config(void);
static void Netif_Config(void);
void ethernet_link_thread( void const * argument );


void udpecho_init(void);
void RemoteIP_init(void);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/**
  * @brief  Configure the MPU attributes
  * @param  None
  * @retval None
  */
#define SDRAM_DEVICE_ADDR  ((uint32_t)0xC0000000)
#define SDRAM_DEVICE_SIZE  ((uint32_t)0x2000000)  /* SDRAM device size in Bytes (32MB)*/

/**
  * @brief  Configure the MPU attributes
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;

  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as Device not cacheable
     for ETH DMA descriptors */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x30040000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Configure the MPU attributes as Cacheable write through
     for LwIP RAM heap which contains the Tx buffers */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x30044000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_16KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER1;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

// -------------------------------------------------------------

  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x30020000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_128KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER2;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

// -------------------------------------------------------------

  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x38800000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_4KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER3;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

// -------------------------------------------------------------

  /* Configure the MPU attributes as WT for SDRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = SDRAM_DEVICE_ADDR;
  MPU_InitStruct.Size = MPU_REGION_SIZE_32MB;
  MPU_InitStruct.Number = MPU_REGION_NUMBER4;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

// -------------------------------------------------------------

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}



/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

void Main_SetIntercomParam(void)
{
 uint32_t k ;
  SetUp_GetParams();
  if (Global_SetUpIntrcomRtpPort == 0) Global_SetUpIntrcomRtpPort = 10000 ;
  sprintf((char *)SIP_AOR           , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[4][0] , (char *)&Global_SetUpStrInput[5][0] );
  sprintf((char *)SIP_REGISTRAR     , "sip:%s"      , (char *)&Global_SetUpStrInput[5][0]                                       );
  sprintf((char *)SIP_USER_NAME     , "%s"          , (char *)&Global_SetUpStrInput[6][0]                                       );
  sprintf((char *)SIP_PASSWORD      , "%s"          , (char *)&Global_SetUpStrInput[7][0]                                       );
  sprintf((char *)SIP_DISPLAY_NAME  , "%s"          , (char *)&Global_SetUpStrInput[8][0]                                       );
  sprintf((char *)SIP_CALLEE_AOR    , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[9][0] , (char *)&Global_SetUpStrInput[5][0] );
  if (SIP_USER_NAME[0]<=' ')
     {
       SIP_REGISTRAR[0] = 0 ;
       SIP_REGISTRAR[1] = 0 ;
     }
  strcpy(account.aor         ,SIP_AOR           );
  strcpy(account.server      ,SIP_REGISTRAR     );
  strcpy(account.userName    ,SIP_USER_NAME     );
  strcpy(account.password    ,SIP_PASSWORD      );
  strcpy(account.displayName ,SIP_DISPLAY_NAME  );
  account.regint = SIP_REG_RENEWAL_TIME_SEC; // Global_IntercomRegisterTm. delete all logical refer to Global_IntercomRegisterTm (set value between 60-1000)

  Global_IntercomFlags       = 0 ;
  Global_IntercomRegisterTm  = 0 ;
  Global_IntercomRingTm      = 0 ;
  k = GetRtcMem(RTC_BKP_DR14 ) ;
  if ((k & 0xff000000) == 0x55000000)
     {
       Global_IntercomRegisterTm = (k & 0xff) ;
       Global_IntercomRingTm     = ((k >> 8) & 0xff) ;
       Global_tTmoD              = ((k >> 16) & 0xff) ;
       if (Global_IntercomRegisterTm < 8) Global_IntercomRegisterTm = 0 ;
       Global_IntercomRegisterTm *= 5 ;
       Global_IntercomRingTm     *= 5 ;
     }
  if (Global_IntercomRingTm     < 10 ) Global_IntercomRingTm     = 90  ;
  if (Global_IntercomRingTm     > 300) Global_IntercomRingTm     = 90  ;
  if (Global_IntercomRegisterTm < 40 ) Global_IntercomRegisterTm = 900 ;
  k = GetRtcMem(RTC_BKP_DR15 ) ;
  if ((k & 0xff000000) == 0x55000000)
     {
       Global_IntercomFlags = k ;
       // set the codecs
     }
}

void Main_ReadAllParam(void);
int main(void)
{

  MPU_Config();
  CPU_CACHE_Enable();
  HAL_Init();
  BR_SystemClock_Config();
  MX_GPIO_Init();
  MX_DCMI_Init();
  MX_FMC_Init();
  MX_I2C1_Init();
  MX_LTDC_Init();
  MX_SAI2_Init();
  MX_QUADSPI_Init();
  SDRAM_Initialization_Sequence(&hsdram1);

  SetUp_GetParams();
  if (Global_SetUpIntrcomRtpPort == 0) Global_SetUpIntrcomRtpPort = 10000 ;

  Main_SetIntercomParam();

  BRD208_Init();
  Main_ReadAllParam();

  __enable_interrupt();

  sys_sem_new(&DHCP_sem,0);
  sys_sem_new(&LINK_sem,0);

  tcpip_init(NULL, NULL);
  Netif_Config();
  RemoteIP_init();
  udpecho_init();

  if (Global_IntercomType != PARKER_INTERCOM)
  {
    sipUaEx_init          (&sipUa              , (void *)&sipCallStatus);
    audioInterfaceAEC_init(&audioInterfaceAEC, &sipUa, &S2C_mem);

    xTaskCreate((pdTASK_CODE)tb_sip_thread              , "main_th"        , 0x1000 , &sipUa, osPriorityNormal , &mainTask                 ) ; // 2048   osPriorityAboveNormal
    xTaskCreate((pdTASK_CODE)tb_sip_action_thread       , "sip_action_th"  , 0x800  , &sipUa, osPriorityNormal , &sipActionHandler         ) ;
  }

  xTaskCreate((pdTASK_CODE)Task_FilesHandler_Main       , "FW_Update_th"  , 0x1000   , &FW_Update_Thread_Mem, osPriorityNormal       , &T_FileHandler ) ;

  osThreadDef(AppMainTask, AppMain, osPriorityNormal , 0      , 0x1000  ) ; // 0x500);
  AppMainHandle = osThreadCreate(osThread(AppMainTask), NULL);

  HAL_PWREx_EnableUSBVoltageDetector();
  osThreadDef   ( USER_Thread , USB_Thread , osPriorityNormal , 0 , 8 * configMINIMAL_STACK_SIZE);
  osThreadCreate( osThread(USER_Thread) , NULL);
  osMessageQDef (osqueue, 1, uint16_t);
  AppliEvent = osMessageCreate(osMessageQ(osqueue), NULL);

  osKernelStart();

  while (1)
  {
  }
}


uint32_t SigActionSip = 0 ;

void main_CallExtention(void)
{
 bdRegisterStatus  bdR ;
  bdR = bdSipUa_getRegisterStatus(&sipUa);
  if (Global_audR != 'e')
      {
        if (bdR == BD_REG_STATUS_REGISTERED)
           {
             SigActionSip = 2 ;
           }
          else
           {
             SigActionSip = 3 ;
           }
      }
}


void *tb_sip_action_thread(void *arg)
{
   bdSipUa_mem_t *bd_sipUa = (bdSipUa_mem_t *)arg;
   while(1)
   {
     SysDelay(100);
     if (Global_IntercomUpdateTm == 1)
        {
          Global_IntercomUpdateTm = 0 ;
//          sys_sem_signal(&SIP_sem);
        }
     if ((SigActionSip) && (Global_SipScode))
     {
       switch (SigActionSip)
       {
         case   1 :
                    bdSipUa_hangUp(bd_sipUa);
                    break ;
         case   2 :
                    Global_Dtmf = 0 ;
                    S2C_Init(&S2C_mem, S2C_MODE_ECNR);
                    if (Global_SetUpIntrcomRtpPort == 0) Global_SetUpIntrcomRtpPort = 10000;
                    bdSipUa_setPortRTP(bd_sipUa , Global_SetUpIntrcomRtpPort);
                    bdSipUa_call(bd_sipUa, SIP_AOR, SIP_CALLEE_AOR, BD_CALL_TYPE_SERVER);
                    break ;
         case   3 :
                    Global_Dtmf = 0 ;
                    if (Global_SetUpIntrcomRtpPort == 0) Global_SetUpIntrcomRtpPort = 10000;
                    bdSipUa_setPortRTP(bd_sipUa , Global_SetUpIntrcomRtpPort);
                    bdSipUa_call(bd_sipUa, SIP_AOR, SIP_CALLEE_AOR, BD_CALL_TYPE_P2P);
                    break ;
       }
       SigActionSip = 0 ;
     }

       /* ---- Block is closed for SP-7879 (YM Jan-04-2022) ---- */
//     /* ---- SP-6555 / 6904 ---- */
//     if (Global_OffLineIsActive == 1)
//     {
//       if (mainTask != NULL)
//       {
//            Global_SipScode = 0;
//            bdSipUa_unregister(&sipUa);
//            SysDelay(100);
//            bdSipUa_deInit(&sipUa);
//            SysDelay(100);
//            vTaskDelete(mainTask);
//            vTaskDelete( xTaskGetHandle("send"));
//            mainTask = NULL;
//       }
//     }
//     else
//     {
//       if (mainTask == NULL)
//       {
//            sipUaEx_init          (&sipUa              , (void *)&sipCallStatus);
//            xTaskCreate((pdTASK_CODE)tb_sip_thread              , "main_th"        , 0x1000 , &sipUa, osPriorityNormal , &mainTask                  ) ; // 2048   osPriorityAboveNormal
//            SigActionSip = 1 ;
//            Global_IntercomCallTry     = 0 ;
//       }
//     }
     /* ---- SP-6555 / 6904 ---- */
   }
}

void Main_ReadAllParam(void)
{
 uint32_t k ;
  SetUp_GetParams();
  if (Global_SetUpIntrcomRtpPort == 0) Global_SetUpIntrcomRtpPort = 10000 ;
  sprintf((char *)SIP_AOR               , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[ 4][0] , (char *)&Global_SetUpStrInput[5][0] );
  sprintf((char *)SIP_REGISTRAR         , "sip:%s"      , (char *)&Global_SetUpStrInput[ 5][0]                                       );
  sprintf((char *)SIP_USER_NAME         , "%s"          , (char *)&Global_SetUpStrInput[ 6][0]                                       );
  sprintf((char *)SIP_PASSWORD          , "%s"          , (char *)&Global_SetUpStrInput[ 7][0]                                       );
  sprintf((char *)SIP_DISPLAY_NAME      , "%s"          , (char *)&Global_SetUpStrInput[ 8][0]                                       );
  sprintf((char *)SIP_CALLEE_AOR        , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[ 9][0] , (char *)&Global_SetUpStrInput[5][0] );
  sprintf((char *)SIP_CALLEE_AOR_SERVER , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[ 9][0] , (char *)&Global_SetUpStrInput[5][0] );
  sprintf((char *)SIP_CALLEE_AOR2       , "sip:%s@%s"   , (char *)&Global_SetUpStrInput[10][0] , (char *)&Global_SetUpStrInput[5][0] );
  if (SIP_USER_NAME[0]<=' ')
     {
       SIP_REGISTRAR[0] = 0 ;
       SIP_REGISTRAR[1] = 0 ;
     }
  strcpy(account.aor         ,SIP_AOR           );
  strcpy(account.server      ,SIP_REGISTRAR     );
  strcpy(account.userName    ,SIP_USER_NAME     );
  strcpy(account.password    ,SIP_PASSWORD      );
  strcpy(account.displayName ,SIP_DISPLAY_NAME  );
  account.regint = SIP_REG_RENEWAL_TIME_SEC; // Global_IntercomRegisterTm. delete all logical refer to Global_IntercomRegisterTm (set value between 60-1000)


  Global_IntercomFlags       = 0 ;
  Global_IntercomRegisterTm  = 0 ;
  Global_IntercomRingTm      = 0 ;
  k = GetRtcMem(RTC_BKP_DR14 ) ;
//  if ((k & 0xff000000) == 0x55000000)
     {
       Global_IntercomRegisterTm = (k & 0xff) ;
       Global_IntercomRingTm     = ((k >> 8) & 0xff) ;
       Global_tTmoD              = ((k >> 16) & 0xff) ;
       if (Global_IntercomRegisterTm < 8) Global_IntercomRegisterTm = 0 ;
       Global_IntercomRegisterTm *= 5 ;
       Global_IntercomRingTm     *= 5 ;
     }
  if (Global_IntercomRingTm     < 10 ) Global_IntercomRingTm     = 90  ;
  if (Global_IntercomRingTm     > 300) Global_IntercomRingTm     = 90  ;
  if (Global_IntercomRegisterTm < 40 ) Global_IntercomRegisterTm = 900 ;
  k = GetRtcMem(RTC_BKP_DR15 ) ;
  if ((k & 0xff000000) == 0x55000000)
     {
       Global_IntercomFlags = k ;
       // set the codecs
     }
}

uint32_t sip_reg;

int sip_register()
{
  return bdSipUa_register(&sipUa,&account);
}

void *tb_sip_thread(void *arg)
{
    sys_sem_new(&SIP_sem,0);
    SysDelay(5000);

//   while( 1)
//   {
      if (statisIP_DHCP == 0) // if there is no per definition for static ip, wahit for dhcp
         sys_sem_wait(&DHCP_sem);
      else // there is a static ip, wait for connecting
         sys_sem_wait(&LINK_sem);

      /*
      * Enable Auto Answer
      */
      bdSipUa_enableAutoAnswer(&sipUa,1);

      /*
      * SipUa Init
      * This must be done in a separate task
      */
      if (Global_SetUpIntrcomSipPort == 0) Global_SetUpIntrcomSipPort = 5060 ;

      //    bdSipUa_set_enable_codec(&sipUa, "L16", BD_CODEC_L16);

      if( Global_IntercomFlags & INTERCOM_BIT_0)   bdSipUa_set_enable_codec(&sipUa, "PCMA", BD_CODEC_G711A);
      if( Global_IntercomFlags & INTERCOM_BIT_1)   bdSipUa_set_enable_codec(&sipUa, "PCMU", BD_CODEC_G711U);
      if( Global_IntercomFlags & INTERCOM_BIT_2)   bdSipUa_set_enable_codec(&sipUa, "G722", BD_CODEC_G722);

      sip_reg = bdSipUa_init(&sipUa, Global_SetUpIntrcomSipPort , (bdSIPTransportProtocol)((Global_IntercomFlags & INTERCOM_BIT_15) >> 15) /*BD_SIP_TRANSPORT_UDP*/);  //SP-5930
//      sip_reg += bdSipUa_register(&sipUa,&account);
     sip_reg += sip_register();
     while (1)
     {
//        if (statisIP_DHCP == 0) // if there is no per definition for static ip, wahit for dhcp
//           sys_sem_wait(&DHCP_sem);
//        else // there is a static ip, wait for connecting
//           sys_sem_wait(&LINK_sem);
//
//                 }
//            }
//         SysDelay(500);
//         bdSipUa_register(&sipUa,&account);
//         SipRegTry++;
//      }
//        sip_reg += bdSipUa_register(&sipUa,&account);

        SysDelay(500);
     }

//         SysDelay(500);

//      while (1)
//      {
//         sys_arch_sem_wait(&SIP_sem,0);
//         Global_SipScode = 0 ;
//         bdSipUa_unregister(&sipUa);
//         SysDelay(100);
//         Main_ReadAllParam();
//         if (Global_IntercomFlags & INTERCOM_BIT_8)
//         {
//            if (Global_IntercomFlags & INTERCOM_BIT_9)
//            {
//
//            }
//         }
//         SysDelay(500);
//         bdSipUa_register(&sipUa,&account);
//      }
//   }
}


void BR_SystemClock_Config(void)
{
  static RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  static RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  static RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /*!< Supply configuration update enable */
  MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0);

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

  /**Supply configuration update enable
  */
  MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0);
  /**Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while ((PWR->D3CR & (PWR_D3CR_VOSRDY)) != PWR_D3CR_VOSRDY)
  {

  }
  /* Enable D2 domain SRAM3 Clock (0x30040000 AXI)*/
  __HAL_RCC_D2SRAM3_CLK_ENABLE();


  HAL_PWR_EnableBkUpAccess();
  HAL_PWREx_EnableBkUpReg();
//  SET_BIT(PWR->CR2, PWR_CR2_BRRDY);
//  SET_BIT(PWR->CR2, PWR_CR2_BREN);

//  HAL_PWR_EnableBkUpAccess();

  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);



  /**Initializes the CPU, AHB and APB busses clocks
  */
//  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 160;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    BR_Error_Handler(0x10);
  }
  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    BR_Error_Handler(0x20);
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LTDC
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_UART4|RCC_PERIPHCLK_USART6
                              |RCC_PERIPHCLK_UART8|RCC_PERIPHCLK_UART5
                              |RCC_PERIPHCLK_SPI1|RCC_PERIPHCLK_SPI2
                              |RCC_PERIPHCLK_SAI2
                              |RCC_PERIPHCLK_SDMMC
                              |RCC_PERIPHCLK_I2C3|RCC_PERIPHCLK_ADC
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_QSPI|RCC_PERIPHCLK_FMC;
  PeriphClkInitStruct.PLL2.PLL2M                 = 25                               ; // 2
  PeriphClkInitStruct.PLL2.PLL2N                 = 429                              ; // 429
  PeriphClkInitStruct.PLL2.PLL2P                 = 38                               ; // 11.289 MHZ
  PeriphClkInitStruct.PLL2.PLL2Q                 = 38                               ; // 11.289 MHZ
  PeriphClkInitStruct.PLL2.PLL2R                 = 38                               ; // 86 MHZ    QSPI
  PeriphClkInitStruct.PLL2.PLL2RGE               = RCC_PLL2VCIRANGE_3               ;
  PeriphClkInitStruct.PLL2.PLL2VCOSEL            = RCC_PLL2VCOMEDIUM                ;
  PeriphClkInitStruct.PLL2.PLL2FRACN             = 0                                ;

  PeriphClkInitStruct.PLL3.PLL3M                 = 25                               ; // 1      MHZ
  PeriphClkInitStruct.PLL3.PLL3N                 = 344                              ; // 344    MHZ
  PeriphClkInitStruct.PLL3.PLL3P                 = 7                                ; // 11.289 MHZ     SAI  , SPI 123
  PeriphClkInitStruct.PLL3.PLL3Q                 = 7                                ; // 49.14  MHz
  PeriphClkInitStruct.PLL3.PLL3R                 = 16                               ; // LTDC

  PeriphClkInitStruct.PLL3.PLL3RGE               = RCC_PLL3VCIRANGE_3               ;
  PeriphClkInitStruct.PLL3.PLL3VCOSEL            = RCC_PLL3VCOWIDE                  ;
  PeriphClkInitStruct.PLL3.PLL3FRACN             = 0                                ;
  PeriphClkInitStruct.FmcClockSelection          = RCC_FMCCLKSOURCE_D1HCLK          ; // BUS CLOCK
  PeriphClkInitStruct.QspiClockSelection         = RCC_QSPICLKSOURCE_PLL2           ; // RCC_QSPICLKSOURCE_D1HCLK;          // BUS CLOCK
  PeriphClkInitStruct.SdmmcClockSelection        = RCC_SDMMCCLKSOURCE_PLL           ; //
  PeriphClkInitStruct.Sai23ClockSelection        = RCC_SAI23CLKSOURCE_PLL3          ; // RCC_SAI23CLKSOURCE_PLL2;
  PeriphClkInitStruct.Spi123ClockSelection       = RCC_SPI123CLKSOURCE_PLL          ; // PLL1 Q
  PeriphClkInitStruct.Usart234578ClockSelection  = RCC_USART234578CLKSOURCE_D2PCLK1 ; // BUS CLOCK
  PeriphClkInitStruct.Usart16ClockSelection      = RCC_USART16CLKSOURCE_D2PCLK2     ; // BUS CLOCK
  PeriphClkInitStruct.I2c123ClockSelection       = RCC_I2C123CLKSOURCE_D2PCLK1      ; // BUS CLOCK
  PeriphClkInitStruct.UsbClockSelection          = RCC_USBCLKSOURCE_PLL3            ;
  PeriphClkInitStruct.AdcClockSelection          = RCC_ADCCLKSOURCE_PLL2            ;
  PeriphClkInitStruct.RTCClockSelection          = RCC_RTCCLKSOURCE_LSE             ;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    BR_Error_Handler(0x200);
  }

  /*activate CSI clock mondatory for I/O Compensation Cell*/
  __HAL_RCC_CSI_ENABLE() ;

  /* Enable SYSCFG clock mondatory for I/O Compensation Cell */
  __HAL_RCC_SYSCFG_CLK_ENABLE() ;

  /* Enables the I/O Compensation Cell */
  HAL_EnableCompensationCell();


  __HAL_RCC_BKPRAM_CLK_ENABLE();

}

//void BR_SystemClock_Config(void)				// Ariel
//{
//  static RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
//  static RCC_OscInitTypeDef RCC_OscInitStruct = {0};
//  static RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
//  /*!< Supply configuration update enable */
//  MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0);
//
//
//  /* The voltage scaling allows optimizing the power consumption when the device is
//     clocked below the maximum system frequency, to update the voltage scaling value
//     regarding system frequency refer to product datasheet.  */
//  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
//
//  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
//
//  /**Supply configuration update enable
//  */
//  MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0);
//  /**Configure the main internal regulator output voltage
//  */
//  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
//
//  while ((PWR->D3CR & (PWR_D3CR_VOSRDY)) != PWR_D3CR_VOSRDY)
//  {
//
//  }
//  /* Enable D2 domain SRAM3 Clock (0x30040000 AXI)*/
//  __HAL_RCC_D2SRAM3_CLK_ENABLE();
//
//
//  HAL_PWR_EnableBkUpAccess();
//  HAL_PWREx_EnableBkUpReg();
////  SET_BIT(PWR->CR2, PWR_CR2_BRRDY);
////  SET_BIT(PWR->CR2, PWR_CR2_BREN);
//
////  HAL_PWR_EnableBkUpAccess();
//
//  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
//  __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
//
//
//
//  /**Initializes the CPU, AHB and APB busses clocks
//  */
////  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
//  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
//  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
//  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
//  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
//  RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
//  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
//  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
//  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
//  RCC_OscInitStruct.PLL.PLLM = 5;
//  RCC_OscInitStruct.PLL.PLLN = 160;
//  RCC_OscInitStruct.PLL.PLLP = 2;
//  RCC_OscInitStruct.PLL.PLLR = 2;
//  RCC_OscInitStruct.PLL.PLLQ = 4;
//  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
//  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
//  RCC_OscInitStruct.PLL.PLLFRACN = 0;
//  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
//  {
//    BR_Error_Handler(0x10);
//  }
//  /**Initializes the CPU, AHB and APB busses clocks
//  */
//  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
//                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
//                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
//  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
//  RCC_ClkInitStruct.SYSCLKDivider  = RCC_SYSCLK_DIV1;
//  RCC_ClkInitStruct.AHBCLKDivider  = RCC_HCLK_DIV2;
//  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
//  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
//  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
//  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;
//
//  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
//  {
//    BR_Error_Handler(0x20);
//  }
//  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LTDC
//                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_USART2
//                              |RCC_PERIPHCLK_UART4|RCC_PERIPHCLK_USART6
//                              |RCC_PERIPHCLK_UART8|RCC_PERIPHCLK_UART5
//                              |RCC_PERIPHCLK_SPI1|RCC_PERIPHCLK_SPI2
//                              |RCC_PERIPHCLK_SAI2
//                              |RCC_PERIPHCLK_SDMMC
//                              |RCC_PERIPHCLK_I2C3|RCC_PERIPHCLK_ADC
//                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_USB
//                              |RCC_PERIPHCLK_QSPI|RCC_PERIPHCLK_FMC;
//  PeriphClkInitStruct.PLL2.PLL2M = 25  ;     // 2
//  PeriphClkInitStruct.PLL2.PLL2N = 429 ;     // 429
//  PeriphClkInitStruct.PLL2.PLL2P = 38  ;     // 11.289 MHZ
//  PeriphClkInitStruct.PLL2.PLL2Q = 38  ;     // 11.289 MHZ
//  PeriphClkInitStruct.PLL2.PLL2R = 38   ;     // 86 MHZ    QSPI
//  PeriphClkInitStruct.PLL2.PLL2RGE = RCC_PLL2VCIRANGE_3;
//  PeriphClkInitStruct.PLL2.PLL2VCOSEL = RCC_PLL2VCOMEDIUM;
//  PeriphClkInitStruct.PLL2.PLL2FRACN = 0  ;
//  PeriphClkInitStruct.PLL3.PLL3M = 25    ; // 1      MHZ
//  PeriphClkInitStruct.PLL3.PLL3N = 344   ; // 429    MHZ
//  PeriphClkInitStruct.PLL3.PLL3P = 7     ; // 11.289 MHZ     SAI  , SPI 123
//  PeriphClkInitStruct.PLL3.PLL3Q = 38    ; // 11.289 MHZ
//  PeriphClkInitStruct.PLL3.PLL3R = 17    ; // LTDC
//  PeriphClkInitStruct.PLL3.PLL3RGE    = RCC_PLL3VCIRANGE_3;
//  PeriphClkInitStruct.PLL3.PLL3VCOSEL = RCC_PLL3VCOWIDE;
//  PeriphClkInitStruct.PLL3.PLL3FRACN  = 0 ;
//  PeriphClkInitStruct.FmcClockSelection          = RCC_FMCCLKSOURCE_D1HCLK ;          // BUS CLOCK
//  PeriphClkInitStruct.QspiClockSelection         = RCC_QSPICLKSOURCE_PLL2  ; // RCC_QSPICLKSOURCE_D1HCLK;          // BUS CLOCK
//  PeriphClkInitStruct.SdmmcClockSelection        = RCC_SDMMCCLKSOURCE_PLL  ;          //
//  PeriphClkInitStruct.Sai23ClockSelection        = RCC_SAI23CLKSOURCE_PLL3 ;          // RCC_SAI23CLKSOURCE_PLL2;
//  PeriphClkInitStruct.Spi123ClockSelection       = RCC_SPI123CLKSOURCE_PLL ;          // PLL1 Q
//  PeriphClkInitStruct.Usart234578ClockSelection  = RCC_USART234578CLKSOURCE_D2PCLK1;  // BUS CLOCK
//  PeriphClkInitStruct.Usart16ClockSelection      = RCC_USART16CLKSOURCE_D2PCLK2;      // BUS CLOCK
//  PeriphClkInitStruct.I2c123ClockSelection       = RCC_I2C123CLKSOURCE_D2PCLK1;       // BUS CLOCK
//  PeriphClkInitStruct.UsbClockSelection          = RCC_USBCLKSOURCE_HSI48;
//  PeriphClkInitStruct.AdcClockSelection          = RCC_ADCCLKSOURCE_PLL2;
//  PeriphClkInitStruct.RTCClockSelection          = RCC_RTCCLKSOURCE_LSE ;
//  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
//  {
//    BR_Error_Handler(0x200);
//  }
//
//  /*activate CSI clock mondatory for I/O Compensation Cell*/
//  __HAL_RCC_CSI_ENABLE() ;
//
//  /* Enable SYSCFG clock mondatory for I/O Compensation Cell */
//  __HAL_RCC_SYSCFG_CLK_ENABLE() ;
//
//  /* Enables the I/O Compensation Cell */
//  HAL_EnableCompensationCell();
//
//
//  __HAL_RCC_BKPRAM_CLK_ENABLE();
//
//}
//

/**
  * @brief DCMI Initialization Function
  * @param None
  * @retval None
  */
static void MX_DCMI_Init(void)
{

  /* USER CODE BEGIN DCMI_Init 0 */

  /* USER CODE END DCMI_Init 0 */

  /* USER CODE BEGIN DCMI_Init 1 */

  /* USER CODE END DCMI_Init 1 */
  hdcmi.Instance = DCMI;
  hdcmi.Init.SynchroMode = DCMI_SYNCHRO_HARDWARE;
  hdcmi.Init.PCKPolarity = DCMI_PCKPOLARITY_FALLING;
  hdcmi.Init.VSPolarity = DCMI_VSPOLARITY_LOW;
  hdcmi.Init.HSPolarity = DCMI_HSPOLARITY_LOW;
  hdcmi.Init.CaptureRate = DCMI_CR_ALL_FRAME;
  hdcmi.Init.ExtendedDataMode = DCMI_EXTEND_DATA_8B;
  hdcmi.Init.JPEGMode = DCMI_JPEG_DISABLE;
  hdcmi.Init.ByteSelectMode = DCMI_BSM_ALL;
  hdcmi.Init.ByteSelectStart = DCMI_OEBS_ODD;
  hdcmi.Init.LineSelectMode = DCMI_LSM_ALL;
  hdcmi.Init.LineSelectStart = DCMI_OELS_ODD;
  if (HAL_DCMI_Init(&hdcmi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DCMI_Init 2 */

  /* USER CODE END DCMI_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x10C0ECFF;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}


/**
  * @brief LTDC Initialization Function
  * @param None
  * @retval None
  */
static void MX_LTDC_Init(void)
{

  /* USER CODE BEGIN LTDC_Init 0 */

  /* USER CODE END LTDC_Init 0 */

  LTDC_LayerCfgTypeDef pLayerCfg = {0};
  LTDC_LayerCfgTypeDef pLayerCfg1 = {0};

  /* USER CODE BEGIN LTDC_Init 1 */

  /* USER CODE END LTDC_Init 1 */
  hltdc.Instance = LTDC;
  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
  hltdc.Init.HorizontalSync = 7;
  hltdc.Init.VerticalSync = 3;
  hltdc.Init.AccumulatedHBP = 14;
  hltdc.Init.AccumulatedVBP = 5;
  hltdc.Init.AccumulatedActiveW = 654;
  hltdc.Init.AccumulatedActiveH = 485;
  hltdc.Init.TotalWidth = 660;
  hltdc.Init.TotalHeigh = 487;
  hltdc.Init.Backcolor.Blue = 0;
  hltdc.Init.Backcolor.Green = 0;
  hltdc.Init.Backcolor.Red = 0;
  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
  {
    Error_Handler();
  }
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 0;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 0;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_ARGB8888;
  pLayerCfg.Alpha = 0;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
  pLayerCfg.FBStartAdress = 0;
  pLayerCfg.ImageWidth = 0;
  pLayerCfg.ImageHeight = 0;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
  {
    Error_Handler();
  }
  pLayerCfg1.WindowX0 = 0;
  pLayerCfg1.WindowX1 = 0;
  pLayerCfg1.WindowY0 = 0;
  pLayerCfg1.WindowY1 = 0;
  pLayerCfg1.PixelFormat = LTDC_PIXEL_FORMAT_ARGB8888;
  pLayerCfg1.Alpha = 0;
  pLayerCfg1.Alpha0 = 0;
  pLayerCfg1.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
  pLayerCfg1.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
  pLayerCfg1.FBStartAdress = 0;
  pLayerCfg1.ImageWidth = 0;
  pLayerCfg1.ImageHeight = 0;
  pLayerCfg1.Backcolor.Blue = 0;
  pLayerCfg1.Backcolor.Green = 0;
  pLayerCfg1.Backcolor.Red = 0;
  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg1, 1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LTDC_Init 2 */

  /* USER CODE END LTDC_Init 2 */

}

/**
  * @brief QUADSPI Initialization Function
  * @param None
  * @retval None
  */
static void MX_QUADSPI_Init(void)
{

  /* USER CODE BEGIN QUADSPI_Init 0 */

  /* USER CODE END QUADSPI_Init 0 */

  /* USER CODE BEGIN QUADSPI_Init 1 */

  /* USER CODE END QUADSPI_Init 1 */
  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 255;
  hqspi.Init.FifoThreshold = 1;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 1;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  hqspi.Init.FlashID = QSPI_FLASH_ID_1;
  hqspi.Init.DualFlash = QSPI_DUALFLASH_DISABLE;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN QUADSPI_Init 2 */

  /* USER CODE END QUADSPI_Init 2 */

}

/**
  * @brief SAI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SAI2_Init(void)
{

  /* USER CODE BEGIN SAI2_Init 0 */

  /* USER CODE END SAI2_Init 0 */

  /* USER CODE BEGIN SAI2_Init 1 */

  /* USER CODE END SAI2_Init 1 */
  hsai_BlockA2.Instance = SAI2_Block_A;
  hsai_BlockA2.Init.Protocol = SAI_FREE_PROTOCOL;
  hsai_BlockA2.Init.AudioMode = SAI_MODEMASTER_TX;
  hsai_BlockA2.Init.DataSize = SAI_DATASIZE_8;
  hsai_BlockA2.Init.FirstBit = SAI_FIRSTBIT_MSB;
  hsai_BlockA2.Init.ClockStrobing = SAI_CLOCKSTROBING_FALLINGEDGE;
  hsai_BlockA2.Init.Synchro = SAI_ASYNCHRONOUS;
  hsai_BlockA2.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockA2.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE;
  hsai_BlockA2.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockA2.Init.AudioFrequency = SAI_AUDIO_FREQUENCY_192K;
  hsai_BlockA2.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
  hsai_BlockA2.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockA2.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockA2.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  hsai_BlockA2.Init.PdmInit.Activation = DISABLE;
  hsai_BlockA2.Init.PdmInit.MicPairsNbr = 0;
  hsai_BlockA2.Init.PdmInit.ClockEnable = SAI_PDM_CLOCK1_ENABLE;
  hsai_BlockA2.FrameInit.FrameLength = 8;
  hsai_BlockA2.FrameInit.ActiveFrameLength = 1;
  hsai_BlockA2.FrameInit.FSDefinition = SAI_FS_STARTFRAME;
  hsai_BlockA2.FrameInit.FSPolarity = SAI_FS_ACTIVE_LOW;
  hsai_BlockA2.FrameInit.FSOffset = SAI_FS_FIRSTBIT;
  hsai_BlockA2.SlotInit.FirstBitOffset = 0;
  hsai_BlockA2.SlotInit.SlotSize = SAI_SLOTSIZE_DATASIZE;
  hsai_BlockA2.SlotInit.SlotNumber = 1;
  hsai_BlockA2.SlotInit.SlotActive = 0x00000000;
  if (HAL_SAI_Init(&hsai_BlockA2) != HAL_OK)
  {
    Error_Handler();
  }
  hsai_BlockB2.Instance = SAI2_Block_B;
  hsai_BlockB2.Init.Protocol = SAI_FREE_PROTOCOL;
  hsai_BlockB2.Init.AudioMode = SAI_MODESLAVE_RX;
  hsai_BlockB2.Init.DataSize = SAI_DATASIZE_8;
  hsai_BlockB2.Init.FirstBit = SAI_FIRSTBIT_MSB;
  hsai_BlockB2.Init.ClockStrobing = SAI_CLOCKSTROBING_FALLINGEDGE;
  hsai_BlockB2.Init.Synchro = SAI_SYNCHRONOUS;
  hsai_BlockB2.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockB2.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockB2.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
  hsai_BlockB2.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockB2.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockB2.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  hsai_BlockB2.Init.PdmInit.Activation = DISABLE;
  hsai_BlockB2.Init.PdmInit.MicPairsNbr = 0;
  hsai_BlockB2.Init.PdmInit.ClockEnable = SAI_PDM_CLOCK1_ENABLE;
  hsai_BlockB2.FrameInit.FrameLength = 24;
  hsai_BlockB2.FrameInit.ActiveFrameLength = 1;
  hsai_BlockB2.FrameInit.FSDefinition = SAI_FS_STARTFRAME;
  hsai_BlockB2.FrameInit.FSPolarity = SAI_FS_ACTIVE_LOW;
  hsai_BlockB2.FrameInit.FSOffset = SAI_FS_FIRSTBIT;
  hsai_BlockB2.SlotInit.FirstBitOffset = 0;
  hsai_BlockB2.SlotInit.SlotSize = SAI_SLOTSIZE_DATASIZE;
  hsai_BlockB2.SlotInit.SlotNumber = 1;
  hsai_BlockB2.SlotInit.SlotActive = 0x00000000;
  if (HAL_SAI_Init(&hsai_BlockB2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SAI2_Init 2 */

  /* USER CODE END SAI2_Init 2 */

}

/**
  * @brief SDMMC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SDMMC1_SD_Init(void)
{

  /* USER CODE BEGIN SDMMC1_Init 0 */

  /* USER CODE END SDMMC1_Init 0 */

  /* USER CODE BEGIN SDMMC1_Init 1 */

  /* USER CODE END SDMMC1_Init 1 */
  hsd1.Instance = SDMMC1;
  hsd1.Init.ClockEdge = SDMMC_CLOCK_EDGE_RISING;
  hsd1.Init.ClockPowerSave = SDMMC_CLOCK_POWER_SAVE_DISABLE;
  hsd1.Init.BusWide = SDMMC_BUS_WIDE_4B;
  hsd1.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd1.Init.ClockDiv = 0;
  if (HAL_SD_Init(&hsd1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SDMMC1_Init 2 */

  /* USER CODE END SDMMC1_Init 2 */

}

//
///**
//  * @brief USB_OTG_FS Initialization Function
//  * @param None
//  * @retval None
//  */
//static void MX_USB_OTG_FS_USB_Init(void)
//{
//
//  /* USER CODE BEGIN USB_OTG_FS_Init 0 */
//
//  /* USER CODE END USB_OTG_FS_Init 0 */
//
//  /* USER CODE BEGIN USB_OTG_FS_Init 1 */
//
//  /* USER CODE END USB_OTG_FS_Init 1 */
//  /* USER CODE BEGIN USB_OTG_FS_Init 2 */
//
//  /* USER CODE END USB_OTG_FS_Init 2 */
//
//}
//
///**
//  * @brief USB_OTG_HS Initialization Function
//  * @param None
//  * @retval None
//  */
//static void MX_USB_OTG_HS_USB_Init(void)
//{
//
//  /* USER CODE BEGIN USB_OTG_HS_Init 0 */
//
//  /* USER CODE END USB_OTG_HS_Init 0 */
//
//  /* USER CODE BEGIN USB_OTG_HS_Init 1 */
//
//  /* USER CODE END USB_OTG_HS_Init 1 */
//  /* USER CODE BEGIN USB_OTG_HS_Init 2 */
//
//  /* USER CODE END USB_OTG_HS_Init 2 */
//
//}
//
/* FMC initialization function */
static void MX_FMC_Init(void)
{
  FMC_SDRAM_TimingTypeDef SdramTiming;

  /** Perform the SDRAM1 memory initialization sequence
  */
  hsdram1.Instance = FMC_SDRAM_DEVICE;
  /* hsdram1.Init */
  hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
  hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_9;
  hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_13;
  hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
  hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
  hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_3;
  hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
  hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_PERIOD_2;
  hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_ENABLE;
  hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
  /* SdramTiming */
  SdramTiming.LoadToActiveDelay = 4;
  SdramTiming.ExitSelfRefreshDelay = 8;
  SdramTiming.SelfRefreshTime = 4;
  SdramTiming.RowCycleDelay = 7;
  SdramTiming.WriteRecoveryTime = 4;
  SdramTiming.RPDelay = 2;
  SdramTiming.RCDDelay = 2;

  if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK)
  {
    Error_Handler( );
  }

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOJ, SOM_LED1_Pin|SOM_LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PB12 PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF12_OTG2_FS;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SOM_LED1_Pin SOM_LED2_Pin */
  GPIO_InitStruct.Pin = SOM_LED1_Pin|SOM_LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

  /*Configure GPIO pin : PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG1_FS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN Header_StartDefaultTask */

static void Netif_Config(void)
{
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;
//  uint32_t  Fstatic = 0 ;
  if ((Global_SetUpDeviceIP) && (Global_SetUpDeviceSubnet) && (Global_SetUpDeviceGateway))
     {
//       Fstatic = 1 ;
       statisIP_DHCP = 1 ;
       IP_ADDR4(&ipaddr  , Global_SetUpDeviceIP      >> 24 , Global_SetUpDeviceIP      >> 16 , Global_SetUpDeviceIP      >> 8 , Global_SetUpDeviceIP      );
       IP_ADDR4(&netmask , Global_SetUpDeviceSubnet  >> 24 , Global_SetUpDeviceSubnet  >> 16 , Global_SetUpDeviceSubnet  >> 8 , Global_SetUpDeviceSubnet  );
       IP_ADDR4(&gw      , Global_SetUpDeviceGateway >> 24 , Global_SetUpDeviceGateway >> 16 , Global_SetUpDeviceGateway >> 8 , Global_SetUpDeviceGateway );
       sprintf((char *)Global_EthIpString, "%s", ip4addr_ntoa(&ipaddr));
       Global_EthIpStat = DHCP_STATIC_CONST ;
     }
    else
     {
       ip_addr_set_zero_ip4(&ipaddr);
       ip_addr_set_zero_ip4(&netmask);
       ip_addr_set_zero_ip4(&gw);
     }

  /* add the network interface */
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);

  /*  Registers the default network interface. */
  netif_set_default(&gnetif);

  ethernet_link_status_updated(&gnetif);

#if LWIP_NETIF_LINK_CALLBACK
  netif_set_link_callback(&gnetif, ethernet_link_status_updated);

  osThreadDef(EthLink, ethernet_link_thread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE *2);
  osThreadCreate (osThread(EthLink), &gnetif);
#endif

//  if (Fstatic == 0)
     if (statisIP_DHCP == 0)
     {
       /* Start DHCPClient */
       osThreadDef(DHCP, DHCP_Thread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);
       osThreadCreate (osThread(DHCP), &gnetif);
     }
    else
     {
//       sys_sem_signal(&DHCP_sem);
     }
}


/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
//  /* USER CODE BEGIN Callback 0 */
//
//  /* USER CODE END Callback 0 */
//  if (htim->Instance == TIM1) {
//    HAL_IncTick();
//  }
//  /* USER CODE BEGIN Callback 1 */
//
//  /* USER CODE END Callback 1 */
//}
//
/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
