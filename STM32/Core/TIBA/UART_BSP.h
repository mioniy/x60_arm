/*******************************************************************************
*
*                                UART_BSP.h
*
*******************************************************************************/

/*------------------------------ Include Files -------------------------------*/

/*------------------------ Precompilation Definitions ------------------------*/

/*---------------------------- Typedef Directives ----------------------------*/

/*---------------------------- External Variables ----------------------------*/

/*------------------------ Global Function Prototypes ------------------------*/
void					UartBSP_Init( void);
void          		UartBSP_SetPort( tUart  *pUart);
void					UartBSP_StartTx(tUart  *pUart);
void 					UartBSP_RS485_ModeRx(void);
void 					UartBSP_RS485_ModeTx(void);
void					UartBSP_ClearUDPip( void);
byte					UartBSP_RS485_GetMode(void);
void 					UartBSP_putchar( uint8_t tx_data, byte comm);
UART_HandleTypeDef   *UartBSP_GetComHandler( byte comm);
/*----------------------------------------------------------------------------*/
