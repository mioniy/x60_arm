
#ifndef __BAT_MEM__
#define __BAT_MEM__


#define  _B_MagicNo             ( BACKUP_BASE_ADDR         )

#define  _B_ETHcomFlag          ( BACKUP_BASE_ADDR + 0x004 )
#define  _B_ETHcomTry           ( BACKUP_BASE_ADDR + 0x008 )

#define  _B_USBcomTry           ( BACKUP_BASE_ADDR + 0x00c )


#endif
