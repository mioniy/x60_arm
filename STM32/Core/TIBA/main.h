/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "sipUa_example.h"
#include "audioInterfaceAEC.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbh_core.h"
#include "port_stm.h"
#include "BdS2C.h"
#include "Ver.h"
//#include "QSPI_Func.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

extern   byte  Global_LinkStat;  // STM32\Core\Ethernet\app_ethernet.c

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define SERVER_PORT   7

#define PLAY_BUFF_SIZE        0x400

#define RET_OK                    0
#define RET_BUSY                  1
#define RET_IDLE                  2
#define RET_TIMEOUT               3
#define RET_ERROR               99


/*Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
#define IP_ADDR0        (uint8_t) 192
#define IP_ADDR1        (uint8_t) 168
#define IP_ADDR2        (uint8_t) 1
#define IP_ADDR3        (uint8_t) 220

/*NETMASK*/
#define NETMASK_ADDR0   (uint8_t) 255
#define NETMASK_ADDR1   (uint8_t) 255
#define NETMASK_ADDR2   (uint8_t) 255
#define NETMASK_ADDR3   (uint8_t) 0

/*Gateway Address*/
#define GW_ADDR0   (uint8_t) 192
#define GW_ADDR1   (uint8_t) 168
#define GW_ADDR2   (uint8_t) 1
#define GW_ADDR3   (uint8_t) 1



#define DEFAULT_MAC_ADDRESS_1  (0x44)
#define DEFAULT_MAC_ADDRESS_2  (0xD5)
#define DEFAULT_MAC_ADDRESS_3  (0xF2)
#define DEFAULT_MAC_ADDRESS_4  (0x00)
#define DEFAULT_MAC_ADDRESS_5  (0x00)
#define DEFAULT_MAC_ADDRESS_6  (0x00)

// -----------------------------------------------------------------------------------------------

#define SDRAM_TIMEOUT                            ((uint32_t)0xFFFF)
#define REFRESH_COUNT                            ((uint32_t)0x0603)   /* SDRAM refresh counter */

#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

// -----------------------------------------------------------------------------------------------
#define AUDIO_FREQUENCY_192K          ((uint32_t)192000)
#define AUDIO_FREQUENCY_96K           ((uint32_t)96000)
#define AUDIO_FREQUENCY_48K           ((uint32_t)48000)
#define AUDIO_FREQUENCY_44K           ((uint32_t)44100)
#define AUDIO_FREQUENCY_32K           ((uint32_t)32000)
#define AUDIO_FREQUENCY_22K           ((uint32_t)22050)
#define AUDIO_FREQUENCY_16K           ((uint32_t)16000)
#define AUDIO_FREQUENCY_11K           ((uint32_t)11025)
#define AUDIO_FREQUENCY_8K            ((uint32_t)8000)


void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SOM_LED1_Pin GPIO_PIN_6
#define SOM_LED1_GPIO_Port GPIOJ
#define SOM_LED2_Pin GPIO_PIN_7
#define SOM_LED2_GPIO_Port GPIOJ
/* USER CODE BEGIN Private defines */

/* Definition for TIMx clock resources */
#define TIMx                           TIM3
#define TIMx_CLK_ENABLE()              __HAL_RCC_TIM3_CLK_ENABLE()

/* Definition for TIMx's NVIC */
#define TIMx_IRQn                      TIM3_IRQn
#define TIMx_IRQHandler                TIM3_IRQHandler



#define FLD_Y_POS                            80



#define INTERCOM_BIT_0               0x00000001       //  Codec 711-U
#define INTERCOM_BIT_1               0x00000002       //  Codec 711-A
#define INTERCOM_BIT_2               0x00000004       //  Codec 722
#define INTERCOM_BIT_3               0x00000008       //
#define INTERCOM_BIT_4               0x00000010       //
#define INTERCOM_BIT_5               0x00000020       //
#define INTERCOM_BIT_6               0x00000040       //
#define INTERCOM_BIT_7               0x00000080       //
#define INTERCOM_BIT_8               0x00000100       //  ReRegister after Each CALL      ( 2 Sec )
#define INTERCOM_BIT_9               0x00000200       //  ReRegister after Each CALL DEEP ( 2 Sec )
#define INTERCOM_BIT_10              0x00000400       //
#define INTERCOM_BIT_11              0x00000800       //
#define INTERCOM_BIT_12              0x00001000       //
#define INTERCOM_BIT_13              0x00002000       //
#define INTERCOM_BIT_14              0x00004000       //
#define INTERCOM_BIT_15              0x00008000       //  UDP - 0, TCP - 1
#define INTERCOM_BIT_16              0x00010000       //
#define INTERCOM_BIT_17              0x00020000       //
#define INTERCOM_BIT_18              0x00040000       //
#define INTERCOM_BIT_19              0x00080000       //
#define INTERCOM_BIT_20              0x00100000       //
#define INTERCOM_BIT_21              0x00200000       //
#define INTERCOM_BIT_22              0x00400000       //
#define INTERCOM_BIT_23              0x00800000       //
#define INTERCOM_BIT_24              0x01000000       //
#define INTERCOM_BIT_25              0x02000000       //
#define INTERCOM_BIT_26              0x04000000       //
#define INTERCOM_BIT_27              0x08000000       //
#define INTERCOM_BIT_28              0x10000000       //
#define INTERCOM_BIT_29              0x20000000       //
#define INTERCOM_BIT_30              0x40000000       //
#define INTERCOM_BIT_31              0x80000000       //



#define SETUP_STATIC_IP                       0
#define SETUP_STATIC_SUBNET                   1
#define SETUP_STATIC_GATEWAY                  2
#define SETUP_UDP_PORT                        3
#define SETUP_INTERCOM_DIRC_NO                4
#define SETUP_INTERCOM_SIP_SRVR               5
#define SETUP_INTERCOM_SIP_USER_NAME          6
#define SETUP_INTERCOM_SIP_PASSWORD           7
#define SETUP_INTERCOM_SIP_EXT_NAME           8
#define SETUP_INTERCOM_CALL_EXT_1             9
#define SETUP_INTERCOM_CALL_EXT_2            10
#define SETUP_INTERCOM_ADMIN_PASS            11
#define SETUP_INTERCOM_2                     12
#define SETUP_INTERCOM_3                     13
#define SETUP_HOST_IP                        14
#define SETUP_HOST_MC_PORT                   15
#define SETUP_HOST_DV_PORT                   16
#define SETUP_INTERCOM_GATE_OPEN             17
#define SETUP_INTERCOM_SIP_PORT              18
#define SETUP_INTERCOM_RTP_PORT              19
#define SETUP_PARKER_IP                      20
#define SETUP_PARKER_SUBNET                  21
#define SETUP_PARKER_GATEWAY                 22


#define SETUP_INTERCOM_TYPE                  3

#define FLD_Y_POS                            80



#define INTERCOM_BIT_0               0x00000001       //  Codec 711-U
#define INTERCOM_BIT_1               0x00000002       //  Codec 711-A
#define INTERCOM_BIT_2               0x00000004       //  Codec 722
#define INTERCOM_BIT_3               0x00000008       //
#define INTERCOM_BIT_4               0x00000010       //
#define INTERCOM_BIT_5               0x00000020       //
#define INTERCOM_BIT_6               0x00000040       //
#define INTERCOM_BIT_7               0x00000080       //
#define INTERCOM_BIT_8               0x00000100       //  ReRegister after Each CALL      ( 2 Sec )
#define INTERCOM_BIT_9               0x00000200       //  ReRegister after Each CALL DEEP ( 2 Sec )



/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "usbh_core.h"
#include "usbh_hid.h"
#include "usbh_hid_parser.h"


/* Exported types ------------------------------------------------------------*/
typedef enum {
  HID_DEMO_IDLE = 0,
  HID_DEMO_WAIT,
  HID_DEMO_START,
  HID_DEMO_MOUSE,
  HID_DEMO_KEYBOARD,
  HID_DEMO_REENUMERATE,
}HID_Demo_State;

typedef enum {
  HID_MOUSE_IDLE = 0,
  HID_MOUSE_WAIT,
  HID_MOUSE_START,
}HID_mouse_State;

typedef enum {
  HID_KEYBOARD_IDLE = 0,
  HID_KEYBOARD_WAIT,
  HID_KEYBOARD_START,
}HID_keyboard_State;

typedef struct _DemoStateMachine {
  __IO HID_Demo_State     state;
  __IO HID_mouse_State    mouse_state;
  __IO HID_keyboard_State keyboard_state;
  __IO uint8_t            select;
  __IO uint8_t            lock;
}HID_DEMO_StateMachine;

typedef enum {
  APPLICATION_IDLE = 0,
  APPLICATION_DISCONNECT,
  APPLICATION_START,
  APPLICATION_READY,
  APPLICATION_RUNNING,
}HID_ApplicationTypeDef;

extern USBH_HandleTypeDef hUSBHost;
extern HID_ApplicationTypeDef Appli_state;
extern HID_MOUSE_Info_TypeDef mouse_info;
extern HID_DEMO_StateMachine hid_demo;
extern uint8_t prev_select;

extern S2C_mem_t S2C_mem;


/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void HID_MenuInit(void);
void HID_UpdateMenu(void);
void HID_MouseMenuProcess(void);
void HID_KeyboardMenuProcess(void);
void HID_MOUSE_ButtonReleased(uint8_t button_idx);
void HID_MOUSE_ButtonPressed(uint8_t button_idx);
void USR_MOUSE_ProcessData(HID_MOUSE_Info_TypeDef *data);






#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
