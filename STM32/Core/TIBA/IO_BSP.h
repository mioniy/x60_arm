/*******************************************************************************
*
*                                IO_BSP.h
*
*******************************************************************************/

/*------------------------------ Include Files -------------------------------*/

/*------------------------ Precompilation Definitions ------------------------*/

/*---------------------------- Typedef Directives ----------------------------*/

/*---------------------------- External Variables ----------------------------*/

/*------------------------ Global Function Prototypes ------------------------*/
void 						IO_BSP_Init(void);
void						IO_BSP_BuzzerOn(void);
void						IO_BSP_BuzzerOff(void);
uint8_t 					IO_BSP_MGC1_GPIO_ReadData( void);
uint8_t 					IO_BSP_MGC1_GPIO_ReadClock( void);
uint8_t 					IO_BSP_MGC2_GPIO_ReadData( void);
uint8_t 					IO_BSP_MGC2_GPIO_ReadClock( void);
void 						IO_BSP_SetLed(byte LedNum, bool fLitOn );
uint8_t 			IO_BSP_Prox1_GPIO_ReadData( void);
uint8_t 			IO_BSP_Prox1_GPIO_ReadClock( void);
uint8_t 			IO_BSP_Prox2_GPIO_ReadData( void);
uint8_t 			IO_BSP_Prox2_GPIO_ReadClock( void);
