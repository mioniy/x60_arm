

#include "udpecho.h"
#include "lwip/opt.h"
#include "lwip/api.h"
#include "lwip/sys.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UDPECHO_THREAD_PRIO  (osPriorityAboveNormal)

/* Private macro -------------------------------------------------------------*/
/* EXTERN variables ---------------------------------------------------------*/
extern uint32_t   Global_UDPip          ;
extern uint16_t   Global_UDPport        ;
/* Private function prototypes -----------------------------------------------*/
void RS485_Rx(uint8_t Data );
/* Private functions ---------------------------------------------------------*/

static uint16_t  ipoi              ;
static char     *pdata             ;
static void     *data              ;
static struct    netconn *UdpConn  ;
static u16_t     len               ;
static struct    netbuf *buf       ;
static err_t     UdpErr            ;

static void udpecho_thread(void *arg)
{
  LWIP_UNUSED_ARG(arg);
  UdpConn = netconn_new(NETCONN_UDP);
  netconn_bind(UdpConn, IP_ADDR_ANY, 1001);
  while (1)
  {
    UdpErr = netconn_recv(UdpConn, &buf);
    if (UdpErr == ERR_OK)
    {
      Global_UDPip    = *(uint32_t *)&buf->addr ;
      Global_UDPport  =  buf->port              ;
      data            =  buf->p->payload        ;
      do {
           netbuf_data(buf, &data, &len);
           pdata = (char *)data ;
           ipoi = 0 ;
           while ((ipoi<len) && (ipoi<255))
           {
             RS485_Rx(pdata[ipoi]) ;
             ipoi++;
           }
        } while (netbuf_next(buf) >= 0);
    }
    netbuf_delete(buf);
  }
}

void ReplayUdpRs485(uint8_t *p , uint16_t Len)
{
  struct netbuf *tx_buf;
  if ((Len<0x400) && (Global_UDPip) && (Global_UDPport))
  {
    tx_buf = netbuf_new();
    if (tx_buf)
       {
         data = netbuf_alloc(tx_buf, Len);
         pbuf_take(tx_buf->p, (const void *)p, Len);
         netconn_sendto(UdpConn, tx_buf, (const ip_addr_t *)&(Global_UDPip) , Global_UDPport);
         netbuf_delete(tx_buf);
       }
  }
}


/*-----------------------------------------------------------------------------------*/
void udpecho_init(void)
{
  sys_thread_new("udpecho_thread", udpecho_thread, NULL, (configMINIMAL_STACK_SIZE*4), UDPECHO_THREAD_PRIO);
}

