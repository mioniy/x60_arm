/*******************************************************************************
*
*                                IO_BSP.c
*
*******************************************************************************/
/*--------------------------------------------------------------------------------
* READERL
*  CN1:
*      CLK - PK0_RDR1_CLK - IN
*      DT  - PK1_RDR1_DT  - IN
*  CN2:
*      CLK - PK3_RDR1_CLK - IN
*      DT  - PK7_RDR1_DT  - IN
*
* MGC:
*  J1:
*      PRST - PG7_MGC1_PRS  - IN
*      CLK  - PH14_MGC1_CLK - IN
*      DT   - PH15_MGC1_DT  - IN
*
*  J2:
*      PRST - PI5_MGC2_PRS  - IN
*      CLK  - PI6_MGC2_CLK  - IN
*      DT   - PI7_MGC2_DT   - IN
*
*  Buzzer:
*      PE3  - Buzzer
*
* Pushbuttons:
*  CN4
*      BT1  - PF7_BT_1      - IN
*      LED1 - PI0_LED_BT1   - OUT
*
*  CN5
*      BT2  - PH6_BT_2      - IN
*      LED2 - PI12_LED_BT2  - OUT
*
*
*
*
*
*
*
--------------------------------------------------------------------------------*/

#pragma pack(1)

/*------------------------------ Include Files -------------------------------*/
#include "main.h"
#include "port_stm.h"    // main.h



/*------------------------ Precompilation Definitions ------------------------*/
#define IO_BSP_HW_AUDIO_OFF

/*---------------------------- Typedef Directives ----------------------------*/


/*---------------------------- External Variables ----------------------------*/


/*----------------------------- Global Variables -----------------------------*/


/*------------------------ Local Function Prototypes -------------------------*/
void IO_BSP_EXTILine15_10_Config(void);
void MgcDRV_INT( byte port);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

/*
*============================================================
*
*									GPIO
*
*============================================================
*/
void		MgcDRV_INT_Wiegand_0(void);
void		MgcDRV_INT_Wiegand_1(void);

/*----------------------------------------------------------------------------*/
static void IO_BSP_EXTILine0_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX1_Clk_Pin;												// Prox1 Clk
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX1_Clk_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI0_IRQHandler(void)
// Prox1 Clk
/*----------------------------------------------------------------------------*/
{
  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);

  MgcDRV_INT( 2);																						// 2 = Proximity 1
//   GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX1_Data_Port , SOM_I_PROX1_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void IO_BSP_EXTILine1_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX1_Data_Pin;											// Prox1 Data
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX1_Data_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI1_IRQHandler(void)
// Prox1 Data
/*----------------------------------------------------------------------------*/
{
  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_1);
//   GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX1_Data_Port , SOM_I_PROX1_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void IO_BSP_EXTILine3_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX2_Clk_Pin;												// Prox2 Clk
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX2_Clk_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI3_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI3_IRQHandler(void)
// Prox2 Clk
/*----------------------------------------------------------------------------*/
{
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_3);

	MgcDRV_INT_Wiegand_0();
  //if (WiegandType & 0x08) GetMgcBit( &Mgc3 , 1 );
  //                  else  GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX2_Data_Port , SOM_I_PROX2_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void IO_BSP_EXTILine5_9_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
//  GPIO_InitStructure.Pin  = SOM_I_MGC2_Clk_Pin ;
//  HAL_GPIO_Init(SOM_I_MGC2_Clk_Port, &GPIO_InitStructure);
  GPIO_InitStructure.Pin  = SOM_I_PROX2_Data_Pin ;											// Prox2 Data
  HAL_GPIO_Init(SOM_I_PROX2_Data_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI9_5_IRQHandler(void)
// Prox2 Data
/*----------------------------------------------------------------------------*/
{
	if  (__HAL_GPIO_EXTI_GET_IT(SOM_I_MGC2_Clk_Pin) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(SOM_I_MGC2_Clk_Pin);
		//GetMgcBit( &Mgc1 , HAL_GPIO_ReadPin(SOM_I_MGC2_Data_Port , SOM_I_MGC2_Data_Pin) );
	}
	if  (__HAL_GPIO_EXTI_GET_IT(SOM_I_PROX2_Data_Pin) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(SOM_I_PROX2_Data_Pin);

		MgcDRV_INT_Wiegand_1();
		//if (WiegandType & 0x08) GetMgcBit( &Mgc3 , 0 );
	}
}


/*----------------------------------------------------------------------------*/
void IO_BSP_EXTILine15_10_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;

  /* Configure PC13 pin as input floating */
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_MGC1_Clk_Pin;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM ; // GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_MGC1_Clk_Port, &GPIO_InitStructure);

  /* Enable and set EXTI15_10 Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0x0f , 0);  // 0x03
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

/*
------------------------------------------------------------------------------------
	EXTI15_10_IRQHandler is a non_changable name from file startup_stm_32h753xx.s
------------------------------------------------------------------------------------
*/
void		MgcDRV_WiegandINT(void);
/*----------------------------------------------------------------------------*/
void EXTI15_10_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_10) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_10);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_11) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_11);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_12) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_12);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_13) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_13);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_14) != RESET)  									// SOM_I_MGC1_Clk_Pin
      {
       	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_14);         									// SOM_I_MGC1_Clk_Pin
			MgcDRV_INT( 0);																			// 0 = MGC1
        //GetMgcBit( &Mgc1 , HAL_GPIO_ReadPin(SOM_I_MGC1_Data_Port , SOM_I_MGC1_Data_Pin) );
      }
	if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_15) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_15);
	}
}


/*----------------------------------------------------------------------------*/
void 						IO_BSP_Init(void)
/*----------------------------------------------------------------------------*/
{
	uint32_t k ;
	GPIO_InitTypeDef  GPIO_InitStruct;

//    PA5      ------> Relay 4
	GPIO_InitStruct.Pin  = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA , GPIO_PIN_5, GPIO_PIN_SET);

//    PH9      ------> Relay 1
//    PH10     ------> Relay 2
//    PH11     ------> Relay 3
//    PH12     ------> Relay 4
	GPIO_InitStruct.Pin  = GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 ;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOH , GPIO_PIN_9  , GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOH , GPIO_PIN_10 , GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOH , GPIO_PIN_11 , GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOH , GPIO_PIN_12 , GPIO_PIN_RESET);

//    PH9      ------> Input (U4 DCMI_U0) - TFT Selection (short with U4 DCMI_U0 [GPIOH:PIN_10])
	GPIO_InitStruct.Pin  = GPIO_PIN_9 ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

//    PI6      ------> CAM Power down
//    PI7      ------> CAM reset
//    PI8      ------> Board reset
	GPIO_InitStruct.Pin  = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOI , GPIO_PIN_6 , GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOI , GPIO_PIN_7, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOI , GPIO_PIN_8, GPIO_PIN_RESET);
	for (k=0; k<100000; k++) ;
	HAL_GPIO_WritePin(GPIOI , GPIO_PIN_8, GPIO_PIN_SET);


//    PJ14    ------> PWM
	GPIO_InitStruct.Pin = SOM_LED1_Pin|SOM_LED2_Pin | SOM_TFT_BACKLIGHT_Pin ;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

	HAL_GPIO_WritePin(SOM_LED1_GPIO_Port  , SOM_LED1_Pin , GPIO_PIN_SET);
	HAL_GPIO_WritePin(SOM_LED2_GPIO_Port , SOM_LED2_Pin , GPIO_PIN_RESET);

//    PG3     ------> RS485 En/Dis   Magnetic Gate		// Board 2
//    PG3     ------> Voice audio AMP						// Board 3
//    PG10    ------> CTP reset
	GPIO_InitStruct.Pin  = GPIO_PIN_3 | GPIO_PIN_10;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOG , GPIO_PIN_10 , GPIO_PIN_SET);  //  CTP RESET Off
//	HAL_GPIO_WritePin(GPIOG , GPIO_PIN_3 , GPIO_PIN_RESET); //  RS485 Rx
	IO_BSP_HW_AUDIO_OFF;
//    PD4    ------> RS485 En/Dis
	GPIO_InitStruct.Pin  = GPIO_PIN_4 ;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_RESET);  //  RS485 Rx

//   if (Global_IntercomType == PARKER_INTERCOM)
   {
      //    PD6    ------> Selector HDMI/RGB
      GPIO_InitStruct.Pin  = GPIO_PIN_6 ;
      GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
      GPIO_InitStruct.Pull = GPIO_PULLUP;
      GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
      HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
      HAL_GPIO_WritePin(GPIOD , GPIO_PIN_6 , GPIO_PIN_RESET);  //  Selector HDMI/RGB
   }

//    PE3    ------> Buzzer
//    PE4    ------> Relay 5
	GPIO_InitStruct.Pin  = GPIO_PIN_3 | GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOE , GPIO_PIN_3 , GPIO_PIN_RESET);  //  Disable BUZZ
	HAL_GPIO_WritePin(GPIOE , GPIO_PIN_4 , GPIO_PIN_RESET);

//    PE6     ------> In LOOP 1
	GPIO_InitStruct.Pin  = GPIO_PIN_6 ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
//    PA4     ------> In LOOP 2
//    PA6     ------> In ARM up/down
	GPIO_InitStruct.Pin  = GPIO_PIN_4 | GPIO_PIN_6 ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//    PB7     ------> In ARM break
	GPIO_InitStruct.Pin  = GPIO_PIN_7 ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
//    PJ12    ------> SD switch
//    PJ15    ------> CTP Int
	GPIO_InitStruct.Pin  = GPIO_PIN_12 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);
// ------------------------------------------------------------
// 	PH14		--------> MGC1 Clk
//		PH15		--------> MGC1 DT
//		PI6		--------> MGC2 Clk
//		PI7		--------> MGC2 DT
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pin  = SOM_I_BUTTON1_Pin;
	HAL_GPIO_Init(SOM_I_BUTTON1_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_I_BUTTON2_Pin;
	HAL_GPIO_Init(SOM_I_BUTTON2_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_I_MGC1_Clk_Pin;
	HAL_GPIO_Init(SOM_I_MGC1_Clk_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_I_MGC1_Data_Pin;
	HAL_GPIO_Init(SOM_I_MGC1_Data_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_I_MGC2_Clk_Pin;
	HAL_GPIO_Init(SOM_I_MGC2_Clk_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_I_MGC2_Data_Pin;
	HAL_GPIO_Init(SOM_I_MGC2_Data_Port, &GPIO_InitStruct);
	// Wiegend reader
	// Prox1 Clk
	GPIO_InitStruct.Pin  = SOM_I_PROX1_Clk_Pin;
	HAL_GPIO_Init(SOM_I_PROX1_Clk_Port, &GPIO_InitStruct);
	// Prox 1 Data
	GPIO_InitStruct.Pin  = SOM_I_PROX1_Data_Pin;
	HAL_GPIO_Init(SOM_I_PROX1_Data_Port, &GPIO_InitStruct);
	// Prox2 Clk
	GPIO_InitStruct.Pin  = SOM_I_PROX2_Clk_Pin;
	HAL_GPIO_Init(SOM_I_PROX2_Clk_Port, &GPIO_InitStruct);
	// Prox2 Data
	GPIO_InitStruct.Pin  = SOM_I_PROX2_Data_Pin;
	HAL_GPIO_Init(SOM_I_PROX2_Data_Port, &GPIO_InitStruct);


	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pin  = SOM_O_BUTTON1_LED_Pin;
	HAL_GPIO_Init(SOM_O_BUTTON1_LED_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_O_BUTTON2_LED_Pin;
	HAL_GPIO_Init(SOM_O_BUTTON2_LED_Port, &GPIO_InitStruct);
	GPIO_InitStruct.Pin  = SOM_O_BUZZER_Pin;
	HAL_GPIO_Init(SOM_O_BUZZER_Port, &GPIO_InitStruct);

//    PH5     ------> WDT signal
	GPIO_InitStruct.Pin  = GPIO_PIN_5  ;
   GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

//		Prox 1
	IO_BSP_EXTILine0_Config();							// Clk
	IO_BSP_EXTILine1_Config();							// Data
//		Prox 1
	IO_BSP_EXTILine3_Config();							// Clk
	IO_BSP_EXTILine5_9_Config();						// Data

	IO_BSP_EXTILine15_10_Config();

}



/*----------------------------------------------------------------------------*/
void				IO_BSP_BuzzerOn(void)
/*----------------------------------------------------------------------------*/
{
	//HAL_GPIO_WritePin(SOM_O_BUZZER_Port , SOM_O_BUZZER_Pin , GPIO_PIN_SET);
	Global_KeyBeep = 40;					// 200 mean 5 seconds buzzing
}


/*----------------------------------------------------------------------------*/
void				IO_BSP_BuzzerOff(void)
/*----------------------------------------------------------------------------*/
{
	//HAL_GPIO_WritePin(SOM_O_BUZZER_Port , SOM_O_BUZZER_Pin , GPIO_PIN_RESET);
  	Global_KeyBeep = 0;
}


/*
*============================================================
*
*									 MGC
*
*============================================================
*/

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_MGC1_GPIO_ReadData( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_MGC1_Data_Port , SOM_I_MGC1_Data_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_MGC1_GPIO_ReadClock( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_MGC1_Clk_Port , SOM_I_MGC1_Clk_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_MGC2_GPIO_ReadData( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_MGC2_Data_Port , SOM_I_MGC2_Data_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_MGC2_GPIO_ReadClock( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_MGC2_Clk_Port , SOM_I_MGC2_Clk_Pin) )
		return 1;
	else
	  	return 0;
}

/*
================================================
                Proximity 1/2
================================================
*/
/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_Prox1_GPIO_ReadData( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_PROX1_Data_Port , SOM_I_PROX1_Data_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_Prox1_GPIO_ReadClock( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_PROX1_Clk_Port , SOM_I_PROX1_Clk_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_Prox2_GPIO_ReadData( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_PROX2_Data_Port , SOM_I_PROX2_Data_Pin) )
		return 1;
	else
	  	return 0;
}

/*----------------------------------------------------------------------------*/
uint8_t 			IO_BSP_Prox2_GPIO_ReadClock( void)
/*----------------------------------------------------------------------------*/
{
	if ( HAL_GPIO_ReadPin(SOM_I_PROX2_Clk_Port , SOM_I_PROX2_Clk_Pin) )
		return 1;
	else
	  	return 0;
}




/*
*============================================================
*
*									LED
*
*============================================================
*/
/*----------------------------------------------------------------------------*/
void 			IO_BSP_SetLed(byte LedNum, bool fLitOn )
/*----------------------------------------------------------------------------*/
{
  	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;

	/* Check the parameters */
  	assert_param(IS_GPIO_PIN(GPIO_Pin));

  	switch ( LedNum )
  	{
  		case 1:
//		  	GPIOx	= SOM_LED1_GPIO_Port;
//	 		GPIO_Pin		= SOM_LED1_Pin;
		  	GPIOx	= SOM_LED2_GPIO_Port;
		  	GPIO_Pin		= SOM_LED2_Pin;
	 		break;

  		case 2:
		  	GPIOx	= SOM_LED2_GPIO_Port;
		  	GPIO_Pin		= SOM_LED2_Pin;
		  	break;
  	}
	if ( fLitOn )
	  	HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
}



/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
