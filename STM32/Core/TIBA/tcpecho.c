

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"

#define TCPECHO_THREAD_PRIO  (osPriorityRealtime)   // (osPriorityAboveNormal)

extern uint8_t     Global_KeyBeep ;

static struct netconn *TcpConn, *newconn ;
static err_t  TcpErr                     ;
static uint16_t  ipoi              ;
static char     *pdata             ;


void HostTcpRx(uint8_t data);
void ReplayUdp2(uint8_t *p , uint16_t Len);



static void tcpecho_thread(void *arg)
{
  LWIP_UNUSED_ARG(arg);
  /* Create a new connection identifier. */
  /* Bind connection to well known port number 7. */
  TcpConn = netconn_new(NETCONN_TCP);
  netconn_bind(TcpConn, IP_ADDR_ANY, 7);

  LWIP_ERROR("tcpecho: invalid TcpConn", (TcpConn != NULL), return;);

  /* Tell connection to go into listening mode. */
  netconn_listen(TcpConn);


  while (1)
  {
    TcpErr = netconn_accept(TcpConn, &newconn);
    if (TcpErr == ERR_OK)
    {
      struct netbuf *buf;
      void *data;
      u16_t len;
      netconn_set_recvtimeout(newconn , 30000);
      while ((TcpErr = netconn_recv(newconn, &buf)) == ERR_OK) {
        do {
              netbuf_data(buf, &data, &len);
              pdata = (char *)data ;
              ipoi = 0 ;
              while (ipoi<len) //  && (ipoi<255))
              {
                HostTcpRx(pdata[ipoi]) ;
                ipoi++;
              }
           } while (netbuf_next(buf) >= 0) ;
        netbuf_delete(buf);
        HostTcpRx(0x0d);
      }
      netconn_close(newconn);
      netconn_delete(newconn);
    }
  }
}

void ReplayTcp(uint8_t *p , uint16_t Len)
{
 void *data;
  if (newconn != NULL)
     {
       data = (void *)p ;
       TcpErr = netconn_write(newconn, data, Len, NETCONN_COPY);
     }
}

/*-----------------------------------------------------------------------------------*/
void tcpecho_init(void)
{
  sys_thread_new("tcpecho_thread", tcpecho_thread, NULL, (configMINIMAL_STACK_SIZE*4), TCPECHO_THREAD_PRIO);
}

