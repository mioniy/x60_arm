
#include "main.h"
#include "port_stm.h"    // main.h


#define SDRAM_TIMEOUT                    ((uint32_t)0xFFFF)
#define REFRESH_COUNT                    ((uint32_t)0x0603)   /* SDRAM refresh counter */

#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

TIM_HandleTypeDef    TimHandle3;

/*------------------------ Local Function Prototypes -------------------------*/
void  SU4API_Tx_ISR           ( void            );
void  SU4API_Rx_ISR           ( byte   Rx_Data  );
void  ReaderAPI_Rx_Reader1_ISR( void            );
void  ReaderAPI_Tx_Reader1_ISR( void            );
void  LanAPI_Tx_ISR           ( void            );
void  LanAPI_Rx_ISR           ( byte   rxByte   );      // MPSW Rx func
void  Intr_UART_RX1           ( byte   x        );
void  PrinterAPI_Tx_ISR       ( void            );
void  PrinterAPI_Rx_ISR       ( byte   Data     );
void  Debug_SendISR           ( void            );
void  Debug_Rx_ISR            ( byte   Data     );


/*--------------------------- Function Prototypes ----------------------------*/

/* Prescaler declaration */
static uint32_t uwPrescalerValue = 0;
/*----------------------------------------------------------------------------*/
static void EXTILine0_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX1_Clk_Pin;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX1_Clk_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI0_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
//   GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX1_Data_Port , SOM_I_PROX1_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void EXTILine1_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX1_Data_Pin;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX1_Data_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI1_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_1);
//   GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX1_Data_Port , SOM_I_PROX1_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void EXTILine3_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_PROX2_Clk_Pin;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_PROX2_Clk_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI3_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI3_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_3);
//  if (WiegandType & 0x08) GetMgcBit( &Mgc3 , 1 );
//                    else  GetMgcBit( &Mgc2 , HAL_GPIO_ReadPin(SOM_I_PROX2_Data_Port , SOM_I_PROX2_Data_Pin) );
}

/*----------------------------------------------------------------------------*/
static void EXTILine5_9_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStructure.Pin  = SOM_I_MGC2_Clk_Pin ;
  HAL_GPIO_Init(SOM_I_MGC2_Clk_Port, &GPIO_InitStructure);
  GPIO_InitStructure.Pin  = SOM_I_PROX2_Data_Pin ;
  HAL_GPIO_Init(SOM_I_PROX2_Data_Port, &GPIO_InitStructure);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0x0f , 0);   // 0x03
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

/*----------------------------------------------------------------------------*/
void EXTI9_5_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  if  (__HAL_GPIO_EXTI_GET_IT(SOM_I_MGC2_Clk_Pin) != RESET)
      {
        __HAL_GPIO_EXTI_CLEAR_IT(SOM_I_MGC2_Clk_Pin);
//        GetMgcBit( &Mgc1 , HAL_GPIO_ReadPin(SOM_I_MGC2_Data_Port , SOM_I_MGC2_Data_Pin) );
      }
  if  (__HAL_GPIO_EXTI_GET_IT(SOM_I_PROX2_Data_Pin) != RESET)
      {
        __HAL_GPIO_EXTI_CLEAR_IT(SOM_I_PROX2_Data_Pin);
//       if (WiegandType & 0x08) GetMgcBit( &Mgc3 , 0 );
      }
}

/*----------------------------------------------------------------------------*/
static void EXTILine10_15_Config(void)
/*----------------------------------------------------------------------------*/
{
  GPIO_InitTypeDef   GPIO_InitStructure;

  /* Configure PC13 pin as input floating */
  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Pin = SOM_I_MGC1_Clk_Pin;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM ; // GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SOM_I_MGC1_Clk_Port, &GPIO_InitStructure);

  /* Enable and set EXTI15_10 Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0x0f , 0);  // 0x03
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void       MgcDRV_INT( void);
/*----------------------------------------------------------------------------*/
void EXTI15_10_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_10) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_10);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_11) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_11);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_12) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_12);
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_13) != RESET) __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_13);

  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_14) != RESET)  									// SOM_I_MGC1_Clk_Pin
  {
       __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_14);         // SOM_I_MGC1_Clk_Pin
       //GetMgcBit( &Mgc1 , HAL_GPIO_ReadPin(SOM_I_MGC1_Data_Port , SOM_I_MGC1_Data_Pin) );
		 MgcDRV_INT();
  }
  
  if  (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_15) != RESET) 										//MGC1 DT
  {
	 	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_15);
  }
}


/*----------------------------------------------------------------------------*/
void RS485_ModeRx(void)
/*----------------------------------------------------------------------------*/
{
  HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_RESET);
  Global_TXRX_485 = 0 ;
}

/*----------------------------------------------------------------------------*/
void RS485_ModeTx(void)
/*----------------------------------------------------------------------------*/
{
  HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_SET);
  Global_TXRX_485 = 1 ;
}

/*----------------------------------------------------------------------------*/
/*                                UART 1                                      */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static void BR_USART1_UART_Init(void)
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle1.Instance = USART1;
  Global_UartHandle1.Init.BaudRate = 9600 ;
  Global_UartHandle1.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle1.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle1.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle1.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&Global_UartHandle1) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
//   __HAL_UART_ENABLE_IT( &Global_UartHandle1 , UART_IT_RXNE);
}



				
// -------------------------------------------------------------------------------------------------------------------


/*----------------------------------------------------------------------------*/
/*                                UART 2                                      */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void BR_USART2_UART_Init(void)	// RS485 ==> CT20 or other main computer
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle2.Instance = USART2;
  Global_UartHandle2.Init.BaudRate = 9600;
  Global_UartHandle2.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle2.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle2.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle2.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle2.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle2.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle2.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle2.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle2.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle2) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
  __HAL_UART_ENABLE_IT( &Global_UartHandle2 , UART_IT_RXNE);
}


/*----------------------------------------------------------------------------*/
void USART2_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
   #ifdef   __USES_BRD3__
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_TC))
          {
            ReaderAPI_Tx_Reader1_ISR();
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_RXNE))
          {
            Global_RxData5 = (uint8_t)(Global_UartHandle2.Instance->RDR & (uint8_t)0x00FF);
            __HAL_UART_CLEAR_FLAG(&Global_UartHandle2, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
            ReaderAPI_Rx_Reader1_ISR();
          }
     }
   #else
   {
    static byte f ;
     f = 0 ;
     if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_TC))
        {
          if ((__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_TC)) && (Global_TXRX_485))
             {
               LanAPI_Tx_ISR();
               f = 1 ;
             }
        }
     if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_RXNE))
        {
          if (__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_RXNE))
             {
               Global_RxData2 = (uint8_t)(Global_UartHandle2.Instance->RDR & (uint8_t)0x00FF);
               __HAL_UART_CLEAR_FLAG(&Global_UartHandle2, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
               if (Global_SysReady)
                  {
                    LanAPI_Rx_ISR( Global_RxData2);
   
                    Global_UDPip1   = 0 ;
                    Global_UDPport1 = 0 ;
                  }
               f = 1 ;
             }
        }
     if (f==0)
        {
          __HAL_UART_DISABLE_IT  ( &Global_UartHandle2 , UART_IT_TC );
          __HAL_UART_ENABLE_IT  ( &Global_UartHandle2 , UART_IT_RXNE);
          RS485_ModeRx();
        }
   }
   #endif
}

/*----------------------------------------------------------------------------*/
void USART2_SendByte( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
	Global_UartHandle2.Instance->TDR = tx_data;
}


	  
/*----------------------------------------------------------------------------*/
/*                                UART 3                                      */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void BR_UART3_Init(void)     // MP - COM3     -  SU4
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle3.Instance = UART8;
  Global_UartHandle3.Init.BaudRate = 9600;
  Global_UartHandle3.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle3.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle3.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle3.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle3.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle3.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle3.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle3.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle3.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle3) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
  __HAL_UART_ENABLE_IT( &Global_UartHandle3 , UART_IT_RXNE);
}



/*----------------------------------------------------------------------------*/
void UART3_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  // COM3  SU4  (UART3)
  //--------------------------------------------------------------
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle3, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle3, UART_IT_TC))
          {
            #ifdef   __USES_BRD3__
            SU4API_Tx_ISR();
            #endif
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle3, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle3, UART_IT_RXNE))
          {
            Global_RxData8 = (uint8_t)(Global_UartHandle3.Instance->RDR & (uint8_t)0x00FF);
            __HAL_UART_CLEAR_FLAG(&Global_UartHandle3, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
            #ifdef   __USES_BRD3__
            SU4API_Rx_ISR( Global_RxData3);
            #endif
          }
     } 
  //--------------------------------------------------------------
}



/*----------------------------------------------------------------------------*/
/*                            UART 4   COM4                                   */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void BR_UART4_Init(void)    // MP - COM4
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle4.Instance = UART4;
  Global_UartHandle4.Init.BaudRate = 115200 ; // 9600 ;
  Global_UartHandle4.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle4.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle4.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle4.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle4.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle4.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle4.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle4.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle4.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle4) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
  __HAL_UART_ENABLE_IT( &Global_UartHandle4 , UART_IT_RXNE);
}



/*----------------------------------------------------------------------------*/
void UART4_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle4, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_TC))
          {
            #ifndef  __USES_BRD3__
				Debug_SendISR();
				#endif
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle4, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_RXNE))
          {
   			Global_RxData4 = (uint8_t)(Global_UartHandle4.Instance->RDR & (uint8_t)0x00FF);
            __HAL_UART_CLEAR_FLAG(&Global_UartHandle4, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
            #ifndef  __USES_BRD3__
            Debug_Rx_ISR( Global_RxData4);
            #endif
          }
     }
}


/*----------------------------------------------------------------------------*/
void COM4_SendByte( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
  	if (__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_TC))
		Global_UartHandle4.Instance->TDR = 0x00FF & (uint16_t)tx_data ;
}




/*----------------------------------------------------------------------------*/
void COM4_putchar( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
  	while ((__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_TC)) == 0);
	Global_UartHandle4.Instance->TDR = 0x00FF & (uint16_t)tx_data ;
}



/*----------------------------------------------------------------------------*/
void	BR_Com4TxEnable( void)
/*----------------------------------------------------------------------------*/
{
	__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_TC );							// Enable Tx IRQ
}


/*----------------------------------------------------------------------------*/
/*                            UART 5   COM3                                   */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void BR_UART5_Init(void)    // MP - COM3
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle5.Instance = UART5;
  Global_UartHandle5.Init.BaudRate = 9600;										// A.C 30 Jul 2019:  ORIGINAL BAUD RATE IS 9600
  Global_UartHandle5.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle5.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle5.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle5.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle5.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle5.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle5.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle5.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle5.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle5.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle5.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle5) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
  /* USER CODE BEGIN UART8_Init 2 */
  __HAL_UART_ENABLE_IT( &Global_UartHandle5 , UART_IT_RXNE);
  /* USER CODE END UART8_Init 2 */

}


/*----------------------------------------------------------------------------*/
void UART5_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle5, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle5, UART_IT_TC))
          {
            #ifndef  __USES_BRD3__
				ReaderAPI_Tx_Reader1_ISR();
				#endif
            
            #ifdef   __USES_BRD3__
            Debug_SendISR();
            #endif
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle5, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle5, UART_IT_RXNE))
          {
            Global_RxData5 = (uint8_t)(Global_UartHandle5.Instance->RDR & (uint8_t)0x00FF);
           	__HAL_UART_CLEAR_FLAG(&Global_UartHandle5, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
            #ifndef  __USES_BRD3__
				ReaderAPI_Rx_Reader1_ISR();
            #endif
            
            #ifdef   __USES_BRD3__
            Debug_Rx_ISR( Global_RxData5);
            #endif
          }
     }
}



/*----------------------------------------------------------------------------*/
byte COM3_GetRxByte( void)
/*----------------------------------------------------------------------------*/
{
 	return Global_RxData5;
}

/*----------------------------------------------------------------------------*/
void COM3_SendByte( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
	Global_UartHandle5.Instance->TDR = tx_data;
}


/*----------------------------------------------------------------------------*/
void	BR_Com3TxEnable( void)
/*----------------------------------------------------------------------------*/
{
	__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
}


/*----------------------------------------------------------------------------*/
/*                            UART 6     COM2                                 */
/*----------------------------------------------------------------------------*/

void Printer_Rx(byte RxData);
/*----------------------------------------------------------------------------*/
void BR_USART6_UART_Init(uint32_t Baud)     // MP - COM2      Printer
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle6.Instance = USART6;
  Global_UartHandle6.Init.BaudRate = 9600;
  Global_UartHandle6.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle6.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle6.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle6.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle6.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle6.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle6.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle6.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle6.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle6) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
}



/*----------------------------------------------------------------------------*/
void USART6_IRQHandler(void)				// COM2/COM6 = UART6 => PRINTER
/*----------------------------------------------------------------------------*/
{
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle6, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle6, UART_IT_TC))
          {
				PrinterAPI_Tx_ISR();				// Remote ISR
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle6, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle6, UART_IT_RXNE))
          {
            Global_RxData6 = (uint8_t)(Global_UartHandle6.Instance->RDR & (uint8_t)0x00FF);
           	__HAL_UART_CLEAR_FLAG(&Global_UartHandle6, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
				PrinterAPI_Rx_ISR( Global_RxData6);
          }
     }
}


/*----------------------------------------------------------------------------*/
void COM2_SendByte( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
	Global_UartHandle6.Instance->TDR = 0x00FF & (uint16_t)tx_data ;
}

/*----------------------------------------------------------------------------*/
byte COM2_GetRxByte( void)
/*----------------------------------------------------------------------------*/
{
 	return Global_RxData6;
}



/*----------------------------------------------------------------------------*/
void COM2_putchar( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
   while ((__HAL_UART_GET_IT(&Global_UartHandle6, UART_IT_TC)) == 0);
   Global_UartHandle6.Instance->TDR = 0x00FF & (uint16_t)tx_data ;
}



/*----------------------------------------------------------------------------*/
void	BR_Com2TxEnable( void)
/*----------------------------------------------------------------------------*/
{
	__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
}



/*----------------------------------------------------------------------------*/
/*                                UART 8                                      */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void BR_UART8_Init(void)     // MP - COM1     -  SU4   (IO board)
/*----------------------------------------------------------------------------*/
{
  Global_UartHandle8.Instance = UART8;
  Global_UartHandle8.Init.BaudRate = 9600;	//115200;
  Global_UartHandle8.Init.WordLength = UART_WORDLENGTH_8B;
  Global_UartHandle8.Init.StopBits = UART_STOPBITS_1;
  Global_UartHandle8.Init.Parity = UART_PARITY_NONE;
  Global_UartHandle8.Init.Mode = UART_MODE_TX_RX;
  Global_UartHandle8.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  Global_UartHandle8.Init.OverSampling = UART_OVERSAMPLING_16;
  Global_UartHandle8.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  Global_UartHandle8.Init.Prescaler = UART_PRESCALER_DIV1;
  Global_UartHandle8.Init.FIFOMode = UART_FIFOMODE_DISABLE;
  Global_UartHandle8.Init.TXFIFOThreshold = UART_TXFIFO_THRESHOLD_1_8;
  Global_UartHandle8.Init.RXFIFOThreshold = UART_RXFIFO_THRESHOLD_1_8;
  Global_UartHandle8.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&Global_UartHandle8) != HAL_OK)
  {
    BR_Error_Handler(0x40);
  }
  __HAL_UART_ENABLE_IT( &Global_UartHandle8 , UART_IT_RXNE);
}

/*----------------------------------------------------------------------------*/
void UART8_IRQHandler(void)
/*----------------------------------------------------------------------------*/
{
  // COM1  SU4  (UART8)
  //--------------------------------------------------------------
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle8, UART_IT_TC))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle8, UART_IT_TC))
          {
            #ifndef   __USES_BRD3__
				SU4API_Tx_ISR();
				#endif
          }
     }
  if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle8, UART_IT_RXNE))
     {
       if (__HAL_UART_GET_IT(&Global_UartHandle8, UART_IT_RXNE))
          {
            Global_RxData8 = (uint8_t)(Global_UartHandle8.Instance->RDR & (uint8_t)0x00FF);
           	__HAL_UART_CLEAR_FLAG(&Global_UartHandle8, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
            #ifndef   __USES_BRD3__
				SU4API_Rx_ISR( Global_RxData8);
				#endif
          }
     } 
  //--------------------------------------------------------------
}


/*----------------------------------------------------------------------------*/
void COM1_SendByte( uint8_t tx_data)
/*----------------------------------------------------------------------------*/
{
	Global_UartHandle8.Instance->TDR = tx_data;
}


/*----------------------------------------------------------------------------*/
void	BR_Com1TxEnable( void)
/*----------------------------------------------------------------------------*/
{
	__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
}



/*----------------------------------------------------------------------------*/
void	BR_USARTx_UART_Disable_Tx_IRQ( byte com)
/*----------------------------------------------------------------------------*/
{
	
	switch (com)
	{
		case 1:
   		__HAL_UART_DISABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
		case 2:
   		__HAL_UART_DISABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 3:
   		__HAL_UART_DISABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 4:
   		__HAL_UART_DISABLE_IT ( &Global_UartHandle4 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 5:
   		//__HAL_UART_DISABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 6:
   		//__HAL_UART_DISABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 7:
   		//__HAL_UART_DISABLE_IT ( &Global_UartHandle7 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 8:
   		//__HAL_UART_DISABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
	  		break;		
	}    
}





/*----------------------------------------------------------------------------*/
void	BR_USARTx_UART_Enable_Tx_IRQ( byte com)
/*
	com values: COM1 .. COM8  = 1..8
*/
/*----------------------------------------------------------------------------*/
{
	
	switch (com)
	{
		case 1:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
		case 2:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 3:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 4:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 5:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 6:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 7:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle7 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
	  
			
		case 8:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
	  		break;
			
	}
}


/*----------------------------------------------------------------------------*/
void	BR_USARTx_UART_Enable_Rx_IRQ( byte com)
/*
	com values: COM1 .. COM8  = 1..8
*/
/*----------------------------------------------------------------------------*/
{
	
	switch (com)
	{
		case 1:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
		case 2:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 3:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 4:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 5:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 6:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 7:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle7 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 8:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
			
	}
}



/*----------------------------------------------------------------------------*/
void	BR_USARTx_UART_Enable_TxRx_IRQ( byte com)
/*
	com values: COM1 .. COM8  = 1..8
*/
/*----------------------------------------------------------------------------*/
{
	
	switch (com)
	{
		case 1:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
		case 2:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 3:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 4:
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_TC );							// Enable Tx IRQ
   		__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 5:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_TC );							// Enable Tx IRQ
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 6:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_TC );							// Enable Tx IRQ
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 7:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle7 , UART_IT_TC );							// Enable Tx IRQ
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle7 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
	  
			
		case 8:
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_TC );							// Enable Tx IRQ
   		//__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_RXNE);							// Enable Rx IRQ
	  		break;
			
	}
}



/**
  * @brief  Perform the SDRAM exernal memory inialization sequence
  * @param  hsdram: SDRAM handle
  * @param  Command: Pointer to SDRAM command structure
  * @retval None
  */
/*----------------------------------------------------------------------------*/
void SDRAM_Initialization_Sequence(SDRAM_HandleTypeDef *hsdram)
/*----------------------------------------------------------------------------*/
{
  __IO uint32_t tmpmrd =0;
  FMC_SDRAM_CommandTypeDef Command = {0} ;
  /* Step 1:  Configure a clock configuration enable command */
  Command.CommandMode = FMC_SDRAM_CMD_CLK_ENABLE;
  Command.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber = 1;
  Command.ModeRegisterDefinition = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(hsdram, &Command, SDRAM_TIMEOUT);

  /* Step 2: Insert 100 us minimum delay */
  /* Inserted delay is equal to 1 ms due to systick time base unit (ms) */
  HAL_Delay(10);

  /* Step 3: Configure a PALL (precharge all) command */
  Command.CommandMode = FMC_SDRAM_CMD_PALL;
  Command.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber = 1;
  Command.ModeRegisterDefinition = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(hsdram, &Command, SDRAM_TIMEOUT);
  HAL_Delay(10);

  /* Step 4 : Configure a Auto-Refresh command */
  Command.CommandMode = FMC_SDRAM_CMD_AUTOREFRESH_MODE;
  Command.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber = 8;
  Command.ModeRegisterDefinition = 0;

  /* Send the command */
  HAL_SDRAM_SendCommand(hsdram, &Command, SDRAM_TIMEOUT);
  HAL_Delay(10);

  /* Step 5: Program the external memory mode register */
  tmpmrd = (uint32_t)SDRAM_MODEREG_BURST_LENGTH_1          |
                     SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL   |
                     SDRAM_MODEREG_CAS_LATENCY_3           |
                     SDRAM_MODEREG_OPERATING_MODE_STANDARD |
                     SDRAM_MODEREG_WRITEBURST_MODE_SINGLE;

  Command.CommandMode = FMC_SDRAM_CMD_LOAD_MODE;
  Command.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber = 1;
  Command.ModeRegisterDefinition = tmpmrd;

  /* Send the command */
  HAL_SDRAM_SendCommand(hsdram, &Command, SDRAM_TIMEOUT);
  HAL_Delay(10);

  /* Step 6: Set the refresh rate counter */
  /* Set the device refresh rate */
  HAL_SDRAM_ProgramRefreshRate(hsdram, REFRESH_COUNT);

}

/*----------------------------------------------------------------------------*/
void HAL_DMA2D_MspInit(DMA2D_HandleTypeDef *hdma2d)
/*----------------------------------------------------------------------------*/
{
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  __HAL_RCC_DMA2D_CLK_ENABLE();

  /*##-2- NVIC configuration  ################################################*/
  /* NVIC configuration for DMA2D transfer complete interrupt */
  HAL_NVIC_SetPriority(DMA2D_IRQn, 0x0e , 0);  // 0x02
  HAL_NVIC_EnableIRQ(DMA2D_IRQn);
}

/*----------------------------------------------------------------------------*/
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
/*----------------------------------------------------------------------------*/
{
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIMx_CLK_ENABLE();

  /*##-2- Configure the NVIC for TIMx ########################################*/
  /* Set the TIMx priority */
  HAL_NVIC_SetPriority(TIMx_IRQn, 3, 0);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIMx_IRQn);
}

/*----------------------------------------------------------------------------*/
void BR_Error_Handler(uint32_t bitx)
/*----------------------------------------------------------------------------*/
{
  Global_ErrorBitX |= bitx ;
}

/*----------------------------------------------------------------------------*/
static void IO_Init(void)
/*----------------------------------------------------------------------------*/
{
 uint32_t k ;
  GPIO_InitTypeDef  GPIO_InitStruct;

//    PA5      ------> Relay 4
    GPIO_InitStruct.Pin  = GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOA , GPIO_PIN_5, GPIO_PIN_SET);

//    PH9      ------> Relay 1
//    PH10     ------> Relay 2
//    PH11     ------> Relay 3
//    PH12     ------> Relay 4
    GPIO_InitStruct.Pin  = GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 ;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOH , GPIO_PIN_9  , GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOH , GPIO_PIN_10 , GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOH , GPIO_PIN_11 , GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOH , GPIO_PIN_12 , GPIO_PIN_RESET);

//    PI6      ------> CAM Power down
//    PI7      ------> CAM reset
//    PI8      ------> Board reset
    GPIO_InitStruct.Pin  = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOI , GPIO_PIN_6 , GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOI , GPIO_PIN_7, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOI , GPIO_PIN_8, GPIO_PIN_RESET);
    for (k=0; k<100000; k++) ;
    HAL_GPIO_WritePin(GPIOI , GPIO_PIN_8, GPIO_PIN_SET);


//    PJ14    ------> PWM
    GPIO_InitStruct.Pin = SOM_LED1_Pin|SOM_LED2_Pin | SOM_TFT_BACKLIGHT_Pin ;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

    HAL_GPIO_WritePin(SOM_LED1_GPIO_Port  , SOM_LED1_Pin , GPIO_PIN_SET);
    HAL_GPIO_WritePin(SOM_LED2_GPIO_Port , SOM_LED2_Pin , GPIO_PIN_RESET);

//    PG3     ------> RS485 En/Dis   Magnetic Gate
//    PG10    ------> CTP reset
    GPIO_InitStruct.Pin  = GPIO_PIN_3 | GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOG , GPIO_PIN_10 , GPIO_PIN_SET);  //  CTP RESET Off
    HAL_GPIO_WritePin(GPIOG , GPIO_PIN_3 , GPIO_PIN_RESET); //  RS485 Rx
//    PD4    ------> RS485 En/Dis
    GPIO_InitStruct.Pin  = GPIO_PIN_4 ;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_RESET);  //  RS485 Rx
//    PE3    ------> Buzzer
//    PE4    ------> Relay 5
    GPIO_InitStruct.Pin  = GPIO_PIN_3 | GPIO_PIN_4;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOE , GPIO_PIN_3 , GPIO_PIN_RESET);  //  Disable BUZZ
    HAL_GPIO_WritePin(GPIOE , GPIO_PIN_4 , GPIO_PIN_RESET);

//    PE6     ------> In LOOP 1
    GPIO_InitStruct.Pin  = GPIO_PIN_6 ;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
//    PA4     ------> In LOOP 2
//    PA6     ------> In ARM up/down
    GPIO_InitStruct.Pin  = GPIO_PIN_4 | GPIO_PIN_6 ;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//    PB7     ------> In ARM break
    GPIO_InitStruct.Pin  = GPIO_PIN_7 ;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
//    PJ12    ------> SD switch
//    PJ15    ------> CTP Int
    GPIO_InitStruct.Pin  = GPIO_PIN_12 | GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);
// ------------------------------------------------------------
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Pin  = SOM_I_BUTTON1_Pin;
    HAL_GPIO_Init(SOM_I_BUTTON1_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_BUTTON2_Pin;
    HAL_GPIO_Init(SOM_I_BUTTON2_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_MGC1_Clk_Pin;
    HAL_GPIO_Init(SOM_I_MGC1_Clk_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_MGC1_Data_Pin;
    HAL_GPIO_Init(SOM_I_MGC1_Data_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_MGC2_Clk_Pin;
    HAL_GPIO_Init(SOM_I_MGC2_Clk_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_MGC2_Data_Pin;
    HAL_GPIO_Init(SOM_I_MGC2_Data_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_PROX1_Clk_Pin;
    HAL_GPIO_Init(SOM_I_PROX1_Clk_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_PROX1_Data_Pin;
    HAL_GPIO_Init(SOM_I_PROX1_Data_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_PROX2_Clk_Pin;
    HAL_GPIO_Init(SOM_I_PROX2_Clk_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_I_PROX2_Data_Pin;
    HAL_GPIO_Init(SOM_I_PROX2_Data_Port, &GPIO_InitStruct);


    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pin  = SOM_O_BUTTON1_LED_Pin;
    HAL_GPIO_Init(SOM_O_BUTTON1_LED_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_O_BUTTON2_LED_Pin;
    HAL_GPIO_Init(SOM_O_BUTTON2_LED_Port, &GPIO_InitStruct);
    GPIO_InitStruct.Pin  = SOM_O_BUZZER_Pin;
    HAL_GPIO_Init(SOM_O_BUZZER_Port, &GPIO_InitStruct);
}


/*----------------------------------------------------------------------------*/
void Init_TFT(void)
/*----------------------------------------------------------------------------*/
{
  /*##-1- Initialize the LCD #################################################*/
  /* Initialize the LCD */
  CTP_Setup();
  BSP_LCD_Init();
  BSP_LCD_SelectLayer(LAYER_1);
  BSP_LCD_LayerDefaultInit(LAYER_1, LCD_FB_START_ADDRESS);

  BSP_LCD_SetFont(&Courier_New_Bold_30);
  /* Clear the LCD */
  BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
  BSP_LCD_Clear(LCD_COLOR_BLACK);
}


/*----------------------------------------------------------------------------*/
void Main_StartTimer3(void)
/*----------------------------------------------------------------------------*/
{
  /* Compute the prescaler value to have TIMx counter clock equal to 100000 Hz */
  uwPrescalerValue = (uint32_t)(SystemCoreClock / (2*100000)) - 1;

  /* Set TIMx instance */
  TimHandle3.Instance = TIMx;

  /* Initialize TIMx peripheral as follows:
       + Period = 10000 - 1    ???????????????
       + Prescaler = (SystemCoreClock/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle3.Init.Period            = 80 - 1;
  TimHandle3.Init.Prescaler         = uwPrescalerValue;
  TimHandle3.Init.ClockDivision     = 0;
  TimHandle3.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle3.Init.RepetitionCounter = 0;

  if (HAL_TIM_Base_Init(&TimHandle3) != HAL_OK)
  {
    /* Initialization Error */
    BR_Error_Handler(0x2000);
  }

  /*##-2- Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if (HAL_TIM_Base_Start_IT(&TimHandle3) != HAL_OK)
  {
    /* Starting Error */
    BR_Error_Handler(0x2000);
  }
}

/*----------------------------------------------------------------------------*/
void MainInit_PutCom4(uint8_t ch)
/*----------------------------------------------------------------------------*/
{
  HAL_UART_Transmit(&Global_UartHandle4, (uint8_t *)&ch, 1 , 10 );
}

/*----------------------------------------------------------------------------*/
void MainInit_SetUpBarcodeLSR118(void)
/*----------------------------------------------------------------------------*/
{
  MainInit_PutCom4(0x16); MainInit_PutCom4(0x4D); MainInit_PutCom4(0x0D);
  MainInit_PutCom4(0x53); MainInit_PutCom4(0x55); MainInit_PutCom4(0x46);
  MainInit_PutCom4(0x42); MainInit_PutCom4(0x4B); MainInit_PutCom4(0x32);
  MainInit_PutCom4(0x39); MainInit_PutCom4(0x39); MainInit_PutCom4(0x30);
  MainInit_PutCom4(0x44); MainInit_PutCom4(0x2E);
  Global_KeyBeep= 20 ;
}

/*----------------------------------------------------------------------------*/
void BRD208_Init(void)
/*----------------------------------------------------------------------------*/
{

  Global_UDPip1    = 0      ;
  Global_UDPip2    = 0      ;
  Global_BackLevel = 3      ;
  HAL_GPIO_WritePin(GPIOJ, SOM_LED1_Pin|SOM_LED2_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(SOM_TFT_BACKLIGHT_Port , SOM_TFT_BACKLIGHT_Pin  , GPIO_PIN_RESET);
  Init_TFT();
  DrawScreen1();
  RTC_init();
  BR_USART1_UART_Init();
  BR_USART2_UART_Init();																			// RS485 --> CT20
  BR_USART3_UART_Init();																			// 
  BR_UART4_Init();																					// COM4
  BR_UART5_Init();																					// COM3
  BR_UART8_Init();																					// COM1
  BR_USART6_UART_Init(9600);																		// COM2	Printer

  Global_PutcDevice = TFT_DEV ;
  IO_Init();

  EXTILine0_Config();
  EXTILine3_Config();
  EXTILine5_9_Config();
  EXTILine10_15_Config();

  Qmem_Init();
  Main_StartTimer3();
//  BSP_LCD_SetTextColor(WHITE);
  BSP_LCD_SetTextColor(BRIGHTBLUE);
//  BSP_LCD_SetTextColor(BRIGHTGREEN);

#if (_fontOrientation == ORIENT_VER)
    BSP_LCD_DrawCircleWidth(300,500,180,70);
  #else
    BSP_LCD_DrawCircleWidth(512,300,180,70);
#endif

  BSP_LCD_SetTextColor(WHITE);
  SetTftFont(' ',4,0,0);
  MoveTo(4,4);
  printf("Init I2C3\n");
  I2C3_Init();
  printf("Init Ethernet\n");
  Global_VoiceVolume = ldaP(_P_fbuzz_off + 1);
  if (Global_VoiceVolume == 0) Global_VoiceVolume= 0x3b ;
  wm8978_Init();
}

/*----------------------------------------------------------------------------*/
void MainC_LF(void)
/*----------------------------------------------------------------------------*/
{
  if (_fontOrientation == ORIENT_HOR)
     {
       Global_cursorY += Global_fontHeight ;
       if (Global_cursorY >= GetMaxY())
           {
             BSP_LCD_Clear(LCD_COLOR_BLACK);
             Global_cursorY = 4 ;
           }
       Global_cursorX = 4 ;
       MoveTo(Global_cursorX , Global_cursorY);
     }
    else
     {
       Global_cursorX += Global_fontHeight ;
       if (Global_cursorX >= GetMaxY())
          {
            BSP_LCD_Clear(LCD_COLOR_BLACK);
            Global_cursorX = 4 ;
          }
       Global_cursorY = 4 ;
       MoveTo(Global_cursorY , Global_cursorX);
     }
}

/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
PUTCHAR_PROTOTYPE
{
  switch (Global_PutcDevice)
  {
    case TFT_DEV    :
    case TFT_DEV_C  :
                      if (ch >=' ')
                         {
                          if (_fontOrientation == ORIENT_HOR)
                              {
                                if (Global_cursorX >= GetMaxX())
                                   {
                                     MainC_LF();
                                   }
                              }
                            else
                              {
                                if (Global_cursorY >= GetMaxX())
                                   {
                                     MainC_LF();
                                   }
                              }
                           if (Global_PutcDevice == TFT_DEV) OutChar(ch,0);
                                                        else OutChar(ch,1);
                         }
                      if (ch == 0x0a)
                         {
                           MainC_LF();
                         }
                       break ;
    case COM1_DEV :
                      HAL_UART_Transmit(&Global_UartHandle1, (uint8_t *)&ch, 1 , 10 );
                      break ;
    case COM4_DEV :
//                      HAL_UART_Transmit(&Global_UartHandle4, (uint8_t *)&ch, 1 , 10 );
                      break ;
    case COM5_DEV :
//                      HAL_UART_Transmit(&Global_UartHandle5, (uint8_t *)&ch, 1 , 10 );
                      break ;
    case COM6_DEV :
                      HAL_UART_Transmit(&Global_UartHandle6, (uint8_t *)&ch, 1 , 10 );
                      break ;
  }
  return ch;
}


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
