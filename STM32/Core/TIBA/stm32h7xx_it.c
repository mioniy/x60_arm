/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32h7xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32h7xx_it.h"
#include "cmsis_os.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
extern ETH_HandleTypeDef    EthHandle;
extern TIM_HandleTypeDef    TimHandle3;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
void ETH_IRQHandler(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  while (1)
  {
     HAL_NVIC_SystemReset();
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  while (1)
  {
     HAL_NVIC_SystemReset();
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  while (1)
  {
     HAL_NVIC_SystemReset();
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  while (1)
  {
     HAL_NVIC_SystemReset();
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

extern	void       Task_Timer_INT( void);
void SysTick_Handler(void)
{
  static uint32_t tYigal = 0 ;
//  HAL_IncTick();
  osSystickHandler();
  tYigal++;
  if (tYigal>9)
     {
       tYigal = 0 ;
       Task_Timer_INT();
     }

  {
    static uint8_t  Tm10 ;
    static uint16_t Sbk  ;
    Global_TmTick++;
    if (Global_TmTick & 0x01) GPIOH->BSRR  = GPIO_PIN_5 ;
                         else GPIOH->BSRR = ((uint32_t)GPIO_PIN_5 << 16U) ;  // WDR
    Tm10++;
    Sbk <<= 1 ;
    if (Sbk == 0) Sbk = 1 ;

   if( Global_CTP_ScreenType == 6)  // 4 = MP/SW/VPS/CPS-60; 6 = APS-60
   {
    if (Sbk & Global_BackAnd) GPIOJ->BSRR = ((uint32_t)SOM_TFT_BACKLIGHT_Pin << 16U) ; //  LTDC Back Light Off    15.4
                         else GPIOJ->BSRR = SOM_TFT_BACKLIGHT_Pin ;                     //  LTDC Back Light On     15.4
   }
   else
   {
    if (Sbk & Global_BackAnd) GPIOJ->BSRR = SOM_TFT_BACKLIGHT_Pin ;                  //  LTDC Back Light Off
                        else GPIOJ->BSRR = ((uint32_t)SOM_TFT_BACKLIGHT_Pin << 16U) ; //  LTDC Back Light On
   }

    if (Tm10>24)
       {
         Tm10 = 0   ;
         Global_Tm++;   // 1/40 Sec
         if (Global_KeyBeep)
            {
              Global_KeyBeep--;
              if (Global_KeyBeep == 0) GPIOE->BSRR = (GPIO_PIN_3<<16);  // PIN SET
            }

//         Intr_overflow();
       }
  }

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32H7xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32h7xx.s).                    */
/******************************************************************************/

/* USER CODE BEGIN 1 */

// -----------------------------
// ****    T I M E R   3    ****
// -----------------------------
void TIM3_IRQHandler(void)
{
 static uint8_t Zig ;
  __HAL_TIM_CLEAR_IT(&TimHandle3, TIM_IT_UPDATE);
  Zig++;
  if (Global_KeyBeep)
     {
       if (Zig & 0x01) GPIOE->BSRR = GPIO_PIN_3 ;  // PIN SET
                  else GPIOE->BSRR = (GPIO_PIN_3<<16) ;  // PIN RESET
     }
//  if (Global_KeyBeep) GPIOE->BSRRL = GPIO_PIN_3 ;  // PIN SET
//                 else GPIOE->BSRRH = GPIO_PIN_3 ;  // PIN RESET
}

// -----------------------------
// ****      Q S P I       ****
// -----------------------------
extern QSPI_HandleTypeDef QSPIHandle;
void QUADSPI_IRQHandler(void)
{
  HAL_QSPI_IRQHandler(&QSPIHandle);
}


void MDMA_IRQHandler(void)
{
  /* Check the interrupt and clear flag */
  HAL_MDMA_IRQHandler(QSPIHandle.hmdma);
}
// -----------------------------

/******************************************************************************/
/*                 STM32H7xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32h7xx.s).                                               */
/******************************************************************************/
/**
  * @brief  This function handles Ethernet interrupt request.
  * @param  None
  * @retval None
  */
void ETH_IRQHandler(void)
{
  HAL_ETH_IRQHandler(&EthHandle);
}

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
