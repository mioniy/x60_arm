

#include "lwip/opt.h"
#include "lwip/api.h"
#include "lwip/sys.h"
#include "port_stm.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UDPECHO_THREAD_PRIO  (osPriorityAboveNormal)

/* Private macro -------------------------------------------------------------*/
/* EXTERN variables ---------------------------------------------------------*/
extern uint32_t   Global_UDPip1          ;
extern uint16_t   Global_UDPport1        ;
extern uint32_t   Global_UDPip2          ;
extern uint16_t   Global_UDPport2        ;
/* Private function prototypes -----------------------------------------------*/
void RS485_Rx(uint8_t Data );
/* Private functions ---------------------------------------------------------*/

static struct    netconn *UdpConn1  ;
static struct    netconn *UdpConn2  ;

void                          LanAPI_Rx_IP_Packet( byte *pBuf, usint Len);


static void udpecho1_thread(void *arg)
{
static uint16_t  ipoi              ;
static char     *pdata             ;
static void     *data              ;
static u16_t     len               ;
static struct    netbuf *buf       ;
static err_t     UdpErr            ;
  LWIP_UNUSED_ARG(arg);
  UdpConn1 = netconn_new(NETCONN_UDP);
  netconn_bind(UdpConn1, IP_ADDR_ANY, Global_SetUpDeviceUdpPort /* was 1001 */);
  while (1)
  {
    UdpErr = netconn_recv(UdpConn1, &buf);
    if (UdpErr == ERR_OK)
    {
      Global_UDPip1    = *(uint32_t *)&buf->addr ;
      Global_UDPport1  =  buf->port              ;
      data             =  buf->p->payload        ;
      if (netbuf_data(buf, &data, &len)==ERR_OK)
         {
           do {
                 pdata = (char *)data ;
                 ipoi = 0 ;
                 while (ipoi<len) // && (ipoi<255))
                 {
//                   RS485_Rx(pdata[ipoi]) ;
                   ipoi++;
                 }
               } while (netbuf_next(buf) >= 0);
LanAPI_Rx_IP_Packet( (byte *)pdata, len);               
           netbuf_delete(buf);
         }
    }
  }
}

void ReplayUdpRs485(uint8_t *p , uint16_t Len)
{
  struct netbuf *tx_buf;
  if (Len<0x400) // && (Global_UDPip1) && (Global_UDPport1))
  {
    tx_buf = netbuf_new();
    if (tx_buf)
       {
         if (netbuf_alloc(tx_buf, Len) != NULL)
            {
               pbuf_take(tx_buf->p, (const void *)p, Len);
             Global_UDPip1 = 0xffffffff ;
               Global_UDPport1 = Global_SetUpDeviceUdpPort /* was 1001 */ ;
               netconn_sendto(UdpConn1, tx_buf, (const ip_addr_t *)&(Global_UDPip1) , Global_UDPport1);
            }
         netbuf_delete(tx_buf);
       }
  }
}


/*-----------------------------------------------------------------------------------*/
void udpecho1_init(void)
{
  sys_thread_new("udpecho1_thread", udpecho1_thread, NULL, (configMINIMAL_STACK_SIZE*4), UDPECHO_THREAD_PRIO);
}



/*-----------------------------------------------------------------------------------*/

void udpecho_init(void)
{
   udpecho1_init();
}
