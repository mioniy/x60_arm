#if 0
================================================================================
0x000000000000000000000000          UART_BSP          0x000000000000000000000000
================================================================================
#endif
#pragma pack(1)
/*------------------------------ Include Files -------------------------------*/
#include "main.h"
#include "port_stm.h"    // main.h
#include "UartDRV.h"

//#pragma pack(1)
/*------------------------ Precompilation Definitions ------------------------*/
#define NUMBER_OF_COMS	(8 + 1)						// COMs starts from 1 (COM_1 == 1)

/*---------------------------- Typedef Directives ----------------------------*/
enum
{
	UartBSP_UART_1 = 0,
	UartBSP_UART_2,
	UartBSP_UART_3,
	UartBSP_UART_4,
	UartBSP_UART_5,
	UartBSP_UART_6,
	UartBSP_UART_7,
	UartBSP_UART_8,
	UartBSP_MAX_UARTS
};


/*---------------------------- External Variables ----------------------------*/

/*----------------------------- Global Variables -----------------------------*/
UART_HandleTypeDef*	UARTs_Coms_Array[NUMBER_OF_COMS];

UART_HandleTypeDef*	UARTs_Coms_Handles[UartBSP_MAX_UARTS];

/*
  	Each UART/USART has a base address for it's set of registers (rx reg, tx, baud rate regs etc.).
	The below array is holding UARTs bases of all 8 UARTs.
*/
USART_TypeDef*	UARTs_UART_BASE_Array[UartBSP_MAX_UARTS];

//----------------- CHANGE HERE IF YOU NEED TO...
//=====================================================================================================================================================================================
//                                               COM_1           COM_2           COM_3           COM_4           COM5            COM_6           COM_7           COM_8 = RS485
#ifdef   __USES_BRD3__
const byte UartBSP_COM_UART_LINKAGE[] = { 0, UartBSP_UART_1, UartBSP_UART_2, UartBSP_UART_3, UartBSP_UART_4, UartBSP_UART_5, UartBSP_UART_6, UartBSP_UART_7, UartBSP_UART_8};
#else
const byte UartBSP_COM_UART_LINKAGE[] = { 0, UartBSP_UART_8, UartBSP_UART_6, UartBSP_UART_5, UartBSP_UART_4, UartBSP_UART_7, UartBSP_UART_7, UartBSP_UART_7, UartBSP_UART_2};
#endif
//=====================================================================================================================================================================================


bool	UARTs_fTxModeForUARTs[ UartBSP_MAX_UARTS ];											// index range: 0 .. 7    ( UartBSP_UART_1 .. UartBSP_UART_8 )
bool	UARTs_fRS485_Active  [ UartBSP_MAX_UARTS ];

//==================== UARTs data array =======================
typedef struct
{
  	byte					RxData;
	void					(*pCOM_TxFunc)( void);
	void					(*pCOM_RxFunc)( void);
	void					(*Uart_TxByte)( byte);
	void					(*Uart_TxEnd)( void);
	byte					(*Uart_RxByte)( void);
} tUartBSP_PortInstance;

typedef struct
{
	tUartBSP_PortInstance		UartBSP_PortsArray[UartBSP_MAX_UARTS];
} tUartBSP_DataArray;

tUartBSP_DataArray	sUartBSP_DataArray;
//=============================================================

/*------------------------ Local Function Prototypes -------------------------*/
byte                    		UartBSP_SetInstance( UART_HandleTypeDef* uart, byte port);
uint32_t								UartBSP_SetBaudRate( tUart *pUart);
uint32_t								UartBSP_SetWordLength( tUart *pUart);
uint32_t								UartBSP_SetStopBits( tUart *pUart);
uint32_t								UartBSP_SetParity( tUart *pUart);
void 									UartBSP_RS485_ModeRx(void);

void									UartBSP_UART1_TxByte( byte TxData);
void									UartBSP_UART1_TxEnd( void);
byte									UartBSP_UART1_RxByte( void);

void									UartBSP_UART2_TxByte( byte TxData);
void									UartBSP_UART2_TxEnd( void);
byte									UartBSP_UART2_RxByte( void);

void									UartBSP_UART3_TxByte( byte TxData);
void									UartBSP_UART3_TxEnd( void);
byte									UartBSP_UART3_RxByte( void);

void									UartBSP_UART4_TxByte( byte TxData);
void									UartBSP_UART4_TxEnd( void);
byte									UartBSP_UART4_RxByte( void);

void									UartBSP_UART5_TxByte( byte TxData);
void									UartBSP_UART5_TxEnd( void);
byte									UartBSP_UART5_RxByte( void);

void									UartBSP_UART6_TxByte( byte TxData);
void									UartBSP_UART6_TxEnd( void);
byte									UartBSP_UART6_RxByte( void);

void									UartBSP_UART7_TxByte( byte TxData);
void									UartBSP_UART7_TxEnd( void);
byte									UartBSP_UART7_RxByte( void);

void									UartBSP_UART8_TxByte( byte TxData);
void									UartBSP_UART8_TxEnd( void);
byte									UartBSP_UART8_RxByte( void);

void									UartBSP_EnableTx(byte Uart);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

/*----------------------------------------------------------------------------*/
void                    		UartBSP_Init( void)
/*----------------------------------------------------------------------------*/
{
  	byte index;

	// Load UARTs Handles into single array of Handles
	//----------------- DO NOT CHANGE
	UARTs_Coms_Handles[UartBSP_UART_1] = &Global_UartHandle1;
	UARTs_Coms_Handles[UartBSP_UART_2] = &Global_UartHandle2;
	UARTs_Coms_Handles[UartBSP_UART_3] = &Global_UartHandle3;
	UARTs_Coms_Handles[UartBSP_UART_4] = &Global_UartHandle4;
	UARTs_Coms_Handles[UartBSP_UART_5] = &Global_UartHandle5;
	UARTs_Coms_Handles[UartBSP_UART_6] = &Global_UartHandle6;
	UARTs_Coms_Handles[UartBSP_UART_7] = &Global_UartHandle7;							// At the moment, BSP by default, doesn't declare a Global_UartHandle7
	UARTs_Coms_Handles[UartBSP_UART_8] = &Global_UartHandle8;
	
	//======================================================================================================
	// Place the right UART Handle in a COM oriented Handle. e.g: COM_1 handles gets the UART8 handle.
	//----------------- DO NOT CHANGE
	UARTs_Coms_Array[COM_1] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_1] ];
	UARTs_Coms_Array[COM_2] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_2] ];
	UARTs_Coms_Array[COM_3] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_3] ];
	UARTs_Coms_Array[COM_4] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_4] ];
	UARTs_Coms_Array[COM_5] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_5] ];
	UARTs_Coms_Array[COM_6] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_6] ];
	UARTs_Coms_Array[COM_7] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_7] ];
	UARTs_Coms_Array[COM_8] = UARTs_Coms_Handles[ UartBSP_COM_UART_LINKAGE[COM_8] ];		// RS485 on BOARD - 2 == COM_8   T.B.D

	// Load an array of UARTs and USARTs Registers BASE addresses. Do Not Change.
	//----------------- DO NOT CHANGE
	UARTs_UART_BASE_Array[UartBSP_UART_1] = USART1;
	UARTs_UART_BASE_Array[UartBSP_UART_2] = USART2;
	UARTs_UART_BASE_Array[UartBSP_UART_3] = USART3;
	UARTs_UART_BASE_Array[UartBSP_UART_4] = UART4;
	UARTs_UART_BASE_Array[UartBSP_UART_5] = UART5;
	UARTs_UART_BASE_Array[UartBSP_UART_6] = USART6;
	UARTs_UART_BASE_Array[UartBSP_UART_7] = UART7;
	UARTs_UART_BASE_Array[UartBSP_UART_8] = UART8;

	//-----------------
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].Uart_TxByte	= UartBSP_UART1_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].Uart_RxByte = UartBSP_UART1_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].Uart_TxEnd  = UartBSP_UART1_TxEnd;

	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].Uart_TxByte	= UartBSP_UART2_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].Uart_RxByte = UartBSP_UART2_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].Uart_TxEnd  = UartBSP_UART2_TxEnd;

	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].Uart_TxByte	= UartBSP_UART3_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].Uart_RxByte = UartBSP_UART3_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].Uart_TxEnd  = UartBSP_UART3_TxEnd;
	
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].Uart_TxByte	= UartBSP_UART4_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].Uart_RxByte = UartBSP_UART4_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].Uart_TxEnd  = UartBSP_UART4_TxEnd;
	
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].Uart_TxByte	= UartBSP_UART5_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].Uart_RxByte = UartBSP_UART5_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].Uart_TxEnd  = UartBSP_UART5_TxEnd;
	
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].Uart_TxByte	= UartBSP_UART6_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].Uart_RxByte = UartBSP_UART6_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].Uart_TxEnd  = UartBSP_UART6_TxEnd;
	
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].Uart_TxByte	= UartBSP_UART7_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].Uart_RxByte = UartBSP_UART7_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].Uart_TxEnd  = UartBSP_UART7_TxEnd;
	
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].Uart_TxByte	= UartBSP_UART8_TxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].Uart_RxByte = UartBSP_UART8_RxByte;
	sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].Uart_TxEnd  = UartBSP_UART8_TxEnd;
	//----------------
	
	//======================================================================================================
		
	for ( index = UartBSP_UART_1; index <= UartBSP_UART_8; index++ )
	{
	  	UARTs_fTxModeForUARTs[index] = FALSE;													// All UARTs are not in Tx mode.
		UARTs_fRS485_Active  [index] = FALSE;
	  
		__HAL_UART_DISABLE_IT( UARTs_Coms_Handles[index], UART_IT_RXNE);
		__HAL_UART_DISABLE_IT( UARTs_Coms_Handles[index], UART_IT_TC );
	  
		// All ISR functions to be NULL
		sUartBSP_DataArray.UartBSP_PortsArray[index].pCOM_RxFunc = NULL;
		sUartBSP_DataArray.UartBSP_PortsArray[index].pCOM_TxFunc = NULL;	  
	}

}

/*
*
*  UartBSP_SetPort 
* Get application UART settings (tUart) and configure UART BSP.
*
*/
/*----------------------------------------------------------------------------*/
void                    		UartBSP_SetPort( tUart  *pUart)
/*----------------------------------------------------------------------------*/
{
	UART_HandleTypeDef* selected_uart;
	byte		sel_uart_index;
	
	selected_uart = UARTs_Coms_Array[ pUart->Com.Port ];									// Choose UART by the Application chosen COM
	sel_uart_index = UartBSP_COM_UART_LINKAGE[pUart->Com.Port];							// Get the UART index for this selected COM. For example: COM_1 <- "UART8"
	selected_uart->Instance = UARTs_UART_BASE_Array[ sel_uart_index ];				// selected_uart->Instance = UARTx  (USART1, USART2, USART3, UART4, UART5, USART6, UART7, UART8 )
	
	selected_uart->Init.BaudRate 			= UartBSP_SetBaudRate( pUart);				// set baud rate
	selected_uart->Init.WordLength 		= UartBSP_SetWordLength( pUart);				// set Word Bits (number of data bits: 8, 7...)
	selected_uart->Init.StopBits 			= UartBSP_SetStopBits( pUart);				// set number of stop bits
	selected_uart->Init.Parity 			= UartBSP_SetParity( pUart);					// set Parity moode
	selected_uart->Init.Mode 				= UART_MODE_TX_RX;
	selected_uart->Init.HwFlowCtl 		= UART_HWCONTROL_NONE;
	selected_uart->Init.OverSampling 	= UART_OVERSAMPLING_16;
	selected_uart->Init.OneBitSampling 	= UART_ONE_BIT_SAMPLE_DISABLE;
	selected_uart->AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init( selected_uart /*&Global_UartHandle5*/) != HAL_OK)
	{
		BR_Error_Handler(0x40);
	}
	
	sUartBSP_DataArray.UartBSP_PortsArray[sel_uart_index].pCOM_TxFunc = pUart->TxFunc;
	sUartBSP_DataArray.UartBSP_PortsArray[sel_uart_index].pCOM_RxFunc = pUart->RxFunc;
	
	pUart->Uart_TxByte	= sUartBSP_DataArray.UartBSP_PortsArray[sel_uart_index].Uart_TxByte;
	pUart->Uart_TxEnd		= sUartBSP_DataArray.UartBSP_PortsArray[sel_uart_index].Uart_TxEnd;
	pUart->Uart_RxByte	= sUartBSP_DataArray.UartBSP_PortsArray[sel_uart_index].Uart_RxByte;
	
	__HAL_UART_ENABLE_IT( selected_uart , UART_IT_RXNE);

	UARTs_fRS485_Active[ sel_uart_index] = FALSE;
	if ( pUart->Com.RS485_Active == 1 )
	{
		__HAL_UART_DISABLE_IT( selected_uart , UART_IT_TC );
		__HAL_UART_ENABLE_IT ( selected_uart , UART_IT_RXNE );
		UartBSP_RS485_ModeRx();
		UARTs_fRS485_Active[ sel_uart_index] = TRUE;
	}
}



/*==============================================================================
/
/                                 UART 1
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	USART1_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle1, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle1, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].pCOM_TxFunc();
         else
            UartBSP_UART1_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle1, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle1, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].RxData = (uint8_t)(Global_UartHandle1.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle1, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}



/*----------------------------------------------------------------------------*/
void					UartBSP_UART1_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle1.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_1);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART1_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
	if ( UARTs_fRS485_Active[ UartBSP_UART_1] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}

	UARTs_fTxModeForUARTs[ UartBSP_UART_1]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle1 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle1 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART1_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_1].RxData;
}



/*==============================================================================
/
/                                 UART 2
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	USART2_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].pCOM_TxFunc();
         else
            UartBSP_UART2_TxEnd();
		}
	}
	
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle2, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle2, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].RxData = (uint8_t)(Global_UartHandle2.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle2, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}


/*----------------------------------------------------------------------------*/
void					UartBSP_ClearUDPip( void)
/*----------------------------------------------------------------------------*/
{
	Global_UDPip1   = 0 ;
	Global_UDPport1 = 0 ;
}

/*----------------------------------------------------------------------------*/
void 					UartBSP_RS485_ModeRx(void)
/*----------------------------------------------------------------------------*/
{
	//HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_RESET);
	//Global_TXRX_485 = 0 ;
}

/*----------------------------------------------------------------------------*/
void 					UartBSP_RS485_ModeTx(void)
/*----------------------------------------------------------------------------*/
{
	//HAL_GPIO_WritePin(GPIOD , GPIO_PIN_4 , GPIO_PIN_SET);
	//Global_TXRX_485 = 1 ;
}



/*----------------------------------------------------------------------------*/
byte					UartBSP_RS485_GetMode(void)
/*----------------------------------------------------------------------------*/
{
	return 0; //Global_TXRX_485;		// 1 = Tx mode    0 = Rx mode
}


/*----------------------------------------------------------------------------*/
void					UartBSP_UART2_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle2.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_2);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART2_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_2] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}
	
	UARTs_fTxModeForUARTs[ UartBSP_UART_2]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle2 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle2 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART2_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_2].RxData;
}


/*==============================================================================
/
/                                 UART 3
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	USART3_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle3, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle3, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].pCOM_TxFunc();
         else
            UartBSP_UART3_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle3, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle3, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].RxData = (uint8_t)(Global_UartHandle3.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle3, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}



/*----------------------------------------------------------------------------*/
void					UartBSP_UART3_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle3.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_3);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART3_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_3] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}

	UARTs_fTxModeForUARTs[ UartBSP_UART_3]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle3 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle3 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART3_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_3].RxData;
}



/*==============================================================================
/
/                                 UART 4
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	UART4_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle4, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].pCOM_TxFunc();
         else
            UartBSP_UART4_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle4, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle4, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].RxData = (uint8_t)(Global_UartHandle4.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle4, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}


/*----------------------------------------------------------------------------*/
void					UartBSP_UART4_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle4.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_4);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART4_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_4] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}

	UARTs_fTxModeForUARTs[ UartBSP_UART_4]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle4 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle4 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART4_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_4].RxData;
}




/*==============================================================================
/
/                                 UART 5
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	UART5_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle5, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle5, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].pCOM_TxFunc();
         else
            UartBSP_UART5_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle5, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle5, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].RxData = (uint8_t)(Global_UartHandle5.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle5, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}


/*----------------------------------------------------------------------------*/
void					UartBSP_UART5_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle5.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_5);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART5_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_5] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}

	UARTs_fTxModeForUARTs[ UartBSP_UART_5]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle5 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle5 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART5_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_5].RxData;
}



/*==============================================================================
/
/                                 UART 6
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	USART6_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle6, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle6, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].pCOM_TxFunc();
         else
            UartBSP_UART6_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle6, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle6, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].RxData = (uint8_t)(Global_UartHandle6.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle6, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}


/*----------------------------------------------------------------------------*/
void					UartBSP_UART6_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle6.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_6);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART6_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_6] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}

	UARTs_fTxModeForUARTs[ UartBSP_UART_6]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle6 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle6 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART6_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_6].RxData;
}



/*==============================================================================
/
/                                 UART 7
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	UART7_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
#if 0  //   Global_UartHandle7 was not created
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle7, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle7, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].pCOM_TxFunc();
         else
            UartBSP_UART7_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle7, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle7, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].RxData = (uint8_t)(Global_UartHandle7.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle7, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
#endif
}


/*----------------------------------------------------------------------------*/
void					UartBSP_UART7_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	//Global_UartHandle7.Instance->TDR = TxData;
  	//UartBSP_EnableTx(UartBSP_UART_7);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART7_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_7] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}
  
	//UARTs_fTxModeForUARTs[ UartBSP_UART_7]	= FALSE;
	//__HAL_UART_DISABLE_IT( &Global_UartHandle7 , UART_IT_TC );
	//__HAL_UART_ENABLE_IT ( &Global_UartHandle7 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART7_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_7].RxData;
}



/*==============================================================================
/
/                                 UART 8
/
/==============================================================================*/
/*----------------------------------------------------------------------------*/
void	UART8_IRQHandler(void)		// WARNING:  DO NOT CHANGE THIS FUNCTION'S NAME !!!
/*----------------------------------------------------------------------------*/
{
	//=== Tx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle8, UART_IT_TC))
	{
		if ((__HAL_UART_GET_IT(&Global_UartHandle8, UART_IT_TC)) /*&& (Global_TXRX_485)*/)
		{
		  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].pCOM_TxFunc != NULL )
		  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].pCOM_TxFunc();
         else
            UartBSP_UART8_TxEnd();
		}
	}
	//=== Rx ===
	if (__HAL_UART_GET_IT_SOURCE(&Global_UartHandle8, UART_IT_RXNE))
	{
		if (__HAL_UART_GET_IT(&Global_UartHandle8, UART_IT_RXNE))
		{
			sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].RxData = (uint8_t)(Global_UartHandle8.Instance->RDR & (uint8_t)0x00FF);		// Get data from Rx data register
			__HAL_UART_CLEAR_FLAG(&Global_UartHandle8, UART_CLEAR_OREF | UART_CLEAR_NEF | UART_CLEAR_PEF | UART_CLEAR_FEF);
			if (Global_SysReady)																		// Stm32MainRunTask() is setting that flag once.
			{
			  	if ( sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].pCOM_RxFunc != NULL )
			  		sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].pCOM_RxFunc();	// Call Rx ISR
			}
		}
	}
}



/*----------------------------------------------------------------------------*/
void					UartBSP_UART8_TxByte( byte TxData)
/*----------------------------------------------------------------------------*/
{
 	Global_UartHandle8.Instance->TDR = TxData;
	UartBSP_EnableTx(UartBSP_UART_8);
}

/*----------------------------------------------------------------------------*/
void					UartBSP_UART8_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
  	if ( UARTs_fRS485_Active[ UartBSP_UART_8] == TRUE )
	{
		UartBSP_RS485_ModeRx();
	}
  
  	UARTs_fTxModeForUARTs[ UartBSP_UART_8]	= FALSE;
	__HAL_UART_DISABLE_IT( &Global_UartHandle8 , UART_IT_TC );
	__HAL_UART_ENABLE_IT ( &Global_UartHandle8 , UART_IT_RXNE );
}

/*----------------------------------------------------------------------------*/
byte					UartBSP_UART8_RxByte( void)
/*----------------------------------------------------------------------------*/
{
  	return sUartBSP_DataArray.UartBSP_PortsArray[UartBSP_UART_8].RxData;
}

//=========================== General Comms functions ==========================

/*----------------------------------------------------------------------------*/
void					UartBSP_StartTx(tUart  *pUart)
/*----------------------------------------------------------------------------*/
{
   __HAL_UART_DISABLE_IT( UARTs_Coms_Array[pUart->Com.Port] , UART_IT_RXNE );	
   __HAL_UART_ENABLE_IT ( UARTs_Coms_Array[pUart->Com.Port] , UART_IT_TC   );
}



/*----------------------------------------------------------------------------*/
void					UartBSP_EnableTx(byte Uart)
/*----------------------------------------------------------------------------*/
{
	if ( UARTs_fTxModeForUARTs[ Uart] == FALSE )
	{
	  	UARTs_fTxModeForUARTs[ Uart] = TRUE;
		
		__HAL_UART_DISABLE_IT( UARTs_Coms_Handles[ Uart] , UART_IT_RXNE );
		__HAL_UART_ENABLE_IT ( UARTs_Coms_Handles[ Uart] , UART_IT_TC   );
	}
}

bool                          Debug_IsActive( void);

/*----------------------------------------------------------------------------*/
void UartBSP_putchar( uint8_t tx_data, byte comm)
/*----------------------------------------------------------------------------*/
{
  	byte uart_index;
  	uart_index = UartBSP_COM_UART_LINKAGE[ comm];
	
        if( Debug_IsActive() == FALSE)
            return;
        
  	while ((__HAL_UART_GET_IT(UARTs_Coms_Handles[uart_index], UART_IT_TC)) == 0);
		UARTs_Coms_Handles[uart_index]->Instance->TDR = 0x00FF & (uint16_t)tx_data ;
}

/*----------------------------------------------------------------------------*/
UART_HandleTypeDef   *UartBSP_GetComHandler( byte comm)
/*----------------------------------------------------------------------------*/
{
   if( comm >= UartBSP_MAX_UARTS)
      return( NULL);

   return( UARTs_Coms_Handles[comm]);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

/*----------------------------------------------------------------------------*/
byte					UartBSP_SetInstance( UART_HandleTypeDef* uart, byte port)
/*----------------------------------------------------------------------------*/
{
  	byte 	uart_index;																					// return the caller an UART index for the selected COM
  	/*
	* Resources:
	* USART1, USART2, USART3, UART4, UART5, USART6, UART8.
	* Notice hardware names:  UART, USART !
	*/
	switch(port)
	{
		case COM_1:
			uart->Instance = UART8;
			uart_index = UartBSP_UART_8;
	  		break;
			
		case COM_2:
	  		uart->Instance = USART6;
			uart_index = UartBSP_UART_6;
	  		break;
			
		case COM_3:
	  		uart->Instance = UART5;
			uart_index = UartBSP_UART_5;
	  		break;
			
		case COM_4:
	  		uart->Instance = UART4;
			uart_index = UartBSP_UART_4;
	  		break;
#if 0			
		case COM_5:
	  		uart->Instance = T.B.D;
			uart_index = UartBSP_UART_T.B.D;
	  		break;
			
		case COM_6:
	  		uart->Instance = T.B.D;
			uart_index = UartBSP_UART_T.B.D;
	  		break;
			
		case COM_7:
	  		uart->Instance = T.B.D;
			uart_index = UartBSP_UART_T.B.D;
	  		break;
#endif	
		case COM_8:
	  		uart->Instance = USART2;
			uart_index = UartBSP_UART_2;
	  		break;
			
		default:
	  		uart_index = 0xFF;				// Unknown
		  	break;
	}
	
	return uart_index;
}


/*----------------------------------------------------------------------------*/
uint32_t								UartBSP_SetBaudRate( tUart *pUart)
/*----------------------------------------------------------------------------*/
{
  	uint32_t	result;
	
  	switch( pUart->Com.BaudRate)
	{
		case	BAUD_RATE_1200:
			 	result	= 1200;
				break;
			 
		case	BAUD_RATE_2400:
				result	= 2400;
				break;
				
		case	BAUD_RATE_9600:
				result	= 9600;
				break;
				  
		case	BAUD_RATE_19200:
				result	= 19200;
				break;
					 
		case	BAUD_RATE_38400:
				result	= 38400;
				break;
						
		case	BAUD_RATE_57600:
				result	= 57600;
				break;
						  
		case	BAUD_RATE_115200:
				result	= 115200;
				break;
                                
		case	BAUD_RATE_128000:
				result	= 128000;
				break;

		case	BAUD_RATE_256000:
				result	= 256000;
				break;
                                
	}
	return result;
}




/*----------------------------------------------------------------------------*/
uint32_t								UartBSP_SetWordLength( tUart *pUart)
/*----------------------------------------------------------------------------*/
{
  	uint32_t	result;
	
  	switch( pUart->Com.DataBits)
	{
	  	case	DATA_BITS_8:
		 	result = UART_WORDLENGTH_8B;
			break;
			
	  	case	DATA_BITS_7:
		 	result = UART_WORDLENGTH_7B;
			break;
			
	  	case	DATA_BITS_6:
		 	result = 0xFFFFFFFF;																		// NOT SUPPORTED
			break;
			
	  	case	DATA_BITS_5:
		 	result = 0xFFFFFFFF;																		// NOT SUPPORTED
			break;			
	}
	return result;
}





/*----------------------------------------------------------------------------*/
uint32_t								UartBSP_SetStopBits( tUart *pUart)
/*----------------------------------------------------------------------------*/
{
  	uint32_t	result;
	
  	switch( pUart->Com.StopBits)
	{
		case STOP_BITS_1:
	  		result = UART_STOPBITS_1;
			break;
			
		case	STOP_BITS_2:
			result = UART_STOPBITS_2;
			break;
	}
	return result;
}



/*----------------------------------------------------------------------------*/
uint32_t								UartBSP_SetParity( tUart *pUart)
/*----------------------------------------------------------------------------*/
{
  	uint32_t	result;  
	
  	switch( pUart->Com.Parity)
	{
	  	case	PARITY_NONE:
		 	result = UART_PARITY_NONE;
		 	break;

	  	case	PARITY_ODD:
		 	result = UART_PARITY_ODD;
		 	break;
			
	  	case	PARITY_EVEN:
		 	result = UART_PARITY_EVEN;
		 	break;			
	}
  
	return result;
}





