

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"

#define TCP_THREAD_PRIO  (osPriorityAboveNormal) // (osPriorityRealtime)   // (osPriorityAboveNormal)

static struct    netconn *TcpConnRM, *TcpNewconnRM ;
static err_t     TcpErrRM                        ;
static uint16_t  TcpIpoiRM                        ;
static char     *TcpPdataRM                     ;

void HostTcpRx(uint8_t data);

static void tcp_REMOTE(void *arg)
{
  LWIP_UNUSED_ARG(arg);
  /* Create a new connection identifier. */
  /* Bind connection to well known port number 7. */
  TcpConnRM = netconn_new(NETCONN_TCP);
  netconn_bind(TcpConnRM, IP_ADDR_ANY, 7);

  LWIP_ERROR("tcpecho: invalid TcpConnRM", (TcpConnRM != NULL), return;);

  /* Tell connection to go into listening mode. */
  netconn_listen(TcpConnRM);


  while (1)
  {
    TcpErrRM = netconn_accept(TcpConnRM, &TcpNewconnRM);
    if (TcpErrRM == ERR_OK)
    {
      static struct netbuf *bufRM;
      static void *dataRM;
      u16_t len;
      netconn_set_recvtimeout(TcpNewconnRM , 30000);
      while ((TcpErrRM = netconn_recv(TcpNewconnRM, &bufRM)) == ERR_OK)
      {
        do {
              netbuf_data(bufRM, &dataRM, &len);
              TcpPdataRM = (char *)dataRM ;
              TcpIpoiRM = 0 ;
              while (TcpIpoiRM<len) //  && (TcpIpoiRM<255))
              {
                HostTcpRx(TcpPdataRM[TcpIpoiRM]) ;
                TcpIpoiRM++;
              }
           } while (netbuf_next(bufRM) >= 0) ;
        netbuf_delete(bufRM);
        HostTcpRx(0x0d) ;
      }
      netconn_close(TcpNewconnRM);
      netconn_delete(TcpNewconnRM);
    }
  }
}

void ReplayRemoteTcp(uint8_t *p , uint16_t Len)
{
 void *dataRMrply;
  if (TcpNewconnRM != NULL)
     {
       dataRMrply = (void *)p ;
       TcpErrRM = netconn_write(TcpNewconnRM, dataRMrply, Len, NETCONN_COPY);
     }
}

/*-----------------------------------------------------------------------------------*/
void RemoteIP_init(void)
{
  sys_thread_new("tcp_REMOTE", tcp_REMOTE, NULL, (configMINIMAL_STACK_SIZE*4), TCP_THREAD_PRIO);
}
/*-----------------------------------------------------------------------------------*/


