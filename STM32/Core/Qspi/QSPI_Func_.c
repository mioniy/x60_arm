


#include "main.h"
#include "QSPI_Func.h"

#define QSPI_STATE_IDLE           0
#define QSPI_STATE_READY          1
#define QSPI_STATE_ERASE_SEC      2
#define QSPI_STATE_ERASE_POLL     3
#define QSPI_READ_WAIT1           4
#define QSPI_WRITE_WAIT1          5
#define QSPI_WRITE_WAIT2         6
#define QSPI_FOUR_ADDRESS_POLL   7

#define Q_TIMEOUT_IDLE           20    // 2 Sec timeout

#define QSPI_NO_ERROR             0
#define QSPI_ERROR_INIT           1
#define QSPI_ERROR_IT             2
#define QSPI_ERROR_COMMAND        3
#define QSPI_ERROR_POLLING        4
#define QSPI_ERROR_AUTO_POLLING   5
#define QSPI_ERROR_TRANSMIT       6
#define QSPI_ERROR_RECEIVE        7
#define QSPI_ERROR_READ1          8
#define QSPI_ERROR_READ2          9
#define QSPI_ERROR_WRITE1        10
#define QSPI_ERROR_WRITE2       11

QSPI_HandleTypeDef   QSPIHandle ;
QSPI_CommandTypeDef  sCommand   ;

__IO uint8_t CmdCplt, RxCplt, TxCplt, StatusMatch, TimeOut;
static uint8_t  QspiState    ;
static uint16_t SectorX  = 0 ;
static uint8_t  QspiError = QSPI_NO_ERROR ;
static uint8_t  TmQ      = 0 ;

static uint32_t QspiStartAddress;
static uint8_t  *QspiBfrPtr      ;
static uint32_t QspiLen          ;

static uint8_t  RunS             ;
static uint16_t FromSector , ToSector ;


#define TST_SIZE    0x1000
static uint8_t  TstBfr[TST_SIZE] ;
extern const uint8_t __aud[];

void Error_Qspi(uint8_t Error);

static uint8_t QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi, uint32_t Timeout);
static uint8_t QSPI_AutoPollingMemReadyIT(QSPI_HandleTypeDef *hqspi, uint32_t Timeout);
static uint8_t QSPI_EnterFourBytesAddress(QSPI_HandleTypeDef *hqspi);
static uint8_t QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi);

static uint8_t QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi);

// ---------------------------------------------------------------------------------
/**
  * @brief  This function send a Write Enable and wait it is effective.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef     s_command;
  QSPI_AutoPollingTypeDef s_config;

  /* Enable write operations */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = WRITE_ENABLE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Configure automatic polling mode to wait for write enabling */
  s_config.Match           = MT25QL512_SR_WREN;
  s_config.Mask            = MT25QL512_SR_WREN;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND;
  s_config.StatusBytesSize = 1;
  s_config.Interval        = 0x10;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  s_command.Instruction    = READ_STATUS_REG_CMD;
  s_command.DataMode       = QSPI_DATA_1_LINE;

  if (HAL_QSPI_AutoPolling(&QSPIHandle, &s_command, &s_config, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}


/**
  * @brief  This function read the SR of the memory and wait the EOP.
  * @param  hqspi: QSPI handle
  * @param  Timeout: timeout value
  * @retval None
  */
static uint8_t QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi, uint32_t Timeout)
{
  QSPI_CommandTypeDef     s_command;
  QSPI_AutoPollingTypeDef s_config;

  /* Configure automatic polling mode to wait for memory ready */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = READ_STATUS_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  s_config.Match           = 0;
  s_config.Mask            = MT25QL512_SR_WIP    ;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND ;
  s_config.StatusBytesSize = 1                  ;
  s_config.Interval        = 0x10               ;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling(&QSPIHandle, &s_command, &s_config, Timeout) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}

/**
  * @brief  This function read the SR of the memory and wait the EOP.
  * @param  hqspi: QSPI handle
  * @param  Timeout: timeout value
  * @retval None
  */
static uint8_t QSPI_AutoPollingMemReadyIT(QSPI_HandleTypeDef *hqspi, uint32_t Timeout)
{
  QSPI_CommandTypeDef     s_command;
  QSPI_AutoPollingTypeDef s_config;

  /* Configure automatic polling mode to wait for memory ready */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = READ_STATUS_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  s_config.Match           = 0;
  s_config.Mask            = MT25QL512_SR_WIP    ;
  s_config.MatchMode       = QSPI_MATCH_MODE_AND ;
  s_config.StatusBytesSize = 1                  ;
  s_config.Interval        = 0x10               ;
  s_config.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling_IT(&QSPIHandle, &s_command, &s_config) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}



/**
  * @brief  This function reset the QSPI memory.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_ResetMemory(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the reset enable command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = RESET_ENABLE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Send the command */
  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Send the reset memory command */
  s_command.Instruction = RESET_MEMORY_CMD;
  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Configure automatic polling mode to wait the memory is ready */
  if (QSPI_AutoPollingMemReady(&QSPIHandle, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != QSPI_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}

/**
  * @brief  This function set the QSPI memory in 4-byte address mode
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_EnterFourBytesAddress(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef s_command;

  /* Initialize the command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = ENTER_4_BYTE_ADDR_MODE_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_NONE;
  s_command.DummyCycles       = 0;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Enable write operations */
  if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_ERROR;
  }

  /* Send the command */
  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Configure automatic polling mode to wait the memory is ready */
  if (QSPI_AutoPollingMemReady(&QSPIHandle, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != QSPI_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}

static uint8_t QSPI_Init(void)
{
  /* Initialize QuadSPI ------------------------------------------------------ */
  QspiState           = QSPI_STATE_IDLE ;
  QSPIHandle.Instance = QUADSPI;
  HAL_QSPI_DeInit(&QSPIHandle);

  QSPIHandle.Init.ClockPrescaler     = 1 ;
  QSPIHandle.Init.FifoThreshold      = 4 ;
  QSPIHandle.Init.SampleShifting     = QSPI_SAMPLE_SHIFTING_HALFCYCLE;
  QSPIHandle.Init.FlashSize          = POSITION_VAL(MT25QL512_FLASH_SIZE) - 1; // QSPI_FLASH_SIZE;
  QSPIHandle.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_5_CYCLE;
  QSPIHandle.Init.ClockMode          = QSPI_CLOCK_MODE_0;
  QSPIHandle.Init.FlashID            = QSPI_FLASH_ID_1;
  QSPIHandle.Init.DualFlash          = QSPI_DUALFLASH_DISABLE;

  if (HAL_QSPI_Init(&QSPIHandle) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_INIT);
    return QSPI_NOT_SUPPORTED;
  }


  /* QSPI memory reset */
  if (QSPI_ResetMemory(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_NOT_SUPPORTED;
  }

  /* Set the QSPI memory in 4-bytes address mode */
  if (QSPI_EnterFourBytesAddress(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_NOT_SUPPORTED;
  }

  /* Configuration of the dummy cycles on QSPI memory side */
  if (QSPI_DummyCyclesCfg(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_NOT_SUPPORTED;
  }

  QspiState                 = QSPI_STATE_READY ;

  RunS                      = 0 ;
  SectorX                   = 0 ;
  return QSPI_OK ;
}

// ---------------------------------------------------------------------------------

void Error_Qspi(uint8_t Error)
{
  QspiError = Error          ;
  TmQ       = Q_TIMEOUT_IDLE ;
}

uint8_t QSPI_EraseSector64K(uint16_t SectorNo)
{
  CmdCplt = 0;
  /* Initialize the erase command */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE       ;
  sCommand.Instruction       = SECTOR_ERASE_4_BYTE_ADDR_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_1_LINE;
  sCommand.AddressSize       = QSPI_ADDRESS_32_BITS;
  sCommand.Address           = (SectorNo * SECTOR_SIZE_Q) ;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_NONE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

//
//  sCommand.Instruction = SECTOR_ERASE_4_BYTE_ADDR_CMD;
//  sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
//  sCommand.Address     = (SectorNo * SECTOR_SIZE_Q) ;
//  sCommand.DataMode    = QSPI_DATA_NONE;
//  sCommand.DummyCycles = 0;
//

  /* Enable write operations */
  if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_ERROR;
  }

  if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
     {
       return RET_ERROR ;
     }

  QspiState = QSPI_STATE_ERASE_SEC ;

  return RET_OK ;
}



void QSPI_ReadBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  StatusMatch = 0 ;
  RxCplt      = 0 ;
  /* Initialize the read command */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = QUAD_OUT_FAST_READ_4_BYTE_ADDR_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_4_LINES;
  sCommand.AddressSize       = QSPI_ADDRESS_32_BITS;
  sCommand.Address           = FromAddress ;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_4_LINES;
  sCommand.DummyCycles       = MT25Q512_DUMMY_CYCLES_READ_QUAD;
  sCommand.NbData            = Len ;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

//  /* Reading Sequence ------------------------------------------------ */
//  sCommand.Instruction = QUAD_OUT_FAST_READ_4_BYTE_ADDR_CMD;
//  sCommand.DummyCycles = DUMMY_CLOCK_CYCLES_READ_QUAD;
//  sCommand.Address     = FromAddress ;
//  sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
//  sCommand.DataMode    = QSPI_DATA_4_LINES;
//  sCommand.NbData      = Len ;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_READ1);
       return ;
     }

  if (HAL_QSPI_Receive_DMA(&QSPIHandle, p) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_READ2);
       return ;
     }
  QspiState = QSPI_READ_WAIT1 ;
}


uint8_t QSPI_WriteBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  TxCplt = 0;
  QSPI_WriteEnable(&QSPIHandle);
  sCommand.Instruction = QUAD_IN_FAST_PROG_4_BYTE_ADDR_CMD ;
  sCommand.AddressMode = QSPI_ADDRESS_1_LINE               ;
  sCommand.DataMode    = QSPI_DATA_4_LINES                 ;
  sCommand.Address     = FromAddress                      ;
  sCommand.NbData      = Len                               ;
  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_WRITE1);
       return RET_ERROR ;
     }

  if (HAL_QSPI_Transmit_DMA(&QSPIHandle, p) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_WRITE2);
       return RET_ERROR ;
     }
  QspiState = QSPI_WRITE_WAIT1 ;
  return RET_OK ;
}

uint8_t QSPI_SetWriteBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  QspiStartAddress= FromAddress ;
  QspiBfrPtr       = p           ;
  QspiLen          = Len         ;
  Len              = 0x100 - (FromAddress & 0xff) ;
  if (QspiLen<Len) Len = QspiLen ;
  if (QSPI_WriteBuffer( QspiStartAddress , QspiBfrPtr , Len ) != RET_OK) return RET_ERROR ;
  QspiBfrPtr += Len ;
  QspiStartAddress+= Len ;
  if (QspiLen>=Len) QspiLen -= Len ;
              else  QspiLen  = 0   ;
  return RET_OK ;
}

void QSPI_SetReadBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  QspiStartAddress= FromAddress ;
  QspiBfrPtr       = p           ;
  QspiLen          = Len         ;
  Len              = 0x8000 - (FromAddress & 0x7fff) ;
  if (QspiLen<Len) Len = QspiLen ;
  QSPI_ReadBuffer( QspiStartAddress , QspiBfrPtr , Len );
  QspiBfrPtr += Len ;
  QspiStartAddress+= Len ;
  if (QspiLen>=Len) QspiLen -= Len ;
              else  QspiLen  = 0   ;
}

void QSPI_RealTime(void)
{
  static uint8_t  Tm0 = 0 ;
  static uint8_t  Tm1 = 0 ;
  static uint32_t Len      ;
  if (Tm0 != Global_Tm)
     {
       Tm0 = Global_Tm ;
       Tm1++;
       if (Tm1>3)
          {  // 0.1 Sec
            Tm1 = 0 ;
            if (TmQ) TmQ-- ;
          }
     }
  if (QspiError != QSPI_NO_ERROR)
     {
       QspiState = QSPI_STATE_IDLE ;
       QspiError = QSPI_NO_ERROR   ;
     }
  switch (QspiState)
  {
    case QSPI_STATE_IDLE           :
                                   if (TmQ==0)
                                      {
                                        QSPI_Init();
                                        TmQ = 100 ; // 10 Sec
                                      }
                                     break ;
    case QSPI_STATE_READY          :
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x01))
                                        {
                                          SectorX = 0 ;
                                          FromSector = 0 ;
                                          ToSector = 64 ;
                                          QSPI_EraseSector64K(FromSector);
                                          RunS &= 0xfe ;
                                        }
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x02))
                                        {
                                          SectorX = 0 ;
                                          RunS &= 0xfd ;
                                          QSPI_ReadBuffer (SectorX * SECTOR_SIZE_Q , TstBfr ,  TST_SIZE );
                                        }
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x04))
                                        {
                                          RunS &= 0xfb ;
                                          if ( SectorX == 70 )
                                             {
                                               SectorX = 7 ;
                                               for (uint16_t i=0; i<3950; i++) TstBfr[i] = ((i & 0xff) + 7) & 0xff ;
                                               QSPI_SetWriteBuffer(SectorX * SECTOR_SIZE_Q  + 9 , TstBfr ,  3950 );
                                               SectorX = 0 ;
                                             }
                                        }
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x08))
                                        {
                                          SectorX = 32 ;
                                          RunS &= 0xf7 ;
                                          QSPI_SetWriteBuffer(SectorX * SECTOR_SIZE_Q  , (void *)__aud , 65979 );
                                          SectorX = 0 ;
                                        }
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x10))
                                        {
                                          QSPI_SetReadBuffer ( 0 , (void *)VOICE_MEM_1 ,  0x100000 );
                                          RunS &= 0xef ;
                                        }

                                     break;
    case QSPI_STATE_ERASE_SEC      :
                                     if(CmdCplt != 0)
                                     {
                                       CmdCplt = 0;
                                       StatusMatch = 0;
                                       /* Configure automatic polling mode to wait for end of erase ------- */
                                       QSPI_AutoPollingMemReadyIT(&QSPIHandle , HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
                                       QspiState = QSPI_STATE_ERASE_POLL ;
                                     }
                                     break;
    case QSPI_STATE_ERASE_POLL     :
                                     if(StatusMatch != 0)
                                     {
                                       SectorX++ ;
                                       QspiState = QSPI_STATE_READY ;
                                       if (SectorX<ToSector) QSPI_EraseSector64K(SectorX);
                                     }
                                    break ;
    case QSPI_READ_WAIT1          :
                                    if (RxCplt != 0)
                                       {
                                         Len              = 0x8000     ;
                                         if (QspiLen<Len) Len = QspiLen ;
                                         if (Len)
                                            {
                                              QSPI_ReadBuffer( QspiStartAddress , QspiBfrPtr , Len );
                                              QspiBfrPtr += Len ;
                                              QspiStartAddress+= Len ;
                                              if (QspiLen>=Len) QspiLen -= Len ;
                                                          else  QspiLen  = 0   ;
                                            }
                                          else
                                            {
                                              QspiState = QSPI_STATE_READY ;
//                                              if ( SectorX < 64 )  QSPI_ReadBuffer (SectorX * SECTOR_SIZE_Q , TstBfr ,  TST_SIZE );
                                            }
                                       }
                                    break ;

    case QSPI_WRITE_WAIT1         :
                                    if (TxCplt != 0)
                                       {
                                         StatusMatch = 0;
                                         QSPI_AutoPollingMemReadyIT(&QSPIHandle , HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
                                         QspiState = QSPI_WRITE_WAIT2 ;

                                       }
                                     break ;

    case QSPI_WRITE_WAIT2         :
                                     if(StatusMatch != 0)
                                       {
                                         Len              = 0x100      ;
                                         if (QspiLen<Len) Len = QspiLen ;
                                         if (Len)
                                            {
                                              QSPI_WriteBuffer( QspiStartAddress , QspiBfrPtr , Len );
                                              QspiStartAddress+= Len ;
                                              QspiBfrPtr += Len ;
                                              if (QspiLen>=Len) QspiLen -= Len ;
                                                          else  QspiLen  = 0   ;
                                            }
                                           else
                                            {
                                              QspiState = QSPI_STATE_READY ;
                                            }
                                       }
                                     break ;
    case QSPI_FOUR_ADDRESS_POLL   :
                                     if(StatusMatch != 0)
                                     {
                                       QspiState = QSPI_STATE_READY ;
                                     }
                                    break ;

    default                       :
                                     QspiState = QSPI_STATE_IDLE ;
                                     break ;
  }
}

/**
  * @brief  Command completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  CmdCplt++;
}

/**
  * @brief  Rx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  RxCplt++;
}

/**
  * @brief  Tx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
 void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  TxCplt++;
}

/**
  * @brief  Status Match callbacks
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_StatusMatchCallback(QSPI_HandleTypeDef *hqspi)
{
  StatusMatch++;
}

/**
  * @brief  This function configure the dummy cycles on memory side.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static uint8_t QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef s_command;
  uint8_t reg;

  /* Initialize the read volatile configuration register command */
  s_command.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  s_command.Instruction       = READ_VOL_CFG_REG_CMD;
  s_command.AddressMode       = QSPI_ADDRESS_NONE;
  s_command.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  s_command.DataMode          = QSPI_DATA_1_LINE;
  s_command.DummyCycles       = 0;
  s_command.NbData            = 1;
  s_command.DdrMode           = QSPI_DDR_MODE_DISABLE;
  s_command.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  s_command.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the command */
  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Reception of the data */
  if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Enable write operations */
  if (QSPI_WriteEnable(&QSPIHandle) != QSPI_OK)
  {
    return QSPI_ERROR;
  }

  /* Update volatile configuration register (with new dummy cycles) */
  s_command.Instruction = WRITE_VOL_CFG_REG_CMD;
  MODIFY_REG(reg, MT25Q512_VCR_NB_DUMMY, (MT25Q512_DUMMY_CYCLES_READ_QUAD << POSITION_VAL(MT25Q512_VCR_NB_DUMMY)));

  /* Configure the write volatile configuration register command */
  if (HAL_QSPI_Command(&QSPIHandle, &s_command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  /* Transmission of the data */
  if (HAL_QSPI_Transmit(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    return QSPI_ERROR;
  }

  return QSPI_OK;
}


/**
* @brief QSPI MSP Initialization
* This function configures the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/
void HAL_QSPI_MspInit(QSPI_HandleTypeDef* hqspi)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  static MDMA_HandleTypeDef hmdma;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable the QuadSPI memory interface clock */
  QSPI_CLK_ENABLE();
  /* Reset the QuadSPI memory interface */
  QSPI_FORCE_RESET();
  QSPI_RELEASE_RESET();
  /* Enable GPIO clocks */
  QSPI_CS_GPIO_CLK_ENABLE();
  QSPI_CLK_GPIO_CLK_ENABLE();
  QSPI_BK1_D0_GPIO_CLK_ENABLE();
  QSPI_BK1_D1_GPIO_CLK_ENABLE();
  QSPI_BK1_D2_GPIO_CLK_ENABLE();
  QSPI_BK1_D3_GPIO_CLK_ENABLE();
  /* Enable DMA clock */
  QSPI_MDMA_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* QSPI CS GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CS_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_CS_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI CLK GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CLK_PIN;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_CLK_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D0 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D0_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D0_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D1 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D1_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D1_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D2 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D2_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D2_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D3 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D3_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D3_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for QSPI #########################################*/
  /* NVIC configuration for QSPI interrupt */
  HAL_NVIC_SetPriority(QUADSPI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ(QUADSPI_IRQn);

  /*##-4- Configure the DMA channel ###########################################*/
  /* Enable MDMA clock */
  /* Input MDMA */
  /* Set the parameters to be configured */
  hmdma.Init.Request             = MDMA_REQUEST_QUADSPI_FIFO_TH;
  hmdma.Init.TransferTriggerMode = MDMA_BUFFER_TRANSFER;
  hmdma.Init.Priority            = MDMA_PRIORITY_HIGH;
  hmdma.Init.Endianness          = MDMA_LITTLE_ENDIANNESS_PRESERVE;

  hmdma.Init.SourceInc           = MDMA_SRC_INC_BYTE;
  hmdma.Init.DestinationInc       = MDMA_DEST_INC_DISABLE;
  hmdma.Init.SourceDataSize       = MDMA_SRC_DATASIZE_BYTE;
  hmdma.Init.DestDataSize         = MDMA_DEST_DATASIZE_BYTE;
  hmdma.Init.DataAlignment        = MDMA_DATAALIGN_PACKENABLE;
  hmdma.Init.BufferTransferLength = 4;   // 4
  hmdma.Init.SourceBurst          = MDMA_SOURCE_BURST_SINGLE;
  hmdma.Init.DestBurst            = MDMA_DEST_BURST_SINGLE;

  hmdma.Init.SourceBlockAddressOffset = 0;
  hmdma.Init.DestBlockAddressOffset = 0;

  hmdma.Instance = MDMA_Channel1;

  /* Associate the DMA handle */
  __HAL_LINKDMA(hqspi, hmdma, hmdma);

  /* DeInitialize the MDMA Stream */
  HAL_MDMA_DeInit(&hmdma);
  /* Initialize the MDMA stream */
  HAL_MDMA_Init(&hmdma);

  /* Enable and set QuadSPI interrupt to the lowest priority */
  HAL_NVIC_SetPriority(MDMA_IRQn, 0x00, 0);
  HAL_NVIC_EnableIRQ(MDMA_IRQn);
}

/**
* @brief QSPI MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/

void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef* hqspi)
{

  if(hqspi->Instance==QUADSPI)
  {
  /* USER CODE BEGIN QUADSPI_MspDeInit 0 */

  /* USER CODE END QUADSPI_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_QSPI_CLK_DISABLE();

    /**QUADSPI GPIO Configuration
    PE2     ------> QUADSPI_BK1_IO2
    PF6     ------> QUADSPI_BK1_IO3
    PF8     ------> QUADSPI_BK1_IO0
    PF9     ------> QUADSPI_BK1_IO1
    PH2     ------> QUADSPI_BK2_IO0
    PH3     ------> QUADSPI_BK2_IO1
    PB2     ------> QUADSPI_CLK
    PG6     ------> QUADSPI_BK1_NCS
    PG9     ------> QUADSPI_BK2_IO2
    PG14     ------> QUADSPI_BK2_IO3
    */
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_2);

    HAL_GPIO_DeInit(GPIOF, GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_2);

    HAL_GPIO_DeInit(GPIOG, GPIO_PIN_6);

  /* USER CODE BEGIN QUADSPI_MspDeInit 1 */

  /* USER CODE END QUADSPI_MspDeInit 1 */
  }

}



uint8_t QSPI_CommandEraseFromToSector(uint16_t From , uint16_t To)
{
  if (QspiState == QSPI_STATE_READY)
     {
       FromSector = From ;
       ToSector   = To   ;
       if (QSPI_EraseSector64K(FromSector) == RET_OK) return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t QSPI_CommandReadBuffer(uint32_t Addr , uint32_t Len)
{
  if (QspiState == QSPI_STATE_READY)
     {
       QSPI_SetReadBuffer ( Addr , TstBfr , Len );
       return RET_OK ;
     }
  return RET_BUSY ;
}

uint8_t QSPI_CommandWriteBuffer(uint32_t Addr , uint8_t *p , uint32_t Len)
{
  uint16_t i ;
  if (QspiState == QSPI_STATE_READY)
     {
       i = 0 ;
       while ((i<Len) && (i<sizeof(TstBfr)))
       {
         TstBfr[i++] = *p++ ;
       }
       if (QSPI_SetWriteBuffer( Addr , TstBfr , Len ) == RET_OK) return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t QSPI_CommandTestReady(void)
{
  if (QspiState == QSPI_STATE_READY)
     {
       return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t *QSPI_GetTstBfrPtr(void)
{
  return TstBfr ;
}

uint8_t QSPI_SetRunS(uint8_t Data)
{
  if (QspiState == QSPI_STATE_READY)
     {
       RunS = Data ;
       return RET_OK ;
     }
  return RET_BUSY ;
}



