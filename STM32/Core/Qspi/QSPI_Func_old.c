
#include "main.h"

#if (QMEM_SOM > 2)

#include "QSPI_Func.h"

#define QSPI_STATE_IDLE           0
#define QSPI_STATE_READY          1
#define QSPI_STATE_PROCESS        2
#define QSPI_STATE_ERASE_SEC      3
#define QSPI_STATE_ERASE_POLL     4
#define QSPI_READ_WAIT1           5
#define QSPI_WRITE_WAIT1          6
#define QSPI_WRITE_WAIT2          7
#define QSPI_FOUR_ADDRESS_POLL    8

#define Q_TIMEOUT_IDLE           20    // 2 Sec timeout
#define Q_TIMEOUT_READY          40
#define Q_TIMEOUT_LONG          100

#define QSPI_NO_ERROR             0
#define QSPI_ERROR_INIT           1
#define QSPI_ERROR_IT             2
#define QSPI_ERROR_COMMAND        3
#define QSPI_ERROR_POLLING        4
#define QSPI_ERROR_AUTO_POLLING   5
#define QSPI_ERROR_TRANSMIT       6
#define QSPI_ERROR_RECEIVE        7
#define QSPI_ERROR_READ1          8
#define QSPI_ERROR_READ2          9
#define QSPI_ERROR_WRITE1        10
#define QSPI_ERROR_WRITE2       11


#define CMDBFR_SIZE          0x1000


QSPI_HandleTypeDef   QSPIHandle ;
QSPI_CommandTypeDef  sCommand   ;

__IO uint8_t CmdCplt, RxCplt, TxCplt, StatusMatch, TimeOut;
static uint8_t  QspiState    ;
static uint16_t SectorX  = 0 ;
static uint8_t  QspiError = QSPI_NO_ERROR ;
static uint8_t  TmQ      = 0 ;

static uint32_t QspiStartAddress;
static uint8_t  *QspiBfrPtr      ;
static uint32_t QspiLen          ;

static uint8_t  RunS             ;

static uint16_t FromSector , ToSector ;

static uint32_t CmdAddr , CmdLen    ;
static uint8_t  Hcmd                ;



#define ADDR_MEM_1     ((uint32_t)0x20000000)      //
static uint8_t  *CmdBfr = (void *)ADDR_MEM_1 ;

//static uint8_t  CmdBfr[CMDBFR_SIZE] ;

// extern const uint8_t __aud[];

void QSPI_Init(void);
void QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi);
void QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi);
void QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi);
void QSPI_EnterFourBytesAddress(QSPI_HandleTypeDef *hqspi);

void Error_Qspi(uint8_t Error)
{
  QspiError = Error          ;
  TmQ       = Q_TIMEOUT_IDLE ;
}

void QSPI_EraseSector64K(uint16_t SectorNo)
{
  CmdCplt = 0;
  QSPI_WriteEnable(&QSPIHandle);
  sCommand.Instruction = SECTOR_ERASE_4_BYTE_ADDR_CMD;
  sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
  sCommand.Address     = (SectorNo * SECTOR_SIZE_Q) ;
  sCommand.DataMode    = QSPI_DATA_NONE;
  sCommand.DummyCycles = 0;
  if (HAL_QSPI_Command_IT(&QSPIHandle, &sCommand) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_IT);
       return ;
     }
  QspiState = QSPI_STATE_ERASE_SEC ;
}



void QSPI_ReadBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  StatusMatch = 0 ;
  RxCplt      = 0 ;
  /* Configure Volatile Configuration register (with new dummy cycles) */
  sCommand.DummyCycles = 0 ;
  QSPI_DummyCyclesCfg(&QSPIHandle);
  /* Reading Sequence ------------------------------------------------ */
  sCommand.Instruction = QUAD_OUT_FAST_READ_4_BYTE_ADDR_CMD;
  sCommand.DummyCycles = DUMMY_CLOCK_CYCLES_READ_QUAD;
  sCommand.Address     = FromAddress ;
  sCommand.AddressMode = QSPI_ADDRESS_1_LINE;
  sCommand.DataMode    = QSPI_DATA_4_LINES;
  sCommand.NbData      = Len ;
  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_READ1);
       return ;
     }

  if (HAL_QSPI_Receive_DMA(&QSPIHandle, p) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_READ2);
       return ;
     }
  QspiState = QSPI_READ_WAIT1 ;
}


void QSPI_WriteBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  QSPI_WriteEnable(&QSPIHandle);
  sCommand.Instruction       = QUAD_IN_FAST_PROG_4_BYTE_ADDR_CMD ;
  sCommand.AddressMode       = QSPI_ADDRESS_1_LINE               ;
  sCommand.DataMode          = QSPI_DATA_4_LINES                 ;
  sCommand.Address           = FromAddress                      ;
  sCommand.NbData            = Len                               ;
  sCommand.DummyCycles       = 0                                 ;
  sCommand.AddressSize       = QSPI_ADDRESS_32_BITS            ;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE         ;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE             ;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY          ;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD           ;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_WRITE1);
       return ;
     }

  TxCplt = 0;
  if (HAL_QSPI_Transmit_DMA(&QSPIHandle, p) != HAL_OK)
     {
       Error_Qspi(QSPI_ERROR_WRITE2);
       return ;
     }
  QspiState = QSPI_WRITE_WAIT1 ;
}

void QSPI_SetWriteBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  QspiStartAddress= FromAddress ;
  QspiBfrPtr       = p           ;
  QspiLen          = Len         ;
  Len              = 0x100 - (FromAddress & 0xff) ;
  if (QspiLen<Len) Len = QspiLen ;
  QSPI_WriteBuffer( QspiStartAddress , QspiBfrPtr , Len );
  QspiBfrPtr += Len ;
  QspiStartAddress+= Len ;
  if (QspiLen>=Len) QspiLen -= Len ;
              else  QspiLen  = 0   ;
}

void QSPI_SetReadBuffer(uint32_t FromAddress , uint8_t *p , uint32_t Len)
{
  QspiStartAddress= FromAddress ;
  QspiBfrPtr       = p           ;
  QspiLen          = Len         ;
  Len              = 0x8000 - (FromAddress & 0x7fff) ;
  if (QspiLen<Len) Len = QspiLen ;
  QSPI_ReadBuffer( QspiStartAddress , QspiBfrPtr , Len );
  QspiBfrPtr += Len ;
  QspiStartAddress+= Len ;
  if (QspiLen>=Len) QspiLen -= Len ;
              else  QspiLen  = 0   ;
}

void QSPI_RealTime(void)
{
  static uint8_t  Tm0 = 0 ;
  static uint8_t  Tm1 = 0 ;
  static uint32_t Len      ;
  if (Tm0 != Global_Tm)
     {
       Tm0 = Global_Tm ;
       Tm1++;
       if (Tm1>3)
          {  // 0.1 Sec
            Tm1 = 0 ;
            if (TmQ) TmQ-- ;
            if ((TmQ==0) && (QspiState > QSPI_STATE_READY)) QspiState = QSPI_STATE_READY ;
          }
     }
  if (QspiError != QSPI_NO_ERROR)
     {
       QspiState = QSPI_STATE_IDLE ;
       QspiError = QSPI_NO_ERROR   ;
     }
  switch (QspiState)
  {
    case QSPI_STATE_IDLE           :
                                     if (TmQ==0)
                                      {
                                        QSPI_Init();
                                      }
                                     break ;
    case QSPI_STATE_READY          :
                                       TmQ       = Q_TIMEOUT_READY ;
                                     if ((QspiState == QSPI_STATE_READY) && (RunS & 0x01))
                                        {
                                          QSPI_SetReadBuffer ( QSECTOR_MEM_FILE_BASE , (void *)QUAD_MEM_SEC_1 ,  (QSECTOR_MEM_FILE_END - QSECTOR_MEM_FILE_START + 1 ) * QSECTOR_SIZE );
                                          RunS &= 0xfe ;
                                          TmQ = Q_TIMEOUT_LONG ;
                                        }
                                     break;
    case QSPI_STATE_PROCESS        :
                                     switch (Hcmd)
                                     {
                                       case   1   :
                                                    QSPI_EraseSector64K(FromSector)  ;
                                                    break ;
                                       case   2   :
                                                    QSPI_SetReadBuffer ( CmdAddr , CmdBfr , CmdLen );
                                                    break ;
                                       case   3   :
                                                    QSPI_SetWriteBuffer( CmdAddr , CmdBfr ,CmdLen );
                                                    break ;
                                     }
                                    if ( QspiState == QSPI_STATE_PROCESS)  QspiState = QSPI_STATE_READY ;
                                     break ;

    case QSPI_STATE_ERASE_SEC      :
                                     if(CmdCplt != 0)
                                     {
                                       CmdCplt = 0;
                                       StatusMatch = 0;
                                       /* Configure automatic polling mode to wait for end of erase ------- */
                                       QSPI_AutoPollingMemReady(&QSPIHandle);
                                       QspiState = QSPI_STATE_ERASE_POLL ;
                                     }
                                     break;
    case QSPI_STATE_ERASE_POLL     :
                                     if(StatusMatch != 0)
                                     {
                                       SectorX++ ;
                                       QspiState = QSPI_STATE_READY ;
                                       TmQ       = Q_TIMEOUT_READY ;
                                       if (SectorX<=ToSector) QSPI_EraseSector64K(SectorX);
                                     }
                                    break ;
    case QSPI_READ_WAIT1          :
                                    if (RxCplt != 0)
                                       {
                                         TmQ              = Q_TIMEOUT_LONG ;
                                         Len              = 0x8000     ;
                                         if (QspiLen<Len) Len = QspiLen ;
                                         if (Len)
                                            {
                                              QSPI_ReadBuffer( QspiStartAddress , QspiBfrPtr , Len );
                                              QspiBfrPtr += Len ;
                                              QspiStartAddress+= Len ;
                                              if (QspiLen>=Len) QspiLen -= Len ;
                                                          else  QspiLen  = 0   ;
                                            }
                                          else
                                            {
                                              QspiState = QSPI_STATE_READY ;
//                                              if ( SectorX < 64 )  QSPI_ReadBuffer (SectorX * SECTOR_SIZE_Q , CmdBfr ,  CMDBFR_SIZE );
                                            }
                                       }
                                    break ;

    case QSPI_WRITE_WAIT1         :
                                    if (TxCplt != 0)
                                       {
                                         StatusMatch = 0;
                                         QSPI_AutoPollingMemReady(&QSPIHandle);
                                         QspiState = QSPI_WRITE_WAIT2 ;

                                       }
                                    break ;

    case QSPI_WRITE_WAIT2         :
                                     if(StatusMatch != 0)
                                       {
                                         Len              = 0x100      ;
                                         if (QspiLen<Len) Len = QspiLen ;
                                         if (Len)
                                            {
                                              QSPI_WriteBuffer( QspiStartAddress , QspiBfrPtr , Len );
                                              QspiStartAddress+= Len ;
                                              QspiBfrPtr += Len ;
                                              if (QspiLen>=Len) QspiLen -= Len ;
                                                          else  QspiLen  = 0   ;
                                            }
                                           else
                                            {
                                              QspiState = QSPI_STATE_READY ;
                                            }
                                       }
                                     break ;
    case QSPI_FOUR_ADDRESS_POLL   :
                                     if(StatusMatch != 0)
                                     {
                                       QspiState = QSPI_STATE_READY ;
                                     }
                                    break ;

    default                       :
                                     QspiState = QSPI_STATE_IDLE ;
                                     break ;
  }
}

void QSPI_Init(void)
{
  /* Initialize QuadSPI ------------------------------------------------------ */
  QspiState           = QSPI_STATE_IDLE ;
  QSPIHandle.Instance = QUADSPI;
  HAL_QSPI_DeInit(&QSPIHandle);

  QSPIHandle.Init.ClockPrescaler     = 1;
  QSPIHandle.Init.FifoThreshold      = 4;
  QSPIHandle.Init.SampleShifting     = QSPI_SAMPLE_SHIFTING_HALFCYCLE;
  QSPIHandle.Init.FlashSize          = QSPI_FLASH_SIZE;
  QSPIHandle.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  QSPIHandle.Init.ClockMode          = QSPI_CLOCK_MODE_0;
  QSPIHandle.Init.FlashID            = QSPI_FLASH_ID_1;
  QSPIHandle.Init.DualFlash          = QSPI_DUALFLASH_DISABLE;

  if (HAL_QSPI_Init(&QSPIHandle) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_INIT);
    return ;
  }

  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.AddressSize       = QSPI_ADDRESS_32_BITS;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;


  QspiState           = QSPI_STATE_READY ;
//  QSPI_EnterFourBytesAddress(&QSPIHandle);
  RunS                      = 0x01 ;
  SectorX                   = 0    ;
}

/**
  * @brief  Command completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  CmdCplt++;
}

/**
  * @brief  Rx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  RxCplt++;
}

/**
  * @brief  Tx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
 void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  TxCplt++;
}

/**
  * @brief  Status Match callbacks
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_StatusMatchCallback(QSPI_HandleTypeDef *hqspi)
{
  StatusMatch++;
}

void QSPI_WriteEnable(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;

  /* Enable write operations ------------------------------------------ */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = WRITE_ENABLE_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_NONE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_COMMAND);
    return ;
  }

  /* Configure automatic polling mode to wait for write enabling ---- */
  sConfig.Match           = 0x02;
  sConfig.Mask            = 0x02;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  sCommand.Instruction    = READ_STATUS_REG_CMD;
  sCommand.DataMode       = QSPI_DATA_1_LINE;

  if (HAL_QSPI_AutoPolling(&QSPIHandle, &sCommand, &sConfig, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_POLLING);
    return ;
  }
}

void QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;

  /* Configure automatic polling mode to wait for memory ready ------ */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = READ_STATUS_REG_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_1_LINE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  sConfig.Match           = 0x00;
  sConfig.Mask            = 0x01;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling_IT(&QSPIHandle, &sCommand, &sConfig) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_AUTO_POLLING);
    return ;
  }
}

void QSPI_DummyCyclesCfg(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef sCommand;
  uint8_t reg;

  /* Read Volatile Configuration register --------------------------- */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = READ_VOL_CFG_REG_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_1_LINE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
  sCommand.NbData            = 1;

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_COMMAND);
    return ;
  }

  if (HAL_QSPI_Receive(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_RECEIVE);
    return ;
  }

  /* Enable write operations ---------------------------------------- */
  QSPI_WriteEnable(&QSPIHandle);

  /* Write Volatile Configuration register (with new dummy cycles) -- */
  sCommand.Instruction = WRITE_VOL_CFG_REG_CMD;
  MODIFY_REG(reg, 0xF0, (DUMMY_CLOCK_CYCLES_READ_QUAD << POSITION_VAL(0xF0)));

  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_COMMAND);
    return ;
  }

  if (HAL_QSPI_Transmit(&QSPIHandle, &reg, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_TRANSMIT);
    return ;
  }
}


void QSPI_EnterFourBytesAddress(QSPI_HandleTypeDef *hqspi)
{
  /* Initialize the command */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = ENTER_4_BYTE_ADDR_MODE_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_NONE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Enable write operations */
  QSPI_WriteEnable(&QSPIHandle);

  /* Send the command */
  if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Qspi(QSPI_ERROR_COMMAND);
    return ;
  }
  QSPI_AutoPollingMemReady(&QSPIHandle);
  QspiState = QSPI_FOUR_ADDRESS_POLL ;
}



/**
* @brief QSPI MSP Initialization
* This function configures the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/
void HAL_QSPI_MspInit(QSPI_HandleTypeDef* hqspi)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  static MDMA_HandleTypeDef hmdma;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable the QuadSPI memory interface clock */
  QSPI_CLK_ENABLE();
  /* Reset the QuadSPI memory interface */
  QSPI_FORCE_RESET();
  QSPI_RELEASE_RESET();
  /* Enable GPIO clocks */
  QSPI_CS_GPIO_CLK_ENABLE();
  QSPI_CLK_GPIO_CLK_ENABLE();
  QSPI_BK1_D0_GPIO_CLK_ENABLE();
  QSPI_BK1_D1_GPIO_CLK_ENABLE();
  QSPI_BK1_D2_GPIO_CLK_ENABLE();
  QSPI_BK1_D3_GPIO_CLK_ENABLE();
  /* Enable DMA clock */
  QSPI_MDMA_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* QSPI CS GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CS_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_CS_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI CLK GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_CLK_PIN;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_CLK_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D0 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D0_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D0_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D1 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D1_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D1_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D2 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D2_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D2_GPIO_PORT, &GPIO_InitStruct);

  /* QSPI D3 GPIO pin configuration  */
  GPIO_InitStruct.Pin       = QSPI_BK1_D3_PIN;
  GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;
  HAL_GPIO_Init(QSPI_BK1_D3_GPIO_PORT, &GPIO_InitStruct);

  /*##-3- Configure the NVIC for QSPI #########################################*/
  /* NVIC configuration for QSPI interrupt */
  HAL_NVIC_SetPriority(QUADSPI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ(QUADSPI_IRQn);

  /*##-4- Configure the DMA channel ###########################################*/
  /* Enable MDMA clock */
  /* Input MDMA */
  /* Set the parameters to be configured */
  hmdma.Init.Request             = MDMA_REQUEST_QUADSPI_FIFO_TH;
  hmdma.Init.TransferTriggerMode = MDMA_BUFFER_TRANSFER;
  hmdma.Init.Priority            = MDMA_PRIORITY_HIGH;
  hmdma.Init.Endianness          = MDMA_LITTLE_ENDIANNESS_PRESERVE;

  hmdma.Init.SourceInc            = MDMA_SRC_INC_BYTE;
  hmdma.Init.DestinationInc       = MDMA_DEST_INC_DISABLE;
  hmdma.Init.SourceDataSize       = MDMA_SRC_DATASIZE_BYTE;
  hmdma.Init.DestDataSize         = MDMA_DEST_DATASIZE_BYTE;
  hmdma.Init.DataAlignment        = MDMA_DATAALIGN_PACKENABLE;
  hmdma.Init.BufferTransferLength = 4;   // 4
  hmdma.Init.SourceBurst          = MDMA_SOURCE_BURST_SINGLE;
  hmdma.Init.DestBurst            = MDMA_DEST_BURST_SINGLE;

  hmdma.Init.SourceBlockAddressOffset = 0;
  hmdma.Init.DestBlockAddressOffset = 0;

  hmdma.Instance = MDMA_Channel5;

  /* Associate the DMA handle */
  __HAL_LINKDMA(hqspi, hmdma, hmdma);

  /* DeInitialize the MDMA Stream */
  HAL_MDMA_DeInit(&hmdma);
  /* Initialize the MDMA stream */
  HAL_MDMA_Init(&hmdma);

  /* Enable and set QuadSPI interrupt to the lowest priority */
  HAL_NVIC_SetPriority(MDMA_IRQn, 0x0F , 0);  // 0x02
  HAL_NVIC_EnableIRQ(MDMA_IRQn);
}

/**
* @brief QSPI MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hqspi: QSPI handle pointer
* @retval None
*/

void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef* hqspi)
{

  if(hqspi->Instance==QUADSPI)
  {
  /* USER CODE BEGIN QUADSPI_MspDeInit 0 */

  /* USER CODE END QUADSPI_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_QSPI_CLK_DISABLE();

    /**QUADSPI GPIO Configuration
    PE2     ------> QUADSPI_BK1_IO2
    PF6     ------> QUADSPI_BK1_IO3
    PF8     ------> QUADSPI_BK1_IO0
    PF9     ------> QUADSPI_BK1_IO1
    PH2     ------> QUADSPI_BK2_IO0
    PH3     ------> QUADSPI_BK2_IO1
    PB2     ------> QUADSPI_CLK
    PG6     ------> QUADSPI_BK1_NCS
    PG9     ------> QUADSPI_BK2_IO2
    PG14     ------> QUADSPI_BK2_IO3
    */
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_2);

    HAL_GPIO_DeInit(GPIOF, GPIO_PIN_6|GPIO_PIN_8|GPIO_PIN_9);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_2);

    HAL_GPIO_DeInit(GPIOG, GPIO_PIN_6);

  /* USER CODE BEGIN QUADSPI_MspDeInit 1 */

  /* USER CODE END QUADSPI_MspDeInit 1 */
  }

}


uint8_t QSPI_CommandEraseFromToSector(uint16_t From , uint16_t To)
{
  if (QspiState == QSPI_STATE_READY)
     {
       FromSector  = From               ;
       SectorX     = From               ;
       ToSector    = To                 ;
       Hcmd        = 1                  ;
       QspiState   = QSPI_STATE_PROCESS ;
       return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t QSPI_CommandReadBuffer(uint32_t Addr , uint32_t Len)
{
  if (QspiState == QSPI_STATE_READY)
     {
       CmdAddr     = Addr              ;
       CmdLen      = Len               ;
       Hcmd        = 2                 ;
       QspiState   = QSPI_STATE_PROCESS ;
       return RET_OK ;
     }
  return RET_BUSY ;
}

uint8_t QSPI_CommandWriteBuffer(uint32_t Addr , uint8_t *p , uint32_t Len)
{
  uint16_t i ;
  if (QspiState == QSPI_STATE_READY)
     {
       i = 0 ;
       while ((i<Len) && (i<CMDBFR_SIZE))
       {
         CmdBfr[i++] = *p++ ;
       }
       CmdAddr     = Addr              ;
       CmdLen      = Len               ;
       Hcmd        = 3                 ;
       QspiState   = QSPI_STATE_PROCESS ;
       return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t QSPI_CommandTestReady(void)
{
  if (QspiState == QSPI_STATE_READY)
     {
       return RET_OK ;
     }
  return RET_BUSY ;
}


uint8_t *QSPI_GetTstBfrPtr(void)
{
  return CmdBfr ;
}

#endif
