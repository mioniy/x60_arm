#ifndef __GT911_H_
#define __GT911_H_

#include "stm32f4xx.h"
#include "delay.h"
#include "CT_I2C.h"

#define CT_CMD_WR		0XBA
#define CT_CMD_RD   	0XBB

#define CT_MAX_TOUCH    5

#define GT911_COMMAND_REG   		0x8040
#define GT911_CONFIG_REG			0x8047
#define GT911_PRODUCT_ID_REG 		0x8140
#define GT911_FIRMWARE_VERSION_REG  0x8144
#define GT911_READ_XY_REG 			0x814E

typedef struct
{
	uint8_t Touch;
	uint8_t TouchpointFlag;
	uint8_t TouchCount;
 
	uint8_t Touchkeytrackid[CT_MAX_TOUCH];
	uint16_t X[CT_MAX_TOUCH];
	uint16_t Y[CT_MAX_TOUCH];
	uint16_t S[CT_MAX_TOUCH];
}GT911_Dev;

void TP_INT_Config(void);
void EXTI4_IRQHandler(void);
void GT911_TEST(void);
uint8_t GT911_WR_Reg(uint16_t reg,uint8_t *buf,uint8_t len);
#endif /*__GT911_H_*/
