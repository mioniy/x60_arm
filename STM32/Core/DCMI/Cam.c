
#ifdef CAMERA_OV


#include "stm32h7xx.h"
#include "port_stm.h"
#include "camera.h"
#include "ov2640.h"

#define RESOLUTION_R160x120      CAMERA_R160x120      /* QQVGA Resolution     */
#define RESOLUTION_R320x240      CAMERA_R320x240      /* QVGA Resolution      */
#define RESOLUTION_R480x272      CAMERA_R480x272      /* 480x272 Resolution   */
#define RESOLUTION_R640x480      CAMERA_R640x480      /* VGA Resolution       */

#define OV2640_ADDR       0x60
#define OV7670_ADDR       0x76

DCMI_HandleTypeDef hdcmi;
DMA_HandleTypeDef hdma_dcmi;
I2C_HandleTypeDef hi2c1;

static DCMI_HandleTypeDef  hdcmi_eval;
       CAMERA_DrvTypeDef   *camera_drv;
uint32_t current_resolution;


uint8_t SCCBTxBuffer[5];
uint8_t SCCBRxBuffer[5];

uint8_t FlagFrame      ;


void MX_DCMI_Init(void);
void MX_I2C1_Init(void);
uint8_t ov7670_init(void);
uint8_t ov7670_read(uint8_t reg);
void ov7670_write(uint8_t reg,uint8_t val);
void ov7670_capturesnapshot(uint8_t *buff);
void ov7670_capture(uint8_t *buff);

/* DCMI init function */
void MX_DCMI_Init(void)
{

  hdcmi.Instance = DCMI;
  hdcmi.Init.SynchroMode = DCMI_SYNCHRO_HARDWARE;
  hdcmi.Init.PCKPolarity = DCMI_PCKPOLARITY_RISING ;   // DCMI_PCKPOLARITY_RISING;
  hdcmi.Init.VSPolarity = DCMI_VSPOLARITY_LOW;
  hdcmi.Init.HSPolarity = DCMI_HSPOLARITY_LOW;
  hdcmi.Init.CaptureRate = DCMI_CR_ALL_FRAME;
  hdcmi.Init.ExtendedDataMode = DCMI_EXTEND_DATA_8B;
  hdcmi.Init.JPEGMode = DCMI_JPEG_DISABLE;
  HAL_DCMI_Init(&hdcmi);

}

/* I2C1 init function */
void MX_I2C1_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing          = 0x80912732 ;
  hi2c1.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE ;
  hi2c1.Init.OwnAddress1     = 0;
  hi2c1.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  hi2c1.Init.OwnAddress2     = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  hi2c1.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLED;
  HAL_I2C_Init(&hi2c1);
  /* Init the I2C */
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**I2C1 GPIO Configuration
    PB8     ------> I2C1_SCL
    PB9     ------> I2C1_SDA
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();
}

void I2C1_Write(uint8_t Addr, uint8_t Reg, uint8_t Value)
{
  Global_I2c_status = HAL_I2C_Mem_Write(&hi2c1, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, 100);
}

uint8_t I2C1_Read(uint8_t Addr, uint8_t Reg)
{
  uint8_t Value = 0;
  Global_I2c_status = HAL_I2C_Mem_Read(&hi2c1, Addr, Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, 100);
  return Value;
}

void CAM_init(void)
{
  MX_DCMI_Init();
  MX_I2C1_Init();
  //Camera setup code
  HAL_DCMI_MspInit(&hdcmi);
  /*** Configure the DMA ***/
  /* Set the parameters to be configured */
  hdma_dcmi.Init.Request            = DMA_REQUEST_DCMI ;
  hdma_dcmi.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  hdma_dcmi.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_dcmi.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_dcmi.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma_dcmi.Init.MemDataAlignment    = DMA_MDATAALIGN_WORD;
  hdma_dcmi.Init.Mode                = DMA_CIRCULAR;
  hdma_dcmi.Init.Priority            = DMA_PRIORITY_HIGH;
  hdma_dcmi.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  hdma_dcmi.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma_dcmi.Init.MemBurst            = DMA_MBURST_SINGLE;
  hdma_dcmi.Init.PeriphBurst         = DMA_PBURST_SINGLE;

  hdma_dcmi.Instance = DMA2_Stream1;

  /* Associate the initialized DMA handle to the DCMI handle */
  __HAL_LINKDMA(&hdcmi, DMA_Handle, hdma_dcmi);

  /*** Configure the NVIC for DCMI and DMA ***/
  /* NVIC configuration for DCMI transfer complete interrupt */
  HAL_NVIC_SetPriority(DCMI_IRQn, 0x0F, 0);
  HAL_NVIC_EnableIRQ(DCMI_IRQn);

  /* NVIC configuration for DMA2D transfer complete interrupt */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0x0e, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

  /* Configure the DMA stream */
  HAL_DMA_Init(hdcmi.DMA_Handle);
}



uint8_t ov7670_init(void)
{
 uint8_t x ;

  //I2C Setup code
  HAL_I2C_MspInit(&hi2c1);
  HAL_GPIO_WritePin(GPIOI, GPIO_PIN_7, GPIO_PIN_RESET);
  HAL_Delay(100);
  HAL_GPIO_WritePin(GPIOI, GPIO_PIN_7, GPIO_PIN_SET);
  x = ov7670_read(0x0A) ;
  if((x == OV7670_ADDR))
     {
               ////////////////////////////////////Working code
       //ov7670_write(0x11,0x01);//20FPS
       ov7670_write(0x3A,0x04);//TSLB register
       ov7670_write(0x12,0x14); // QVGA resolution
       //ov7670_write(0x12,0x0C);//QCIF Resolution
       ov7670_write(0x8C,0x00);//RGB444 register
       ov7670_write(0x04,0x00);//COM1 register
       ov7670_write(0x40,0xD0);//COM15 register(RGB565)
       ov7670_write(0x72,0x11);
       ov7670_write(0x73,0xF1);
                       ////////////////////////////////////
       /*
       ov7670_write(0x3A,0x04);//TSLB
       ov7670_write(0x3B,0x0A);//COM11 50Hz autodetection
       ov7670_write(0x12,0x04);//COM7 RGB output
       ov7670_write(0x8C,0x00);//RGB444 disable
       ov7670_write(0x40,0xD0);//COM15  RGB565 output
       ov7670_write(0x0C,0x0C);//COM3
       ov7670_write(0x3E,0x11);//COM14
       ov7670_write(0x70,0x3A);//Scaling XSC
       ov7670_write(0x71,0x35);//Scaling YSC
       ov7670_write(0x72,0x11);//Scaling DCWCTR
       ov7670_write(0x73,0xF1);//Scaling PCLK_DIV
       ov7670_write(0xA2,0x52);//Scaling PCLK_DELAY
       */
       ov7670_write(0x32,0x80);
       ov7670_write(0x17,0x3A);
       ov7670_write(0x18,0x03);
       ov7670_write(0x19,0x02);
       ov7670_write(0x1A,0x7B);
       ov7670_write(0x03,0x06);
       ov7670_write(0x13,0xE0);
       ov7670_write(0x00,0x00);
       ov7670_write(0x14,0x48);
       ov7670_write(0x13,0xE5);
       ov7670_write(0xA4,0x89);
       ////////// Color settings
       ov7670_write(0x4f,0x80);
       ov7670_write(0x50,0x80);
       ov7670_write(0x51,0x00);
       ov7670_write(0x52,0x22);
       ov7670_write(0x53,0x5e);
       ov7670_write(0x54,0x80);
       ov7670_write(0x56,0x40);
       ov7670_write(0x58,0x9e);
       ov7670_write(0x59,0x88);
       ov7670_write(0x5a,0x88);
       ov7670_write(0x5b,0x44);
       ov7670_write(0x5c,0x67);
       ov7670_write(0x5d,0x49);
       ov7670_write(0x5e,0x0e);
       ov7670_write(0x69,0x00);
       ov7670_write(0x6a,0x40);
       ov7670_write(0x6b,0x0a);
       ov7670_write(0x6c,0x0a);
       ov7670_write(0x6d,0x55);
       ov7670_write(0x6e,0x11);
       ov7670_write(0x6f,0x9f);
       ov7670_write(0xb0,0x84);

       return 1;
     }
   else
     {
       return 0;
     }
}

uint8_t ov7670_read(uint8_t reg){
	SCCBTxBuffer[0]=reg;
	while(HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)0x42, (uint8_t*)SCCBTxBuffer, 1, 10000)!= HAL_OK){};
	while(HAL_I2C_Master_Receive(&hi2c1, (uint16_t)0x42, (uint8_t *)SCCBRxBuffer, 1, 10000) != HAL_OK){};
	return SCCBRxBuffer[0];
	}

void ov7670_write(uint8_t reg,uint8_t val){
	SCCBTxBuffer[0]=reg;
	SCCBTxBuffer[1]=val;
	while(HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)0x42, (uint8_t*)SCCBTxBuffer, 2, 10000)!= HAL_OK){};
	}




/**
  * @brief  Get the capture size.
  * @param  resolution: the current resolution.
  * @retval capture size.
  */
static uint32_t GetSize(uint32_t resolution)
{
  uint32_t size = 0;

  /* Get capture size */
  switch (resolution)
  {
  case CAMERA_R160x120:
    {
      size =  0x2580;
    }
    break;
  case CAMERA_R320x240:
    {
      size =  0x9600;
    }
    break;
  case CAMERA_R480x272:
    {
      size =  0xFF00;
    }
    break;
  case CAMERA_R640x480:
    {
      size =  0x25800;
    }
    break;
  default:
    {
      break;
    }
  }

  return size;
}


void CAM_capturesnapshot(uint8_t *buff)
{
  HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_SNAPSHOT, (uint32_t)buff,GetSize(current_resolution));  // QVGA size
}
void CAM_capture(uint8_t *buff)
{
  HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_CONTINUOUS,(uint32_t)buff,GetSize(current_resolution));  // QVGA size
}


/**
  * @brief  Handles DCMI interrupt request.
  */
void CAM_CAMERA_IRQHandler(void)
{
  HAL_DCMI_IRQHandler(&hdcmi);
}


/**
  * @brief  Handles DMA interrupt request.
  */
void CAM_CAMERA_DMA_IRQHandler(void)
{
  HAL_DMA_IRQHandler(hdcmi.DMA_Handle);
  FlagFrame = 1 ;
}


static DMA2D_HandleTypeDef hdma2d_eval;



/**
  * @brief  Converts a line to an ARGB8888 pixel format.
  * @param  pSrc: Pointer to source buffer
  * @param  pDst: Output color
  * @param  xSize: Buffer width
  * @param  ColorMode: Input color mode
  * @retval None
  */
static void LCD_LL_ConvertLineToARGB8888(void *pSrc, void *pDst)
{
  /* Enable DMA2D clock */
  __HAL_RCC_DMA2D_CLK_ENABLE();

  /* Configure the DMA2D Mode, Color Mode and output offset */
  hdma2d_eval.Init.Mode         = DMA2D_M2M_PFC;
  hdma2d_eval.Init.ColorMode    = DMA2D_RGB565 ;
  hdma2d_eval.Init.OutputOffset = 0;

  /* Foreground Configuration */
  hdma2d_eval.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d_eval.LayerCfg[1].InputAlpha = 0xFF;
  hdma2d_eval.LayerCfg[1].InputColorMode = DMA2D_INPUT_RGB565;
  hdma2d_eval.LayerCfg[1].InputOffset = 0;

  hdma2d_eval.Instance = DMA2D;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&hdma2d_eval) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&hdma2d_eval, 1) == HAL_OK)
    {
      if (HAL_DMA2D_Start(&hdma2d_eval, (uint32_t)pSrc, (uint32_t)pDst, 480, 1) == HAL_OK)
      {
        /* Polling For DMA transfer */
        HAL_DMA2D_PollForTransfer(&hdma2d_eval, 10);
        while (READ_REG(hdma2d_eval.Instance->CR) & 0x01);
      }
    }
  }
  else
  {
    /* FatFs Initialization Error */
    BR_Error_Handler(0x5000);
  }
}

void BSP_CAMERA_LineEventCallback(void)
{
  static uint32_t tmp, tmp2, counter;

  if(272 > counter)
  {
    LCD_LL_ConvertLineToARGB8888((uint16_t *)(STM_CAMERA_BUFFER + tmp), (uint16_t *)(SDRAM_BANK_ADDR + tmp2));
    tmp  = tmp + 960 ;
    tmp2 = tmp2 + 2048;
    counter++;
  }
  else
  {
    tmp = 0     ;
    tmp2 = 0    ;
    counter = 0 ;
  }
}


/**
  * @brief  Line event callback
  * @param  hdcmi: pointer to the DCMI handle
  */
void HAL_DCMI_LineEventCallback(DCMI_HandleTypeDef *hdcmi)
{
//  BSP_CAMERA_LineEventCallback();
}


void ov2640_init(uint32_t Resolution)
{
  if(ov2640_ReadID(OV2640_ADDR) == OV2640_ID)
  {
    /* Initialize the camera driver structure */
    camera_drv = &ov2640_drv;

    /* Camera Init */
    camera_drv->Init(OV2640_ADDR, Resolution);

    /* Return CAMERA_OK status */
  }

  current_resolution = Resolution;
}


void CAM_DrawCapture(void)
{
  uint8_t r,g,b ;
  uint32_t i,j  ;
  uint16_t c,*p ;
  p = (void *)STM_CAMERA_BUFFER ;
  for (i=0; i<272; i++)
  {
    for (j=0; j<480; j++)
    {
      c = *p++ ;
      if ((i & 0x01)==0)
         {
           b = c & 0xff ;
           g = (c >> 8) ;
           r = 0 ;
         }
        else
         {
           g = c & 0xff ;
           r = (c >> 8) ;
           b = 0 ;
         }
      BSP_LCD_DrawPixel(j,i,RGBConvert(r,g,b));
    }
  }
}




void CAM_main(void)
{
  static uint8_t t,x ;
    CAM_init();
    ov2640_init(RESOLUTION_R480x272);
//    ov7670_init();
    CAM_capture((uint8_t *)STM_CAMERA_BUFFER);
    while (1)
    {
      if (t != Global_TmTick) { t = Global_TmTick ; x++; }
      if (FlagFrame)
          {
            FlagFrame = 0  ;
            x        = 0 ;
          }
      if (x>100) { CAM_DrawCapture() ; CAM_capturesnapshot((uint8_t *)STM_CAMERA_BUFFER); x = 0 ; }
    }
}




#endif


