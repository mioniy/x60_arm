
#include "lwip/opt.h"
#include "main.h"
#if LWIP_DHCP
#include "lwip/dhcp.h"
#endif
#include "lwip/sys.h"
#include "app_ethernet.h"
#include "ethernetif.h"
#ifdef USE_LCD
#include "Log/lcd_log.h"
#endif

#ifndef __PRG_30__
extern void * DHCP_sem;
#endif
#if LWIP_DHCP
    #define MAX_DHCP_TRIES     4
    #define MAX_DHCP_TRIES_2   8
#endif

__IO uint8_t DHCP_state = DHCP_OFF;

uint8_t StatIp[4]= {IP_ADDR0 , IP_ADDR1 , IP_ADDR2 , IP_ADDR3 } ;
/* Private function prototypes -----------------------------------------------*/
byte SetStatIpArray(uint8_t *p);
void ClearStatIpArray(void);

void ethernet_link_status_updated(struct netif *netif)
{
  if (netif_is_up(netif))
 {
#if LWIP_DHCP
    DHCP_state = DHCP_START;
#endif
  }
  else
  {
#if LWIP_DHCP
    DHCP_state = DHCP_LINK_DOWN;
#endif
#if __PRG_30__
//    PRINT_TFT( TFT_DEV_C , ("The network cable is not connected \n") );
#endif
  }
}

#if LWIP_DHCP
void DHCP_Thread(void const * argument)
{
  struct netif *netif = (struct netif *) argument;
  static ip_addr_t ipaddr  ;
  static ip_addr_t netmask ;
  static ip_addr_t gw      ;
  static uint8_t   x       ;
  struct dhcp *dhcp ;
#ifdef __PRG_30__
  uint8_t iptxt[20];
#endif

  for (;;)
  {
    switch (DHCP_state)
    {
    case DHCP_START:
      {
        Global_LinkStat = 0 ;
        ip_addr_set_zero_ip4(&netif->ip_addr);
        ip_addr_set_zero_ip4(&netif->netmask);
        ip_addr_set_zero_ip4(&netif->gw);
        DHCP_state = DHCP_WAIT_ADDRESS;
#ifdef __PRG_30__
        PRINT_TFT( TFT_DEV_C , ("State: Looking for DHCP server ...\n") );
#endif
        ClearStatIpArray();
        dhcp_start(netif);
      }
      break;
    case DHCP_WAIT_ADDRESS:
      {
        Global_LinkStat = 0 ;
        Global_EthIpStat = DHCP_WAIT_PC ;
        if (dhcp_supplied_address(netif))
        {
          DHCP_state       = DHCP_ADDRESS_ASSIGNED    ;
          Global_EthIpStat = DHCP_ADDRESS_ASSIGNED_PC ;
          sprintf((char *)Global_EthIpString, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));

#ifdef __PRG_30__
          sprintf((char *)iptxt, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
          PRINT_TFT( TFT_DEV_C , ("IP address assigned by a DHCP server: %s\n", iptxt) );
#endif
#ifndef __PRG_30__
          sys_sem_signal(&DHCP_sem);
#endif
        }
        else
        {
          dhcp = (struct dhcp *)netif_get_client_data(netif, LWIP_NETIF_CLIENT_DATA_INDEX_DHCP);

          /* DHCP timeout */
          if (dhcp->tries > MAX_DHCP_TRIES)
          {
            if ((SetStatIpArray(StatIp) == 0) || (dhcp->tries > MAX_DHCP_TRIES_2))
               {
                  DHCP_state = DHCP_TIMEOUT;

                  /* Stop DHCP */
                  dhcp_stop(netif);

                  /* Static address used */
#ifdef __PRG_30__
#endif
#if (__CT_30__) || (__XC_30__)
                  StatIp[3] = 200 + Global_Kupa ;
                  if (StatIp[3]>=210) StatIp[3] = 209 ;
#endif
#ifdef __MP_30__
                  StatIp[3] = 210 + ((GetStmSW(1) ^ 0xff) & 0x1f) ;
#endif

                  IP_ADDR4(&ipaddr, StatIp[0] ,StatIp[1] , StatIp[2] , StatIp[3] );
                  IP_ADDR4(&netmask, NETMASK_ADDR0, NETMASK_ADDR1, NETMASK_ADDR2, NETMASK_ADDR3);
                  x = 1 ;
                  if ((StatIp[0] == 10) && (StatIp[1]==0) && (StatIp[2]==0)) x = 138 ;
                  IP_ADDR4(&gw, StatIp[0] , StatIp[1] , StatIp[2] , x );
                  netif_set_addr(netif, ip_2_ip4(&ipaddr), ip_2_ip4(&netmask), ip_2_ip4(&gw));
                  Global_EthIpStat = DHCP_ADDRESS_STATIC_PC ;
                  sprintf((char *)Global_EthIpString, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));

#ifdef __PRG_30__
                  sprintf((char *)iptxt, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
                  PRINT_TFT( TFT_DEV_C  , ("DHCP Timeout !! \n") );
                  PRINT_TFT( TFT_DEV_C , ("Static IP address: %s\n", iptxt) );
#endif
#ifndef __PRG_30__
                  sys_sem_signal(&DHCP_sem);
#endif
                }
          }
        }
      }
      break;
  case DHCP_LINK_DOWN:
    {
      /* Stop DHCP */
      Global_LinkStat = 0 ;
      dhcp_stop(netif);
      DHCP_state       = DHCP_OFF;
      Global_EthIpStat = DHCP_LINK_DOWN_PC ;
#ifdef __PRG_30__
      PRINT_TFT( TFT_DEV_C , ("The network cable is not connected \n") );
#endif
    }
           break;
    default:
           Global_LinkStat = 1 ;
           break;
    }

    /* wait 500 ms */
    osDelay(500);
  }
}
#endif  /* LWIP_DHCP */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
