

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "stm32h7xx_hal.h"
#include "lwip/timeouts.h"
#include "netif/ethernet.h"
#include "netif/etharp.h"
#include "lwip/stats.h"
#include "lwip/snmp.h"
#include "lwip/tcpip.h"
#include "ethernetif.h"
#include "lan8742.h"
#include <string.h>

#define    ETH_TIBA_LOGIC

#ifdef __PRG_30__
   #define    TXRX_DEBUG_PR
   extern uint8_t    Global_RxByte5  ;
   extern uint32_t   Global_GlobTm   ;
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* The time to block waiting for input. */
#define TIME_WAITING_FOR_INPUT                 ( osWaitForever )
/* Stack size of the interface thread */
#define INTERFACE_THREAD_STACK_SIZE            ( 350 )

/* Define those to better describe your network interface. */
#define IFNAME0 's'
#define IFNAME1 't'

#define ETH_RX_BUFFER_SIZE                     (1536UL)

#define ETH_DMA_TRANSMIT_TIMEOUT                (20U)
extern uint32_t Global_PacketIn ;
extern void *LINK_sem;

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/*
@Note: This interface is implemented to operate in zero-copy mode only:
        - Rx buffers are allocated statically and passed directly to the LwIP stack,
          they will return back to ETH DMA after been processed by the stack.
        - Tx Buffers will be allocated from LwIP stack memory heap,
          then passed to ETH HAL driver.

@Notes:
  1.a. ETH DMA Rx descriptors must be contiguous, the default count is 4,
       to customize it please redefine ETH_RX_DESC_CNT in stm32xxxx_hal_conf.h
  1.b. ETH DMA Tx descriptors must be contiguous, the default count is 4,
       to customize it please redefine ETH_TX_DESC_CNT in stm32xxxx_hal_conf.h

  2.a. Rx Buffers number must be between ETH_RX_DESC_CNT and 2*ETH_RX_DESC_CNT
  2.b. Rx Buffers must have the same size: ETH_RX_BUFFER_SIZE, this value must
       passed to ETH DMA in the init field (EthHandle.Init.RxBuffLen)
*/

#if defined ( __ICCARM__ ) /*!< IAR Compiler */

#pragma location=0x30040000
ETH_DMADescTypeDef  DMARxDscrTab[ETH_RX_DESC_CNT]; /* Ethernet Rx DMA Descriptors */
#pragma location=0x30040060
ETH_DMADescTypeDef  DMATxDscrTab[ETH_TX_DESC_CNT]; /* Ethernet Tx DMA Descriptors */
#pragma location=0x30040200
uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_RX_BUFFER_SIZE]; /* Ethernet Receive Buffers */

#elif defined ( __CC_ARM )  /* MDK ARM Compiler */

__attribute__((section(".RxDecripSection"))) ETH_DMADescTypeDef  DMARxDscrTab[ETH_RX_DESC_CNT]; /* Ethernet Rx DMA Descriptors */
__attribute__((section(".TxDecripSection"))) ETH_DMADescTypeDef  DMATxDscrTab[ETH_TX_DESC_CNT]; /* Ethernet Tx DMA Descriptors */
__attribute__((section(".RxArraySection"))) uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_RX_BUFFER_SIZE]; /* Ethernet Receive Buffer */

#elif defined ( __GNUC__ ) /* GNU Compiler */

ETH_DMADescTypeDef DMARxDscrTab[ETH_RX_DESC_CNT] __attribute__((section(".RxDecripSection"))); /* Ethernet Rx DMA Descriptors */
ETH_DMADescTypeDef DMATxDscrTab[ETH_TX_DESC_CNT] __attribute__((section(".TxDecripSection")));   /* Ethernet Tx DMA Descriptors */
uint8_t Rx_Buff[ETH_RX_DESC_CNT][ETH_RX_BUFFER_SIZE] __attribute__((section(".RxArraySection"))); /* Ethernet Receive Buffers */

#endif

ETH_HandleTypeDef EthHandle;
ETH_TxPacketConfig TxConfig;

lan8742_Object_t LAN8742;

osSemaphoreId RxPktSemaphore = NULL; /* Semaphore to signal incoming packets */

/* Private function prototypes -----------------------------------------------*/
void ScanInputP( ETH_BufferTypeDef *p , uint16_t len , uint8_t rt);
void ClearStatIpArray(void);
static void ethernetif_input( void const * argument );
static err_t low_level_output(struct netif *netif, struct pbuf *p);
u32_t    sys_now(void);
void     pbuf_free_custom(struct pbuf *p);
uint32_t SetUp_StrToIp(uint8_t *p);
uint32_t SetUp_StrToNo(uint8_t *p);

int32_t ETH_PHY_IO_Init(void);
int32_t ETH_PHY_IO_DeInit (void);
int32_t ETH_PHY_IO_ReadReg(uint32_t DevAddr, uint32_t RegAddr, uint32_t *pRegVal);
int32_t ETH_PHY_IO_WriteReg(uint32_t DevAddr, uint32_t RegAddr, uint32_t RegVal);
int32_t ETH_PHY_IO_GetTick(void);

uint8_t macaddress[6]= {DEFAULT_MAC_ADDRESS_1, DEFAULT_MAC_ADDRESS_2, DEFAULT_MAC_ADDRESS_3, DEFAULT_MAC_ADDRESS_4, DEFAULT_MAC_ADDRESS_5, DEFAULT_MAC_ADDRESS_6};

lan8742_IOCtx_t  LAN8742_IOCtx = {ETH_PHY_IO_Init,
                               ETH_PHY_IO_DeInit,
                               ETH_PHY_IO_WriteReg,
                               ETH_PHY_IO_ReadReg,
                               ETH_PHY_IO_GetTick};

LWIP_MEMPOOL_DECLARE(RX_POOL, 10, sizeof(struct pbuf_custom), "Zero-copy RX PBUF pool");

#define ARRAY_TEST_SIZE                 10
static uint8_t  IpArrayTst[ARRAY_TEST_SIZE][4]  ;
static uint32_t IpArrayCnt [ARRAY_TEST_SIZE]    ;
static uint32_t MaxIpCnt                       ;
static uint8_t  IpSourceTst[4]                  ;

uint CRCx16(uint32_t Data)
{
static const uint wCRCTable[] = {
0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };
  byte nTemp;
  uint wCRCWord = 0xFFFF;
   nTemp = (Data & 0xff) ^ wCRCWord;
   wCRCWord >>= 8;
   wCRCWord ^= wCRCTable[nTemp];
   Data >>= 8 ;
   nTemp = (Data & 0xff) ^ wCRCWord;
   wCRCWord >>= 8;
   wCRCWord ^= wCRCTable[nTemp];
   Data >>= 8 ;
   nTemp = (Data & 0xff) ^ wCRCWord;
   wCRCWord >>= 8;
   wCRCWord ^= wCRCTable[nTemp];
   Data >>= 8 ;
   nTemp = (Data & 0xff) ^ wCRCWord;
   wCRCWord >>= 8;
   wCRCWord ^= wCRCTable[nTemp];
   return wCRCWord;
}

/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
                       LL Driver Interface ( LwIP stack --> ETH)
*******************************************************************************/
/**
  * @brief In this function, the hardware should be initialized.
  * Called from ethernetif_init().
  *
  * @param netif the already initialized lwip network interface structure
  *        for this ethernetif
  */
static void low_level_init(struct netif *netif)
{
  uint32_t idx, duplex, speed = 0;
  int32_t PHYLinkState;
  ETH_MACConfigTypeDef MACConf;
  uint32_t n,x,x1,x2,x3 ;
  ClearStatIpArray();
  x1  = *(uint32_t *)0x1FF1E800 ;
  x2  = *(uint32_t *)0x1FF1E804 ;
  x3  = *(uint32_t *)0x1FF1E808 ;
  x   = CRCx16(x1    );
  x   = CRCx16(x2 + x);
  x   = CRCx16(x3 + x);
  n   = CRCx16(x );
  macaddress[3]  =  n         ;
  macaddress[3] &= 0x0f       ;
  macaddress[4]  = (x  >>  8) ;
  macaddress[5]  =  x         ;

#ifdef __PRG_30__
  {
    uint8_t  i ;
    PRINT_TFT( TFT_DEV_C , ("%X  %X  %X \n",x1,x2,x3 ) );
    PRINT_TFT( TFT_DEV_C , ("MAC address = ") );
    for (i=0; i<6; i++) PRINT_TFT( TFT_DEV_C , ("%02X ",macaddress[i]) );
    PRINT_TFT( TFT_DEV_C , ("\n") );
  }
#endif

  EthHandle.Instance = ETH;
  EthHandle.Init.MACAddr = macaddress;
  EthHandle.Init.MediaInterface = HAL_ETH_RMII_MODE;
  EthHandle.Init.RxDesc = DMARxDscrTab;
  EthHandle.Init.TxDesc = DMATxDscrTab;
  EthHandle.Init.RxBuffLen = ETH_RX_BUFFER_SIZE;

  /* configure ethernet peripheral (GPIOs, clocks, MAC, DMA) */
  HAL_ETH_Init(&EthHandle);

  /* set MAC hardware address length */
  netif->hwaddr_len = ETH_HWADDR_LEN;

  /* set MAC hardware address */
  netif->hwaddr[0] =  macaddress[0];
  netif->hwaddr[1] =  macaddress[1];
  netif->hwaddr[2] =  macaddress[2];
  netif->hwaddr[3] =  macaddress[3];
  netif->hwaddr[4] =  macaddress[4];
  netif->hwaddr[5] =  macaddress[5];

  /* maximum transfer unit */
  netif->mtu = 1500;

  /* device capabilities */
  /* don't set NETIF_FLAG_ETHARP if this device is not an ethernet one */
  netif->flags |= NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP;

  for(idx = 0; idx < ETH_RX_DESC_CNT; idx ++)
  {
    HAL_ETH_DescAssignMemory(&EthHandle, idx, Rx_Buff[idx], NULL);
  }

  /* Initialize the RX POOL */
  LWIP_MEMPOOL_INIT(RX_POOL);

  memset(&TxConfig, 0 , sizeof(ETH_TxPacketConfig));
  TxConfig.Attributes = ETH_TX_PACKETS_FEATURES_CSUM | ETH_TX_PACKETS_FEATURES_CRCPAD;
  TxConfig.ChecksumCtrl = ETH_CHECKSUM_IPHDR_PAYLOAD_INSERT_PHDR_CALC;
  TxConfig.CRCPadCtrl = ETH_CRC_PAD_INSERT;

  /* create a binary semaphore used for informing ethernetif of frame reception */
  RxPktSemaphore = xSemaphoreCreateBinary();

  /* create the task that handles the ETH_MAC */
  osThreadDef(EthIf, ethernetif_input, osPriorityRealtime, 0, INTERFACE_THREAD_STACK_SIZE);
  osThreadCreate (osThread(EthIf), netif);

  /* Set PHY IO functions */
  LAN8742_RegisterBusIO(&LAN8742, &LAN8742_IOCtx);

  /* Initialize the LAN8742 ETH PHY */
  LAN8742_Init(&LAN8742);

  PHYLinkState = LAN8742_GetLinkState(&LAN8742);

  /* Get link state */
  if(PHYLinkState <= LAN8742_STATUS_LINK_DOWN)
  {
    netif_set_link_down(netif);
    netif_set_down(netif);
#ifdef __PRG_30__
    Global_LinkStat = 0 ;
#endif
  }
  else
  {
    switch (PHYLinkState)
    {
    case LAN8742_STATUS_100MBITS_FULLDUPLEX:
      duplex = ETH_FULLDUPLEX_MODE;
      speed = ETH_SPEED_100M;
#ifdef __PRG_30__
      Global_LinkStat = 1 ;
#endif
      break;
    case LAN8742_STATUS_100MBITS_HALFDUPLEX:
      duplex = ETH_HALFDUPLEX_MODE;
      speed = ETH_SPEED_100M;
#ifdef __PRG_30__
      Global_LinkStat = 2 ;
#endif
      break;
    case LAN8742_STATUS_10MBITS_FULLDUPLEX:
      duplex = ETH_FULLDUPLEX_MODE;
      speed = ETH_SPEED_10M;
#ifdef __PRG_30__
      Global_LinkStat = 3 ;
#endif
      break;
    case LAN8742_STATUS_10MBITS_HALFDUPLEX:
      duplex = ETH_HALFDUPLEX_MODE;
      speed = ETH_SPEED_10M;
#ifdef __PRG_30__
      Global_LinkStat = 4 ;
#endif
      break;
    default:
      duplex = ETH_FULLDUPLEX_MODE;
      speed = ETH_SPEED_100M;
#ifdef __PRG_30__
      Global_LinkStat = 1 ;
#endif
      break;
    }

    /* Get MAC Config MAC */
    HAL_ETH_GetMACConfig(&EthHandle, &MACConf);
    MACConf.DuplexMode = duplex;
    MACConf.Speed = speed;
    HAL_ETH_SetMACConfig(&EthHandle, &MACConf);
    HAL_ETH_Start_IT(&EthHandle);
    netif_set_up(netif);
    netif_set_link_up(netif);
  }
}

/**
  * @brief Should allocate a pbuf and transfer the bytes of the incoming
  * packet from the interface into the pbuf.
  *
  * @param netif the lwip network interface structure for this ethernetif
  * @return a pbuf filled with the received packet (including MAC header)
  *         NULL on memory error
  */
static struct pbuf * low_level_input(struct netif *netif)
{
	struct pbuf *p = NULL;
	ETH_BufferTypeDef RxBuff;
	uint32_t framelength = 0;
	struct pbuf_custom* custom_pbuf;

	if(HAL_ETH_GetRxDataBuffer(&EthHandle, &RxBuff) == HAL_OK)
	{
		HAL_ETH_GetRxDataLength(&EthHandle, &framelength);

		/* Build Rx descriptor to be ready for next data reception */
		HAL_ETH_BuildRxDescriptors(&EthHandle);

		/* Invalidate data cache for ETH Rx Buffers */
		SCB_InvalidateDCache_by_Addr((uint32_t *)RxBuff.buffer, framelength);
#if 0 /* Old code */
		custom_pbuf = (struct pbuf_custom*)LWIP_MEMPOOL_ALLOC(RX_POOL);
		custom_pbuf->custom_free_function = pbuf_free_custom;
		p = pbuf_alloced_custom(PBUF_RAW, framelength, PBUF_REF, custom_pbuf, RxBuff.buffer, ETH_RX_BUFFER_SIZE);
#else /* New code */
		p = pbuf_alloc(PBUF_RAW, framelength, PBUF_POOL);
		if (p)
		{
			pbuf_take(p, RxBuff.buffer, framelength);
		}
#endif
	}

	return p;
}


/**
  * @brief Should be called at the beginning of the program to set up the
  * network interface. It calls the function low_level_init() to do the
  * actual setup of the hardware.
  *
  * This function should be passed as a parameter to netif_add().
  *
  * @param netif the lwip network interface structure for this ethernetif
  * @return ERR_OK if the loopif is initialized
  *         ERR_MEM if private data couldn't be allocated
  *         any other err_t on error
  */
err_t ethernetif_init(struct netif *netif)
{
  LWIP_ASSERT("netif != NULL", (netif != NULL));

#if LWIP_NETIF_HOSTNAME
  /* Initialize interface hostname */
  netif->hostname = "lwip";
#endif /* LWIP_NETIF_HOSTNAME */

  /*
   * Initialize the snmp variables and counters inside the struct netif.
   * The last argument should be replaced with your link speed, in units
   * of bits per second.
   */
  MIB2_INIT_NETIF(netif, snmp_ifType_ethernet_csmacd, LINK_SPEED_OF_YOUR_NETIF_IN_BPS);

  netif->name[0] = IFNAME0;
  netif->name[1] = IFNAME1;

  /* We directly use etharp_output() here to save a function call.
   * You can instead declare your own function an call etharp_output()
   * from it if you have to do some checks before sending (e.g. if link
   * is available...) */
  netif->output = etharp_output;
  netif->linkoutput = low_level_output;

  /* initialize the hardware */
  low_level_init(netif);

  return ERR_OK;
}

/**
  * @brief  Custom Rx pbuf free callback
  * @param  pbuf: pbuf to be freed
  * @retval None
  */
void pbuf_free_custom(struct pbuf *p)
{
  struct pbuf_custom* custom_pbuf = (struct pbuf_custom*)p;
  /* invalidate data cache: lwIP and/or application may have written into buffer */
  SCB_InvalidateDCache_by_Addr((uint32_t *)p->payload, p->tot_len);
  LWIP_MEMPOOL_FREE(RX_POOL, custom_pbuf);
}

/**
  * @brief  Returns the current time in milliseconds
  *         when LWIP_TIMERS == 1 and NO_SYS == 1
  * @param  None
  * @retval Current Time value
  */
u32_t sys_now(void)
{
  return HAL_GetTick();
}

/*******************************************************************************
                       Ethernet MSP Routines
*******************************************************************************/
/**
  * @brief  Initializes the ETH MSP.
  * @param  heth: ETH handle
  * @retval None
*/
void HAL_ETH_MspInit(ETH_HandleTypeDef *heth)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /* Ethernett MSP init: RMII Mode */

  /* Enable GPIOs clocks */
   __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    /**ETH GPIO Configuration
    PC1     ------> ETH_MDC
    PA1     ------> ETH_REF_CLK
    PA2     ------> ETH_MDIO
    PA7     ------> ETH_CRS_DV
    PC4     ------> ETH_RXD0
    PC5     ------> ETH_RXD1
    PG11     ------> ETH_TX_EN
    PG12     ------> ETH_TXD1
    PG13     ------> ETH_TXD0
    */
    GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* Enable the Ethernet global Interrupt */
  HAL_NVIC_SetPriority(ETH_IRQn, 0x7, 0);
  HAL_NVIC_EnableIRQ(ETH_IRQn);

  /* Enable Ethernet clocks */
  __HAL_RCC_ETH1MAC_CLK_ENABLE();
  __HAL_RCC_ETH1TX_CLK_ENABLE();
  __HAL_RCC_ETH1RX_CLK_ENABLE();
}

/**
  * @brief  Ethernet Rx Transfer completed callback
  * @param  heth: ETH handle
  * @retval None
  */
void HAL_ETH_RxCpltCallback(ETH_HandleTypeDef *heth)
{
  osSemaphoreRelease(RxPktSemaphore);
}

/*******************************************************************************
                       PHI IO Functions
*******************************************************************************/
/**
  * @brief  Initializes the MDIO interface GPIO and clocks.
  * @param  None
  * @retval 0 if OK, -1 if ERROR
  */
int32_t ETH_PHY_IO_Init(void)
{
  /* We assume that MDIO GPIO configuration is already done
     in the ETH_MspInit() else it should be done here
  */

  /* Configure the MDIO Clock */
  HAL_ETH_SetMDIOClockRange(&EthHandle);

  return 0;
}

/**
  * @brief  De-Initializes the MDIO interface .
  * @param  None
  * @retval 0 if OK, -1 if ERROR
  */
int32_t ETH_PHY_IO_DeInit (void)
{
  return 0;
}

/**
  * @brief  Read a PHY register through the MDIO interface.
  * @param  DevAddr: PHY port address
  * @param  RegAddr: PHY register address
  * @param  pRegVal: pointer to hold the register value
  * @retval 0 if OK -1 if Error
  */
int32_t ETH_PHY_IO_ReadReg(uint32_t DevAddr, uint32_t RegAddr, uint32_t *pRegVal)
{
  if(HAL_ETH_ReadPHYRegister(&EthHandle, DevAddr, RegAddr, pRegVal) != HAL_OK)
  {
    return -1;
  }

  return 0;
}

/**
  * @brief  Write a value to a PHY register through the MDIO interface.
  * @param  DevAddr: PHY port address
  * @param  RegAddr: PHY register address
  * @param  RegVal: Value to be written
  * @retval 0 if OK -1 if Error
  */
int32_t ETH_PHY_IO_WriteReg(uint32_t DevAddr, uint32_t RegAddr, uint32_t RegVal)
{
  if(HAL_ETH_WritePHYRegister(&EthHandle, DevAddr, RegAddr, RegVal) != HAL_OK)
  {
    return -1;
  }

  return 0;
}

/**
  * @brief  Get the time in millisecons used for internal PHY driver process.
  * @retval Time value
  */
int32_t ETH_PHY_IO_GetTick(void)
{
  return HAL_GetTick();
}

/**
  * @brief  Check the ETH link state and update netif accordingly.
  * @param  argument: netif
  * @retval None
  */
void ethernet_link_thread( void const * argument )
{
  static ETH_MACConfigTypeDef MACConf;
  static int32_t PHYLinkState, prevPHYLinkState;
  static uint32_t linkchanged = 0, speed = 0, duplex =0;
  struct netif *netif = (struct netif *) argument;

  for(;;)
  {

    PHYLinkState = LAN8742_GetLinkState(&LAN8742);

    if (prevPHYLinkState != PHYLinkState)
    {
       prevPHYLinkState = PHYLinkState;
       if (PHYLinkState > LAN8742_STATUS_LINK_DOWN) sys_sem_signal(&LINK_sem);
    }

    if(netif_is_link_up(netif) && (PHYLinkState <= LAN8742_STATUS_LINK_DOWN))
    {
      HAL_ETH_Stop_IT(&EthHandle);
      netif_set_down(netif);
      netif_set_link_down(netif);
      Global_LinkStat = 0 ;
    }
    else if(!netif_is_link_up(netif) && (PHYLinkState > LAN8742_STATUS_LINK_DOWN))
    {
      switch (PHYLinkState)
      {
      case LAN8742_STATUS_100MBITS_FULLDUPLEX:
        duplex = ETH_FULLDUPLEX_MODE;
        speed = ETH_SPEED_100M;
        linkchanged = 1;
#ifdef __PRG_30__
        Global_LinkStat = 1 ;
#endif
        break;
      case LAN8742_STATUS_100MBITS_HALFDUPLEX:
        duplex = ETH_HALFDUPLEX_MODE;
        speed = ETH_SPEED_100M;
        linkchanged = 1;
#ifdef __PRG_30__
        Global_LinkStat = 2 ;
#endif
        break;
      case LAN8742_STATUS_10MBITS_FULLDUPLEX:
        duplex = ETH_FULLDUPLEX_MODE;
        speed = ETH_SPEED_10M;
        linkchanged = 1;
#ifdef __PRG_30__
        Global_LinkStat = 3 ;
#endif
        break;
      case LAN8742_STATUS_10MBITS_HALFDUPLEX:
        duplex = ETH_HALFDUPLEX_MODE;
        speed = ETH_SPEED_10M;
        linkchanged = 1;
#ifdef __PRG_30__
        Global_LinkStat = 4 ;
#endif
        break;
      default:
#ifdef __PRG_30__
        Global_LinkStat = 5 ;
#endif
        break;
      }

      if(linkchanged)
      {
        /* Get MAC Config MAC */
        HAL_ETH_GetMACConfig(&EthHandle, &MACConf);
        MACConf.DuplexMode = duplex;
        MACConf.Speed = speed;
        HAL_ETH_SetMACConfig(&EthHandle, &MACConf);
        HAL_ETH_Start_IT(&EthHandle);
        netif_set_up(netif);
        netif_set_link_up(netif);
      }
    }

    osDelay(100);
  }
}

//static void low_level_output_tiba(uint8_t *p , uint16_t Len)
//{
//  ETH_BufferTypeDef Txbuffer[ETH_TX_DESC_CNT];
//
//  memset(Txbuffer, 0 , ETH_TX_DESC_CNT*sizeof(ETH_BufferTypeDef));
//
//  Txbuffer[0].buffer = p        ;
//  Txbuffer[0].len    = Len      ;
//  Txbuffer[0].next   = NULL     ;
//  TxConfig.Length    = Len      ;
//  TxConfig.TxBuffer  = Txbuffer ;
//  HAL_ETH_Transmit(&EthHandle, &TxConfig, ETH_DMA_TRANSMIT_TIMEOUT);
//}
//
//void Tiba_MakeTxPack(uint8_t b)
//{
//  PacketBuf[TxPackPoi++] = b ;
//}
//
//
//void Tiba_PacketLogic(uint32_t Data)
//{
//  TxPackPoi = 0;
//  if (Data == 1000)
//  {
//    Tiba_MakeTxPack(0xff);
//    Tiba_MakeTxPack(0xff);
//    Tiba_MakeTxPack(0xff);
//    Tiba_MakeTxPack(0xff);
//    Tiba_MakeTxPack(0xff);
//    Tiba_MakeTxPack(0xff);
//
//    Tiba_MakeTxPack(macaddress[0]);
//    Tiba_MakeTxPack(macaddress[1]);
//    Tiba_MakeTxPack(macaddress[2]);
//    Tiba_MakeTxPack(macaddress[3]);
//    Tiba_MakeTxPack(macaddress[4]);
//    Tiba_MakeTxPack(macaddress[5]);
//
//    Tiba_MakeTxPack(0x08);
//    Tiba_MakeTxPack(0x00);
//
//    Tiba_MakeTxPack(0x45);  // Version[4]:IHL[4]
//    Tiba_MakeTxPack(0x00);  // DSCP[6]:ECN[2]
//    Tiba_MakeTxPack(0x00);  // Len
//    Tiba_MakeTxPack(0x30);  // Len
//
//    Tiba_MakeTxPack(0x00);  // Identification
//    Tiba_MakeTxPack(0xdf);  // Identification
//
//    Tiba_MakeTxPack(0x00);  // Flags[3]:Fragment Offset[5]
//    Tiba_MakeTxPack(0x00);  // Fragment Offset
//
//    Tiba_MakeTxPack(0xff);  // TTL   Time To Live
//    Tiba_MakeTxPack(0x11);  // Protocol    UDP=11
//
//    Tiba_MakeTxPack(0x00);  // CRC
//    Tiba_MakeTxPack(0x00);  // CRC
//
//    Tiba_MakeTxPack(0xff);  // IP Source
//    Tiba_MakeTxPack(0xff);  // IP Source
//    Tiba_MakeTxPack(0xff);  // IP Source
//    Tiba_MakeTxPack(0xff);  // IP Source
//
//    Tiba_MakeTxPack(0xff);  // IP Destination
//    Tiba_MakeTxPack(0xff);  // IP Destination
//    Tiba_MakeTxPack(0xff);  // IP Destination
//    Tiba_MakeTxPack(0xff);  // IP Destination
//
//    Tiba_MakeTxPack(0xf0);  // Port
//    Tiba_MakeTxPack(0xf0);  // Port
//
//    Tiba_MakeTxPack(0xf0);  // Port
//    Tiba_MakeTxPack(0xf0);  // Port
//
//    Tiba_MakeTxPack(0x00);  // Length
//    Tiba_MakeTxPack(0x1c);  // Length
//
//    Tiba_MakeTxPack(0x00);  // CRC
//    Tiba_MakeTxPack(0x00);  // CRC
//
//    Tiba_MakeTxPack(0x11);  // Data
//    Tiba_MakeTxPack(0x12);  // Data
//    Tiba_MakeTxPack(0x13);  // Data
//    Tiba_MakeTxPack(0x14);  // Data
//    Tiba_MakeTxPack(0x11);  // Data
//    Tiba_MakeTxPack(0x12);  // Data
//    Tiba_MakeTxPack(0x13);  // Data
//    Tiba_MakeTxPack(0x14);  // Data
//    Tiba_MakeTxPack(0x11);  // Data
//    Tiba_MakeTxPack(0x12);  // Data
//
//    low_level_output_tiba( PacketBuf , TxPackPoi );
//
//  }
//}


/**
  * @brief This function should do the actual transmission of the packet. The packet is
  * contained in the pbuf that is passed to the function. This pbuf
  * might be chained.
  *
  * @param netif the lwip network interface structure for this ethernetif
  * @param p the MAC packet to send (e.g. IP packet including MAC addresses and type)
  * @return ERR_OK if the packet could be sent
  *         an err_t value if the packet couldn't be sent
  *
  * @note Returning ERR_MEM here if a DMA queue of your MAC is full can lead to
  *       strange results. You might consider waiting for space in the DMA queue
  *       to become available since the stack doesn't retry to send a packet
  *       dropped because of memory failure (except for the TCP timers).
  */
static err_t low_level_output(struct netif *netif, struct pbuf *p)
{
  uint32_t i=0, framelen = 0;
  struct pbuf *q;
  err_t errval = ERR_OK;
  static ETH_BufferTypeDef Txbuffer[ETH_TX_DESC_CNT];

  memset(Txbuffer, 0 , ETH_TX_DESC_CNT*sizeof(ETH_BufferTypeDef));

  for(q = p; q != NULL; q = q->next)
  {
    if (i >= ETH_TX_DESC_CNT)
       {
         return ERR_IF;
       }

    Txbuffer[i].buffer = q->payload;
    Txbuffer[i].len = q->len;
    framelen += q->len;
    if(i>0)
    {
      Txbuffer[i-1].next = &Txbuffer[i];
    }

    if(q->next == NULL)
    {
      Txbuffer[i].next = NULL;
    }

    i++;
  }
  TxConfig.Length   = framelen ;
  TxConfig.TxBuffer = Txbuffer ;
#ifdef ETH_TIBA_LOGIC
//  ScanInputP(Txbuffer,framelen,2);
#endif
  SCB_CleanInvalidateDCache();
  HAL_ETH_Transmit(&EthHandle, &TxConfig, ETH_DMA_TRANSMIT_TIMEOUT);

  return errval;
}

/**
  * @brief This function is the ethernetif_input task, it is processed when a packet
  * is ready to be read from the interface. It uses the function low_level_input()
  * that should handle the actual reception of bytes from the network
  * interface. Then the type of the received packet is determined and
  * the appropriate input function is called.
  *
  * @param netif the lwip network interface structure for this ethernetif
  */
void ethernetif_input( void const * argument )
{
  static ETH_BufferTypeDef Rxbuffer[1];
  struct pbuf *p;
  struct netif *netif = (struct netif *) argument;
  for( ;; )
  {
    if (osSemaphoreWait( RxPktSemaphore, TIME_WAITING_FOR_INPUT)==osOK)
    {
      do
      {
        LOCK_TCPIP_CORE();
        p = low_level_input( netif );
        if (p != NULL)
        {

          Global_PacketIn++;
          Rxbuffer[0].buffer = p->payload;
          Rxbuffer[0].len    = p->len;
          Rxbuffer[0].next   = NULL;
          ScanInputP(Rxbuffer,p->len,1);
          if (netif->input( p, netif) != ERR_OK )
          {
            pbuf_free(p);
          }
        }
        SCB_CleanInvalidateDCache();

        UNLOCK_TCPIP_CORE();

      }while(p!=NULL);
    }
  }
}

// --------------------------------------------------------------------------
/*
@44D5F20D54C6_TYP_MP
@44D5F20D54C6_DEV_001
@44D5F20D54C6_PRT_SANEI
@44D5F20D54C6_000_192.168.4.76
@44D5F20D54C6_001_255.255.255.0
@44D5F20D54C6_002_192.168.4.1
@44D5F20D54C6_003_20002
@44D5F20D54C6_004_106
@44D5F20D54C6_005_192.168.4.80
@44D5F20D54C6_006_106
@44D5F20D54C6_007_1234
@44D5F20D54C6_008_106
@44D5F20D54C6_009_102
@44D5F20D54C6_010_100
@44D5F20D54C6_MET_010000fd8020.00$ Month
@44D5F20D54C6_MET_010000fdc010.00$ Daily
@44D5F20D54C6_999_000
*/
void ProcessTibaMACcommand(uint8_t *Pb , uint16_t size)
{
 static uint16_t  i,j           ;
 static uint8_t   b1,b2         ;
 static uint8_t   m1,m2,m3      ;
 static uint32_t  k , k1 , k2   ;
 static uint8_t   *Pbk          ;
 static uint8_t   fmac          ;
  fmac = 1 ;
  b1 = Pb[0]; if (b1>'9') b1 -= 7 ;
  b2 = Pb[1]; if (b2>'9') b2 -= 7 ;
  m1 = (b1 & 0x0f) * 0x10 + (b2 & 0x0f) ;
  b1 = Pb[2]; if (b1>'9') b1 -= 7 ;
  b2 = Pb[3]; if (b2>'9') b2 -= 7 ;
  m2 = (b1 & 0x0f) * 0x10 + (b2 & 0x0f) ;
  b1 = Pb[4]; if (b1>'9') b1 -= 7 ;
  b2 = Pb[5]; if (b2>'9') b2 -= 7 ;
  m3 = (b1 & 0x0f) * 0x10 + (b2 & 0x0f) ;
  if ((m1 != macaddress[3]) || (m2 != macaddress[4]) || (m3 != macaddress[5]))
     {
       if ((m1 != 0) || (m2 != 0) || (m3 != 0)) return ;
       fmac = 0 ;
     }

  if ((Pb[7]=='T') && (Pb[8]=='Y') && (Pb[9]=='P'))            // TYP
     {
       k = 0 ;
       if ((Pb[11]=='M') && (Pb[12]=='P')) k = '1' ;
       if ((Pb[11]=='S') && (Pb[12]=='W')) k = '2' ;
       if ((Pb[11]=='C') && (Pb[12]=='R')) k = '3' ;
       if ((Pb[11]=='C') && (Pb[12]=='P')) k = '4' ;
       if ((Pb[11]=='M') && (Pb[12]=='C')) k = '5' ;
       if ((Pb[11]=='C') && (Pb[12]=='T')) k = '6' ;
       if ((Pb[11]=='A') && (Pb[12]=='E')) k = '5' ; // APS/POF

#ifdef   __POF_60__
       k =  '5';
#endif
       SetRtcMem( RTC_BKP_DR2 , 0x5aa5 ) ;
       SetRtcMem( RTC_BKP_DR3 , k      ) ;
       SetRtcMem( RTC_BKP_DR4 , 0      ) ;
       SetRtcMem( RTC_BKP_DR5 , 0      ) ;
       return ;
     }
  if ((Pb[7]=='D') && (Pb[8]=='E') && (Pb[9]=='V'))            // DEV
     {
       k  = (Pb[11] & 0x0f) ;
       k *= 10              ;
       k += (Pb[12] & 0x0f) ;
       k *= 10              ;
       k += (Pb[13] & 0x0f) ;
       if (k<64)
          {
            SetRtcMem( RTC_BKP_DR4 , k ) ;
          }
       return ;
     }
  if ((Pb[7]=='C') && (Pb[8]=='T') && (Pb[9]=='P'))            // CTP       Touch PANEL
     {
       k  = (Pb[11] & 0x0f) ;
       k *= 10              ;
       k += (Pb[12] & 0x0f) ;
       k *= 10              ;
       k += (Pb[13] & 0x0f) ;
       SetRtcMem( RTC_BKP_DR8 , k ) ;
       return ;
     }
  if ((Pb[7]=='P') && (Pb[8]=='R') && (Pb[9]=='T'))            // PRT       Printer TYPE
     {
       k = 0 ;
       if ((Pb[11]=='T') && (Pb[12]=='I')) k = '1' ;
       if ((Pb[11]=='S') && (Pb[12]=='A')) k = '2' ;
       if (k)
          {
            SetRtcMem( RTC_BKP_DR5 , k ) ;
          }
       return ;
     }
  if ((Pb[7]=='B') && (Pb[8]=='A') && (Pb[9]=='U'))            // BAU
     {
       b1 = Pb[10] ;
       k  = Pb[11] ;
       if (b1 == '1') SetRtcMem( RTC_BKP_DR11 , k ) ;         // '1'    Printer
       if (b1 == '2') SetRtcMem( RTC_BKP_DR12 , k ) ;         // '2'    Scanner
     }
  if ((Pb[7]=='B') && (Pb[8]=='T') && (Pb[9]=='M'))            // BTM - Bit Map for BKUP_16
     {
        b1  = (Pb[11] & 0x0f) ;
       b1 *= 10              ;
       b1 += (Pb[12] & 0x0f) ;
       b2  = (Pb[13] & 0x0f) ;
       b2 *= 10              ;
       b2 += (Pb[14] & 0x0f) ;
       k   = b2              ;
       k  *= 0x100           ;
       k  += b1              ;
        SetRtcMem( RTC_BKP_DR16 , k ) ;
        return;
     }
  if ((Pb[7]=='V') && (Pb[8]=='O') && (Pb[9]=='L'))           // Volume SPEAKER
     {
       b1  = (Pb[11] & 0x0f) ;
       b1 *= 10              ;
       b1 += (Pb[12] & 0x0f) ;
       b2  = (Pb[13] & 0x0f) ;
       b2 *= 10              ;
       b2 += (Pb[14] & 0x0f) ;
       k   = b2              ;
       k  *= 0x100           ;
       k  += b1              ;
       SetRtcMem( RTC_BKP_DR10 , k ) ;
       return ;
     }
  if ((Pb[7]=='V') && (Pb[8]=='O') && (Pb[9]=='A'))           // Volume SPEAKER hardware BRD
     {
       k  = (Pb[11] & 0x0f) ;
       k *= 10              ;
       k += (Pb[12] & 0x0f) ;
       if ((Pb[13]>='0') && (Pb[13]<='9'))
          {
            k *= 10              ;
            k += (Pb[13] & 0x0f) ;
          }
       SetRtcMem( RTC_BKP_DR13 , k ) ;                        //  Volume
       return ;
     }
  if ((Pb[7]=='I') && (Pb[8]=='T') && (Pb[9]=='M'))           // ITM   Intercom Timers   _ITM_AABBCC  AA-ReRegister , BB-Ring TimeOut , CC-      DD-55
     {
       b1  = (Pb[11] & 0x0f) ;
       b1 *= 10              ;
       b1 += (Pb[12] & 0x0f) ;
       b2  = (Pb[13] & 0x0f) ;
       b2 *= 10              ;
       b2 += (Pb[14] & 0x0f) ;
       k   = b1              ;
       k  *= 100            ;
       k  += b2              ;
       SetRtcMem( RTC_BKP_DR14 , k ) ;                        // Intercom TIMERS
       return ;
     }
  if ((Pb[7]=='I') && (Pb[8]=='F') && (Pb[9]=='G'))           // Intercom Flags
     {
       b1  = (Pb[11] & 0x0f) ;
       k  = b1 ;
       SetRtcMem( RTC_BKP_DR15 , k ) ;                        // Intercom Flags
       return ;
     }
  if ((Pb[7]=='C') && (Pb[8]=='L') && (Pb[9]=='R'))           // Clear MEM
     {
       if (fmac)  // Only work with Direct Address
          {
            memset((void *)BACKUP_BASE_ADDR_SETUP , 0 , 0xC00 );    // Clear SetUp Mem only
            for (i=2; i<8 ; i++) SetRtcMem( i , k ) ;
            for (i=9; i<32; i++) SetRtcMem( i , k ) ;
          }
       return ;
     }
  if (((Pb[7]>='0') && (Pb[7]<='9')) && ((Pb[8]>='0') && (Pb[8]<='9')) && ((Pb[9]>='0') && (Pb[9]<='9')))
     {
       k  = (Pb[7] & 0x0f) ;
       k *= 10             ;
       k += (Pb[8] & 0x0f) ;
       k *= 10             ;
       k += (Pb[9] & 0x0f) ;
       Global_SendFld = k ;
       if (k<SETUP_MAX_FIELDS)
          {
            *(uint32_t *)_B_MagicNo = SETUP_FILED_MAGIC_VER2 ;
            Pbk = (uint8_t *)(BACKUP_BASE_ADDR_SETUP + k * SETUP_FIELDS_WIDTH) ;
            memset((uint8_t *)Pbk , 0 , SETUP_FIELDS_WIDTH );
            i = 11 ;
            j = 0 ;
            while ((i<size) && (j<(SETUP_FIELDS_WIDTH - 1)))
            {
              if (Pb[i+2] > ' ')
                 {
                   if (Pb[i]>=' ') Pbk[j] = Pb[i] ;
                              else i      = size  ;
                   j++;
                 }
                else
                 {
                   i = size ;
                 }
              i++;
            }
            Pbk[SETUP_FIELDS_WIDTH-1] = 0 ;
          }
        else
          {
            if ((k>=100) && (k<199))
               {
                  k1 = 0 ;
                  i = 11 ;
                  while ((i<size) && (i<40))
                  {
                    if (Pb[i]>=' ') k1 = k1*10 + (Pb[i] & 0x0f) ;
                               else i  = size  ;
                    i++;
                  }
                  *(uint32_t *)(BACKUP_BASE_ADDR + (k-100)*4) = k1 ;
               }
          }
     }
}

void ScanInputP( ETH_BufferTypeDef *p , uint16_t len , uint8_t rt)
{
 static uint8_t   f       , *pi   , Protocol ;
 static uint16_t  pType   , cType            ;
 static uint16_t  srcPort , destPort         ;
 static uint16_t  ulen    , i                ;
 uint32_t 			curr_ip;
   pType = 0 ;



   if (rt == 1)
      {
        if (len > 34)
        {
          pi       = p->buffer               ;
          pType    = pi[12] * 0x100 + pi[13] ;
          cType    = pi[14] * 0x100 + pi[15] ;
          Protocol = pi[23]                  ;
          i = 0 ;
          if (pType == 0x800)
             {
               i = 26 ;
               srcPort  = pi[34] * 0x100 + pi[35] ;
               destPort = pi[36] * 0x100 + pi[37] ;
#ifndef __PRG_30__
               if (destPort == Global_SetUpIntrcomRtpPort) Global_RtpCnt++;
#endif
             }
          if ((pType == 0x806) && (cType == 0x0001)) i = 28 ;
          if (i)
          {
            if (Global_EthIpStat == DHCP_WAIT_PC)
               {
                 IpSourceTst[0] = pi[i++] ;
                 IpSourceTst[1] = pi[i++] ;
                 IpSourceTst[2] = pi[i++] ;
                 IpSourceTst[3] = pi[i++] ;
                 if ((IpSourceTst[0]) || (IpSourceTst[1]) || (IpSourceTst[2]) || (IpSourceTst[3]))
                   {
                     f = 0 ;
                      for (i=0; i<ARRAY_TEST_SIZE; i++)
                      {
                        if ((IpArrayTst[i][0] == IpSourceTst[0]) && (IpArrayTst[i][1] == IpSourceTst[1]) && (IpArrayTst[i][2] == IpSourceTst[2]))
                        {
                          f = 1 ;
                          IpArrayCnt[i]++;
                          if (MaxIpCnt<IpArrayCnt[i]) MaxIpCnt = IpArrayCnt[i] ;
                          i = ARRAY_TEST_SIZE ;
                        }
                      }
                      if (f == 0)
                      {
                        for (i=0; i<ARRAY_TEST_SIZE; i++)
                        {
                          if ((IpArrayTst[i][0] == 0) && (IpArrayTst[i][1] == 0) && (IpArrayTst[i][2] == 0))
                          {
                             IpArrayTst[i][0] = IpSourceTst[0] ;
                             IpArrayTst[i][1] = IpSourceTst[1] ;
                             IpArrayTst[i][2] = IpSourceTst[2] ;
                             IpArrayCnt[i] = 1 ;
                             if (MaxIpCnt==0) MaxIpCnt = 1 ;
                             i = ARRAY_TEST_SIZE ;
                          }
                        }
                      }
                   }
               }
          }
// ff.ff.ff.ff.ff.ff  00.e0.4c.68.1b.c5  08.00  45.00.00.36.a8.a9.00.00.80.11.cc.cd  c0.a8.04.98 - ff.ff.ff.ff
// e6.66.03.e9.00.22.af.2b.0d.23.40.34.34.44.35.46.32.30.44.35.34.43.36.5f.30.30.38.5f.35.38.33.30.38.0d.
#ifdef __PRG_30__
        if (len >= 60)
  #else
        if ((Global_SysReady == 0x55) && (len >= 60))
#endif
           {
             if (pType == 0x800)
             {
               ulen   = pi[14] * 0x100 + pi[15] ;  // - 0x14
               if (ulen > 0x30)
                  {
                    ulen -= 0x16 ;
                    i = 0 ;
                    if (Protocol == 0x06) // TCP
                       {
                         ulen -= 8       ;
                         ulen -= 9       ;
                         i     = 0x2a+11 ;
                       }
                    if (Protocol == 0x11) // UDP
                       {
                         ulen -= 8    ;
                         i     = 0x2a ;
                       }
                    if (i)
                       {
                         if (pi[i] < '!') { i++; ulen--; }
                         if (pi[i] < '!') { i++; ulen--; }
                         if ((pi[i] == '#') && (pi[i+1] == '@') && (pi[i+2] == '4') && (pi[i+3] == '4') && (pi[i+4] == 'D') && (pi[i+5] == '5') && (pi[i+6] == 'F') && (pi[i+7] == '2') && (pi[i+8] == '0') && (pi[i+14] == '_'))
                            {
                              if (ulen > 17)
                                 {
                                   ulen -= 8 ;
                                   ProcessTibaMACcommand(&pi[i+8],ulen);
                                 }
                            }
                       }
                  }
             }
           }
        }
      }
#ifdef TXRX_DEBUG_PR
   {
     static uint32_t j,k ;
      f = 0 ;
      if (Global_RxByte5 == 'a')              f = 1 ;
      if ((Global_RxByte5 == 'r') && (rt==1)) f = 1 ;
      if ((Global_RxByte5 == 't') && (rt==2)) f = 1 ;
      if (Global_RxByte5 == 'A')              f = 2 ;
      if ((Global_RxByte5 == 'R') && (rt==1)) f = 2 ;
      if ((Global_RxByte5 == 'T') && (rt==2)) f = 2 ;
      if ((Global_RxByte5 == 'S') || (Global_RxByte5 == 's'))
         {
           pi = p->buffer ;
           if (pType != 0x800) f = 1 ;
           if (rt==2) f = 1 ;
           if ((rt==1) && (pi[29] < 0xfa)) f = 1 ;
           if ((Global_RxByte5 == 'S') && (f)) f = 2 ;
         }
      if (f)
         {
           k = Global_GlobTm ;
           PRINT_TFT( COM5_DEV , ("%6d" , (k/1000)));
           PRINT_TFT( COM5_DEV , (".%03d ",(k % 1000)));
           switch (rt)
           {
             case 1 : PRINT_TFT( COM5_DEV , ("RX =") ); break ;
             case 2 : PRINT_TFT( COM5_DEV , ("TX =") ); break ;
           }
           PRINT_TFT( COM5_DEV , (" %4d ", len) );
           pi   = p->buffer ;
           i    = 0 ;
           j    = 0 ;
           ulen = len ;
           if ((f!=2) && (ulen>64)) ulen = 64 ;
           while (i<ulen)
           {
             if ( j >= p->len)
                {
                  p   = p->next   ;
                  if (p != NULL)
                     {
                       pi  = p->buffer ;
                       j   = 0         ;
                     }
                    else
                     {
                       PRINT_TFT( COM5_DEV , ("\n") );
                       return ;
                     }
                }
             PRINT_TFT( COM5_DEV , ("%02x",pi[j] ) );
             switch (i)
             {
               case 5  :
               case 11 :
               case 13 :
               case 25 :
               case 33 :
                       PRINT_TFT( COM5_DEV , ("  ") );
                       break ;
               case 29 :
                       PRINT_TFT( COM5_DEV , (" - ") );
                       break ;
               default :
                       PRINT_TFT( COM5_DEV , (".") );
                       break ;
             }
             j++ ;
             i++ ;
           }
           PRINT_TFT( COM5_DEV , ("\n") );
         }
   }
#endif
}

void ClearStatIpArray(void)
{
 static uint8_t i ;
  for (i=0; i<ARRAY_TEST_SIZE; i++)
  {
    IpArrayTst[i][0] = 0 ;
    IpArrayTst[i][1] = 0 ;
    IpArrayTst[i][2] = 0 ;
    IpArrayTst[i][3] = 0 ;
    IpArrayCnt[i]    = 0 ;
    MaxIpCnt         = 0 ;
  }
}

byte SetStatIpArray(uint8_t *p)
{
#ifdef ETH_TIBA_LOGIC
  static uint8_t i , j ;
  static uint32_t Maxi ;
  if (MaxIpCnt < 4) return 1 ;
  Maxi = 0 ;
  j    = 0 ;
#ifdef TXRX_DEBUG_PR
  PRINT_TFT( COM5_DEV , ("\n") );
#endif
  for (i=0; i<ARRAY_TEST_SIZE; i++)
  {
#ifdef TXRX_DEBUG_PR
    if (IpArrayCnt[i])
    {
      PRINT_TFT( TFT_DEV  , ("Cntr IP = %d.%d.%d.XXX    Cnt = %d \n",IpArrayTst[i][0],IpArrayTst[i][1],IpArrayTst[i][2],IpArrayCnt[i]) );
    }
#endif
    if (Maxi < IpArrayCnt[i])
    {
      Maxi = IpArrayCnt[i] ;
      j    = i             ;
    }
  }
#ifdef TXRX_DEBUG_PR
  PRINT_TFT( COM5_DEV , ("\n") );
#endif
  if (Maxi)
  {
    p[0] = IpArrayTst[j][0] ;
    p[1] = IpArrayTst[j][1] ;
    p[2] = IpArrayTst[j][2] ;
  }
  p[3] = 200 + ((macaddress[3] + macaddress[4] + macaddress[5]) % 50) ;
#endif
  return 0 ;
}

