/*****************************************************************************
 * FileName:        Font16.c
 * Processor:       PIC24F, PIC24H, dsPIC
 * Compiler:        MPLAB C30 (see release notes for tested revision)
 * Linker:          MPLAB LINK30
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright � 2010 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AUTO-GENERATED CODE:  Graphics Resource Converter version: 3.8.21
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include <Graphics/Graphics.h>

/*****************************************************************************
 * Converted Resources
 * -------------------
 *
 *
 * Fonts
 * -----
 * Courier_New_16 - Height: 19 pixels, range: ' ' to '~'
 * Courier_New_16_1 - Height: 19 pixels, range: '�' to '�'
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Fonts
 *****************************************************************************/

/*********************************
 * Font Structure
 * Label: Courier_New_16
 * Description:  Height: 19 pixels, range: ' ' to '~'
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Courier_New_16[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Courier_New_16 =
{
    (FLASH | COMP_NONE),
    __Courier_New_16
};

const char __Courier_New_16[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Courier_New_16[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0x20, 0x00,     // First Character
    0x7E, 0x00,     // Last Character
    0x13,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x0A, 0x84, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xAA, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD0, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF6, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x1C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x42, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x68, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x8E, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB4, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDA, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x00, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x26, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x4C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x72, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x98, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xBE, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xE4, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x0A, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x30, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x56, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x7C, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xA2, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC8, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xEE, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x14, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x3A, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x60, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x86, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xAC, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD2, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF8, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x1E, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x44, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x6A, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x90, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB6, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDC, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x02, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x28, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x4E, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x74, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x9A, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC0, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xE6, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x0C, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x32, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x58, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x7E, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xA4, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xCA, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF0, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x16, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x3C, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x62, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x88, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xAE, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD4, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xFA, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x20, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x46, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x6C, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x92, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB8, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDE, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x04, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x2A, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x50, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x76, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x9C, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC2, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xE8, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x0E, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x34, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x5A, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x80, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xA6, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xCC, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF2, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x18, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x3E, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x64, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x8A, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB0, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD6, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xFC, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x22, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x48, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x6E, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x94, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xBA, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xE0, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x06, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x2C, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x52, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x78, 0x0F, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xD8, 0x00,         //    ** **        
    0xD8, 0x00,         //    ** **        
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x90, 0x00,         //     *  *        
    0x90, 0x00,         //     *  *        
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0xFE, 0x01,         //  ********       
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0xFE, 0x01,         //  ********       
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x24, 0x00,         //   *  *          
    0x24, 0x00,         //   *  *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0xF8, 0x00,         //    *****        
    0x84, 0x00,         //   *    *        
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x78, 0x00,         //    ****         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x84, 0x00,         //   *    *        
    0x7C, 0x00,         //   *****         
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x24, 0x00,         //   *  *          
    0x24, 0x00,         //   *  *          
    0x18, 0x00,         //    **           
    0xE0, 0x00,         //      ***        
    0x1C, 0x00,         //   ***           
    0x60, 0x00,         //      **         
    0x90, 0x00,         //     *  *        
    0x90, 0x00,         //     *  *        
    0x60, 0x00,         //      **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x70, 0x00,         //     ***         
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x18, 0x00,         //    **           
    0x94, 0x00,         //   * *  *        
    0x64, 0x00,         //   *  **         
    0x44, 0x00,         //   *   *         
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x04, 0x00,         //   *             
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x04, 0x00,         //   *             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x00,         //  *******        
    0x10, 0x00,         //     *           
    0x28, 0x00,         //    * *          
    0x44, 0x00,         //   *   *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x00,         //  *******        
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x08, 0x00,         //    *            
    0x0C, 0x00,         //   **            
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x18, 0x00,         //    **           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x2C, 0x00,         //   ** *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x08, 0x00,         //    *            
    0x04, 0x00,         //   *             
    0x82, 0x00,         //  *     *        
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x86, 0x00,         //  **    *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x70, 0x00,         //     ***         
    0x40, 0x00,         //       *         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x82, 0x00,         //  *     *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x60, 0x00,         //      **         
    0x50, 0x00,         //     * *         
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x44, 0x00,         //   *   *         
    0xFC, 0x00,         //   ******        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x7C, 0x00,         //   *****         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x82, 0x00,         //  *     *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x04, 0x00,         //   *             
    0xF4, 0x00,         //   * ****        
    0x0C, 0x01,         //   **    *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x82, 0x00,         //  *     *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xC4, 0x00,         //   *   **        
    0xB8, 0x00,         //    *** *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x1C, 0x00,         //   ***           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x18, 0x00,         //    **           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x18, 0x00,         //    **           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x10, 0x00,         //     *           
    0x18, 0x00,         //    **           
    0x08, 0x00,         //    *            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x01,         //        **       
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x08, 0x00,         //    *            
    0x06, 0x00,         //  **             
    0x08, 0x00,         //    *            
    0x30, 0x00,         //     **          
    0x40, 0x00,         //       *         
    0x80, 0x01,         //        **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x06, 0x00,         //  **             
    0x08, 0x00,         //    *            
    0x30, 0x00,         //     **          
    0x40, 0x00,         //       *         
    0x80, 0x01,         //        **       
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x08, 0x00,         //    *            
    0x06, 0x00,         //  **             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x08, 0x01,         //    *    *       
    0x04, 0x01,         //   *     *       
    0xC4, 0x01,         //   *   ***       
    0x24, 0x01,         //   *  *  *       
    0x24, 0x01,         //   *  *  *       
    0x24, 0x01,         //   *  *  *       
    0xC4, 0x01,         //   *   ***       
    0x04, 0x00,         //   *             
    0x88, 0x00,         //    *   *        
    0x70, 0x00,         //     ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x30, 0x00,         //     **          
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0xFC, 0x00,         //   ******        
    0x84, 0x00,         //   *    *        
    0x02, 0x01,         //  *      *       
    0x87, 0x03,         // ***    ***      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x7C, 0x00,         //   *****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x01,         //    **** *       
    0x84, 0x01,         //   *    **       
    0x02, 0x01,         //  *      *       
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x04, 0x01,         //   *     *       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x84, 0x00,         //   *    *        
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x24, 0x00,         //   *  *          
    0x3C, 0x00,         //   ****          
    0x24, 0x00,         //   *  *          
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x24, 0x00,         //   *  *          
    0x3C, 0x00,         //   ****          
    0x24, 0x00,         //   *  *          
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x01,         //    **** *       
    0x84, 0x01,         //   *    **       
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0xE2, 0x03,         //  *   *****      
    0x02, 0x01,         //  *      *       
    0x04, 0x01,         //   *     *       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xFC, 0x00,         //   ******        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x01,         //    ******       
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x42, 0x00,         //  *    *         
    0x42, 0x00,         //  *    *         
    0x42, 0x00,         //  *    *         
    0x3C, 0x00,         //   ****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x01,         //  *** ****       
    0x44, 0x00,         //   *   *         
    0x24, 0x00,         //   *  *          
    0x14, 0x00,         //   * *           
    0x3C, 0x00,         //   ****          
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x84, 0x00,         //   *    *        
    0x8E, 0x01,         //  ***   **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x00,         //  *****          
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0xC6, 0x00,         //  **   **        
    0xAA, 0x00,         //  * * * *        
    0xAA, 0x00,         //  * * * *        
    0xAA, 0x00,         //  * * * *        
    0x92, 0x00,         //  *  *  *        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0xC7, 0x01,         // ***   ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0x86, 0x00,         //  **    *        
    0x8A, 0x00,         //  * *   *        
    0x8A, 0x00,         //  * *   *        
    0x92, 0x00,         //  *  *  *        
    0xA2, 0x00,         //  *   * *        
    0xA2, 0x00,         //  *   * *        
    0xC2, 0x00,         //  *    **        
    0xC7, 0x00,         // ***   **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x00,         //   ******        
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0xF8, 0x00,         //    *****        
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x02, 0x01,         //  *      *       
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0xF0, 0x01,         //     *****       
    0x08, 0x00,         //    *            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x7C, 0x00,         //   *****         
    0x24, 0x00,         //   *  *          
    0x44, 0x00,         //   *   *         
    0x84, 0x00,         //   *    *        
    0x8E, 0x01,         //  ***   **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xB8, 0x00,         //    *** *        
    0xC4, 0x00,         //   *   **        
    0x84, 0x00,         //   *    *        
    0x04, 0x00,         //   *             
    0x78, 0x00,         //    ****         
    0x80, 0x00,         //        *        
    0x84, 0x00,         //   *    *        
    0x8C, 0x00,         //   **   *        
    0x74, 0x00,         //   * ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x8C, 0x00,         //   **   *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x87, 0x03,         // ***    ***      
    0x02, 0x01,         //  *      *       
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEF, 0x01,         // **** ****       
    0x82, 0x00,         //  *     *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0xAA, 0x00,         //  * * * *        
    0xAA, 0x00,         //  * * * *        
    0xAA, 0x00,         //  * * * *        
    0xAA, 0x00,         //  * * * *        
    0x44, 0x00,         //   *   *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0x82, 0x00,         //  *     *        
    0x44, 0x00,         //   *   *         
    0x28, 0x00,         //    * *          
    0x10, 0x00,         //     *           
    0x28, 0x00,         //    * *          
    0x44, 0x00,         //   *   *         
    0x82, 0x00,         //  *     *        
    0xC7, 0x01,         // ***   ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0x82, 0x00,         //  *     *        
    0x44, 0x00,         //   *   *         
    0x28, 0x00,         //    * *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x00,         //   ******        
    0x84, 0x00,         //   *    *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x88, 0x00,         //    *   *        
    0x84, 0x00,         //   *    *        
    0xFC, 0x00,         //   ******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x70, 0x00,         //     ***         
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x70, 0x00,         //     ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x00,         //   ***           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x1C, 0x00,         //   ***           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x28, 0x00,         //    * *          
    0x44, 0x00,         //   *   *         
    0x82, 0x00,         //  *     *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x03,         // **********      
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x84, 0x00,         //   *    *        
    0x80, 0x00,         //        *        
    0xFC, 0x00,         //   ******        
    0x82, 0x00,         //  *     *        
    0xC2, 0x00,         //  *    **        
    0xBC, 0x01,         //   **** **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x06, 0x00,         //  **             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x74, 0x00,         //   * ***         
    0x8C, 0x00,         //   **   *        
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x8C, 0x00,         //   **   *        
    0x76, 0x00,         //  ** ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x01,         //    **** *       
    0x84, 0x01,         //   *    **       
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x04, 0x01,         //   *     *       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x00,         //       **        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0xB8, 0x00,         //    *** *        
    0xC4, 0x00,         //   *   **        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0xC4, 0x00,         //   *   **        
    0xB8, 0x01,         //    *** **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x44, 0x00,         //   *   *         
    0x82, 0x00,         //  *     *        
    0xFE, 0x00,         //  *******        
    0x02, 0x00,         //  *              
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0xFE, 0x00,         //  *******        
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xB8, 0x01,         //    *** **       
    0xC4, 0x00,         //   *   **        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0xC4, 0x00,         //   *   **        
    0xB8, 0x00,         //    *** *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x06, 0x00,         //  **             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x74, 0x00,         //   * ***         
    0x8C, 0x00,         //   **   *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x00,         //   ***           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x06, 0x00,         //  **             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0xE4, 0x00,         //   *  ***        
    0x24, 0x00,         //   *  *          
    0x14, 0x00,         //   * *           
    0x1C, 0x00,         //   ***           
    0x24, 0x00,         //   *  *          
    0x44, 0x00,         //   *   *         
    0xE6, 0x01,         //  **  ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x00,         //   ***           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x4B, 0x00,         // ** *  *         
    0xB6, 0x00,         //  ** ** *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0xB7, 0x01,         // *** ** **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x76, 0x00,         //  ** ***         
    0x8C, 0x00,         //   **   *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x44, 0x00,         //   *   *         
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x44, 0x00,         //   *   *         
    0x38, 0x00,         //    ***          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x76, 0x00,         //  ** ***         
    0x8C, 0x00,         //   **   *        
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x8C, 0x00,         //   **   *        
    0x74, 0x00,         //   * ***         
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xB8, 0x01,         //    *** **       
    0xC4, 0x00,         //   *   **        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0x82, 0x00,         //  *     *        
    0xC4, 0x00,         //   *   **        
    0xB8, 0x00,         //    *** *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCC, 0x01,         //   **  ***       
    0x38, 0x00,         //    ***          
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xBC, 0x00,         //   **** *        
    0xC2, 0x00,         //  *    **        
    0x02, 0x00,         //  *              
    0x7C, 0x00,         //   *****         
    0x80, 0x00,         //        *        
    0x82, 0x00,         //  *     *        
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x7F, 0x00,         // *******         
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC6, 0x00,         //  **   **        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xC4, 0x00,         //   *   **        
    0xB8, 0x01,         //    *** **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x84, 0x00,         //   *    *        
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x48, 0x00,         //    *  *         
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x07, 0x03,         // ***     **      
    0x22, 0x02,         //  *   *   *      
    0x22, 0x02,         //  *   *   *      
    0x54, 0x01,         //   * * * *       
    0x54, 0x01,         //   * * * *       
    0x54, 0x01,         //   * * * *       
    0x88, 0x00,         //    *   *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x84, 0x00,         //   *    *        
    0x48, 0x00,         //    *  *         
    0x30, 0x00,         //     **          
    0x68, 0x00,         //    * **         
    0x84, 0x00,         //   *    *        
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0x82, 0x00,         //  *     *        
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x28, 0x00,         //    * *          
    0x28, 0x00,         //    * *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x1F, 0x00,         // *****           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x42, 0x00,         //  *    *         
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x84, 0x00,         //   *    *        
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0C, 0x00,         //   **            
    0x92, 0x00,         //  *  *  *        
    0x60, 0x00,         //      **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

/*********************************
 * Font Structure
 * Label: Courier_New_16_1
 * Description:  Height: 19 pixels, range: '�' to '�'
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Courier_New_16_1[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Courier_New_16_1 =
{
    (FLASH | COMP_NONE),
    __Courier_New_16_1
};

const char __Courier_New_16_1[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Courier_New_16_1[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0xD0, 0x05,     // First Character
    0xEA, 0x05,     // Last Character
    0x13,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x0A, 0x74, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x9A, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC0, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xE6, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x0C, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x32, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x58, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x7E, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xA4, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xCA, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF0, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x16, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x3C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x62, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x88, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xAE, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD4, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xFA, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x20, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x46, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x6C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x92, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB8, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDE, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x04, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x2A, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x50, 0x04, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC4, 0x00,         //   *   **        
    0x88, 0x00,         //    *   *        
    0x90, 0x00,         //     *  *        
    0x58, 0x00,         //    ** *         
    0x24, 0x00,         //   *  *          
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x8C, 0x00,         //   **   *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x00,         //  *****          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x30, 0x00,         //     **          
    0x4C, 0x00,         //   **  *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x18, 0x00,         //    **           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE7, 0x00,         // ***  ***        
    0xA4, 0x00,         //   *  * *        
    0xA4, 0x00,         //   *  * *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xE0, 0x00,         //      ***        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x00,         //  *****          
    0x40, 0x00,         //       *         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x7C, 0x00,         //   *****         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xFC, 0x00,         //   ******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x76, 0x00,         //  ** ***         
    0x8C, 0x00,         //   **   *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xE4, 0x00,         //   *  ***        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xE0, 0x00,         //      ***        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x3C, 0x00,         //   ****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC6, 0x00,         //  **   **        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x44, 0x00,         //   *   *         
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3C, 0x00,         //   ****          
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x5C, 0x00,         //   *** *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xC0, 0x00,         //       **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0xB8, 0x00,         //    *** *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x00,         //  *** ***        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x48, 0x00,         //    *  *         
    0x38, 0x00,         //    ***          
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x38, 0x00,         //    ***          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x00,         //  ***  **        
    0x88, 0x00,         //    *   *        
    0x48, 0x00,         //    *  *         
    0x30, 0x00,         //     **          
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x00,         //   ******        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x84, 0x00,         //   *    *        
    0x44, 0x00,         //   *   *         
    0x24, 0x00,         //   *  *          
    0x24, 0x00,         //   *  *          
    0x64, 0x00,         //   *  **         
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x00,         //  *****          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xD6, 0x00,         //  ** * **        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x84, 0x00,         //   *    *        
    0x86, 0x00,         //  **    *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

