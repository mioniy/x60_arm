/*****************************************************************************
 * FileName:        Font_Aharoni_16_20_29.c
 * Processor:       PIC24F, PIC24H, dsPIC
 * Compiler:        MPLAB C30 (see release notes for tested revision)
 * Linker:          MPLAB LINK30
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright � 2010 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AUTO-GENERATED CODE:  Graphics Resource Converter version: 3.8.21
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include <Graphics/Graphics.h>

/*****************************************************************************
 * Converted Resources
 * -------------------
 *
 *
 * Fonts
 * -----
 * Aharoni_Bold_16 - Height: 17 pixels, range: ' ' to ''
 * Aharoni_Bold_20 - Height: 21 pixels, range: ' ' to ''
 * Aharoni_Bold_29 - Height: 30 pixels, range: ' ' to ''
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Fonts
 *****************************************************************************/

/*********************************
 * Font Structure
 * Label: Aharoni_Bold_16
 * Description:  Height: 17 pixels, range: ' ' to ''
 ***********************************/
#ifdef _MICROCHIP_
extern const char __Aharoni_Bold_16[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Aharoni_Bold_16 =
{
    (FLASH | COMP_NONE),
    __Aharoni_Bold_16
};
const char __Aharoni_Bold_16[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Aharoni_Bold_16[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0x20, 0x00,     // First Character
    0x7F, 0x00,     // Last Character
    0x11,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x04, 0x88, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x99, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0xAA, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xBB, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xCC, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDD, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xFF, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x21, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x32, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x43, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x54, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x65, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x87, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x98, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0xA9, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0xBA, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xCB, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xDC, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xED, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xFE, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x0F, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x20, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x31, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x42, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x53, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x64, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x75, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x86, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x97, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB9, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDB, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xFD, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x0E, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x30, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x52, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x74, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x96, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xB8, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xC9, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xDA, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xFC, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x1E, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x2F, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x40, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x62, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0x73, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0x95, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xB7, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD9, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xFB, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x1D, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x3F, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x61, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x72, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x94, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x10, 0xB6, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0xD8, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xFA, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x1C, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x3E, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x4F, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x60, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x71, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x82, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x93, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xA4, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC6, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xE8, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xF9, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x1B, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x3D, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x4E, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x70, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x92, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0xA3, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB4, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0xD6, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0xE7, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x09, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x2B, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x4D, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x6F, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x91, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xA2, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0xB3, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xC4, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0xE6, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0x08, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x2A, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x4C, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x6E, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x7F, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x02, 0x90, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xA1, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0xB2, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD4, 0x0A, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x2C,         //   ** *  
    0x36,         //  ** **  
    0x12,         //  *  *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x3F,         // ******  
    0x12,         //  *  *   
    0x3F,         // ******  
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x20,         //      *  
    0xF8,         //    *****
    0xAC,         //   ** * *
    0xFC,         //   ******
    0xF8,         //    *****
    0xA0,         //      * *
    0xAC,         //   ** * *
    0xF8,         //    *****
    0x20,         //      *  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x8C, 0x00,         //   **   *        
    0x52, 0x00,         //  *  * *         
    0x52, 0x00,         //  *  * *         
    0xAC, 0x01,         //   ** * **       
    0x50, 0x02,         //     * *  *      
    0x50, 0x02,         //     * *  *      
    0x88, 0x01,         //    *   **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x7C, 0x00,         //   *****         
    0x6C, 0x00,         //   ** **         
    0x6C, 0x00,         //   ** **         
    0x38, 0x00,         //    ***          
    0x3C, 0x00,         //   ****          
    0xE6, 0x01,         //  **  ****       
    0xC6, 0x00,         //  **   **        
    0xFE, 0x01,         //  ********       
    0xBC, 0x00,         //   **** *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x06,         //  **     
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x04,         //   *     
    0x16,         //  ** *   
    0x0C,         //   **    
    0x16,         //  ** *   
    0x04,         //   *     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xFC, 0x01,         //   *******       
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x03,         // **      
    0x01,         // *       
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x10,         //     *   
    0x08,         //    *    
    0x08,         //    *    
    0x04,         //   *     
    0x04,         //   *     
    0x04,         //   *     
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x7E,         //  ****** 
    0x66,         //  **  ** 
    0x66,         //  **  ** 
    0x66,         //  **  ** 
    0x7E,         //  ****** 
    0x3C,         //   ****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x18,         //    **   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1E,         //  ****   
    0x3E,         //  *****  
    0x30,         //     **  
    0x10,         //     *   
    0x08,         //    *    
    0x3C,         //   ****  
    0x3E,         //  *****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x3C,         //   ****  
    0x30,         //     **  
    0x18,         //    **   
    0x30,         //     **  
    0x3C,         //   ****  
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x30,         //     **  
    0x38,         //    ***  
    0x34,         //   * **  
    0x32,         //  *  **  
    0x7E,         //  ****** 
    0x30,         //     **  
    0x30,         //     **  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x3C,         //   ****  
    0x06,         //  **     
    0x1E,         //  ****   
    0x30,         //     **  
    0x3E,         //  *****  
    0x1E,         //  ****   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x30,         //     **  
    0x18,         //    **   
    0x3C,         //   ****  
    0x7E,         //  ****** 
    0x66,         //  **  ** 
    0x7E,         //  ****** 
    0x3C,         //   ****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3E,         //  *****  
    0x30,         //     **  
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x0C,         //   **    
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x3E,         //  *****  
    0x36,         //  ** **  
    0x1C,         //   ***   
    0x36,         //  ** **  
    0x3E,         //  *****  
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x7E,         //  ****** 
    0x66,         //  **  ** 
    0x7E,         //  ****** 
    0x3C,         //   ****  
    0x18,         //    **   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x03,         // **      
    0x01,         // *       
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x00,         //       **        
    0x30, 0x00,         //     **          
    0x0C, 0x00,         //   **            
    0x02, 0x00,         //  *              
    0x1C, 0x00,         //   ***           
    0x70, 0x00,         //     ***         
    0xC0, 0x00,         //       **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x06, 0x00,         //  **             
    0x18, 0x00,         //    **           
    0x60, 0x00,         //      **         
    0x80, 0x00,         //        *        
    0x70, 0x00,         //     ***         
    0x1C, 0x00,         //   ***           
    0x06, 0x00,         //  **             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x1E,         //  ****   
    0x3F,         // ******  
    0x33,         // **  **  
    0x30,         //     **  
    0x30,         //     **  
    0x1C,         //   ***   
    0x0C,         //   **    
    0x00,         //         
    0x0C,         //   **    
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0x0C, 0x03,         //   **    **      
    0x02, 0x02,         //  *       *      
    0xF1, 0x04,         // *   ****  *     
    0x99, 0x04,         // *  **  *  *     
    0x89, 0x04,         // *  *   *  *     
    0x89, 0x04,         // *  *   *  *     
    0xC9, 0x02,         // *  *  ** *      
    0xF2, 0x01,         //  *  *****       
    0x04, 0x03,         //   *     **      
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x70, 0x00,         //     ***         
    0xF8, 0x00,         //    *****        
    0xF8, 0x00,         //    *****        
    0xD8, 0x00,         //    ** **        
    0xDC, 0x01,         //   *** ***       
    0xDC, 0x01,         //   *** ***       
    0x8C, 0x01,         //   **   **       
    0xFE, 0x03,         //  *********      
    0xFE, 0x03,         //  *********      
    0x06, 0x03,         //  **     **      
    0x07, 0x07,         // ***     ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0xFE, 0x01,         //  ********       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0xF8, 0x00,         //    *****        
    0xFC, 0x00,         //   ******        
    0x1E, 0x00,         //  ****           
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x1E, 0x00,         //  ****           
    0xFC, 0x00,         //   ******        
    0xF8, 0x00,         //    *****        
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x00,         //  *******        
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x3E,         //  *****  
    0x3E,         //  *****  
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x3E,         //  *****  
    0x3E,         //  *****  
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0xF8, 0x03,         //    *******      
    0xFC, 0x07,         //   *********     
    0x1E, 0x03,         //  ****   **      
    0x0E, 0x00,         //  ***            
    0xCE, 0x07,         //  ***  *****     
    0xCE, 0x07,         //  ***  *****     
    0x1E, 0x07,         //  ****   ***     
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0xFE, 0x07,         //  **********     
    0xFE, 0x07,         //  **********     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x70,         //     *** 
    0x72,         //  *  *** 
    0x3F,         // ******  
    0x1E,         //  ****   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x8E, 0x03,         //  ***   ***      
    0xCE, 0x01,         //  ***  ***       
    0xEE, 0x00,         //  *** ***        
    0x6E, 0x00,         //  *** **         
    0x7E, 0x00,         //  ******         
    0x3E, 0x00,         //  *****          
    0x7E, 0x00,         //  ******         
    0xEE, 0x00,         //  *** ***        
    0xEE, 0x00,         //  *** ***        
    0xCE, 0x01,         //  ***  ***       
    0x8E, 0x03,         //  ***   ***      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x1C, 0x1C,         //   ***     ***   
    0x3C, 0x1E,         //   ****   ****   
    0x3C, 0x1E,         //   ****   ****   
    0x3C, 0x1E,         //   ****   ****   
    0x7C, 0x1F,         //   ***** *****   
    0x7E, 0x3F,         //  ****** ******  
    0xFE, 0x3F,         //  *************  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0x8E, 0x38,         //  ***   *   ***  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x0E, 0x0E,         //  ***     ***    
    0x1E, 0x0E,         //  ****    ***    
    0x3E, 0x0E,         //  *****   ***    
    0x7E, 0x0E,         //  ******  ***    
    0x7E, 0x0E,         //  ******  ***    
    0xEE, 0x0E,         //  *** *** ***    
    0xCE, 0x0F,         //  ***  ******    
    0xCE, 0x0F,         //  ***  ******    
    0x8E, 0x0F,         //  ***   *****    
    0x0E, 0x0F,         //  ***    ****    
    0x0E, 0x0E,         //  ***     ***    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0xF8, 0x03,         //    *******      
    0xFC, 0x07,         //   *********     
    0x1E, 0x0F,         //  ****   ****    
    0x0E, 0x0E,         //  ***     ***    
    0x0E, 0x0E,         //  ***     ***    
    0x0E, 0x0E,         //  ***     ***    
    0x1E, 0x0F,         //  ****   ****    
    0xFC, 0x07,         //   *********     
    0xF8, 0x03,         //    *******      
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x00,         //  *******        
    0x7E, 0x00,         //  ******         
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0xF8, 0x03,         //    *******      
    0xFC, 0x07,         //   *********     
    0x1E, 0x0F,         //  ****   ****    
    0x0E, 0x0E,         //  ***     ***    
    0x0E, 0x0E,         //  ***     ***    
    0xCE, 0x0E,         //  ***  ** ***    
    0x9E, 0x0F,         //  ****  *****    
    0xFC, 0x07,         //   *********     
    0xF8, 0x07,         //    ********     
    0xF0, 0x0E,         //     **** ***    
    0x00, 0x0C,         //           **    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xFE, 0x00,         //  *******        
    0x7E, 0x00,         //  ******         
    0xEE, 0x00,         //  *** ***        
    0xCE, 0x01,         //  ***  ***       
    0x8E, 0x03,         //  ***   ***      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0xFC, 0x00,         //   ******        
    0x4E, 0x00,         //  ***  *         
    0x0E, 0x00,         //  ***            
    0x1E, 0x00,         //  ****           
    0x7C, 0x00,         //   *****         
    0xFC, 0x00,         //   ******        
    0xE0, 0x00,         //      ***        
    0xE4, 0x00,         //   *  ***        
    0xFE, 0x00,         //  *******        
    0x3C, 0x00,         //   ****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x7F,         // ******* 
    0x7F,         // ******* 
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x07, 0x07,         // ***     ***     
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0xDC, 0x01,         //   *** ***       
    0xDC, 0x01,         //   *** ***       
    0xD8, 0x00,         //    ** **        
    0xF8, 0x00,         //    *****        
    0xF8, 0x00,         //    *****        
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x87, 0xE1,         // ***    **    ***
    0x87, 0xE3,         // ***    ***   ***
    0xCE, 0x63,         //  ***  ****   ** 
    0xCE, 0x73,         //  ***  ****  *** 
    0xCE, 0x77,         //  ***  ***** *** 
    0xEC, 0x77,         //   ** ****** *** 
    0x7C, 0x3E,         //   *****  *****  
    0x7C, 0x3E,         //   *****  *****  
    0x7C, 0x3E,         //   *****  *****  
    0x38, 0x1C,         //    ***    ***   
    0x38, 0x1C,         //    ***    ***   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x9E, 0x07,         //  ****  ****     
    0x9C, 0x03,         //   ***  ***      
    0x9C, 0x03,         //   ***  ***      
    0xF8, 0x01,         //    ******       
    0xF0, 0x00,         //     ****        
    0xF0, 0x00,         //     ****        
    0xF8, 0x01,         //    ******       
    0xFC, 0x03,         //   ********      
    0x9C, 0x03,         //   ***  ***      
    0x0E, 0x07,         //  ***    ***     
    0x0F, 0x07,         // ****    ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x8F, 0x03,         // ****   ***      
    0x8E, 0x03,         //  ***   ***      
    0xDC, 0x01,         //   *** ***       
    0xDC, 0x01,         //   *** ***       
    0xF8, 0x00,         //    *****        
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0xFE, 0x00,         //  *******        
    0xE0, 0x00,         //      ***        
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x78, 0x00,         //    ****         
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x1C, 0x00,         //   ***           
    0xFC, 0x01,         //   *******       
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x01,         // *       
    0x03,         // **      
    0x02,         //  *      
    0x02,         //  *      
    0x06,         //  **     
    0x04,         //   *     
    0x04,         //   *     
    0x0C,         //   **    
    0x08,         //    *    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x0C,         //   **    
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x21,         // *    *  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0xFF,         // ********
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x06,         //  **     
    0x0C,         //   **    
    0x18,         //    **   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xD8, 0x01,         //    ** ***       
    0xFC, 0x01,         //   *******       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFC, 0x01,         //   *******       
    0xD8, 0x01,         //    ** ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x6E, 0x00,         //  *** **         
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x00,         //  *******        
    0x6E, 0x00,         //  *** **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x78,         //    **** 
    0x7C,         //   ***** 
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x7C,         //   ***** 
    0x78,         //    **** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xDC, 0x01,         //   *** ***       
    0xFE, 0x01,         //  ********       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xDC, 0x01,         //   *** ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x00,         //    ***          
    0x7C, 0x00,         //   *****         
    0xEE, 0x00,         //  *** ***        
    0xFE, 0x00,         //  *******        
    0x0E, 0x00,         //  ***            
    0xFC, 0x00,         //   ******        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x38,         //    ***  
    0x3C,         //   ****  
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x3F,         // ******  
    0x3F,         // ******  
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xD8, 0x01,         //    ** ***       
    0xFC, 0x01,         //   *******       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFC, 0x01,         //   *******       
    0xD8, 0x01,         //    ** ***       
    0xC0, 0x01,         //       ***       
    0xCE, 0x01,         //  ***  ***       
    0xFC, 0x00,         //   ******        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xEE, 0x00,         //  *** ***        
    0xFE, 0x01,         //  ********       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xCE, 0x03,         //  ***  ****      
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x00,         //  *******        
    0x7E, 0x00,         //  ******         
    0xFE, 0x00,         //  *******        
    0xEE, 0x01,         //  *** ****       
    0xCE, 0x03,         //  ***  ****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x1C,         //  *** ***  ***   
    0xFE, 0x3F,         //  *************  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x00,         //  *** ***        
    0xFE, 0x01,         //  ********       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0xFC, 0x00,         //   ******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFC, 0x00,         //   ******        
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x6E, 0x00,         //  *** **         
    0xFE, 0x00,         //  *******        
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x00,         //  *******        
    0x6E, 0x00,         //  *** **         
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xDC, 0x01,         //   *** ***       
    0xFE, 0x01,         //  ********       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xDC, 0x01,         //   *** ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x2E,         //  *** *  
    0x3E,         //  *****  
    0x1E,         //  ****   
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x7C,         //   ***** 
    0x3E,         //  *****  
    0x0E,         //  ***    
    0x3C,         //   ****  
    0x70,         //     *** 
    0x7C,         //   ***** 
    0x3E,         //  *****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x1F,         // *****   
    0x1F,         // *****   
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xFE, 0x01,         //  ********       
    0xFC, 0x00,         //   ******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC7, 0x01,         // ***   ***       
    0xC6, 0x00,         //  **   **        
    0xEE, 0x00,         //  *** ***        
    0x6C, 0x00,         //   ** **         
    0x7C, 0x00,         //   *****         
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC6, 0x31,         //  **   ***   **  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCC, 0x19,         //   **  ***  **   
    0x6C, 0x1B,         //   ** ** ** **   
    0x78, 0x0F,         //    **** ****    
    0x38, 0x0E,         //    ***   ***    
    0x30, 0x06,         //     **   **     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x8E, 0x03,         //  ***   ***      
    0xDC, 0x01,         //   *** ***       
    0xF8, 0x00,         //    *****        
    0x70, 0x00,         //     ***         
    0xF8, 0x00,         //    *****        
    0xDC, 0x01,         //   *** ***       
    0x8E, 0x03,         //  ***   ***      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x01,         //  ***   **       
    0xDC, 0x01,         //   *** ***       
    0xDC, 0x00,         //   *** **        
    0xF8, 0x00,         //    *****        
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0xFE,         //  *******
    0x7E,         //  ****** 
    0x70,         //     *** 
    0x38,         //    ***  
    0x1C,         //   ***   
    0xFC,         //   ******
    0xFE,         //  *******
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x10,         //     *   
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x04,         //   *     
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x10,         //     *   
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x08,         //    *    
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x20,         //      *  
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x10,         //     *   
    0x08,         //    *    
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x8E, 0x00,         //  ***   *        
    0x71, 0x00,         // *   ***         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF7, 0x00,         // *** ****        
    0xF9, 0x00,         // *  *****        
    0x99, 0x00,         // *  **  *        
    0xF7, 0x03,         // *** ******      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

/*********************************
 * Font Structure
 * Label: Aharoni_Bold_20
 * Description:  Height: 21 pixels, range: ' ' to ''
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Aharoni_Bold_20[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Aharoni_Bold_20 =
{
    (FLASH | COMP_NONE),
    __Aharoni_Bold_20
};
const char __Aharoni_Bold_20[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Aharoni_Bold_20[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0x20, 0x00,     // First Character
    0x7F, 0x00,     // Last Character
    0x15,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x06, 0x88, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x9D, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xB2, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xC7, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xDC, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xF1, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0x1B, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x45, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x5A, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x6F, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x84, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x99, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0xC3, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0xD8, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0xED, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0x02, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x17, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x2C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x41, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x56, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x6B, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x80, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x95, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xAA, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xBF, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xD4, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0xE9, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0xFE, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x13, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x3D, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x67, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x91, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xBB, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE5, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x0F, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x39, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x63, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x8D, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xB7, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x10, 0xE1, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0x0B, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x35, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x4A, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x74, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x9E, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0xC8, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0x07, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x10, 0x31, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x5B, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x10, 0x85, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xAF, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0xD9, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x03, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x2D, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x57, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x14, 0x81, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xC0, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xEA, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0x14, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x3E, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x53, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x68, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x7D, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x92, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xBC, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xE6, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x10, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x3A, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x64, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x8E, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xB8, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xCD, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xF7, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x21, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x36, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x4B, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x05, 0x75, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x8A, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xC9, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xF3, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x1D, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x47, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x71, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x86, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xB0, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xC5, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xEF, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x19, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0x58, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x82, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xAC, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD6, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x03, 0x00, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0x15, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x3F, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x69, 0x0E, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x6C,         //   ** ** 
    0x24,         //   *  *  
    0x36,         //  ** **  
    0x12,         //  *  *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x48,         //    *  * 
    0x6C,         //   ** ** 
    0x24,         //   *  *  
    0xFF,         // ********
    0x24,         //   *  *  
    0x24,         //   *  *  
    0xFF,         // ********
    0x24,         //   *  *  
    0x24,         //   *  *  
    0x12,         //  *  *   
    0x12,         //  *  *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x10,         //     *   
    0x7C,         //   ***** 
    0xFE,         //  *******
    0x16,         //  ** *   
    0x3E,         //  *****  
    0xFC,         //   ******
    0xD0,         //     * **
    0xD0,         //     * **
    0xFE,         //  *******
    0x7C,         //   ***** 
    0x10,         //     *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0C, 0x01,         //   **    *       
    0x92, 0x00,         //  *  *  *        
    0x92, 0x00,         //  *  *  *        
    0x52, 0x00,         //  *  * *         
    0x6C, 0x03,         //   ** ** **      
    0xA0, 0x04,         //      * *  *     
    0x90, 0x04,         //     *  *  *     
    0x90, 0x04,         //     *  *  *     
    0x08, 0x03,         //    *    **      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0xFC, 0x03,         //   ********      
    0x9C, 0x03,         //   ***  ***      
    0x9C, 0x03,         //   ***  ***      
    0xF8, 0x01,         //    ******       
    0xF8, 0x00,         //    *****        
    0xFC, 0x04,         //   ******  *     
    0xEE, 0x0F,         //  *** *******    
    0xCE, 0x07,         //  ***  *****     
    0x8E, 0x07,         //  ***   ****     
    0xFC, 0x0F,         //   **********    
    0xF8, 0x04,         //    *****  *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x04,         //   *     
    0x06,         //  **     
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x1E,         //  ****   
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x1E,         //  ****   
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x1E,         //  ****   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x1E,         //  ****   
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x08,         //    *    
    0x2A,         //  * * *  
    0x1C,         //   ***   
    0x2A,         //  * * *  
    0x08,         //    *    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFC, 0x07,         //   *********     
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x06,         //  **     
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x20,         //      *  
    0x10,         //     *   
    0x10,         //     *   
    0x08,         //    *    
    0x08,         //    *    
    0x08,         //    *    
    0x04,         //   *     
    0x04,         //   *     
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x38,         //    ***  
    0x7C,         //   ***** 
    0xEE,         //  *** ***
    0xC6,         //  **   **
    0xC6,         //  **   **
    0xC6,         //  **   **
    0xC6,         //  **   **
    0x7C,         //   ***** 
    0x38,         //    ***  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x30,         //     **  
    0x38,         //    ***  
    0x3C,         //   ****  
    0x34,         //   * **  
    0x30,         //     **  
    0x30,         //     **  
    0x30,         //     **  
    0x30,         //     **  
    0x30,         //     **  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x7C,         //   ***** 
    0x60,         //      ** 
    0x60,         //      ** 
    0x60,         //      ** 
    0x30,         //     **  
    0x18,         //    **   
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x7C,         //   ***** 
    0x60,         //      ** 
    0x60,         //      ** 
    0x38,         //    ***  
    0x60,         //      ** 
    0x60,         //      ** 
    0x7C,         //   ***** 
    0x3C,         //   ****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x60,         //      ** 
    0x70,         //     *** 
    0x78,         //    **** 
    0x6C,         //   ** ** 
    0x66,         //  **  ** 
    0xFE,         //  *******
    0xFE,         //  *******
    0x60,         //      ** 
    0x60,         //      ** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x78,         //    **** 
    0x78,         //    **** 
    0x0C,         //   **    
    0x3C,         //   ****  
    0x60,         //      ** 
    0x60,         //      ** 
    0x60,         //      ** 
    0x3C,         //   ****  
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x70,         //     *** 
    0x38,         //    ***  
    0x18,         //    **   
    0x7C,         //   ***** 
    0xFE,         //  *******
    0xC6,         //  **   **
    0xC6,         //  **   **
    0xFE,         //  *******
    0x7C,         //   ***** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0xF8,         //    *****
    0x7C,         //   ***** 
    0x60,         //      ** 
    0x30,         //     **  
    0x30,         //     **  
    0x38,         //    ***  
    0x18,         //    **   
    0x18,         //    **   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x3C,         //   ****  
    0x7E,         //  ****** 
    0x66,         //  **  ** 
    0x66,         //  **  ** 
    0x3C,         //   ****  
    0x66,         //  **  ** 
    0x66,         //  **  ** 
    0x7E,         //  ****** 
    0x3C,         //   ****  
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x7C,         //   ***** 
    0xFE,         //  *******
    0xC6,         //  **   **
    0xC6,         //  **   **
    0xFE,         //  *******
    0x7C,         //   ***** 
    0x30,         //     **  
    0x38,         //    ***  
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x03,         // **      
    0x01,         // *       
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x06,         //          **     
    0x80, 0x03,         //        ***      
    0xE0, 0x00,         //      ***        
    0x3C, 0x00,         //   ****          
    0x0C, 0x00,         //   **            
    0x3C, 0x00,         //   ****          
    0xE0, 0x00,         //      ***        
    0x80, 0x03,         //        ***      
    0x00, 0x06,         //          **     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x07,         //   *********     
    0x00, 0x00,         //                 
    0xFC, 0x07,         //   *********     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0C, 0x00,         //   **            
    0x38, 0x00,         //    ***          
    0xE0, 0x00,         //      ***        
    0x80, 0x07,         //        ****     
    0x00, 0x06,         //          **     
    0x80, 0x07,         //        ****     
    0xE0, 0x00,         //      ***        
    0x38, 0x00,         //    ***          
    0x0C, 0x00,         //   **            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0xFC, 0x00,         //   ******        
    0xC6, 0x00,         //  **   **        
    0xC0, 0x00,         //       **        
    0xF0, 0x00,         //     ****        
    0x70, 0x00,         //     ***         
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x03,         //     ******      
    0x18, 0x0C,         //    **     **    
    0x06, 0x10,         //  **         *   
    0xC2, 0x33,         //  *    ****  **  
    0x21, 0x23,         // *    *  **   *  
    0x11, 0x22,         // *   *    *   *  
    0x11, 0x22,         // *   *    *   *  
    0x11, 0x22,         // *   *    *   *  
    0x31, 0x13,         // *   **  **  *   
    0xE2, 0x0F,         //  *   *******    
    0x04, 0x08,         //   *        *    
    0x18, 0x1C,         //    **     ***   
    0xE0, 0x07,         //      ******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0xE0, 0x03,         //      *****      
    0xF0, 0x03,         //     ******      
    0xF0, 0x03,         //     ******      
    0x38, 0x07,         //    ***  ***     
    0x38, 0x07,         //    ***  ***     
    0x38, 0x07,         //    ***  ***     
    0x1C, 0x0E,         //   ***    ***    
    0xFC, 0x0F,         //   **********    
    0xFE, 0x1F,         //  ************   
    0xFE, 0x1F,         //  ************   
    0x0E, 0x1C,         //  ***      ***   
    0x0F, 0x3C,         // ****      ****  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x00,         //  *******        
    0xFE, 0x03,         //  *********      
    0xFE, 0x03,         //  *********      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0xFE, 0x01,         //  ********       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0xFE, 0x07,         //  **********     
    0xFE, 0x03,         //  *********      
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x07,         //      ******     
    0xF8, 0x07,         //    ********     
    0xFC, 0x07,         //   *********     
    0x3C, 0x04,         //   ****    *     
    0x1E, 0x00,         //  ****           
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x1E, 0x00,         //  ****           
    0x3C, 0x04,         //   ****    *     
    0xFC, 0x07,         //   *********     
    0xF8, 0x07,         //    ********     
    0xE0, 0x07,         //      ******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0xFE, 0x07,         //  **********     
    0xFE, 0x0F,         //  ***********    
    0x0E, 0x0F,         //  ***    ****    
    0x0E, 0x1E,         //  ***     ****   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1E,         //  ***     ****   
    0x0E, 0x0F,         //  ***    ****    
    0xFE, 0x0F,         //  ***********    
    0xFE, 0x07,         //  **********     
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0xFE, 0x01,         //  ********       
    0xFE, 0x01,         //  ********       
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xFE, 0x00,         //  *******        
    0xFE, 0x00,         //  *******        
    0xFE, 0x00,         //  *******        
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xFE, 0x01,         //  ********       
    0xFE, 0x01,         //  ********       
    0xFE, 0x01,         //  ********       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x03,         //   ********      
    0xFC, 0x03,         //   ********      
    0xFC, 0x03,         //   ********      
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x07,         //      ******     
    0xF8, 0x1F,         //    **********   
    0xFC, 0x3F,         //   ************  
    0x3C, 0x18,         //   ****     **   
    0x1E, 0x00,         //  ****           
    0x0E, 0x7F,         //  ***    ******* 
    0x0E, 0x7F,         //  ***    ******* 
    0x0E, 0x7F,         //  ***    ******* 
    0x1E, 0x70,         //  ****       *** 
    0x3C, 0x38,         //   ****     ***  
    0xFC, 0x3F,         //   ************  
    0xF8, 0x1F,         //    **********   
    0xE0, 0x07,         //      ******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0xFE, 0x3F,         //  *************  
    0xFE, 0x3F,         //  *************  
    0xFE, 0x3F,         //  *************  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x0E, 0x38,         //  ***       ***  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE8, 0x00,         //    * ***        
    0xFC, 0x00,         //   ******        
    0x7E, 0x00,         //  ******         
    0x3C, 0x00,         //   ****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x3C,         //   ***     ****  
    0x1C, 0x1E,         //   ***    ****   
    0x1C, 0x0F,         //   ***   ****    
    0x9C, 0x07,         //   ***  ****     
    0xDC, 0x03,         //   *** ****      
    0xFC, 0x01,         //   *******       
    0xFC, 0x00,         //   ******        
    0xFC, 0x01,         //   *******       
    0xDC, 0x03,         //   *** ****      
    0x9C, 0x07,         //   ***  ****     
    0x1C, 0x0F,         //   ***   ****    
    0x1C, 0x1E,         //   ***    ****   
    0x1C, 0x3C,         //   ***     ****  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x38, 0x70, 0x00,         //    ***      ***         
    0x78, 0x78, 0x00,         //    ****    ****         
    0x78, 0x78, 0x00,         //    ****    ****         
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0xFC, 0xFC, 0x00,         //   ******  ******        
    0xFC, 0xFC, 0x00,         //   ******  ******        
    0xDC, 0xEF, 0x00,         //   *** ****** ***        
    0xDC, 0xEF, 0x00,         //   *** ****** ***        
    0x9C, 0xEF, 0x00,         //   ***  ***** ***        
    0x9C, 0xC7, 0x00,         //   ***  ****   **        
    0x8E, 0xC7, 0x01,         //  ***   ****   ***       
    0x0E, 0xC3, 0x01,         //  ***    **    ***       
    0x0E, 0xC3, 0x01,         //  ***    **    ***       
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x38,         //  ***       ***  
    0x1E, 0x38,         //  ****      ***  
    0x3E, 0x38,         //  *****     ***  
    0x7E, 0x38,         //  ******    ***  
    0xFE, 0x38,         //  *******   ***  
    0xEE, 0x39,         //  *** ****  ***  
    0xCE, 0x39,         //  ***  ***  ***  
    0xCE, 0x3B,         //  ***  **** ***  
    0x8E, 0x3F,         //  ***   *******  
    0x0E, 0x3F,         //  ***    ******  
    0x0E, 0x3E,         //  ***     *****  
    0x0E, 0x3C,         //  ***      ****  
    0x0E, 0x38,         //  ***       ***  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x07,         //      ******     
    0xF8, 0x1F,         //    **********   
    0xFC, 0x3F,         //   ************  
    0x3C, 0x3C,         //   ****    ****  
    0x1E, 0x78,         //  ****      **** 
    0x0E, 0x70,         //  ***        *** 
    0x0E, 0x70,         //  ***        *** 
    0x0E, 0x70,         //  ***        *** 
    0x1E, 0x78,         //  ****      **** 
    0x3C, 0x3C,         //   ****    ****  
    0xFC, 0x3F,         //   ************  
    0xF8, 0x1F,         //    **********   
    0xE0, 0x07,         //      ******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0xFE, 0x07,         //  **********     
    0xFE, 0x03,         //  *********      
    0xFE, 0x01,         //  ********       
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x07,         //      ******     
    0xF8, 0x1F,         //    **********   
    0xFC, 0x3F,         //   ************  
    0x3C, 0x3C,         //   ****    ****  
    0x1E, 0x78,         //  ****      **** 
    0x0E, 0x70,         //  ***        *** 
    0x0E, 0x70,         //  ***        *** 
    0x8E, 0x77,         //  ***   **** *** 
    0x1E, 0x7F,         //  ****   ******* 
    0x3C, 0x3E,         //   ****   *****  
    0xFC, 0x1F,         //   ***********   
    0xF0, 0x3F,         //     **********  
    0xE0, 0x77,         //      ****** *** 
    0x00, 0x20,         //              *  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x01,         //   *******       
    0xFC, 0x03,         //   ********      
    0xFC, 0x07,         //   *********     
    0x1C, 0x07,         //   ***   ***     
    0x1C, 0x07,         //   ***   ***     
    0x1C, 0x07,         //   ***   ***     
    0xFC, 0x03,         //   ********      
    0xFC, 0x01,         //   *******       
    0xFC, 0x00,         //   ******        
    0xDC, 0x01,         //   *** ***       
    0xDC, 0x03,         //   *** ****      
    0x9C, 0x07,         //   ***  ****     
    0x1C, 0x0F,         //   ***   ****    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0xFC, 0x01,         //   *******       
    0xFE, 0x01,         //  ********       
    0x8E, 0x00,         //  ***   *        
    0x0E, 0x00,         //  ***            
    0xFE, 0x00,         //  *******        
    0xFC, 0x01,         //   *******       
    0xF8, 0x03,         //    *******      
    0x80, 0x03,         //        ***      
    0x84, 0x03,         //   *    ***      
    0xFE, 0x03,         //  *********      
    0xFE, 0x01,         //  ********       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x01,         // *********       
    0xFF, 0x01,         // *********       
    0xFF, 0x01,         // *********       
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x0E, 0x1C,         //  ***      ***   
    0x1E, 0x1E,         //  ****    ****   
    0xFC, 0x0F,         //   **********    
    0xFC, 0x07,         //   *********     
    0xF0, 0x03,         //     ******      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x3C,         // ****      ****  
    0x0E, 0x1C,         //  ***      ***   
    0x1E, 0x1E,         //  ****    ****   
    0x1C, 0x0E,         //   ***    ***    
    0x3C, 0x0F,         //   ****  ****    
    0x3C, 0x0F,         //   ****  ****    
    0x38, 0x07,         //    ***  ***     
    0xF8, 0x07,         //    ********     
    0xF0, 0x03,         //     ******      
    0xF0, 0x03,         //     ******      
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x0F, 0x0E, 0x0E,         // ****     ***     ***    
    0x0F, 0x0F, 0x0F,         // ****    ****    ****    
    0x0E, 0x0F, 0x07,         //  ***    ****    ***     
    0x1E, 0x1F, 0x07,         //  ****   *****   ***     
    0x9E, 0x9F, 0x07,         //  ****  ******  ****     
    0x9C, 0x9F, 0x03,         //   ***  ******  ***      
    0x9C, 0xBF, 0x03,         //   ***  ******* ***      
    0xFC, 0xF9, 0x03,         //   *******  *******      
    0xF8, 0xF9, 0x01,         //    ******  ******       
    0xF8, 0xF9, 0x01,         //    ******  ******       
    0xF8, 0xF0, 0x01,         //    *****    *****       
    0xF0, 0xF0, 0x01,         //     ****    *****       
    0xF0, 0xF0, 0x00,         //     ****    ****        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x3E,         //  *****   *****  
    0x3C, 0x1E,         //   ****   ****   
    0x78, 0x0F,         //    **** ****    
    0x78, 0x0F,         //    **** ****    
    0xF0, 0x07,         //     *******     
    0xE0, 0x03,         //      *****      
    0xE0, 0x03,         //      *****      
    0xF0, 0x07,         //     *******     
    0xF8, 0x0F,         //    *********    
    0x78, 0x0F,         //    **** ****    
    0x3C, 0x1E,         //   ****   ****   
    0x3E, 0x3E,         //  *****   *****  
    0x1F, 0x3C,         // *****     ****  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x1E,         // ****     ****   
    0x1E, 0x0F,         //  ****   ****    
    0xBC, 0x07,         //   **** ****     
    0xB8, 0x03,         //    *** ***      
    0xF8, 0x03,         //    *******      
    0xF0, 0x01,         //     *****       
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x0F,         //   **********    
    0xFC, 0x07,         //   *********     
    0xFC, 0x07,         //   *********     
    0x80, 0x03,         //        ***      
    0xC0, 0x01,         //       ***       
    0xE0, 0x01,         //      ****       
    0xE0, 0x00,         //      ***        
    0xF0, 0x00,         //     ****        
    0x70, 0x00,         //     ***         
    0x38, 0x00,         //    ***          
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFE, 0x0F,         //  ***********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x03,         // **      
    0x03,         // **      
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x04,         //   *     
    0x0C,         //   **    
    0x0C,         //   **    
    0x08,         //    *    
    0x08,         //    *    
    0x18,         //    **   
    0x10,         //     *   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x18,         //    **   
    0x18,         //    **   
    0x34,         //   * **  
    0x24,         //   *  *  
    0x26,         //  **  *  
    0x42,         //  *    * 
    0x43,         // **    * 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x03,         // **********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x08, 0x00,         //    *            
    0x1C, 0x00,         //   ***           
    0x38, 0x00,         //    ***          
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x07,         //    **** ***     
    0xFC, 0x07,         //   *********     
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x07,         //   *********     
    0x78, 0x07,         //    **** ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFE, 0x03,         //  *********      
    0xEE, 0x01,         //  *** ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0xFC, 0x00,         //   ******        
    0xFE, 0x00,         //  *******        
    0x1E, 0x00,         //  ****           
    0x0E, 0x00,         //  ***            
    0x1E, 0x00,         //  ****           
    0xFE, 0x00,         //  *******        
    0xFC, 0x00,         //   ******        
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x78, 0x07,         //    **** ***     
    0xFC, 0x07,         //   *********     
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x07,         //   *********     
    0x78, 0x07,         //    **** ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0xFC, 0x01,         //   *******       
    0x8E, 0x03,         //  ***   ***      
    0xFE, 0x03,         //  *********      
    0xFE, 0x03,         //  *********      
    0x0E, 0x00,         //  ***            
    0x8E, 0x03,         //  ***   ***      
    0xFC, 0x01,         //   *******       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x78,         //    **** 
    0x7C,         //   ***** 
    0x7E,         //  ****** 
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x7F,         // ******* 
    0x7F,         // ******* 
    0x7F,         // ******* 
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x07,         //    **** ***     
    0xFC, 0x07,         //   *********     
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x07,         //   *********     
    0x78, 0x07,         //    **** ***     
    0x00, 0x07,         //         ***     
    0x8E, 0x07,         //  ***   ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         

    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x8E, 0x07,         //  ***   ****     
    0xCE, 0x03,         //  ***  ****      
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x00,         //  *******        
    0xFE, 0x00,         //  *******        
    0xFE, 0x01,         //  ********       
    0xEE, 0x03,         //  *** *****      
    0xCE, 0x07,         //  ***  *****     
    0x8E, 0x0F,         //  ***   *****    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xEE, 0x79, 0x00,         //  *** ****  ****         
    0xFE, 0xFF, 0x00,         //  ***************        
    0xFE, 0xFF, 0x00,         //  ***************        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x8E, 0xE3, 0x00,         //  ***   ***   ***        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x01,         //    ******       
    0xFC, 0x03,         //   ********      
    0xFE, 0x07,         //  **********     
    0x8E, 0x07,         //  ***   ****     
    0x0E, 0x07,         //  ***    ***     
    0x8E, 0x07,         //  ***   ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xEE, 0x01,         //  *** ****       
    0xFE, 0x03,         //  *********      
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFE, 0x03,         //  *********      
    0xEE, 0x01,         //  *** ****       
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x07,         //    **** ***     
    0xFC, 0x07,         //   *********     
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x07,         //   *********     
    0x78, 0x07,         //    **** ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x07,         //         ***     
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0xEE,         //  *** ***
    0xFE,         //  *******
    0xFE,         //  *******
    0x1E,         //  ****   
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0x7E, 0x00,         //  ******         
    0x0E, 0x00,         //  ***            
    0x3E, 0x00,         //  *****          
    0xFC, 0x00,         //   ******        
    0xE0, 0x00,         //      ***        
    0xFE, 0x00,         //  *******        
    0x7F, 0x00,         // *******         
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x7E,         //  ****** 
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x1C,         //   ***   
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x0F,         // ****    ****    
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0x9C, 0x03,         //   ***  ***      
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0xF0, 0x00,         //     ****        
    0xF0, 0x00,         //     ****        
    0x60, 0x00,         //      **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x0F, 0xC3, 0x03,         // ****    **    ****      
    0x8E, 0xC7, 0x01,         //  ***   ****   ***       
    0x9E, 0xE7, 0x01,         //  ****  ****  ****       
    0xDC, 0xEF, 0x00,         //   *** ****** ***        
    0xDC, 0xFF, 0x00,         //   *** **********        
    0xF8, 0x7C, 0x00,         //    *****  *****         
    0xF8, 0x7C, 0x00,         //    *****  *****         
    0x70, 0x3C, 0x00,         //     ***   ****          
    0x70, 0x38, 0x00,         //     ***    ***          
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3C, 0x0F,         //   ****  ****    
    0x38, 0x07,         //    ***  ***     
    0xF0, 0x03,         //     ******      
    0xF0, 0x03,         //     ******      
    0xE0, 0x01,         //      ****       
    0xF0, 0x03,         //     ******      
    0x38, 0x07,         //    ***  ***     
    0x3C, 0x0F,         //   ****  ****    
    0x1E, 0x1E,         //  ****    ****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x0F,         // ****    ****    
    0x8F, 0x07,         // ****   ****     
    0x9E, 0x07,         //  ****  ****     
    0xDC, 0x03,         //   *** ****      
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0xF8, 0x01,         //    ******       
    0xF0, 0x00,         //     ****        
    0x70, 0x00,         //     ***         
    0x78, 0x00,         //    ****         
    0x38, 0x00,         //    ***          
    0x3C, 0x00,         //   ****          
    0x1C, 0x00,         //   ***           
    0x1E, 0x00,         //  ****           
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x03,         //  *********      
    0xFE, 0x01,         //  ********       
    0xFE, 0x01,         //  ********       
    0xF0, 0x00,         //     ****        
    0x70, 0x00,         //     ***         
    0x78, 0x00,         //    ****         
    0xFC, 0x03,         //   ********      
    0xFC, 0x03,         //   ********      
    0xFE, 0x03,         //  *********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x0C, 0x00,         //   **            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x02,         //  *      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xC0, 0x00,         //       **        
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1E, 0x02,         //  ****    *      
    0xE3, 0x01,         // **   ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE7, 0x02,         // ***  *** *      
    0x29, 0x02,         // *  * *   *      
    0xE9, 0x02,         // *  * *** *      
    0x29, 0x02,         // *  * *   *      
    0xE7, 0x0E,         // ***  *** ***    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

/*********************************
 * Font Structure
 * Label: Aharoni_Bold_29
 * Description:  Height: 30 pixels, range: ' ' to ''
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Aharoni_Bold_29[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Aharoni_Bold_29 =
{
    (FLASH | COMP_NONE),
    __Aharoni_Bold_29
};
const char __Aharoni_Bold_29[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Aharoni_Bold_29[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0x20, 0x00,     // First Character
    0x7F, 0x00,     // Last Character
    0x1E,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x08, 0x88, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x07, 0xA6, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0xC4, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x00, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x3C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x78, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x13, 0xD2, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x2C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x4A, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x86, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0xC2, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0xFE, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0x58, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x76, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x06, 0xB2, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xD0, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x0C, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x48, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x84, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xC0, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xFC, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x38, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x74, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xB0, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0xEC, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x28, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x64, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x82, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0xA0, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0xFA, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x54, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAE, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x14, 0xEA, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x15, 0x44, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x9E, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0xF8, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x14, 0x52, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAC, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE8, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x17, 0x24, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x15, 0x7E, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0xD8, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x14, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x14, 0x50, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAA, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x1B, 0xE6, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x17, 0x5E, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x18, 0xB8, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x12, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x18, 0x6C, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0xC6, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x20, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x7A, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x15, 0xB6, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x14, 0x10, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x1D, 0x6A, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x15, 0xE2, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x13, 0x3C, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x96, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0xF0, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x2C, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x09, 0x4A, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0B, 0x86, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0xC2, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0xFE, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x3A, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x94, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0xEE, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x2A, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x84, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDE, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x1A, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x74, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xCE, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0xEC, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x0A, 0x16, 0x00,           // width, MSB Offset, LSB offset
    0x08, 0x64, 0x16, 0x00,           // width, MSB Offset, LSB offset
    0x19, 0x82, 0x16, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0xFA, 0x16, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x54, 0x17, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0xAE, 0x17, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x08, 0x18, 0x00,           // width, MSB Offset, LSB offset
    0x0C, 0x62, 0x18, 0x00,           // width, MSB Offset, LSB offset
    0x0D, 0x9E, 0x18, 0x00,           // width, MSB Offset, LSB offset
    0x0A, 0xDA, 0x18, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x16, 0x19, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x70, 0x19, 0x00,           // width, MSB Offset, LSB offset
    0x1A, 0xCA, 0x19, 0x00,           // width, MSB Offset, LSB offset
    0x13, 0x42, 0x1A, 0x00,           // width, MSB Offset, LSB offset
    0x12, 0x9C, 0x1A, 0x00,           // width, MSB Offset, LSB offset
    0x0F, 0xF6, 0x1A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x32, 0x1B, 0x00,           // width, MSB Offset, LSB offset
    0x04, 0x6E, 0x1B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x8C, 0x1B, 0x00,           // width, MSB Offset, LSB offset
    0x10, 0xC8, 0x1B, 0x00,           // width, MSB Offset, LSB offset
    0x11, 0x04, 0x1C, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x00,         //         
    0x0C,         //   **    
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x9C, 0x03,         //   ***  ***      
    0x8C, 0x01,         //   **   **       
    0xCE, 0x01,         //  ***  ***       
    0xCE, 0x01,         //  ***  ***       
    0xC6, 0x00,         //  **   **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x98, 0x01,         //    **  **       
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0xFF, 0x03,         // **********      
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0xFE, 0x03,         //  *********      
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0xCC, 0x00,         //   **  **        
    0x44, 0x00,         //   *   *         
    0x44, 0x00,         //   *   *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0xF8, 0x00,         //    *****        
    0xFC, 0x01,         //   *******       
    0xFE, 0x03,         //  *********      
    0xAE, 0x03,         //  *** * ***      
    0x7E, 0x00,         //  ******         
    0xFC, 0x01,         //   *******       
    0xF8, 0x03,         //    *******      
    0xA0, 0x03,         //      * ***      
    0xA0, 0x03,         //      * ***      
    0xAE, 0x03,         //  *** * ***      
    0xFE, 0x03,         //  *********      
    0xFC, 0x01,         //   *******       
    0xF8, 0x00,         //    *****        
    0x20, 0x00,         //      *          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x38, 0x30, 0x00,         //    ***      **          
    0x7C, 0x10, 0x00,         //   *****     *           
    0xC6, 0x18, 0x00,         //  **   **   **           
    0xC6, 0x0C, 0x00,         //  **   **  **            
    0xC6, 0x04, 0x00,         //  **   **  *             
    0xC6, 0xE6, 0x00,         //  **   ** **  ***        
    0x7C, 0xF2, 0x01,         //   *****  *  *****       
    0x38, 0x1B, 0x03,         //    ***  ** **   **      
    0x00, 0x19, 0x03,         //         *  **   **      
    0x80, 0x19, 0x03,         //        **  **   **      
    0xC0, 0x18, 0x03,         //       **   **   **      
    0x40, 0xF0, 0x01,         //       *     *****       
    0x60, 0xE0, 0x00,         //      **      ***        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x80, 0x07, 0x00,         //        ****             
    0xE0, 0x1F, 0x00,         //      ********           
    0xF0, 0x3F, 0x00,         //     **********          
    0xF0, 0x3C, 0x00,         //     ****  ****          
    0xF0, 0x3C, 0x00,         //     ****  ****          
    0xF0, 0x3C, 0x00,         //     ****  ****          
    0xE0, 0x1F, 0x00,         //      ********           
    0xE0, 0x0F, 0x00,         //      *******            
    0xE0, 0x07, 0x00,         //      ******             
    0xF0, 0xCF, 0x00,         //     ********  **        
    0xF8, 0xDF, 0x01,         //    ********** ***       
    0x3C, 0xFE, 0x01,         //   ****   ********       
    0x3C, 0xFC, 0x00,         //   ****    ******        
    0x3C, 0xF8, 0x01,         //   ****     ******       
    0x7C, 0xFC, 0x03,         //   *****   ********      
    0xF8, 0xFF, 0x01,         //    **************       
    0xF0, 0xDF, 0x01,         //     ********* ***       
    0xE0, 0x87, 0x00,         //      ******    *        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x0C,         //   **    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x00,         //      ***        
    0xF8, 0x00,         //    *****        
    0xFC, 0x00,         //   ******        
    0x3C, 0x00,         //   ****          
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x1C, 0x00,         //   ***           
    0x3C, 0x00,         //   ****          
    0xFC, 0x00,         //   ******        
    0xF8, 0x00,         //    *****        
    0xE0, 0x00,         //      ***        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1E, 0x00,         //  ****           
    0x3E, 0x00,         //  *****          
    0x7E, 0x00,         //  ******         
    0x78, 0x00,         //    ****         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x78, 0x00,         //    ****         
    0x7E, 0x00,         //  ******         
    0x3E, 0x00,         //  *****          
    0x0E, 0x00,         //  ***            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x54, 0x00,         //   * * *         
    0xFE, 0x00,         //  *******        
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFE, 0x00,         //  *******        
    0x54, 0x00,         //   * * *         
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x03, 0x00,         //         **              
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x1C,         //   ***   
    0x0C,         //   **    
    0x0E,         //  ***    
    0x0E,         //  ***    
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7E, 0x00,         //  ******         
    0x7E, 0x00,         //  ******         
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x01,         //        **       
    0x80, 0x00,         //        *        
    0xC0, 0x00,         //       **        
    0x40, 0x00,         //       *         
    0x60, 0x00,         //      **         
    0x20, 0x00,         //      *          
    0x30, 0x00,         //     **          
    0x10, 0x00,         //     *           
    0x18, 0x00,         //    **           
    0x08, 0x00,         //    *            
    0x0C, 0x00,         //   **            
    0x0C, 0x00,         //   **            
    0x06, 0x00,         //  **             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0xF8, 0x01,         //    ******       
    0xFC, 0x03,         //   ********      
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFC, 0x03,         //   ********      
    0xF8, 0x01,         //    ******       
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x01,         //        **       
    0xC0, 0x01,         //       ***       
    0xE0, 0x01,         //      ****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xD0, 0x01,         //     * ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0xFC, 0x01,         //   *******       
    0xFC, 0x03,         //   ********      
    0x80, 0x03,         //        ***      
    0x80, 0x03,         //        ***      
    0x80, 0x03,         //        ***      
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xE0, 0x00,         //      ***        
    0x70, 0x00,         //     ***         
    0xF8, 0x07,         //    ********     
    0xFC, 0x07,         //   *********     
    0xFC, 0x03,         //   ********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0xF8, 0x01,         //    ******       
    0xF8, 0x03,         //    *******      
    0x80, 0x03,         //        ***      
    0x80, 0x03,         //        ***      
    0xE0, 0x01,         //      ****       
    0xE0, 0x03,         //      *****      
    0x80, 0x07,         //        ****     
    0x00, 0x07,         //         ***     
    0x80, 0x07,         //        ****     
    0xF8, 0x07,         //    ********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x03,         //        ***      
    0xC0, 0x03,         //       ****      
    0xE0, 0x03,         //      *****      
    0xF0, 0x03,         //     ******      
    0xB0, 0x03,         //     ** ***      
    0x98, 0x03,         //    **  ***      
    0x8C, 0x03,         //   **   ***      
    0x86, 0x03,         //  **    ***      
    0xFE, 0x0F,         //  ***********    
    0xFE, 0x0F,         //  ***********    
    0xFE, 0x07,         //  **********     
    0x80, 0x03,         //        ***      
    0x80, 0x03,         //        ***      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x07,         //     *******     
    0xF0, 0x03,         //     ******      
    0xF0, 0x03,         //     ******      
    0x78, 0x00,         //    ****         
    0xF8, 0x00,         //    *****        
    0xF8, 0x01,         //    ******       
    0xC0, 0x03,         //       ****      
    0x80, 0x03,         //        ***      
    0x80, 0x03,         //        ***      
    0xC0, 0x03,         //       ****      
    0xF8, 0x01,         //    ******       
    0xF8, 0x01,         //    ******       
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x03,         //       ****      
    0xE0, 0x01,         //      ****       
    0xF0, 0x00,         //     ****        
    0x78, 0x00,         //    ****         
    0xFC, 0x01,         //   *******       
    0xFC, 0x03,         //   ********      
    0xFE, 0x07,         //  **********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFC, 0x03,         //   ********      
    0xFC, 0x03,         //   ********      
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x03,         //    *******      
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0xC0, 0x01,         //       ***       
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0xE0, 0x00,         //      ***        
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x38, 0x00,         //    ***          
    0x1C, 0x00,         //   ***           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0xFC, 0x01,         //   *******       
    0xFE, 0x03,         //  *********      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0xFC, 0x01,         //   *******       
    0xFC, 0x01,         //   *******       
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0x8E, 0x03,         //  ***   ***      
    0xFE, 0x03,         //  *********      
    0xFC, 0x01,         //   *******       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0xFC, 0x03,         //   ********      
    0xFC, 0x07,         //   *********     
    0x9E, 0x07,         //  ****  ****     
    0x0E, 0x07,         //  ***    ***     
    0x9E, 0x07,         //  ****  ****     
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x03,         //    *******      
    0xE0, 0x01,         //      ****       
    0xF0, 0x00,         //     ****        
    0x78, 0x00,         //    ****         
    0x3C, 0x00,         //   ****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0C,         //   **    
    0x1E,         //  ****   
    0x1E,         //  ****   
    0x0C,         //   **    
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x0E,         //  ***    
    0x06,         //  **     
    0x07,         // ***     
    0x07,         // ***     
    0x03,         // **      
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x60, 0x00,         //              **         
    0x00, 0x7C, 0x00,         //           *****         
    0x00, 0x1F, 0x00,         //         *****           
    0xC0, 0x07, 0x00,         //       *****             
    0xF0, 0x01, 0x00,         //     *****               
    0x7C, 0x00, 0x00,         //   *****                 
    0x0C, 0x00, 0x00,         //   **                    
    0x7C, 0x00, 0x00,         //   *****                 
    0xF0, 0x01, 0x00,         //     *****               
    0xC0, 0x07, 0x00,         //       *****             
    0x00, 0x1F, 0x00,         //         *****           
    0x00, 0x7C, 0x00,         //           *****         
    0x00, 0x60, 0x00,         //              **         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x0C, 0x00, 0x00,         //   **                    
    0x7C, 0x00, 0x00,         //   *****                 
    0xF0, 0x01, 0x00,         //     *****               
    0xC0, 0x07, 0x00,         //       *****             
    0x00, 0x1F, 0x00,         //         *****           
    0x00, 0x7C, 0x00,         //           *****         
    0x00, 0x60, 0x00,         //              **         
    0x00, 0x7C, 0x00,         //           *****         
    0x00, 0x1F, 0x00,         //         *****           
    0xC0, 0x07, 0x00,         //       *****             
    0xF0, 0x01, 0x00,         //     *****               
    0x7C, 0x00, 0x00,         //   *****                 
    0x0C, 0x00, 0x00,         //   **                    
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0xFC, 0x07,         //   *********     
    0xFC, 0x07,         //   *********     
    0x1E, 0x0F,         //  ****   ****    
    0x00, 0x0F,         //         ****    
    0x80, 0x0F,         //        *****    
    0xF0, 0x0F,         //     ********    
    0xF0, 0x07,         //     *******     
    0xF0, 0x03,         //     ******      
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x60, 0x00,         //      **         
    0xF0, 0x00,         //     ****        
    0xF0, 0x00,         //     ****        
    0x60, 0x00,         //      **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x80, 0x3F, 0x00,         //        *******          
    0xE0, 0xFF, 0x00,         //      ***********        
    0x78, 0xE0, 0x01,         //    ****      ****       
    0x1C, 0x80, 0x03,         //   ***          ***      
    0x0E, 0x00, 0x07,         //  ***            ***     
    0x06, 0x7F, 0x06,         //  **     *******  **     
    0xC7, 0x7F, 0x0C,         // ***   *********   **    
    0xC3, 0x70, 0x0C,         // **    **    ***   **    
    0x63, 0x60, 0x0C,         // **   **      **   **    
    0x63, 0x60, 0x0C,         // **   **      **   **    
    0x63, 0x60, 0x0C,         // **   **      **   **    
    0x63, 0x70, 0x06,         // **   **     ***  **     
    0xC6, 0x78, 0x07,         //  **   **   **** ***     
    0xC6, 0xFF, 0x03,         //  **   ************      
    0x0C, 0xEF, 0x01,         //   **    **** ****       
    0x1C, 0x00, 0x00,         //   ***                   
    0x78, 0x80, 0x01,         //    ****        **       
    0xE0, 0xFF, 0x03,         //      *************      
    0x80, 0x7F, 0x00,         //        ********         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x80, 0x1F, 0x00,         //        ******           
    0x80, 0x1F, 0x00,         //        ******           
    0xC0, 0x3F, 0x00,         //       ********          
    0xC0, 0x3F, 0x00,         //       ********          
    0xC0, 0x7F, 0x00,         //       *********         
    0xE0, 0x7F, 0x00,         //      **********         
    0xE0, 0x7B, 0x00,         //      ***** ****         
    0xF0, 0xF9, 0x00,         //     *****  *****        
    0xF0, 0xF1, 0x00,         //     *****   ****        
    0xF0, 0xF1, 0x00,         //     *****   ****        
    0xF8, 0xF0, 0x01,         //    *****    *****       
    0xF8, 0xE0, 0x01,         //    *****     ****       
    0xF8, 0xFF, 0x03,         //    ***************      
    0xFC, 0xFF, 0x03,         //   ****************      
    0xFC, 0xFF, 0x03,         //   ****************      
    0xFE, 0xFF, 0x07,         //  ******************     
    0x3E, 0xC0, 0x07,         //  *****        *****     
    0x3E, 0xC0, 0x07,         //  *****        *****     
    0x1F, 0xC0, 0x0F,         // *****         ******    
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0x1F, 0x00,         //   ***********           
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0xFF, 0x00,         //   **************        
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0xFF, 0x00,         //   **************        
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0xFC, 0xFF, 0x01,         //   ***************       
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x3F, 0x00,         //   ************          
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x7F, 0x00,         //         *******         
    0xC0, 0xFF, 0x00,         //       **********        
    0xF0, 0xFF, 0x00,         //     ************        
    0xF8, 0xFF, 0x00,         //    *************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xC1, 0x00,         //   *******     **        
    0x7E, 0x80, 0x00,         //  ******        *        
    0x3E, 0x00, 0x00,         //  *****                  
    0x3E, 0x00, 0x00,         //  *****                  
    0x3E, 0x00, 0x00,         //  *****                  
    0x3E, 0x00, 0x00,         //  *****                  
    0x3E, 0x00, 0x00,         //  *****                  
    0x7E, 0x80, 0x00,         //  ******        *        
    0xFC, 0xC1, 0x00,         //   *******     **        
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0xFF, 0x00,         //    *************        
    0xF0, 0xFF, 0x00,         //     ************        
    0xC0, 0xFF, 0x00,         //       **********        
    0x00, 0x7F, 0x00,         //         *******         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x01,         //   ***************       
    0x7C, 0xF8, 0x03,         //   *****    *******      
    0x7C, 0xF0, 0x03,         //   *****     ******      
    0x7C, 0xE0, 0x03,         //   *****      *****      
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xE0, 0x03,         //   *****      *****      
    0x7C, 0xF0, 0x03,         //   *****     ******      
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0xFC, 0xFF, 0x01,         //   ***************       
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x0F, 0x00,         //   **********            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0xFF, 0x00,         //         ********        
    0xC0, 0xFF, 0x03,         //       ************      
    0xF0, 0xFF, 0x0F,         //     ****************    
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xFC, 0xFF, 0x07,         //   *****************     
    0xFC, 0x01, 0x00,         //   *******               
    0x7E, 0x00, 0x00,         //  ******                 
    0x7E, 0xF0, 0x3F,         //  ******     **********  
    0x3E, 0xF0, 0x3F,         //  *****      **********  
    0x3E, 0xF0, 0x3F,         //  *****      **********  
    0x3E, 0xF0, 0x3F,         //  *****      **********  
    0x7E, 0x00, 0x3E,         //  ******          *****  
    0x7E, 0x00, 0x1F,         //  ******         *****   
    0xFC, 0x81, 0x1F,         //   *******      ******   
    0xFC, 0xFF, 0x0F,         //   ******************    
    0xF8, 0xFF, 0x0F,         //    *****************    
    0xF0, 0xFF, 0x07,         //     ***************     
    0xC0, 0xFF, 0x01,         //       ***********       
    0x00, 0x7F, 0x00,         //         *******         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0xFC, 0xFF, 0x07,         //   *****************     
    0xFC, 0xFF, 0x07,         //   *****************     
    0xFC, 0xFF, 0x07,         //   *****************     
    0xFC, 0xFF, 0x07,         //   *****************     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x80, 0x0F,         //        *****    
    0x90, 0x0F,         //     *  *****    
    0xF8, 0x0F,         //    *********    
    0xFE, 0x07,         //  **********     
    0xFC, 0x03,         //   ********      
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xE0, 0x03,         //   *****      *****      
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x3E, 0x00,         //   *****  *****          
    0x7C, 0x1F, 0x00,         //   ***** *****           
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x07, 0x00,         //   *********             
    0xFC, 0x07, 0x00,         //   *********             
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x1F, 0x00,         //   ***********           
    0x7C, 0x3F, 0x00,         //   ***** ******          
    0x7C, 0x7E, 0x00,         //   *****  ******         
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0x7C, 0xF0, 0x03,         //   *****     ******      
    0x7C, 0xE0, 0x07,         //   *****      ******     
    0x7C, 0xC0, 0x0F,         //   *****       ******    
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0xF0, 0x01, 0x7C, 0x00,         //     *****         *****         
    0xF0, 0x03, 0x7E, 0x00,         //     ******       ******         
    0xF0, 0x03, 0x7E, 0x00,         //     ******       ******         
    0xF8, 0x07, 0x7F, 0x00,         //    ********     *******         
    0xF8, 0x07, 0x7F, 0x00,         //    ********     *******         
    0xF8, 0x87, 0xFF, 0x00,         //    ********    *********        
    0xF8, 0x8F, 0xFF, 0x00,         //    *********   *********        
    0xF8, 0x8F, 0xFF, 0x00,         //    *********   *********        
    0xF8, 0xDF, 0xFB, 0x00,         //    ********** **** *****        
    0x7C, 0xDF, 0xFB, 0x00,         //   ***** ***** **** *****        
    0x7C, 0xFE, 0xFB, 0x00,         //   *****  ********* *****        
    0x7C, 0xFE, 0xF9, 0x00,         //   *****  ********  *****        
    0x7C, 0xFE, 0xF9, 0x00,         //   *****  ********  *****        
    0x7C, 0xFC, 0xF8, 0x00,         //   *****   ******   *****        
    0x7C, 0xFC, 0xF8, 0x01,         //   *****   ******   ******       
    0x7C, 0x78, 0xF0, 0x01,         //   *****    ****     *****       
    0x3E, 0x78, 0xF0, 0x01,         //  *****     ****     *****       
    0x3E, 0x78, 0xF0, 0x01,         //  *****     ****     *****       
    0x3E, 0x30, 0xF0, 0x01,         //  *****      **      *****       
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0x00, 0x1F,         //   *****         *****   
    0xFC, 0x00, 0x1F,         //   ******        *****   
    0xFC, 0x01, 0x1F,         //   *******       *****   
    0xFC, 0x03, 0x1F,         //   ********      *****   
    0xFC, 0x03, 0x1F,         //   ********      *****   
    0xFC, 0x07, 0x1F,         //   *********     *****   
    0xFC, 0x0F, 0x1F,         //   **********    *****   
    0xFC, 0x1F, 0x1F,         //   ***********   *****   
    0x7C, 0x3F, 0x1F,         //   ***** ******  *****   
    0x7C, 0x3E, 0x1F,         //   *****  *****  *****   
    0x7C, 0x7E, 0x1F,         //   *****  ****** *****   
    0x7C, 0xFC, 0x1F,         //   *****   ***********   
    0x7C, 0xF8, 0x1F,         //   *****    **********   
    0x7C, 0xF0, 0x1F,         //   *****     *********   
    0x7C, 0xE0, 0x1F,         //   *****      ********   
    0x7C, 0xE0, 0x1F,         //   *****      ********   
    0x7C, 0xC0, 0x1F,         //   *****       *******   
    0x7C, 0x80, 0x1F,         //   *****        ******   
    0x7C, 0x00, 0x1F,         //   *****         *****   
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0xFF, 0x00,         //         ********        
    0xC0, 0xFF, 0x03,         //       ************      
    0xF0, 0xFF, 0x0F,         //     ****************    
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xFC, 0x81, 0x3F,         //   *******      *******  
    0xFC, 0x00, 0x3F,         //   ******        ******  
    0x7E, 0x00, 0x7E,         //  ******          ****** 
    0x3E, 0x00, 0x7C,         //  *****            ***** 
    0x3E, 0x00, 0x7C,         //  *****            ***** 
    0x3E, 0x00, 0x7C,         //  *****            ***** 
    0x7E, 0x00, 0x7E,         //  ******          ****** 
    0xFC, 0x00, 0x3F,         //   ******        ******  
    0xFC, 0x81, 0x3F,         //   *******      *******  
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xF0, 0xFF, 0x0F,         //     ****************    
    0xC0, 0xFF, 0x03,         //       ************      
    0x00, 0xFF, 0x00,         //         ********        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x3F, 0x00,         //   ************          
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x3F, 0x00,         //   ************          
    0xFC, 0x1F, 0x00,         //   ***********           
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0xFF, 0x00,         //         ********        
    0xC0, 0xFF, 0x03,         //       ************      
    0xF0, 0xFF, 0x0F,         //     ****************    
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xFC, 0x81, 0x3F,         //   *******      *******  
    0xFC, 0x00, 0x3E,         //   ******         *****  
    0x7E, 0x00, 0x7C,         //  ******           ***** 
    0x3E, 0x00, 0x7C,         //  *****            ***** 
    0x3E, 0x00, 0x7C,         //  *****            ***** 
    0x3E, 0xE0, 0x7C,         //  *****       ***  ***** 
    0x7E, 0xE0, 0x7F,         //  ******      ********** 
    0xFC, 0xC0, 0x3F,         //   ******      ********  
    0xFC, 0x83, 0x3F,         //   ********     *******  
    0xF8, 0xFF, 0x1F,         //    ******************   
    0xF8, 0xFF, 0x0F,         //    *****************    
    0xF0, 0xFF, 0x1F,         //     *****************   
    0xC0, 0xFF, 0x3F,         //       ****************  
    0x00, 0xFF, 0x7C,         //         ********  ***** 
    0x00, 0x00, 0x18,         //                    **   
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0x1F, 0x00,         //   ***********           
    0xFC, 0x3F, 0x00,         //   ************          
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0xFF, 0x00,         //   **************        
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xF8, 0x00,         //   *****    *****        
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x1F, 0x00,         //   ***********           
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x1F, 0x00,         //   ***********           
    0x7C, 0x1F, 0x00,         //   ***** *****           
    0x7C, 0x3F, 0x00,         //   ***** ******          
    0x7C, 0x7E, 0x00,         //   *****  ******         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xC0, 0x0F, 0x00,         //       ******            
    0xF0, 0x7F, 0x00,         //     ***********         
    0xF8, 0x3F, 0x00,         //    ***********          
    0xF8, 0x3F, 0x00,         //    ***********          
    0x7C, 0x10, 0x00,         //   *****     *           
    0x7C, 0x00, 0x00,         //   *****                 
    0xFC, 0x01, 0x00,         //   *******               
    0xFC, 0x0F, 0x00,         //   **********            
    0xF8, 0x3F, 0x00,         //    ***********          
    0xF0, 0x7F, 0x00,         //     ***********         
    0xC0, 0xFF, 0x00,         //       **********        
    0x00, 0xFE, 0x00,         //          *******        
    0x00, 0xF8, 0x00,         //            *****        
    0x10, 0xF8, 0x00,         //     *      *****        
    0x38, 0xFC, 0x00,         //    ***    ******        
    0xF8, 0x7F, 0x00,         //    ************         
    0xFC, 0x7F, 0x00,         //   *************         
    0xF8, 0x3F, 0x00,         //    ***********          
    0xE0, 0x0F, 0x00,         //      *******            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x1F,         // *************   
    0xFF, 0x1F,         // *************   
    0xFF, 0x1F,         // *************   
    0xFF, 0x1F,         // *************   
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xC0, 0x07,         //   *****       *****     
    0x7C, 0xE0, 0x07,         //   *****      ******     
    0xFC, 0xF0, 0x07,         //   ******    *******     
    0xFC, 0xFF, 0x03,         //   ****************      
    0xF8, 0xFF, 0x03,         //    ***************      
    0xF0, 0xFF, 0x01,         //     *************       
    0xE0, 0xFF, 0x00,         //      ***********        
    0x80, 0x3F, 0x00,         //        *******          
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x3F, 0xC0, 0x0F,         // ******        ******    
    0x3E, 0xC0, 0x07,         //  *****        *****     
    0x3E, 0xC0, 0x07,         //  *****        *****     
    0x7E, 0xE0, 0x07,         //  ******      ******     
    0x7C, 0xE0, 0x03,         //   *****      *****      
    0xFC, 0xF0, 0x03,         //   ******    ******      
    0xF8, 0xF0, 0x01,         //    *****    *****       
    0xF8, 0xF0, 0x01,         //    *****    *****       
    0xF0, 0xF9, 0x00,         //     *****  *****        
    0xF0, 0xF9, 0x00,         //     *****  *****        
    0xF0, 0xFF, 0x00,         //     ************        
    0xE0, 0x7F, 0x00,         //      **********         
    0xE0, 0x7F, 0x00,         //      **********         
    0xC0, 0x3F, 0x00,         //       ********          
    0xC0, 0x3F, 0x00,         //       ********          
    0x80, 0x1F, 0x00,         //        ******           
    0x80, 0x1F, 0x00,         //        ******           
    0x80, 0x1F, 0x00,         //        ******           
    0x00, 0x0F, 0x00,         //         ****            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x1F, 0xE0, 0x01, 0x1F,         // *****        ****       *****   
    0x3F, 0xF0, 0x01, 0x1F,         // ******      *****       *****   
    0x3E, 0xF0, 0x81, 0x1F,         //  *****      *****      ******   
    0x3E, 0xF0, 0x83, 0x0F,         //  *****      ******     *****    
    0x3E, 0xF8, 0x83, 0x0F,         //  *****     *******     *****    
    0x7C, 0xF8, 0x83, 0x0F,         //   *****    *******     *****    
    0x7C, 0xF8, 0xC7, 0x07,         //   *****    ********   *****     
    0x7C, 0xFC, 0xC7, 0x07,         //   *****   *********   *****     
    0xF8, 0xBC, 0xC7, 0x07,         //    *****  **** ****   *****     
    0xF8, 0xBC, 0xEF, 0x03,         //    *****  **** ***** *****      
    0xF8, 0xBE, 0xEF, 0x03,         //    ***** ***** ***** *****      
    0xF8, 0x1E, 0xEF, 0x03,         //    ***** ****   **** *****      
    0xF0, 0x1F, 0xFF, 0x01,         //     *********   *********       
    0xF0, 0x1F, 0xFF, 0x01,         //     *********   *********       
    0xF0, 0x0F, 0xFE, 0x01,         //     ********     ********       
    0xE0, 0x0F, 0xFE, 0x00,         //      *******     *******        
    0xE0, 0x0F, 0xFE, 0x00,         //      *******     *******        
    0xE0, 0x07, 0xFC, 0x00,         //      ******       ******        
    0xC0, 0x07, 0x7C, 0x00,         //       *****       *****         
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFE, 0xE0, 0x0F,         //  *******     *******    
    0xFC, 0xE0, 0x07,         //   ******     ******     
    0xF8, 0xF1, 0x03,         //    ******   ******      
    0xF8, 0xFB, 0x03,         //    ******* *******      
    0xF0, 0xFB, 0x01,         //     ****** ******       
    0xE0, 0xFF, 0x00,         //      ***********        
    0xE0, 0xFF, 0x00,         //      ***********        
    0xC0, 0x7F, 0x00,         //       *********         
    0x80, 0x3F, 0x00,         //        *******          
    0x80, 0x3F, 0x00,         //        *******          
    0xC0, 0x7F, 0x00,         //       *********         
    0xE0, 0xFF, 0x00,         //      ***********        
    0xF0, 0xFF, 0x01,         //     *************       
    0xF0, 0xFB, 0x01,         //     ****** ******       
    0xF8, 0xF1, 0x03,         //    ******   ******      
    0xFC, 0xF1, 0x07,         //   *******   *******     
    0xFC, 0xE0, 0x07,         //   ******     ******     
    0x7E, 0xC0, 0x0F,         //  ******       ******    
    0x3F, 0xC0, 0x1F,         // ******        *******   
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x3F, 0xE0, 0x07,         // ******       ******     
    0x7E, 0xF0, 0x03,         //  ******     ******      
    0xFC, 0xF8, 0x01,         //   ******   ******       
    0xFC, 0xF9, 0x01,         //   *******  ******       
    0xF8, 0xFD, 0x00,         //    ****** ******        
    0xF0, 0x7F, 0x00,         //     ***********         
    0xE0, 0x3F, 0x00,         //      *********          
    0xE0, 0x3F, 0x00,         //      *********          
    0xC0, 0x1F, 0x00,         //       *******           
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x80, 0x0F, 0x00,         //        *****            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0xFF, 0x01,         //   ***************       
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0x7F, 0x00,         //   *************         
    0x00, 0x7E, 0x00,         //          ******         
    0x00, 0x3F, 0x00,         //         ******          
    0x00, 0x3F, 0x00,         //         ******          
    0x80, 0x1F, 0x00,         //        ******           
    0x80, 0x0F, 0x00,         //        *****            
    0xC0, 0x0F, 0x00,         //       ******            
    0xC0, 0x07, 0x00,         //       *****             
    0xE0, 0x07, 0x00,         //      ******             
    0xF0, 0x03, 0x00,         //     ******              
    0xF0, 0x03, 0x00,         //     ******              
    0xF8, 0x01, 0x00,         //    ******               
    0xF8, 0xFF, 0x01,         //    **************       
    0xFC, 0xFF, 0x01,         //   ***************       
    0xFC, 0xFF, 0x01,         //   ***************       
    0xFE, 0xFF, 0x01,         //  ****************       
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x0E, 0x00,         //  ***            
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x03,         // **      
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x0C,         //   **    
    0x0C,         //   **    
    0x0C,         //   **    
    0x18,         //    **   
    0x18,         //    **   
    0x18,         //    **   
    0x30,         //     **  
    0x30,         //     **  
    0x30,         //     **  
    0x60,         //      ** 
    0x60,         //      ** 
    0x60,         //      ** 
    0xC0,         //       **
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x70, 0x00,         //     ***         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x60, 0x00,         //      **         
    0x70, 0x00,         //     ***         
    0xD0, 0x00,         //     * **        
    0xD8, 0x00,         //    ** **        
    0x88, 0x00,         //    *   *        
    0x8C, 0x01,         //   **   **       
    0x0C, 0x01,         //   **    *       
    0x06, 0x03,         //  **     **      
    0x06, 0x02,         //  **      *      
    0x02, 0x06,         //  *       **     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x7F,         // *************** 
    0xFF, 0x7F,         // *************** 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x70, 0x00,         //     ***         
    0xF8, 0x00,         //    *****        
    0xF0, 0x00,         //     ****        
    0xE0, 0x01,         //      ****       
    0xC0, 0x00,         //       **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0x7C, 0x7F, 0x00,         //   ***** *******         
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xF8, 0x01,         //   ******   ******       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0xFC, 0xF8, 0x01,         //   ******   ******       
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7C, 0x7F, 0x00,         //   ***** *******         
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x03,         //      *****      
    0xF8, 0x07,         //    ********     
    0xFC, 0x07,         //   *********     
    0xFC, 0x07,         //   *********     
    0x7E, 0x04,         //  ******   *     
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0x7E, 0x04,         //  ******   *     
    0xFC, 0x07,         //   *********     
    0xFC, 0x07,         //   *********     
    0xF8, 0x07,         //    ********     
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xE0, 0x0F, 0x00,         //      *******            
    0xF8, 0x1F, 0x00,         //    **********           
    0xFC, 0x3F, 0x00,         //   ************          
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0xFE, 0xFF, 0x00,         //  ***************        
    0xFE, 0xFF, 0x00,         //  ***************        
    0xFE, 0xFF, 0x00,         //  ***************        
    0x3E, 0x00, 0x00,         //  *****                  
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0xFC, 0x7F, 0x00,         //   *************         
    0xF0, 0x3F, 0x00,         //     **********          
    0xC0, 0x0F, 0x00,         //       ******            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0xE0, 0x03,         //      *****      
    0xF0, 0x03,         //     ******      
    0xF8, 0x03,         //    *******      
    0xFC, 0x03,         //   ********      
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0xFF, 0x00,         //    *************        
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0xFC, 0x7F, 0x00,         //   *************         
    0xF8, 0x7F, 0x00,         //    ************         
    0xF0, 0x3F, 0x00,         //     **********          
    0xE0, 0x0F, 0x00,         //      *******            
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0xFC, 0x3F, 0x00,         //   ************          
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00,         //         
    0x00,         //         
    0x38,         //    ***  
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x38,         //    ***  
    0x00,         //         
    0x00,         //         
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00,         //         
    0x00,         //         
    0x38,         //    ***  
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x38,         //    ***  
    0x00,         //         
    0x00,         //         
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x00,         //         

    0x00, 0x00, 0x00,         //                         
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0xF8, 0x03,         //   *****    *******      
    0x7C, 0xFC, 0x01,         //   *****   *******       
    0x7C, 0x7E, 0x00,         //   *****  ******         
    0x7C, 0x3F, 0x00,         //   ***** ******          
    0xFC, 0x1F, 0x00,         //   ***********           
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x0F, 0x00,         //   **********            
    0xFC, 0x1F, 0x00,         //   ***********           
    0x7C, 0x3F, 0x00,         //   ***** ******          
    0x7C, 0x7E, 0x00,         //   *****  ******         
    0x7C, 0xFC, 0x00,         //   *****   ******        
    0x7C, 0xF8, 0x01,         //   *****    ******       
    0x7C, 0xF0, 0x03,         //   *****     ******      
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00,         //         
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x7C,         //   ***** 
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x7C, 0x3E, 0x7C, 0x00,         //   *****  *****    *****         
    0x7C, 0x7F, 0xFE, 0x00,         //   ***** *******  *******        
    0xFC, 0xFF, 0xFF, 0x01,         //   ***********************       
    0xFC, 0xFF, 0xFF, 0x01,         //   ***********************       
    0xFC, 0xFC, 0xF9, 0x01,         //   ******  *******  ******       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x7C, 0xF8, 0xF0, 0x01,         //   *****    *****    *****       
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0xFC, 0x3F, 0x00,         //   ************          
    0xFC, 0x7F, 0x00,         //   *************         
    0xFC, 0x7F, 0x00,         //   *************         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xC0, 0x0F, 0x00,         //       ******            
    0xF0, 0x3F, 0x00,         //     **********          
    0xF8, 0x7F, 0x00,         //    ************         
    0xFC, 0xFF, 0x00,         //   **************        
    0x7E, 0xF8, 0x01,         //  ******    ******       
    0x3E, 0xF0, 0x01,         //  *****      *****       
    0x3E, 0xF0, 0x01,         //  *****      *****       
    0x3E, 0xF0, 0x01,         //  *****      *****       
    0x7E, 0xF8, 0x01,         //  ******    ******       
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0x7F, 0x00,         //    ************         
    0xF0, 0x3F, 0x00,         //     **********          
    0xC0, 0x0F, 0x00,         //       ******            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0x7C, 0x7F, 0x00,         //   ***** *******         
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xF8, 0x01,         //   ******   ******       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0x7C, 0xF0, 0x01,         //   *****     *****       
    0xFC, 0xF8, 0x01,         //   ******   ******       
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7C, 0x7F, 0x00,         //   ***** *******         
    0x7C, 0x1E, 0x00,         //   *****  ****           
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x7C, 0x00, 0x00,         //   *****                 
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7E, 0xFC, 0x00,         //  ******   ******        
    0xFC, 0xFF, 0x00,         //   **************        
    0xFC, 0xFF, 0x00,         //   **************        
    0xF8, 0xFB, 0x00,         //    ******* *****        
    0xE0, 0xF9, 0x00,         //      ****  *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0xF8, 0x00,         //            *****        
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x0E,         //   *****  ***    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x0F,         //   **********    
    0xFC, 0x00,         //   ******        
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x0F,         //     ********    
    0xFC, 0x07,         //   *********     
    0xFC, 0x03,         //   ********      
    0x3E, 0x00,         //  *****          
    0x3E, 0x00,         //  *****          
    0xFE, 0x01,         //  ********       
    0xFC, 0x07,         //   *********     
    0xF8, 0x0F,         //    *********    
    0x80, 0x0F,         //        *****    
    0xFC, 0x0F,         //   **********    
    0xFE, 0x0F,         //  ***********    
    0xFF, 0x07,         // ***********     
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0xFF, 0x03,         // **********      
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0xFC, 0x7F, 0x00,         //   *************         
    0xF8, 0x3F, 0x00,         //    ***********          
    0xF8, 0x3F, 0x00,         //    ***********          
    0xE0, 0x0F, 0x00,         //      *******            
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x3F, 0xF8, 0x01,         // ******     ******       
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0xF8, 0x3E, 0x00,         //    ***** *****          
    0xF8, 0x3E, 0x00,         //    ***** *****          
    0xF0, 0x1F, 0x00,         //     *********           
    0xE0, 0x0F, 0x00,         //      *******            
    0xE0, 0x0F, 0x00,         //      *******            
    0xC0, 0x07, 0x00,         //       *****             
    0xC0, 0x07, 0x00,         //       *****             
    0x80, 0x03, 0x00,         //        ***              
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x1F, 0x78, 0xE0, 0x03,         // *****      ****      *****      
    0x3E, 0x78, 0xF0, 0x01,         //  *****     ****     *****       
    0x3E, 0xFC, 0xF0, 0x01,         //  *****    ******    *****       
    0x7C, 0xFC, 0xF8, 0x00,         //   *****   ******   *****        
    0x7C, 0xFE, 0xF8, 0x00,         //   *****  *******   *****        
    0xF8, 0xFE, 0x7D, 0x00,         //    ***** ******** *****         
    0xF8, 0xFF, 0x7D, 0x00,         //    ************** *****         
    0xF0, 0xCF, 0x3F, 0x00,         //     ********  ********          
    0xF0, 0xCF, 0x3F, 0x00,         //     ********  ********          
    0xE0, 0xC7, 0x1F, 0x00,         //      ******   *******           
    0xE0, 0x87, 0x1F, 0x00,         //      ******    ******           
    0xC0, 0x83, 0x0F, 0x00,         //       ****     *****            
    0xC0, 0x03, 0x07, 0x00,         //       ****      ***             
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 
    0x00, 0x00, 0x00, 0x00,         //                                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0xFC, 0xF0, 0x03,         //   ******    ******      
    0xF8, 0xF9, 0x01,         //    ******  ******       
    0xF0, 0xFF, 0x00,         //     ************        
    0xE0, 0x7F, 0x00,         //      **********         
    0xC0, 0x3F, 0x00,         //       ********          
    0x80, 0x1F, 0x00,         //        ******           
    0xC0, 0x3F, 0x00,         //       ********          
    0xC0, 0x3F, 0x00,         //       ********          
    0xE0, 0x7F, 0x00,         //      **********         
    0xF0, 0xFF, 0x00,         //     ************        
    0xF8, 0xF9, 0x01,         //    ******  ******       
    0xFC, 0xF0, 0x03,         //   ******    ******      
    0x7E, 0xE0, 0x07,         //  ******      ******     
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x1F, 0xF0, 0x01,         // *****       *****       
    0x3F, 0xF8, 0x00,         // ******     *****        
    0x3E, 0xF8, 0x00,         //  *****     *****        
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0x7C, 0x7C, 0x00,         //   *****   *****         
    0xF8, 0x3E, 0x00,         //    ***** *****          
    0xF8, 0x3E, 0x00,         //    ***** *****          
    0xF0, 0x1F, 0x00,         //     *********           
    0xF0, 0x1F, 0x00,         //     *********           
    0xE0, 0x0F, 0x00,         //      *******            
    0xE0, 0x0F, 0x00,         //      *******            
    0xC0, 0x07, 0x00,         //       *****             
    0xE0, 0x07, 0x00,         //      ******             
    0xE0, 0x03, 0x00,         //      *****              
    0xF0, 0x03, 0x00,         //     ******              
    0xF0, 0x01, 0x00,         //     *****               
    0xF8, 0x01, 0x00,         //    ******               
    0xF8, 0x00, 0x00,         //    *****                
    0xFC, 0x00, 0x00,         //   ******                
    0x7C, 0x00, 0x00,         //   *****                 
    0x00, 0x00, 0x00,         //                         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x3F,         //   ************  
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x1F,         //   ***********   
    0xFC, 0x0F,         //   **********    
    0x80, 0x0F,         //        *****    
    0xC0, 0x07,         //       *****     
    0xC0, 0x03,         //       ****      
    0xE0, 0x03,         //      *****      
    0xF0, 0x01,         //     *****       
    0xF0, 0x3F,         //     **********  
    0xF8, 0x3F,         //    ***********  
    0xF8, 0x3F,         //    ***********  
    0xFC, 0x3F,         //   ************  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x01,         //        **       
    0xC0, 0x00,         //       **        
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x70, 0x00,         //     ***         
    0x18, 0x00,         //    **           
    0x70, 0x00,         //     ***         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0xC0, 0x00,         //       **        
    0x80, 0x01,         //        **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x06,         //  **     
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         
    0x00,         //         

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x60, 0x00,         //      **         
    0xC0, 0x00,         //       **        
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x03,         //        ***      
    0x00, 0x06,         //          **     
    0x80, 0x03,         //        ***      
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0x80, 0x01,         //        **       
    0xC0, 0x00,         //       **        
    0x60, 0x00,         //      **         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x40,         //    ****       * 
    0xFC, 0x61,         //   *******    ** 
    0x86, 0x3F,         //  **    *******  
    0x03, 0x1E,         // **       ****   
    0x02, 0x00,         //  *              
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x9E, 0x2F, 0x00,         //  ****  ***** *          
    0xA2, 0x20, 0x00,         //  *   * *     *          
    0xA2, 0x20, 0x00,         //  *   * *     *          
    0xA2, 0x2F, 0x00,         //  *   * ***** *          
    0xA2, 0x20, 0x00,         //  *   * *     *          
    0xB2, 0x20, 0x00,         //  *  ** *     *          
    0x9E, 0xEF, 0x01,         //  ****  ***** ****       
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         
    0x00, 0x00, 0x00,         //                         

};

