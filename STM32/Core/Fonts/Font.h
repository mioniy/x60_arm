
#ifndef __FONT_H__
#define __FONT_H__


extern const uint8_t  __Font88                  ;
extern const uint8_t  __Courier_New_16          ;
extern const uint8_t  __Courier_New_16_1        ;
extern const uint8_t  __Courier_New_24          ;
extern const uint8_t  __Courier_New_24_1        ;
extern const uint8_t  __Courier_New_Bold_24     ;
extern const uint8_t  __Courier_New_Bold_24_1   ;
extern const uint8_t  __Courier_New_30          ;
extern const uint8_t  __Courier_New_30_1        ;
extern const uint8_t  __Courier_New_Bold_30     ;
extern const uint8_t  __Courier_New_Bold_30_1   ;
extern const uint8_t  __Courier_New_36          ;
extern const uint8_t  __Courier_New_36_1        ;
extern const uint8_t  __Courier_New_Bold_36     ;
extern const uint8_t  __Courier_New_Bold_36_1   ;
extern const uint8_t  __Courier_New_49          ;
extern const uint8_t  __Courier_New_49_1        ;
extern const uint8_t  __Courier_New_Bold_49     ;
extern const uint8_t  __Courier_New_Bold_49_1   ;
extern const uint8_t  __Aharoni_Bold_16         ;
extern const uint8_t  __Aharoni_Bold_20         ;
extern const uint8_t  __Aharoni_Bold_29         ;
extern const uint8_t  __Aharoni_Bold_24         ;
extern const uint8_t  __Aharoni_Bold_36        ;
extern const uint8_t  __Aharoni_Bold_49         ;
#ifdef __CT_30__
extern const uint8_t __Montserrat_Medium_Bold_22 ;
extern const uint8_t __Montserrat_Medium_Bold_40 ;
#endif
extern sFONT Font88                  ;
extern sFONT Courier_New_16          ;
extern sFONT Courier_New_16_1        ;
extern sFONT Courier_New_24          ;
extern sFONT Courier_New_24_1        ;
extern sFONT Courier_New_Bold_24     ;
extern sFONT Courier_New_Bold_24_1   ;
extern sFONT Courier_New_30          ;
extern sFONT Courier_New_30_1        ;
extern sFONT Courier_New_Bold_30     ;
extern sFONT Courier_New_Bold_30_1   ;
extern sFONT Courier_New_36          ;
extern sFONT Courier_New_36_1        ;
extern sFONT Courier_New_Bold_36     ;
extern sFONT Courier_New_Bold_36_1   ;
extern sFONT Courier_New_49          ;
extern sFONT Courier_New_49_1        ;
extern sFONT Courier_New_Bold_49     ;
extern sFONT Courier_New_Bold_49_1   ;

extern sFONT Aharoni_Bold_16         ;
extern sFONT Aharoni_Bold_20         ;
extern sFONT Aharoni_Bold_29         ;
extern sFONT Aharoni_Bold_24         ;
extern sFONT Aharoni_Bold_36         ;
extern sFONT Aharoni_Bold_49         ;

extern sFONT Montserrat_Medium_Bold_22;
extern sFONT Montserrat_Medium_Bold_40 ;

extern const uint8_t __Green_Led    ;
extern const uint8_t __Red_Led      ;

extern const uint8_t __CarIN               ;
extern const uint8_t __ChipAndPinUnit_64    ;
extern const uint8_t __CollectBankNote_64   ;
extern const uint8_t __CollectCoins_64      ;
extern const uint8_t __Inactive            ;
extern const uint8_t __InsertTicket_2       ;
extern const uint8_t __OutOfOrder          ;
extern const uint8_t __ParkSign_128        ;
extern const uint8_t __ParkSign_48         ;
extern const uint8_t __ParkSign_64         ;
extern const uint8_t __PayWithCoins_32      ;
extern const uint8_t __PayWithCoins_48      ;
extern const uint8_t __PayWithCredit_32     ;
extern const uint8_t __PayWithCredit_48     ;
extern const uint8_t __PayWithNotes_32      ;
extern const uint8_t __PayWithNotes_48      ;
extern const uint8_t __PressForTicket_128   ;
extern const uint8_t __Printer_128         ;
extern const uint8_t __SandWatch           ;
extern const uint8_t __TakeTheTicket_2      ;
extern const uint8_t __TibaLogoF1_240       ;
extern const uint8_t __Ticket_200           ;
extern const uint8_t __cancel3              ;
extern const uint8_t __PB_G1                ;
//extern const uint8_t __tiba2               ;

#endif
