
#include "fonts.h"
#include "port_stm.h"


sFONT Font88                    = { &__Font88                  };
sFONT Courier_New_16            = { &__Courier_New_16          };
sFONT Courier_New_16_1          = { &__Courier_New_16_1        };
sFONT Courier_New_24            = { &__Courier_New_24          };
sFONT Courier_New_24_1          = { &__Courier_New_24_1        };
sFONT Courier_New_Bold_24       = { &__Courier_New_Bold_24     };
sFONT Courier_New_Bold_24_1     = { &__Courier_New_Bold_24_1   };
sFONT Courier_New_30            = { &__Courier_New_30          };
sFONT Courier_New_30_1          = { &__Courier_New_30_1        };
sFONT Courier_New_Bold_30       = { &__Courier_New_Bold_30     };
sFONT Courier_New_Bold_30_1     = { &__Courier_New_Bold_30_1   };
sFONT Courier_New_36            = { &__Courier_New_36          };
sFONT Courier_New_36_1          = { &__Courier_New_36_1        };
sFONT Courier_New_Bold_36       = { &__Courier_New_Bold_36     };
sFONT Courier_New_Bold_36_1     = { &__Courier_New_Bold_36_1   };
sFONT Courier_New_49            = { &__Courier_New_49          };
sFONT Courier_New_49_1          = { &__Courier_New_49_1        };
sFONT Courier_New_Bold_49       = { &__Courier_New_Bold_49     };
sFONT Courier_New_Bold_49_1     = { &__Courier_New_Bold_49_1   };
sFONT Aharoni_Bold_16           = { &__Aharoni_Bold_16         };
sFONT Aharoni_Bold_20           = { &__Aharoni_Bold_20         };
sFONT Aharoni_Bold_29           = { &__Aharoni_Bold_29         };
sFONT Aharoni_Bold_24           = { &__Aharoni_Bold_24         };
sFONT Aharoni_Bold_36           = { &__Aharoni_Bold_36         };
sFONT Aharoni_Bold_49           = { &__Aharoni_Bold_49         };

#ifdef __CT_30__
sFONT Montserrat_Medium_Bold_22= { &__Montserrat_Medium_Bold_22 };
sFONT Montserrat_Medium_Bold_40= { &__Montserrat_Medium_Bold_40 };
#endif
