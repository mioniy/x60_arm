/*****************************************************************************
 * FileName:        Font24.c
 * Processor:       PIC24F, PIC24H, dsPIC
 * Compiler:        MPLAB C30 (see release notes for tested revision)
 * Linker:          MPLAB LINK30
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright � 2010 Microchip Technology Inc.  All rights reserved.
 * Microchip licenses to you the right to use, modify, copy and distribute
 * Software only when embedded on a Microchip microcontroller or digital
 * signal controller, which is integrated into your product or third party
 * product (pursuant to the sublicense terms in the accompanying license
 * agreement).
 *
 * You should refer to the license agreement accompanying this Software
 * for additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
 * COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
 * OR OTHER SIMILAR COSTS.
 *
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AUTO-GENERATED CODE:  Graphics Resource Converter version: 3.8.21
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Includes
 *****************************************************************************/
#include <Graphics/Graphics.h>

/*****************************************************************************
 * Converted Resources
 * -------------------
 *
 *
 * Fonts
 * -----
 * Courier_New_24 - Height: 28 pixels, range: ' ' to '~'
 * Courier_New_24_1 - Height: 28 pixels, range: '�' to '�'
 *****************************************************************************/

/*****************************************************************************
 * SECTION:  Fonts
 *****************************************************************************/

/*********************************
 * Font Structure
 * Label: Courier_New_24
 * Description:  Height: 28 pixels, range: ' ' to '~'
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Courier_New_24[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Courier_New_24 =
{
    (FLASH | COMP_NONE),
    __Courier_New_24
};

const char __Courier_New_24[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Courier_New_24[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0x20, 0x00,     // First Character
    0x7E, 0x00,     // Last Character
    0x1C,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x0E, 0x84, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xBC, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xF4, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x2C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x64, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x9C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xD4, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x0C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x44, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x7C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xB4, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xEC, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x24, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x5C, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x94, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xCC, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x04, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x3C, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x74, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAC, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE4, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x1C, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x54, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x8C, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xC4, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xFC, 0x06, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x34, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x6C, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xA4, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xDC, 0x07, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x14, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x4C, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x84, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xBC, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xF4, 0x08, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x2C, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x64, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x9C, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xD4, 0x09, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x0C, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x44, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x7C, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xB4, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xEC, 0x0A, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x24, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x5C, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x94, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xCC, 0x0B, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x04, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x3C, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x74, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAC, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE4, 0x0C, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x1C, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x54, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x8C, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xC4, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xFC, 0x0D, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x34, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x6C, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xA4, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xDC, 0x0E, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x14, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x4C, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x84, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xBC, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xF4, 0x0F, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x2C, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x64, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x9C, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xD4, 0x10, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x0C, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x44, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x7C, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xB4, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xEC, 0x11, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x24, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x5C, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x94, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xCC, 0x12, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x04, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x3C, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x74, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAC, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE4, 0x13, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x1C, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x54, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x8C, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xC4, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xFC, 0x14, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x34, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x6C, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xA4, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xDC, 0x15, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x14, 0x16, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x38, 0x07,         //    ***  ***     
    0x38, 0x07,         //    ***  ***     
    0x38, 0x07,         //    ***  ***     
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x04,         //       *   *     
    0x40, 0x04,         //       *   *     
    0x40, 0x04,         //       *   *     
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0xFC, 0x1F,         //   ***********   
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0xFC, 0x1F,         //   ***********   
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0xE0, 0x07,         //      ******     
    0x10, 0x06,         //     *    **     
    0x08, 0x04,         //    *      *     
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x30, 0x00,         //     **          
    0xC0, 0x03,         //       ****      
    0x00, 0x06,         //          **     
    0x00, 0x04,         //           *     
    0x08, 0x04,         //    *      *     
    0x18, 0x06,         //    **    **     
    0xE8, 0x03,         //    * *****      
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0x08, 0x01,         //    *    *       
    0xF0, 0x00,         //     ****        
    0x00, 0x06,         //          **     
    0xE0, 0x01,         //      ****       
    0x18, 0x00,         //    **           
    0xC0, 0x03,         //       ****      
    0x20, 0x04,         //      *    *     
    0x20, 0x04,         //      *    *     
    0x20, 0x04,         //      *    *     
    0x20, 0x04,         //      *    *     
    0xC0, 0x03,         //       ****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x07,         //       *****     
    0x60, 0x02,         //      **  *      
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x70, 0x00,         //     ***         
    0x88, 0x04,         //    *   *  *     
    0x88, 0x02,         //    *   * *      
    0x08, 0x03,         //    *    **      
    0x08, 0x01,         //    *    *       
    0x10, 0x03,         //     *   **      
    0xE0, 0x0C,         //      ***  **    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xC0, 0x00,         //       **        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x02,         //          *      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x60, 0x00,         //      **         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x60, 0x00,         //      **         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFC, 0x07,         //   *********     
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xA0, 0x00,         //      * *        
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFE, 0x0F,         //  ***********    
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x00,         //      ***        
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x0F,         //   **********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x00,         //       **        
    0xB8, 0x00,         //    *** *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0xF8, 0x0F,         //    *********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0x08, 0x02,         //    *     *      
    0x04, 0x04,         //   *       *     
    0x04, 0x04,         //   *       *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0x04, 0x04,         //   *       *     
    0xFC, 0x07,         //   *********     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x18, 0x02,         //    **    *      
    0x04, 0x04,         //   *       *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0xC0, 0x01,         //       ***       
    0x00, 0x02,         //          *      
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x04, 0x02,         //   *      *      
    0x08, 0x03,         //    *    **      
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x03,         //         **      
    0x80, 0x02,         //        * *      
    0x80, 0x02,         //        * *      
    0x40, 0x02,         //       *  *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x08, 0x02,         //    *     *      
    0x08, 0x02,         //    *     *      
    0xF8, 0x07,         //    ********     
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0xC0, 0x07,         //       *****     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x03,         //    *******      
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0xE8, 0x03,         //    * *****      
    0x18, 0x04,         //    **     *     
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x04, 0x04,         //   *       *     
    0x18, 0x06,         //    **    **     
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x0F,         //         ****    
    0xC0, 0x00,         //       **        
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x08, 0x00,         //    *            
    0xC8, 0x03,         //    *  ****      
    0x28, 0x04,         //    * *    *     
    0x18, 0x08,         //    **      *    
    0x18, 0x08,         //    **      *    
    0x08, 0x08,         //    *       *    
    0x10, 0x08,         //     *      *    
    0x10, 0x08,         //     *      *    
    0x20, 0x04,         //      *    *     
    0xC0, 0x03,         //       ****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x07,         //   *********     
    0x04, 0x04,         //   *       *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x0C,         //    *      **    
    0x10, 0x0A,         //     *    * *    
    0xE0, 0x09,         //      ****  *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0xF8, 0x00,         //    *****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x03,         //       ****      
    0xC0, 0x03,         //       ****      
    0xC0, 0x03,         //       ****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x00,         //      ***        
    0x60, 0x00,         //      **         
    0x60, 0x00,         //      **         
    0x30, 0x00,         //     **          
    0x30, 0x00,         //     **          
    0x10, 0x00,         //     *           
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x18,         //            **   
    0x00, 0x06,         //          **     
    0x00, 0x01,         //         *       
    0xC0, 0x00,         //       **        
    0x30, 0x00,         //     **          
    0x08, 0x00,         //    *            
    0x06, 0x00,         //  **             
    0x08, 0x00,         //    *            
    0x30, 0x00,         //     **          
    0xC0, 0x00,         //       **        
    0x00, 0x01,         //         *       
    0x00, 0x06,         //          **     
    0x00, 0x08,         //            *    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x1F,         //  ************   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x1F,         //  ************   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0C, 0x00,         //   **            
    0x30, 0x00,         //     **          
    0x40, 0x00,         //       *         
    0x80, 0x01,         //        **       
    0x00, 0x06,         //          **     
    0x00, 0x08,         //            *    
    0x00, 0x30,         //             **  
    0x00, 0x08,         //            *    
    0x00, 0x06,         //          **     
    0x80, 0x01,         //        **       
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x0C, 0x00,         //   **            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x18, 0x02,         //    **    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0x80, 0x01,         //        **       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x01,         //       ***       
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x01,         //       ***       
    0x20, 0x02,         //      *   *      
    0x18, 0x04,         //    **     *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x07,         //    *    ***     
    0x88, 0x04,         //    *   *  *     
    0x48, 0x04,         //    *  *   *     
    0x48, 0x04,         //    *  *   *     
    0x48, 0x04,         //    *  *   *     
    0x48, 0x04,         //    *  *   *     
    0x88, 0x0F,         //    *   *****    
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x00,         //   ******        
    0xC0, 0x00,         //       **        
    0x20, 0x01,         //      *  *       
    0x20, 0x01,         //      *  *       
    0x20, 0x01,         //      *  *       
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0xF0, 0x07,         //     *******     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x1F, 0x3E,         // *****    *****  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x02,         //    *     *      
    0xF8, 0x01,         //    ******       
    0x08, 0x06,         //    *     **     
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x04,         //    *      *     
    0xFE, 0x03,         //  *********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x09,         //     *****  *    
    0x18, 0x0E,         //    **    ***    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x04, 0x00,         //   *             
    0x04, 0x08,         //   *        *    
    0x18, 0x04,         //    **     *     
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x03,         //  *********      
    0x08, 0x04,         //    *      *     
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x10,         //    *        *   
    0x08, 0x10,         //    *        *   
    0x08, 0x10,         //    *        *   
    0x08, 0x10,         //    *        *   
    0x08, 0x10,         //    *        *   
    0x08, 0x10,         //    *        *   
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x04,         //    *      *     
    0xFE, 0x03,         //  *********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x0F,         //  ***********    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x00,         //    *            
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0xF8, 0x00,         //    *****        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0xFE, 0x0F,         //  ***********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x0F,         //  ***********    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x00,         //    *            
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0xF8, 0x00,         //    *****        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0xFE, 0x00,         //  *******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x09,         //      ****  *    
    0x18, 0x0E,         //    **    ***    
    0x04, 0x08,         //   *        *    
    0x04, 0x00,         //   *             
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x02, 0x00,         //  *              
    0x82, 0x3F,         //  *     *******  
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x04, 0x08,         //   *        *    
    0x08, 0x08,         //    *       *    
    0xF0, 0x07,         //     *******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x1F,         //  *****  *****   
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0xF8, 0x07,         //    ********     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x3E, 0x1F,         //  *****  *****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x07,         //   *********     
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFC, 0x07,         //   *********     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x0F,         //     ********    
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x04, 0x02,         //   *      *      
    0x04, 0x02,         //   *      *      
    0x04, 0x02,         //   *      *      
    0x04, 0x02,         //   *      *      
    0x08, 0x01,         //    *    *       
    0xF0, 0x00,         //     ****        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x1E,         //  *****   ****   
    0x08, 0x04,         //    *      *     
    0x08, 0x02,         //    *     *      
    0x08, 0x01,         //    *    *       
    0x88, 0x00,         //    *   *        
    0x48, 0x00,         //    *  *         
    0x28, 0x00,         //    * *          
    0x78, 0x00,         //    ****         
    0x88, 0x01,         //    *   **       
    0x08, 0x01,         //    *    *       
    0x08, 0x02,         //    *     *      
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x3E, 0x1C,         //  *****    ***   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x00,         //    *****        
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x10,         //      *      *   
    0x20, 0x10,         //      *      *   
    0x20, 0x10,         //      *      *   
    0x20, 0x10,         //      *      *   
    0x20, 0x10,         //      *      *   
    0xF8, 0x1F,         //    **********   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x3C,         // ****      ****  
    0x0C, 0x0C,         //   **      **    
    0x14, 0x0A,         //   * *    * *    
    0x14, 0x0A,         //   * *    * *    
    0x14, 0x0A,         //   * *    * *    
    0x24, 0x09,         //   *  *  *  *    
    0x24, 0x09,         //   *  *  *  *    
    0x44, 0x09,         //   *   * *  *    
    0xC4, 0x08,         //   *   **   *    
    0x84, 0x08,         //   *    *   *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x1F, 0x3E,         // *****    *****  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x3E,         // ****     *****  
    0x0C, 0x08,         //   **       *    
    0x14, 0x08,         //   * *      *    
    0x14, 0x08,         //   * *      *    
    0x24, 0x08,         //   *  *     *    
    0x44, 0x08,         //   *   *    *    
    0x44, 0x08,         //   *   *    *    
    0x84, 0x08,         //   *    *   *    
    0x84, 0x08,         //   *    *   *    
    0x04, 0x09,         //   *     *  *    
    0x04, 0x0A,         //   *      * *    
    0x04, 0x0A,         //   *      * *    
    0x04, 0x0C,         //   *       **    
    0x1F, 0x0C,         // *****     **    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x18, 0x06,         //    **    **     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x08, 0x06,         //    *     **     
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x07,         //   *********     
    0x10, 0x08,         //     *      *    
    0x10, 0x10,         //     *       *   
    0x10, 0x10,         //     *       *   
    0x10, 0x10,         //     *       *   
    0x10, 0x10,         //     *       *   
    0x10, 0x08,         //     *      *    
    0xF0, 0x07,         //     *******     
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x18, 0x06,         //    **    **     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x02, 0x10,         //  *          *   
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x18, 0x06,         //    **    **     
    0xE0, 0x01,         //      ****       
    0x20, 0x00,         //      *          
    0xF0, 0x11,         //     *****   *   
    0x18, 0x0E,         //    **    ***    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x02,         //    *     *      
    0xF8, 0x01,         //    ******       
    0x08, 0x01,         //    *    *       
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x08,         //    *       *    
    0x3E, 0x18,         //  *****     **   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x0B,         //      ***** *    
    0x10, 0x0C,         //     *     **    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0xE0, 0x03,         //      *****      
    0x00, 0x04,         //           *     
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x1C, 0x04,         //   ***     *     
    0xE4, 0x03,         //   *  *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x0F,         //  ***********    
    0x42, 0x08,         //  *    *    *    
    0x42, 0x08,         //  *    *    *    
    0x42, 0x08,         //  *    *    *    
    0x42, 0x08,         //  *    *    *    
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3E, 0x3E,         //  *****   *****  
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x10, 0x04,         //     *     *     
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1F, 0x3E,         // *****    *****  
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x08, 0x08,         //    *       *    
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x02,         //     *    *      
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0x20, 0x01,         //      *  *       
    0x40, 0x01,         //       * *       
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x3E,         // ****     *****  
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x42, 0x08,         //  *    *    *    
    0x42, 0x08,         //  *    *    *    
    0xA4, 0x04,         //   *  * *  *     
    0xA4, 0x04,         //   *  * *  *     
    0xA4, 0x04,         //   *  * *  *     
    0xA4, 0x04,         //   *  * *  *     
    0x14, 0x05,         //   * *   * *     
    0x14, 0x05,         //   * *   * *     
    0x14, 0x05,         //   * *   * *     
    0x14, 0x05,         //   * *   * *     
    0x08, 0x02,         //    *     *      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1E, 0x0F,         //  ****   ****    
    0x08, 0x02,         //    *     *      
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0xA0, 0x00,         //      * *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xA0, 0x00,         //      * *        
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x08, 0x02,         //    *     *      
    0x04, 0x04,         //   *       *     
    0x04, 0x04,         //   *       *     
    0x1E, 0x1E,         //  ****    ****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3C, 0x1E,         //   ****   ****   
    0x08, 0x08,         //    *       *    
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x20, 0x02,         //      *   *      
    0x40, 0x01,         //       * *       
    0x40, 0x01,         //       * *       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x0F,         //    *********    
    0x08, 0x08,         //    *       *    
    0x08, 0x04,         //    *      *     
    0x08, 0x02,         //    *     *      
    0x08, 0x01,         //    *    *       
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x08,         //      *     *    
    0x10, 0x08,         //     *      *    
    0x08, 0x08,         //    *       *    
    0x04, 0x08,         //   *        *    
    0xFC, 0x0F,         //   **********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xC0, 0x03,         //       ****      
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xC0, 0x03,         //       ****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x04,         //           *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x78, 0x00,         //    ****         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x78, 0x00,         //    ****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xA0, 0x00,         //      * *        
    0x10, 0x01,         //     *   *       
    0x08, 0x02,         //    *     *      
    0x08, 0x02,         //    *     *      
    0x04, 0x04,         //   *       *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFF, 0x3F,         // **************  
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x20, 0x00,         //      *          
    0x40, 0x00,         //       *         
    0x80, 0x00,         //        *        
    0x00, 0x01,         //         *       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x18, 0x02,         //    **    *      
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0xF0, 0x07,         //     *******     
    0x08, 0x04,         //    *      *     
    0x04, 0x04,         //   *       *     
    0x04, 0x04,         //   *       *     
    0x04, 0x07,         //   *     ***     
    0xF8, 0x1C,         //    *****  ***   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x07, 0x00,         // ***             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0xE4, 0x01,         //   *  ****       
    0x14, 0x02,         //   * *    *      
    0x0C, 0x04,         //   **      *     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x0C, 0x04,         //   **      *     
    0x14, 0x02,         //   * *    *      
    0xE7, 0x01,         // ***  ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x0B,         //      ***** *    
    0x10, 0x0C,         //     *     **    
    0x08, 0x08,         //    *       *    
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x08, 0x10,         //    *        *   
    0x10, 0x0C,         //     *     **    
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x0E,         //          ***    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0xF0, 0x09,         //     *****  *    
    0x08, 0x0A,         //    *     * *    
    0x04, 0x0C,         //   *       **    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x04, 0x0C,         //   *       **    
    0x08, 0x0A,         //    *     * *    
    0xF0, 0x39,         //     *****  ***  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0xFC, 0x0F,         //   **********    
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x08, 0x08,         //    *       *    
    0x10, 0x04,         //     *     *     
    0xE0, 0x03,         //      *****      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x1F,         //        ******   
    0xC0, 0x00,         //       **        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xF8, 0x0F,         //    *********    
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xF8, 0x0F,         //    *********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x1C,         //     ****  ***   
    0x08, 0x05,         //    *    * *     
    0x04, 0x06,         //   *      **     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x04, 0x06,         //   *      **     
    0x08, 0x05,         //    *    * *     
    0xF0, 0x04,         //     ****  *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x00,         //  ***            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0xC8, 0x01,         //    *  ***       
    0x38, 0x02,         //    ***   *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x3E, 0x1F,         //  *****  *****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFE, 0x0F,         //  ***********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x01,         //  ********       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x7C, 0x00,         //   *****         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x00,         //   ***           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x0F,         //     *   ****    
    0x10, 0x02,         //     *    *      
    0x10, 0x01,         //     *   *       
    0x90, 0x00,         //     *  *        
    0x50, 0x00,         //     * *         
    0x70, 0x00,         //     ***         
    0x90, 0x00,         //     *  *        
    0x10, 0x01,         //     *   *       
    0x10, 0x02,         //     *    *      
    0x10, 0x04,         //     *     *     
    0x1C, 0x1F,         //   ***   *****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x7C, 0x00,         //   *****         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xFE, 0x0F,         //  ***********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x77, 0x0E,         // *** ***  ***    
    0x8C, 0x11,         //   **   **   *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x9F, 0x33,         // *****  ***  **  
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x38, 0x02,         //    ***   *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x3E, 0x1F,         //  *****  *****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x01,         //     *****       
    0x08, 0x02,         //    *     *      
    0x04, 0x04,         //   *       *     
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x02, 0x08,         //  *         *    
    0x04, 0x04,         //   *       *     
    0x08, 0x02,         //    *     *      
    0xF0, 0x01,         //     *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE7, 0x01,         // ***  ****       
    0x14, 0x06,         //   * *    **     
    0x0C, 0x04,         //   **      *     
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x04, 0x08,         //   *        *    
    0x0C, 0x04,         //   **      *     
    0x14, 0x02,         //   * *    *      
    0xE4, 0x01,         //   *  ****       
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x04, 0x00,         //   *             
    0x3F, 0x00,         // ******          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x1C,         //     ****  ***   
    0x08, 0x05,         //    *    * *     
    0x04, 0x06,         //   *      **     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x02, 0x04,         //  *        *     
    0x04, 0x06,         //   *      **     
    0x08, 0x05,         //    *    * *     
    0xF0, 0x04,         //     ****  *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x80, 0x1F,         //        ******   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x07,         //   ***   ***     
    0xD0, 0x08,         //     * **   *    
    0x30, 0x08,         //     **     *    
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x03,         //  *********      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x05,         //     ***** *     
    0x08, 0x06,         //    *     **     
    0x08, 0x04,         //    *      *     
    0x08, 0x00,         //    *            
    0xF0, 0x01,         //     *****       
    0x00, 0x02,         //          *      
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x04, 0x04,         //   *       *     
    0x0C, 0x02,         //   **     *      
    0xF4, 0x01,         //   * *****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0xFE, 0x07,         //  **********     
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x10, 0x00,         //     *           
    0x20, 0x18,         //      *     **   
    0xC0, 0x07,         //       *****     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x07,         //  ***    ***     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x07,         //    *    ***     
    0xF0, 0x1C,         //     ****  ***   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1E, 0x1E,         //  ****    ****   
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x20, 0x01,         //      *  *       
    0x20, 0x01,         //      *  *       
    0x20, 0x01,         //      *  *       
    0xC0, 0x00,         //       **        
    0xC0, 0x00,         //       **        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1F, 0x3C,         // *****     ****  
    0x04, 0x10,         //   *         *   
    0x04, 0x10,         //   *         *   
    0x84, 0x10,         //   *    *    *   
    0x84, 0x10,         //   *    *    *   
    0x48, 0x09,         //    *  * *  *    
    0x48, 0x09,         //    *  * *  *    
    0x28, 0x0A,         //    * *   * *    
    0x28, 0x0A,         //    * *   * *    
    0x28, 0x0A,         //    * *   * *    
    0x10, 0x04,         //     *     *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x3C, 0x0F,         //   ****  ****    
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0x20, 0x01,         //      *  *       
    0xC0, 0x00,         //       **        
    0xC0, 0x00,         //       **        
    0x20, 0x01,         //      *  *       
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x04, 0x08,         //   *        *    
    0x1E, 0x1E,         //  ****    ****   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0F, 0x1F,         // ****    *****   
    0x04, 0x04,         //   *       *     
    0x04, 0x04,         //   *       *     
    0x08, 0x02,         //    *     *      
    0x08, 0x02,         //    *     *      
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0x10, 0x01,         //     *   *       
    0xA0, 0x00,         //      * *        
    0xA0, 0x00,         //      * *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x10, 0x00,         //     *           
    0x7E, 0x00,         //  ******         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x0F,         //    *********    
    0x08, 0x08,         //    *       *    
    0x08, 0x04,         //    *      *     
    0x00, 0x02,         //          *      
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x20, 0x00,         //      *          
    0x10, 0x08,         //     *      *    
    0x08, 0x08,         //    *       *    
    0xF8, 0x0F,         //    *********    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x80, 0x01,         //        **       
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x80, 0x01,         //        **       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x80, 0x01,         //        **       
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x30, 0x00,         //     **          
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x30, 0x00,         //     **          
    0x48, 0x08,         //    *  *    *    
    0x84, 0x04,         //   *    *  *     
    0x00, 0x03,         //         **      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

/*********************************
 * Font Structure
 * Label: Courier_New_24_1
 * Description:  Height: 28 pixels, range: '�' to '�'
 ***********************************/

#ifdef _MICROCHIP_
extern const char __Courier_New_24_1[] __attribute__((space(prog),aligned(2)));

const FONT_FLASH Courier_New_24_1 =
{
    (FLASH | COMP_NONE),
    __Courier_New_24_1
};

const char __Courier_New_24_1[] __attribute__((space(prog),aligned(2))) =
   #else
const uint8_t __Courier_New_24_1[] =
#endif
{
/****************************************
 * Font header
 ****************************************/
    0x00,           // Information
    0x00,           // ID
    0xD0, 0x05,     // First Character
    0xEA, 0x05,     // Last Character
    0x1C,           // Height
    0x00,           // Reserved
/****************************************
 * Font Glyph Table
 ****************************************/
    0x0E, 0x74, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xAC, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xE4, 0x00, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x1C, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x54, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x8C, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xC4, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xFC, 0x01, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x34, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x6C, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xA4, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xDC, 0x02, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x14, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x4C, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x84, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xBC, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xF4, 0x03, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x2C, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x64, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x9C, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xD4, 0x04, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x0C, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x44, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x7C, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xB4, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0xEC, 0x05, 0x00,           // width, MSB Offset, LSB offset
    0x0E, 0x24, 0x06, 0x00,           // width, MSB Offset, LSB offset
/***********************************
 * Font Characters
 ***********************************/
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x04, 0x0E,         //   *      ***    
    0x08, 0x08,         //    *       *    
    0x10, 0x08,         //     *      *    
    0x10, 0x08,         //     *      *    
    0x30, 0x08,         //     **     *    
    0x48, 0x06,         //    *  *  **     
    0x84, 0x01,         //   *    **       
    0x04, 0x01,         //   *     *       
    0x04, 0x01,         //   *     *       
    0x04, 0x02,         //   *      *      
    0x04, 0x04,         //   *       *     
    0x1C, 0x08,         //   ***      *    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x03,         //   ********      
    0x00, 0x06,         //          **     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0xFC, 0x1F,         //   ***********   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x80, 0x02,         //        * *      
    0x60, 0x02,         //      **  *      
    0x38, 0x04,         //    ***    *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x0F,         //  ***********    
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x03,         //   ********      
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x07,         //     *******     
    0x00, 0x01,         //         *       
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x0F,         //   **********    
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x10, 0x02,         //     *    *      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x0E, 0x03,         //  ***    **      
    0x88, 0x04,         //    *   *  *     
    0x88, 0x04,         //    *   *  *     
    0x88, 0x04,         //    *   *  *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x0F,         //  ***********    
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x0F,         //         ****    
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x01,         //   *******       
    0x00, 0x06,         //          **     
    0x00, 0x04,         //           *     
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x04,         //           *     
    0x00, 0x06,         //          **     
    0xFC, 0x01,         //   *******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0xF8, 0x07,         //    ********     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x03,         //         **      
    0x80, 0x00,         //        *        
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0x40, 0x00,         //       *         
    0xC0, 0x01,         //       ***       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x03,         //  *********      
    0x10, 0x02,         //     *    *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0xF8, 0x07,         //    ********     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xCE, 0x01,         //  ***  ***       
    0x28, 0x02,         //    * *   *      
    0x18, 0x04,         //    **     *     
    0x18, 0x04,         //    **     *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x88, 0x07,         //    *   ****     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF0, 0x00,         //     ****        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x00,         //        *        
    0x80, 0x07,         //        ****     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xE0, 0x01,         //      ****       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFE, 0x03,         //  *********      
    0x08, 0x06,         //    *     **     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x10, 0x02,         //     *    *      
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x1C, 0x07,         //   ***   ***     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x02,         //     *    *      
    0x10, 0x03,         //     *   **      
    0xFC, 0x00,         //   ******        
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x03,         //    *******      
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0xF0, 0x04,         //     ****  *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x1C,         //           ***   
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xF8, 0x01,         //    ******       
    0x08, 0x02,         //    *     *      
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0x08, 0x04,         //    *      *     
    0xF0, 0x04,         //     ****  *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x02,         //          *      
    0xF8, 0x01,         //    ******       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xBC, 0x07,         //   **** ****     
    0x20, 0x04,         //      *    *     
    0x20, 0x04,         //      *    *     
    0x20, 0x02,         //      *   *      
    0x20, 0x02,         //      *   *      
    0xA0, 0x01,         //      * **       
    0x60, 0x00,         //      **         
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0x20, 0x00,         //      *          
    0xE0, 0x01,         //      ****       
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x9C, 0x07,         //   ***  ****     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x20, 0x04,         //      *    *     
    0x20, 0x02,         //      *   *      
    0xC0, 0x01,         //       ***       
    0x80, 0x00,         //        *        
    0x00, 0x01,         //         *       
    0x00, 0x01,         //         *       
    0x00, 0x02,         //          *      
    0x00, 0x02,         //          *      
    0xF0, 0x03,         //     ******      
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x0F,         //   **********    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x00, 0x08,         //            *    
    0x08, 0x08,         //    *       *    
    0x08, 0x08,         //    *       *    
    0x08, 0x06,         //    *     **     
    0x08, 0x01,         //    *    *       
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x88, 0x00,         //    *   *        
    0x88, 0x03,         //    *   ***      
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x08, 0x00,         //    *            
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x03,         //   ********      
    0x00, 0x06,         //          **     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x04,         //           *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xB8, 0x0E,         //    *** * ***    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x88, 0x08,         //    *   *   *    
    0x98, 0x0C,         //    **  *  **    
    0xF0, 0x07,         //     *******     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0xFC, 0x03,         //   ********      
    0x10, 0x06,         //     *    **     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x10, 0x04,         //     *     *     
    0x1C, 0x04,         //   ***     *     
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 
    0x00, 0x00,         //                 

};

