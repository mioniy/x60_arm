

/* Includes ------------------------------------------------------------------*/
#include "lwip/opt.h"
#include "lwip/init.h"
#include "netif/etharp.h"
#include "lwip/netif.h"
#include "lwip/timeouts.h"
#if LWIP_DHCP
#include "lwip/dhcp.h"
#endif
#include "ethernetif.h"
#include "main.h"
#include "app_ethernet.h"
#include "tcp_server.h"

struct netif gnetif;


uint32_t EthernetLinkTimer;

#if LWIP_DHCP
#define MAX_DHCP_TRIES  2
uint32_t DHCPfineTimer = 0;
uint8_t DHCP_state = DHCP_OFF;
#endif

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Notify the User about the nework interface config status
  * @param  netif: the network interface
  * @retval None
  */
void ethernet_link_status_updated(struct netif *netif)
{
  if (netif_is_link_up(netif))
  {
#if LWIP_DHCP
    /* Update DHCP state machine */
    DHCP_state = DHCP_START;
#endif /* LWIP_DHCP */
  }
  else
  {
#if LWIP_DHCP
    /* Update DHCP state machine */
    DHCP_state = DHCP_LINK_DOWN;
#endif /* LWIP_DHCP */
  }
}

#if LWIP_NETIF_LINK_CALLBACK
/**
  * @brief  Ethernet Link periodic check
  * @param  netif
  * @retval None
  */
void Ethernet_Link_Periodic_Handle(struct netif *netif)
{
  /* Ethernet Link every 100ms */
  if (HAL_GetTick() - EthernetLinkTimer >= 100)
  {
    EthernetLinkTimer = HAL_GetTick();
    ethernet_link_check_state(netif);
  }
}
#endif

#if LWIP_DHCP
/**
  * @brief  DHCP_Process_Handle
  * @param  None
  * @retval None
  */
void DHCP_Process(struct netif *netif)
{
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;
  struct dhcp *dhcp;

  switch (DHCP_state)
  {
    case DHCP_START:
    {
      if (Global_TftPrintDebug==1)
         {
           printf ("  State: Looking for DHCP server ...\n");
         }
      ip_addr_set_zero_ip4(&netif->ip_addr);
      ip_addr_set_zero_ip4(&netif->netmask);
      ip_addr_set_zero_ip4(&netif->gw);
      dhcp_start(netif);
      DHCP_state = DHCP_WAIT_ADDRESS;
    }
    break;

  case DHCP_WAIT_ADDRESS:
    {
      Global_EthPcStat = DHCP_WAIT_PC ;
      if (dhcp_supplied_address(netif))
      {
        DHCP_state       = DHCP_ADDRESS_ASSIGNED;
        Global_EthPcStat = DHCP_ADDRESS_ASSIGNED_PC ;
        sprintf((char *)Global_EthPcIpString, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
        if (Global_TftPrintDebug==1)
          {
           printf ("IP assigned by DHCP : %s\n", Global_EthPcIpString);
          }
      }
      else
      {
        dhcp = (struct dhcp *)netif_get_client_data(netif, LWIP_NETIF_CLIENT_DATA_INDEX_DHCP);

        /* DHCP timeout */
        if (dhcp->tries > MAX_DHCP_TRIES)
        {
          DHCP_state = DHCP_TIMEOUT;

          /* Stop DHCP */
          dhcp_stop(netif);

          /* Static address used */
          IP_ADDR4(&ipaddr, IP_ADDR0 ,IP_ADDR1 , IP_ADDR2 , IP_ADDR3 );
          IP_ADDR4(&netmask, NETMASK_ADDR0, NETMASK_ADDR1, NETMASK_ADDR2, NETMASK_ADDR3);
          IP_ADDR4(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
          netif_set_addr(netif, &ipaddr, &netmask, &gw);
          sprintf((char *)Global_EthPcIpString, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
          Global_EthPcStat = DHCP_ADDRESS_STATIC_PC ;

          if (Global_TftPrintDebug==1)
             {
               printf ("DHCP Timeout !! \n");
               printf ("Static IP address: %s\n", Global_EthPcIpString);
             }
        }
      }
    }
    break;
  case DHCP_LINK_DOWN:
    {
      /* Stop DHCP */
      dhcp_stop(netif);
      DHCP_state = DHCP_OFF;
      Global_EthPcStat = DHCP_LINK_DOWN_PC ;
      if (Global_TftPrintDebug==1)
         {
           printf ("The network cable is not connected \n");
         }
    }
    break;
  default: break;
  }
}

/**
  * @brief  DHCP periodic check
  * @param  netif
  * @retval None
  */
void DHCP_Periodic_Handle(struct netif *netif)
{
  /* Fine DHCP periodic process every 500ms */
  if (HAL_GetTick() - DHCPfineTimer >= DHCP_FINE_TIMER_MSECS)
  {
    DHCPfineTimer =  HAL_GetTick();
    /* process DHCP state machine */
    DHCP_Process(netif);
  }
}
#endif


/**
  * @brief  Setup the network interface
  * @param  None
  * @retval None
  */
static void Netif_Config(void)
{
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;

#if LWIP_DHCP
  ip_addr_set_zero_ip4(&ipaddr);
  ip_addr_set_zero_ip4(&netmask);
  ip_addr_set_zero_ip4(&gw);
#else

  /* IP address default setting */
  IP4_ADDR(&ipaddr, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
  IP4_ADDR(&netmask, NETMASK_ADDR0, NETMASK_ADDR1 , NETMASK_ADDR2, NETMASK_ADDR3);
  IP4_ADDR(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);

#endif

  /* add the network interface */
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &ethernet_input);

  /*  Registers the default network interface */
  netif_set_default(&gnetif);

  ethernet_link_status_updated(&gnetif);

#if LWIP_NETIF_LINK_CALLBACK
  netif_set_link_callback(&gnetif, ethernet_link_status_updated);
#endif
}


void ServerInit(void)
{
  Global_EthRxPcFlag = NULL ;
  Global_EthTxPcFlag = NULL ;
  Global_EthPcStat   = NULL ;
  /* Initialize the LwIP stack */
  lwip_init();

  /* Configure the Network interface */
  Netif_Config();

  /* TCP server Init */
  tcp_server_init();

  Global_EthPackets = 0 ;
}


void ServerRealTime(void)
{
  static PUT_DEVICES  HoldPutcDev ;
   HoldPutcDev = Global_PutcDevice ;
   if (Global_TftPrintDebug==1)
      {
        SetColor(WHITE);
        SetTftFont(' ',1,0,0);
        Global_PutcDevice = TFT_DEV_C ;
      }
    else
      {
        Global_PutcDevice = COM4_DEV ;
      }
    /* Read a received packet from the Ethernet buffers and send it
       to the lwIP for handling */
   ethernetif_input(&gnetif);
#if LWIP_NETIF_LINK_CALLBACK
   Ethernet_Link_Periodic_Handle(&gnetif);
#endif
   /* Handle timeouts */
   sys_check_timeouts();


#if LWIP_DHCP
   DHCP_Periodic_Handle(&gnetif);
#endif
   Global_PutcDevice = HoldPutcDev ;
}



