/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2017        */
/*                                                                       */
/*   Portions COPYRIGHT 2017 STMicroelectronics                          */
/*   Portions Copyright (C) 2017, ChaN, all right reserved               */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various existing      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "diskio.h"
#include "sd_diskio.h"
#include "ff_gen_drv.h"
#include "stm32h7xx_hal.h"
#if defined ( __GNUC__ )
#ifndef __weak
#define __weak __attribute__((weak))
#endif
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern Disk_drvTypeDef  disk;
extern FATFS SDFatFs;  /* File system object for SD card logical drive */
extern FIL MyFile;     /* File object */
extern char SDPath[4]; /* SD card logical drive path */
extern SD_HandleTypeDef    hsd_sdmmc[];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static void SD_MspInit(SD_HandleTypeDef *hsd);
static HAL_StatusTypeDef MX_SDMMC1_SD_Init(SD_HandleTypeDef *hsd);


/**
  * @brief This function handles SDMMC1 global interrupt.
  */
void SDMMC1_IRQHandler(void)
{
  HAL_SD_IRQHandler(&hsd_sdmmc[0]);
}

DSTATUS sd_connected (void)
{
  int32_t ret = SD_NOT_PRESENT;

  if (HAL_GPIO_ReadPin(SD_CARD_PIN_CONNCTED_Port, SD_CARD_PIN_CONNCTED_Pin) == GPIO_PIN_SET)
  {
    ret = (int32_t)SD_NOT_PRESENT;
  }
  else
  {
    ret = (int32_t)SD_PRESENT;
  }
  return ret;
}

/**
  * @brief  Init HW Disk Status
  * @param
  * @retval DSTATUS: Operation status
  */
int Disk_Sd_init (void)
{
  int32_t ret = 0;
  ret += FATFS_LinkDriver(&SD_Driver, SDPath);
  ret += SD_initialize(0);
  return ret;
}

int Disk_Sd_DeInit (void)
{
  int32_t ret = 0;
  HAL_SD_DeInit(&hsd_sdmmc[0]);

  ret += FATFS_UnLinkDriver(SDPath);
  return ret;
}


/**
  * @brief  Gets Disk Status
  * @param  pdrv: Physical drive number (0..)
  * @retval DSTATUS: Operation status
  */
#if _USE_STATUS == 1
DSTATUS disk_status (
	BYTE pdrv		/* Physical drive number to identify the drive */
)
{
  DSTATUS stat;

  stat = disk.drv[pdrv]->disk_status(disk.lun[pdrv]);
  return stat;
}
#endif
/**
  * @brief  Initializes a Drive
  * @param  pdrv: Physical drive number (0..)
  * @retval DSTATUS: Operation status
  */
DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
  DSTATUS stat = RES_OK;

  if(disk.is_initialized[pdrv] == 0)
  {
    disk.is_initialized[pdrv] = 1;
    stat = disk.drv[pdrv]->disk_initialize(disk.lun[pdrv]);
  }
  return stat;
}

/**
  * @brief  Reads Sector(s)
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data buffer to store read data
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to read (1..128)
  * @retval DRESULT: Operation result
  */
#if _USE_READ == 1
DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	        /* Sector address in LBA */
	UINT count		/* Number of sectors to read */
)
{
  DRESULT res;

  res = disk.drv[pdrv]->disk_read(disk.lun[pdrv], buff, sector, count);
  return res;
}
#endif
/**
  * @brief  Writes Sector(s)
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data to be written
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to write (1..128)
  * @retval DRESULT: Operation result
  */
#if _USE_WRITE == 1
DRESULT disk_write (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address in LBA */
	UINT count        	/* Number of sectors to write */
)
{
  DRESULT res;

  res = disk.drv[pdrv]->disk_write(disk.lun[pdrv], buff, sector, count);
  return res;
}
#endif /* _USE_WRITE == 1 */

/**
  * @brief  I/O control operation
  * @param  pdrv: Physical drive number (0..)
  * @param  cmd: Control code
  * @param  *buff: Buffer to send/receive control data
  * @retval DRESULT: Operation result
  */
#if _USE_IOCTL == 1
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
  DRESULT res;

  res = disk.drv[pdrv]->disk_ioctl(disk.lun[pdrv], cmd, buff);
  return res;
}
#endif /* _USE_IOCTL == 1 */

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
__weak DWORD get_fattime (void)
{
  return 0;
}


/**
  * @brief  Initializes the SD MSP.
  * @param  hsd  SD handle
  * @retval None
  */
static void SD_MspInit(SD_HandleTypeDef *hsd)
{
  GPIO_InitTypeDef gpio_init_structure;

  if(hsd == &hsd_sdmmc[0])
  {
    /* Enable SDIO clock */
    __HAL_RCC_SDMMC1_CLK_ENABLE();

    /* Enable GPIOs clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();


    /* Common GPIO configuration */
    gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
    gpio_init_structure.Pull      = GPIO_PULLUP;
    gpio_init_structure.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
    /* D0(PC8), D1(PC9), D2(PC10), D3(PC11), CK(PC12), CMD(PD2) */
    /* Common GPIO configuration */
    gpio_init_structure.Alternate = GPIO_AF12_SDIO1;
    /* GPIOC configuration */
    gpio_init_structure.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
    HAL_GPIO_Init(GPIOC, &gpio_init_structure);
    /* GPIOD configuration */
    gpio_init_structure.Pin = GPIO_PIN_2;
    HAL_GPIO_Init(GPIOD, &gpio_init_structure);

    __HAL_RCC_SDMMC1_FORCE_RESET();
    __HAL_RCC_SDMMC1_RELEASE_RESET();

    /* NVIC configuration for SDIO interrupts   */
    HAL_NVIC_SetPriority(SDMMC1_IRQn, 14, 0);
    HAL_NVIC_EnableIRQ(SDMMC1_IRQn);
  }
}

/**
  * @brief  Initializes the SDMMC1 peripheral.
  * @param  hsd SD handle
  * @retval HAL status
  */
static HAL_StatusTypeDef MX_SDMMC1_SD_Init(SD_HandleTypeDef *hsd)
{
  HAL_StatusTypeDef ret = HAL_OK;
  /* uSD device interface configuration */
  hsd->Instance                 = SDMMC1 ;
  hsd->Init.ClockEdge           = SDMMC_CLOCK_EDGE_RISING;
  hsd->Init.ClockPowerSave      = SDMMC_CLOCK_POWER_SAVE_DISABLE;
  hsd->Init.BusWide             = SDMMC_BUS_WIDE_4B;
  hsd->Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_ENABLE ; // SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd->Init.ClockDiv            = SDMMC_HSpeed_CLK_DIV ;

  /* HAL SD initialization   */
  if(HAL_SD_Init(hsd) != HAL_OK)
  {
    ret = HAL_ERROR;
  }

  return ret;
}

DSTATUS init_sd_hw_lines(void)
{
    int32_t ret = RES_OK;
    GPIO_InitTypeDef gpio_init_structure;

    __HAL_RCC_GPIOJ_CLK_ENABLE();
    gpio_init_structure.Pin = SD_CARD_PIN_CONNCTED_Pin;
    gpio_init_structure.Pull = GPIO_PULLUP;
    gpio_init_structure.Mode = GPIO_MODE_INPUT;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(SD_CARD_PIN_CONNCTED_Port, &gpio_init_structure);

    if((uint32_t)sd_connected() != SD_PRESENT)
    {
      ret = RES_NOT_PRESENT;
    }
    else
    {
      SD_MspInit(&hsd_sdmmc[0]);

      if(ret == RES_OK)
      {
        /* HAL SD initialization and Enable wide operation   */
        if(MX_SDMMC1_SD_Init(&hsd_sdmmc[0]) != HAL_OK)
        {
          ret = RES_ERROR;
        }
        else if(HAL_SD_ConfigWideBusOperation(&hsd_sdmmc[0], SDMMC_BUS_WIDE_4B) != HAL_OK)
        {
          ret = RES_ERROR;
        }
        else
        {
          /* Switch to High Speed mode if the card support this mode */
          (void)HAL_SD_ConfigSpeedBusOperation(&hsd_sdmmc[0], SDMMC_SPEED_MODE_HIGH);
        }
      }
    }
    return ret;
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

