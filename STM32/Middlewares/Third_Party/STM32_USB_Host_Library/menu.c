/* Includes ------------------------------------------------------------------ */
#include "main.h"

HID_DEMO_StateMachine  hid_demo;
uint8_t                prev_select = 0;
osSemaphoreId          MenuEvent;
USBH_HandleTypeDef     hUSBHost;
osMessageQId           AppliEvent;
HID_ApplicationTypeDef Appli_state = APPLICATION_IDLE;

static void USBH_KeybdDemo(USBH_HandleTypeDef * phost);
static void HID_MenuThread(void const *argument);


void HID_MenuInit(void)
{
  osSemaphoreDef(osSem);
  MenuEvent = osSemaphoreCreate(osSemaphore(osSem), 1);
  osSemaphoreRelease(MenuEvent);
  osThreadDef(Menu_Thread, HID_MenuThread, osPriorityHigh, 0, 8 * configMINIMAL_STACK_SIZE);
  osThreadCreate(osThread(Menu_Thread), NULL);
}

void HID_UpdateMenu(void)
{
  /* Force menu to show Item 0 by default */
  hid_demo.state = HID_DEMO_IDLE;
  osSemaphoreRelease(MenuEvent);
}

void HID_MenuThread(void const *argument)
{
  for (;;)
  {
    if (osSemaphoreWait(MenuEvent, osWaitForever) == osOK)
    {
      switch (hid_demo.state)
      {
      case HID_DEMO_IDLE:
        BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
        BSP_LCD_DisplayStringAtLine(18,
                              (uint8_t *)
                              "Use [Joystick Left/Right] to scroll up/down");
        hid_demo.state = HID_DEMO_WAIT;
        hid_demo.select = 0;
        osSemaphoreRelease(MenuEvent);
        break;

      case HID_DEMO_WAIT:
        hid_demo.state = HID_DEMO_START;
        if (hid_demo.select != prev_select)
        {
          prev_select = hid_demo.select;

          /* Handle select item */
          if (hid_demo.select & 0x80)
          {
            switch (hid_demo.select & 0x7F)
            {
            case 0:
              hid_demo.state = HID_DEMO_START;
              osSemaphoreRelease(MenuEvent);
              break;

            case 1:
              hid_demo.state = HID_DEMO_REENUMERATE;
              osSemaphoreRelease(MenuEvent);
              break;

            default:
              break;
            }
          }
        }
        break;

      case HID_DEMO_START:
        if (Appli_state == APPLICATION_READY)
        {
          if (USBH_HID_GetDeviceType(&hUSBHost) == HID_KEYBOARD)
          {
            hid_demo.keyboard_state = HID_KEYBOARD_IDLE;
            hid_demo.state = HID_DEMO_KEYBOARD;
          }
          else if (USBH_HID_GetDeviceType(&hUSBHost) == HID_MOUSE)
          {
            hid_demo.mouse_state = HID_MOUSE_IDLE;
            hid_demo.state = HID_DEMO_MOUSE;
          }
        }
        else
        {
          hid_demo.state = HID_DEMO_WAIT;
        }
        osSemaphoreRelease(MenuEvent);
        break;

      case HID_DEMO_REENUMERATE:
        /* Force MSC Device to re-enumerate */
        USBH_ReEnumerate(&hUSBHost);
        hid_demo.state = HID_DEMO_WAIT;
        osSemaphoreRelease(MenuEvent);
        break;

      case HID_DEMO_KEYBOARD:
        if (Appli_state == APPLICATION_READY)
        {
          USBH_KeybdDemo(&hUSBHost);
        }
        break;

      default:
        break;
      }

      if (Appli_state == APPLICATION_DISCONNECT)
      {
        Appli_state = APPLICATION_IDLE;
        hid_demo.state = HID_DEMO_IDLE;
        hid_demo.select = 0;
      }
      hid_demo.select &= 0x7F;
    }
  }
}

void USBH_HID_EventCallback(USBH_HandleTypeDef * phost)
{
  osSemaphoreRelease(MenuEvent);
}

static void HID_InitApplication(void)
{
  HID_MenuInit();
}

static void USBH_UserProcess(USBH_HandleTypeDef * phost, uint8_t id)
{
  switch (id)
  {
    case HOST_USER_SELECT_CONFIGURATION:
         break;
    case HOST_USER_DISCONNECTION:
         osMessagePut(AppliEvent, APPLICATION_DISCONNECT, 0);
         break;

    case HOST_USER_CLASS_ACTIVE:
         osMessagePut(AppliEvent, APPLICATION_READY, 0);
         break;

    case HOST_USER_CONNECTION:
         osMessagePut(AppliEvent, APPLICATION_START, 0);
         break;

    default:
         break;
  }
}

void USB_Thread(void const *argument)
{
  osEvent event;
  HID_InitApplication();
  USBH_Init(&hUSBHost, USBH_UserProcess, 0);
  USBH_RegisterClass(&hUSBHost, USBH_HID_CLASS);
  USBH_Start(&hUSBHost);
  for (;;)
  {
    event = osMessageGet(AppliEvent, osWaitForever);
    if (event.status == osEventMessage)
    {
      switch (event.value.v)
      {
        case APPLICATION_DISCONNECT:
             Appli_state = APPLICATION_DISCONNECT;
             HID_UpdateMenu();
             break;

        case APPLICATION_READY:
             Appli_state = APPLICATION_READY;

        default:
             break;
      }
    }
  }
}


//void TagMaster_Rx(byte Data);
void                    		BC_ZebraBSP_GetDataChar( char DataChar);
/*----------------------------------------------------------------------------*/
static void USBH_KeybdDemo(USBH_HandleTypeDef * phost)
{
  HID_KEYBD_Info_TypeDef *k_pinfo;
  char c;

  if (hid_demo.keyboard_state != HID_KEYBOARD_START)
  {

    k_pinfo = USBH_HID_GetKeybdInfo(phost);

    if (k_pinfo != NULL)
    {
      c = USBH_HID_GetASCIICode(k_pinfo);														// c is the accepted char from the ZEBRA
      if (c != 0)
      {
//        if (Global_BfRxU45HeadPoi <  0x40) Global_BfRxU45Bf[Global_BfRxU45HeadPoi++] = c ;
//        if (Global_BfRxU45HeadPoi >= 0x40) Global_BfRxU45HeadPoi = 0 ;
//        TagMaster_Rx(c);
		  BC_ZebraBSP_GetDataChar( c );
        PRINT_TFT( TFT_DEBUG_C , ( "%c" , c ) );
      }
    }
  }
}


// ----------------------------------------------------------------------------
//      BAR CODEs
// ---------------------
//        Length = 10    ( Ticket  )
//                       TTTTTNNNNN
//                       TTTTT       [5]- Time in minutes
//                       NNNNN       [5]- Ticket No.
//
//        Length = 14    ( Ticket )
//                       5TTTTTTNNNNNNN
//                       '5'          [1]- Start BARCODE Ticket type
//                       TTTTTT       [6]- Time in minutes
//                       NNNNNNN      [7]- Ticket No.  ( 7 digits )
//
//        Length = 22    ( Ticket )
//                       5XnnnnnTTTTTTPPNNNNNNN
//                       '5'          [1]- Start BARCODE Ticket type
//                       X            [1]- 0 = Reg Ticket , 1 = Off Line issued ticket
//                       nnnnn        [5]- if X==3 then nnnnn = Exit Until in minutes
//                                         if X==4 then nnnnn = Money Paid / 10
//                       TTTTTT       [6]- Time in minutes
//                       PP           [2]- Park No.
//                       NNNNNNN      [7]- Ticket No.  ( 7 digits )
// ---------------------
//        Length = 12    ( Sticker )
//                       PPNNNNNNXCCC
//
//                       PP             - Sticker Profile
//                       NNNNNN         - Sticker No
//                       X              - Sticker X * 1000000   Add to Sticker No.
//                       CCC            - Sticker checksum      ( SUM( digits( XX YYYYYY N ) ) * 7854 + PARAM(190) ) mod 1000
// ---------------------
//        Length = 14    ( Coupon    Start with 1,2 )
//                       1AAAAAAAAAAAAA
//                       2BBBBBBBBBBBBB
//        COPOUN
//                       1 NNNNNN  PPPPP  LL
//                       2 TT EEEE HHH RR CC
//
//                       NNNNNN   - Coupon No.     ( 6 digits )
//                       PPPPP    - Coupon price   ( 5 digits )
//                       LL       - Client No.
//                       TT       - Coupon Type
//                       EEEE     - Expired from 01/01/04 by days
//                       HHH      - Parking No.
//                       RR       - Hour
//                       CC       - Checksum
//
//        COPOUN As a Sticker
//                       3AAAAAAAAAAA
//                       4BBBBBBBBBBB
// ---------------------
//
//
//                   1         2         3
// QR(01) - 123456789012345678901234567
//          AAPPPPTTTTTTTTDDMNYYHHMMIII
//          AA           - QR type
//          PPPP         - Park No
//          TTTTTTTT     - Ticket No
//          DDMNYY       - Date
//          HHMM         - Time
//          III          - Reader ID
//                   1         2         3         4
// QR(02) - 12345678901234567890123456789012345678901
//          AAPPPPTTTTTTTTDDMNYYHHMMIIIKKKKKKKK
//          AA           - QR type         = 02
//          PPPP         - Park No         = 123
//          TTTTTTTT     - Ticket No       = 40052
//          DDMNYY       - Date            = 090913
//          HHMM         - Time            = 1009
//          III          - Reader ID       = 181
//          KKKKKKKK     - Car L.P.        = 1234567
//
// QR(03) - 12345678901234567890123456789012345678901  Max 41 chars
//          AAPPPPTTTTTTTTTDDMNYYHHMMRRRZZZZZXXXXX
//          AA           - QR type         = 03
//          PPPP         - Park No         = 1234
//          TTTTTTTTT    - Ticket No       = 110340052
//          DDMNYY       - Date            = 090913
//          HHMM         - Time            = 1009
//          RRR          - Rate            =
//          ZZZZZ        - Payment / 10    = 0020 -> 2.00$
//          XXXXX        - Valid/Pay Time  = In minutes from entry
//
//
//   Ticket SAMPLEs with QR 1,2,3
//
//          5000000564854060100151
//          0102546010015101022016540000000000000
//
//          5000000564858060100152
//          0202546010015201022016580000000000000
//
//          5000000564859060100153
//          5000000 564859 060100153
//          03 0254 060100153 010220 1659 0000000000000
//


//
//
//
//      50000005648540 60100151
//      01 0254 60100151 01 02 20 16 54 0000000000000
//
//
//      5000000564858060100152
//      0202546010015201022016580000000000000
//
//
//      5000000564859060100153
//      03025406010015301022016590000000000000
//
//
