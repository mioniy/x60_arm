/*
 * (C) 2020, BDSOUND SRL. ALL RIGHTS RESERVED.
 *  THIS FILE IS RELEASED UNDER A VALID LICENSE ASSIGNED BY BDSOUND SRL ONLY.
 * IT IS FORBIDDEN ANY USE OF THIS CODE, WHOLE OR IN PART, IN ANY WAY, WITHOUT A VALID LICENSE FROM BDSOUND SRL.
 */

/******************************************************************************
*
*   Simply Sounds Clear 
*   
*   Copyright (C) 2020-- BDSOUND S.r.l. -- www.bdsound.com
*
*******************************************************************************/

#ifndef __BDS2C_API_WR_H
#define __BDS2C_API_WR_H

#include <stdbool.h>

#define S2C_AUDIO_SAMPLE_FREQUENCY						16000
#define S2C_AUDIO_FRAME_SIZE_MS								5
#define S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH  			((S2C_AUDIO_SAMPLE_FREQUENCY*S2C_AUDIO_FRAME_SIZE_MS)/1000)
#define S2C_MEM_SIZE 									        94000 //This is the minimum memory requirement for S2C. Do not change!


//=============================================================================
//  MCHP Interface Class Definition
//=============================================================================

typedef struct S2C_mem_t {                                                       // MCHP Interface Class
	unsigned char reserved[S2C_MEM_SIZE];
}S2C_mem_t;

/********************************************************************************************************************************
 * S2C Modes
 ***
 * S2C_MODE_ECNR:			Echo Canceller and Noise Reduction mode
 * S2C_MODE_SR_BARGE_IN:	Echo Canceller and Noise Reduction mode for barge-in case
 *******************************************************************************************************************************/
typedef enum S2C_mode {
	S2C_MODE_ECNR			= 0,
	S2C_MODE_ECNR_BARGE_IN	= 1,
} S2C_mode;

enum E_S2C_EnableIndex
{
	S2C_AEC_ENABLE          = 0,            // EchoCanceller
	S2C_RESCUE_ENABLE       = 1,            // RescueDetector
	S2C_DUALTONE_DET_ENABLE = 2,            // DualToneDetector
	S2C_POSTFILTER_ENABLE   = 3,            // Postfilter
	S2C_NOISE_RED_ENABLE    = 4,            // Noise Reduction
	S2C_BUZZ_DET_ENABLE     = 5,            // BuzzDetector
	S2C_TX_CNG_ENABLE       = 6,            // TxComfortNoise
	S2C_TX_EQ_ENABLE        = 7,            // TxEqualizer
	S2C_SPEECH_DET_ENABLE   = 8,            // SpeechDetector
	S2C_TX_AGC_ENABLE       = 9,            // TxAGC
	S2C_RX_LIMITER_ENABLE	= 10,           // RxLimiter
	S2C_RX_PEQ_ENABLE       = 11,           // RxParamEQ
	S2C_TX_MUTE_ENABLE      = 12,           // TX Mute
	S2C_RX_MUTE_ENABLE      = 13,           // RX Mute
};

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************************************************************************

* S2C Initialization Function
***
* It is used to initialize the S2C memory 
*
* @pmode:   Allows to init S2C for		* S2C_MODE_ECNR:			Echo Canceller and Noise Reduction mode
										* S2C_MODE_SR_BARGE_IN:	Echo Canceller and Noise Reduction mode for barge-in case

  returns S2C allocated memory size. 
	
*********************************************************************************************************************************/
int S2C_Init(S2C_mem_t *S2C_mem, S2C_mode mode);
	
	
/********************************************************************************************************************************

* S2C Restart Function
***
* It is recommended to call this function any time Vocal Assistant session begins ALDO::
	
*********************************************************************************************************************************/
void S2C_Restart(S2C_mem_t *S2C_mem);
	

/********************************************************************************************************************************
 * S2C Microphone Processing function
 ***
 * With this function S2C processes MIC and SPK audio samples.
 *
 * @micInOut:         pointer to the buffer with the MIC samples to be processed (float type). It'll be holding the processed MIC buffer, when completed. 
 * @speakerInOut: 	  pointer to the buffer of speaker reference samples, ready to send to near-end (float type). It'll be holding the processed SPK buffer, when completed.
 * 
 *  In both the cases: Float buffer size = S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH 
 ************************************************************************************************************************************************************/
void S2C_ProcessMicSpkFrame(S2C_mem_t *S2C_mem, short* micInOut, short* speakerInOut);
	
/********************************************************************************************************************************
 * S2C Get S2S sample rate
 ***
 * This function returns the sample rate of the S2C library (Hz)
 * 
 *********************************************************************************************************************************/	
int S2C_GetSampleRate (S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
 * S2C Get S2S Frame size
 ***
 * With this function S2C  returns the frame size, in samples.
 *
 *******************************************************************************************************************************/	
int S2C_GetFrameSize (S2C_mem_t *S2C_mem);
	
/********************************************************************************************************************************
 * S2C Get S2S Frame size
 ***
 * With this function you can enable/disable the several processing blocks in  S2C:
 *  
	AEC_ENABLE          = 0,            // EchoCanceller
    RESCUE_ENABLE       = 1,            // RescueDetector
    DUALTONE_DET_ENABLE = 2,            // DualToneDetector
    POSTFILTER_ENABLE   = 3,            // Postfilter
    NOISE_RED_ENABLE    = 4,            // Noise Reduction
    BUZZ_DET_ENABLE     = 5,            // BuzzDetector
    TX_CNG_ENABLE       = 6,            // TxComfortNoise
    TX_EQ_ENABLE        = 7,            // TxEqualizer
    SPEECH_DET_ENABLE   = 8,            // SpeechDetector
    TX_AGC_ENABLE       = 9,            // TxAGC
    RX_LIMITER_ENABLE,	= 10,           // RxLimiter
    RX_PEQ_ENABLE,      = 11,           // RxParamEQ
    TX_MUTE_ENABLE,     = 12,           // TX Mute
    RX_MUTE_ENABLE,     = 13,           // RX Mute
 
 *******************************************************************************************************************************/
void S2C_SetEnable(S2C_mem_t *S2C_mem, int EnableIndex, bool Flag);

/********************************************************************************************************************************
 * S2C Get S2S Frame size
 ***
 * This function returns the enabled/disabled state of the several processing blocks in  S2C:
 *  
	AEC_ENABLE          = 0,            // EchoCanceller
    RESCUE_ENABLE       = 1,            // RescueDetector
    DUALTONE_DET_ENABLE = 2,            // DualToneDetector
    POSTFILTER_ENABLE   = 3,            // Postfilter
    NOISE_RED_ENABLE    = 4,            // Noise Reduction
    BUZZ_DET_ENABLE     = 5,            // BuzzDetector
    TX_CNG_ENABLE       = 6,            // TxComfortNoise
    TX_EQ_ENABLE        = 7,            // TxEqualizer
    SPEECH_DET_ENABLE   = 8,            // SpeechDetector
    TX_AGC_ENABLE       = 9,            // TxAGC
    RX_LIMITER_ENABLE,	= 10,           // RxLimiter
    RX_PEQ_ENABLE,      = 11,           // RxParamEQ
    TX_MUTE_ENABLE,     = 12,           // TX Mute
    RX_MUTE_ENABLE,     = 13,           // RX Mute
 
 *******************************************************************************************************************************/
bool S2C_GetEnable(S2C_mem_t *S2C_mem, int EnableIndex);

/********************************************************************************************************************************
 * S2C get MIC AGC value
 ***
 * With this function S2C returns the AGC value currently set. 
 *
 *********************************************************************************************************************************/	
float S2C_GetTxAGC_Ref_dB(S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
* S2C sets MIC AGC value
***
* Automatic Gain Control is intended to add an additional gain in the signal path with a value that is automatically 
* controlled according to a target value of speech RMS power set with this function.
* This value is referenced to a 0dB maximum power (0dBFS) hence its value can be only negative.
* Its range goes from -6.0 dB to -30 dB.
*********************************************************************************************************************************/
void S2C_SetTxAGC_Ref_dB(S2C_mem_t *S2C_mem, float Ref_dB);

/********************************************************************************************************************************
* S2C get SPK volume
***
* The speaker signal can be digitally amplified; this function returns the current value in dB
*********************************************************************************************************************************/
float S2C_GetVolume_dB(S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
* S2C set SPK volume
***
* The speaker signal can be digitally amplified; with this function you set the current value in dB
* Its range goes from -40.0 dB to +20 dB.
*********************************************************************************************************************************/
void S2C_SetVolume_dB(S2C_mem_t *S2C_mem, float Ref_dB);

/********************************************************************************************************************************
* S2C get MIC gain
***
* The mic signal can be digitally pre-amplified; this function returns the current value in dB
*********************************************************************************************************************************/
float S2C_GetMicGain_dB(S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
* S2C set MIC gain
***
* The mic signal can be digitally pre-amplified; with this function you set the value in dB
* Its range goes from -6.0 dB to +20 dB.
*********************************************************************************************************************************/
void S2C_SetMicGain_dB(S2C_mem_t *S2C_mem, float Ref_dB);

/********************************************************************************************************************************
* Set the MIC 1/3 octave equalizer gains
***
The Graphic Equalizer block is a useful feature to selectively modify the signal spectrum adding gain or attenuation in a selected frequency band centered at the specified filter frequency.
The frequencies are fixed with a 1/3rd octave bandwidth.The available frequencies with the corresponding indexes are the following:

INDEX	FREQ
0		25
1		100
2		125
3		150
4		200
5		250
6		325
7		400
8		500
9		625
10		800
11		1000
12		1250
13		1600
14		2000
15		2500
16		3150
17		4000
18		5000
19		6300
20		8000
*********************************************************************************************************************************/
void S2C_SetMicEqLogBandGain_dB(S2C_mem_t *S2C_mem, int eq_index, float Gain_dB);

/********************************************************************************************************************************
* Get the MIC 1/3 octave equalizer gains
***
By selecting the EQ index, it's possible to get the corresponding band gain in dB
*********************************************************************************************************************************/
float S2C_GetMicEqLogBandGain_dB(S2C_mem_t *S2C_mem, int eq_index);

/********************************************************************************************************************************
* Get the MIC 1/3 octave equalizer frequency
***
By selecting the EQ index, it's possible to get the corresponding band center frequency
*********************************************************************************************************************************/
float S2C_GetMicEqLogBandFreq_Hz(S2C_mem_t *S2C_mem, int eq_index);

/********************************************************************************************************************************
* Get the MIC 1/3 octave equalizer number of bands
*********************************************************************************************************************************/
int S2C_GetMicEqNumberOfBands(S2C_mem_t *S2C_mem);


/********************************************************************************************************************************
* S2C get Limiter on SPK
***
* this function returns the SPK limiter value
*********************************************************************************************************************************/
float S2C_GetLimiterRef_dB(S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
* S2C set Limiter on SPK
***
* This block limits the level of the signal delivered to the loudspeakers. 
* It is mainly used to avoid the enclosure vibration that may appear with high loudspeaker signals. 
* The limiter, basically acting like a compressor, will limit the output signal while keeping the perceived loudness constant as much as possible.
* Its range goes from 0 dB to -20 dB, and the value is referred to the full scale.
*********************************************************************************************************************************/
void S2C_SetLimiterRef_dB (S2C_mem_t *S2C_mem, float Ref_dB);

#define S2C_PEQ_UG      0       // Flat EQ unit gain (default)
#define S2C_PEQ_LP      1       // Low Pass filter (Gain_dB set the pass band gain)
#define S2C_PEQ_HP      2       // High Pass filter (Gain_dB set the pass band gain)
#define S2C_PEQ_BP      3       // Band Pass filter (Gain_dB set the peak gain)
#define S2C_PEQ_NO      4       // Notch filter (Gain_dB set the pass band gain)
#define S2C_PEQ_PB      5       // Peaking Band filter (Gain_dB set the peak gain)
#define S2C_PEQ_LS      6       // Low Shelf filter (Gain_dB set the pass band gain)
#define S2C_PEQ_HS      7       // High Shelf filter (Gain_dB set the pass band gain)

/********************************************************************************************************************************
* S2C set Parametric EQ on SPK
***
* The parametric equalizer block is a useful feature to selectively modify the signal spectrum adding gain or attenuation in a frequency band centered at the user selected filter frequency.
* You can select the preferred frequency through f, select the gain through Gain_dB, select the Q-factor through Q. Of course it's possible to set the fiter type between:
*
* You can use up to 6 PEQ in daisy-chain, to be selected through stage_index, and set each filter tyoe
*********************************************************************************************************************************/
void S2C_PEQ_SetStage(S2C_mem_t *S2C_mem, int stage_index, int PEQ_Type, float f, float Q,float Gain_dB);

/********************************************************************************************************************************
* S2C get Parametric EQ on SPK
***
* For each PEQ stage you can get the EQ parameters and type: PEQ_Type, frequency f, Q, and Gain_dB
*********************************************************************************************************************************/
void S2C_PEQ_GetStage(S2C_mem_t *S2C_mem, int stage_index, int *PEQ_Type, float *f, float *Q,float *Gain_dB);

/********************************************************************************************************************************
* S2C get post-processing MIC gain
***
* The signal after the Echo Canceller Block can be digitally amplified;
* this function returns its value in dB.
*********************************************************************************************************************************/
float S2C_GetPostGain_dB(S2C_mem_t *S2C_mem);

/********************************************************************************************************************************
* S2C set post-processing MIC gain
***
* The signal after the Echo Canceller Block can be digitally amplified;
* this function sets its value in dB.
*********************************************************************************************************************************/
void S2C_SetPostGain_dB(S2C_mem_t *S2C_mem, float Gain_dB);

/********************************************************************************************************************************
 * S2C set Double Talk Sensitivity Level
 ***
 * This parameter sets the full duplex capability of system. It is possible to select a level between 0 and 2: 
 * system behaves with more full-duplex capability with a higher level.
 *******************************************************************************************************************************/	
void S2C_SetTweakMode(S2C_mem_t *S2C_mem, int TweakMode);

int S2C_GetTweakMode(S2C_mem_t *S2C_mem);
	
#ifdef __cplusplus
}
#endif


#endif // __BDS2C_API_WR_H


