/*
 * (C) 2020, BDSOUND SRL. ALL RIGHTS RESERVED.
 *  THIS FILE IS RELEASED UNDER A VALID LICENSE ASSIGNED BY BDSOUND SRL ONLY.
 * IT IS FORBIDDEN ANY USE OF THIS CODE, WHOLE OR IN PART, IN ANY WAY, WITHOUT A VALID LICENSE FROM BDSOUND SRL.
 */


#ifndef _BD_SIP_UA_H__
#define _BD_SIP_UA_H__

#include <stdint.h>

typedef enum bdSIPTransportProtocol {
	BD_SIP_TRANSPORT_UDP    = 0,
	BD_SIP_TRANSPORT_TCP   	= 1,
} bdSIPTransportProtocol;

typedef enum bdRegisterStatus {
	BD_REG_STATUS_NOT_REGISTERED    = 0,
	BD_REG_STATUS_REGISTERED      	= 1,
	BD_REG_STATUS_REGISTERING       = 2,
} bdRegisterStatus;

#define BD_SIZE_AOR                 120 // sip:<username>@<server>
#define BD_SIZE_DISPLAY_NAME        60
#define BD_SIZE_USERNAME            60
#define BD_SIZE_PASSWORD            60
#define BD_SIZE_SERVER              60 // sip:<server>

#if __BD_SOUND__ == 20210503  // See IAR project settings; Using file SIP_S2C_lib_03052021.a
   #undef   BD_SIZE_DISPLAY_NAME
   #undef   BD_SIZE_USERNAME
   #undef   BD_SIZE_SERVER

   #define  BD_SIZE_DISPLAY_NAME    120
   #define  BD_SIZE_USERNAME        120
   #define  BD_SIZE_SERVER          120 // sip:<server>
#endif

typedef struct bdAccount_t {
	char aor[BD_SIZE_AOR];
	char displayName[BD_SIZE_DISPLAY_NAME];
	char userName[BD_SIZE_USERNAME];
	char password[BD_SIZE_PASSWORD];
  char server[BD_SIZE_SERVER];

	uint16_t regint;             /**< Registration interval in [seconds] */
} bdAccount_t;


/******************************************************************
 * Sip Ua Data Mem
 ******************************************************************/
typedef struct {
	unsigned char reserved[5000];
} bdSipUa_mem_t;


/************************************************************************************************************************************
 * SIP Handlers
 ************************************************************************************************************************************/

/******************************************************************
 * Sip Register Status Handler
 * Gives information every time the register status change.
 *
 *	@registered		information about registration [1=successed - 0=failed].
 * 	@scode				SIP code identifies the registration failure.
 * 	@reason				SIP reason describes the registration failure cause.
 * 	@user_data		pointer to the user data passed at set of SIP handler.
 ******************************************************************/
typedef void (* bdSipUa_register_handler) (uint8_t registered, uint16_t scode, char* reason, void *user_data);


/******************************************************************
 * Call Status Enum
 ******************************************************************/
typedef enum bdCallStatus {
	BD_CALL_STATUS_ESTABLISHED      = 0,
	BD_CALL_STATUS_IN_PROGRESS      = 1,
    BD_CALL_STATUS_RINGING          = 2,
    BD_CALL_STATUS_TERMINATED       = 3,
	BD_CALL_STATUS_INCOMING_CALL	= 4,
} bdCallStatus;

/******************************************************************
* Call Types Enum
******************************************************************/
typedef enum bdCallType {
	BD_CALL_TYPE_SERVER = 1,
	BD_CALL_TYPE_P2P = 2
} bdCallType;


/******************************************************************
* SIP MESSAGE Enum
******************************************************************/
typedef enum bdSIPMessageType {
	BD_SIP_MESSAGE_TEXT	 =	1,
	BD_SIP_MESSAGE_SCAIP =	2,
	BD_SIP_MESSAGE_ESIVOIP = 3
} bdSIPMessageType;

/******************************************************************
 * Sip Call Status Handler
 * Gives information every time the call status change.
 *
 * 	@status					status of the call [bdCallStatus].
 * 	@incoming_aor		if status is BD_CALL_STATUS_INCOMING_CALL, contains the aor of the incoming user [ex: sip:<userName>@<sipServerAddress>].
 * 	@scode					if staus is BD_CALL_STATUS_TERMINATED, SIP code identifies the end of the call.
 *	@reason					if staus is BD_CALL_STATUS_TERMINATED, SIP reason describes the end of the call cause.
 * 	@sample_rate		sample rate used in the call (BD_CALL_STATUS_ESTABLISHED).
 * 	@user_data			pointer to the user data passed at set of SIP handler.
 ******************************************************************/
typedef void (* bdSipUa_call_status_handler) (bdCallStatus status, char* incoming_aor, uint16_t scode, char* reason, uint16_t sample_rate, void *user_data);

/******************************************************************
 * Sip DTMF Info Handler
 * This callback is fired up anytime a DTMF info message is recieved. It's called for each single recieved DTMF.
 *
 * 	@sig						buffer that contains the single dtmf (sig[0] = DTMF)
 * 	@duration				single DTMF duration (ms)
 * 	@user_data			user specified memory pointer
 ******************************************************************/
typedef void (* bdSipUa_call_sip_info_dtmf_handler) (const char* sig, const char* duration, void *user_data);


/******************************************************************
 * Sip DTMF RFC2833 Handler
 * This callback is fired up anytime a DTMF info message is recieved. It's called for each single recieved DTMF.
 *
 * 	@sig						buffer that contains the single dtmf (sig[0] = DTMF)
 * 	@user_data					user specified memory pointer
 ******************************************************************/
typedef void(*bdSipUa_call_sip_rfc2833_dtmf_handler) (const char sig, void *user_data);


/******************************************************************
 * Sip Message received Handler
 * This callback is fired up anytime a SIP message is recieved.
 *
 * 	@sender						pointer to the sender URI
 * 	@ctype						pointer to the Content-Type of the message
 * 	@body						pointer to the SIP message body buffer
 * 	@body_size					lenght of the message body in bytes.
 * 	@user_data					user specified memory pointer
 ******************************************************************/
typedef void(*bdSipUa_message_handler) (char *sender, const char *ctype, char *body, uint32_t body_size, void *user_data);

/******************************************************************
 * Sip Handlers Struct
 ******************************************************************/
typedef struct bdSipUa_sipHandlers_t {
	bdSipUa_register_handler register_handler;
	void *register_user_data;
	bdSipUa_call_status_handler call_status_handler;
	void *call_status_user_data;

	bdSipUa_call_sip_info_dtmf_handler sip_info_dtmf_handler;
	void *sip_info_dtmf_user_data;

	bdSipUa_call_sip_rfc2833_dtmf_handler sip_rfc2833_dtmf_handler;
	void *sip_rfc2833_dtmf_user_data;

	bdSipUa_message_handler sip_message_handler;
	void *sip_message_user_data;
} bdSipUa_sipHandlers_t;

/******************************************************************
 * Set Sip Handlers (after SipUa init)
 * This function returns 0 if set is correct, -1 if an error occurs.
 ******************************************************************/
int16_t bdSipUa_setSipHandlers(bdSipUa_mem_t *mem, bdSipUa_sipHandlers_t sipHandlers);


/************************************************************************************************************************************
 * INITIALIZATION
 ************************************************************************************************************************************/

/******************************************************************
 * Alloc Ua
 * This must be done before the Task Scheduler is started.
 * This function returns 0 if set is correct, -1 if an error occurs.
 ******************************************************************/
int16_t bdSipUa_alloc(bdSipUa_mem_t *mem);

/******************************************************************
 * Init Ua
 *
 * This function must be called when network is connected
 * and IP is obtained (DHCP)
 * This must be done in a task after the Task Scheduler is started.
 * This function returns 0 if set is correct, or a specific error code if an exception occurs.
 * Error codes:
 * 	-1 = error initializing timebase
 * 	-2 = unable to create DNS client
 * 	-3 = unable to allocate SIP memory
 * 	-4 = unable to retrieve IP address from LWIP
 * 	-5 = unable to set up transport layer (TCP or UDP)
 * 	-6 = unable to initiate SIP listener
 * 	-7 = unable to create SIP session socket
 * 	-8 = unable to allocate SDP session
 ******************************************************************/
int16_t bdSipUa_init(bdSipUa_mem_t *mem, uint16_t portSIP, bdSIPTransportProtocol transportProtocol);

/******************************************************************
 * Ua is started and is ready to register
 * @return: 0 - not yet started (ready). 1 - started (ready)
 ******************************************************************/
uint8_t bdSipUa_isStarted(bdSipUa_mem_t *mem);

/******************************************************************
 * DeInit
 ******************************************************************/
void bdSipUa_deInit(bdSipUa_mem_t *mem);


/************************************************************************************************************************************
 * SIP Functions
 ************************************************************************************************************************************/

/******************************************************************
 * Register to a Sip Server
 * This must be done in a task after the Task Scheduler is started.
 *
 *	@mem					pointer to bdSipUa_mem_t memory.
 *	@aor					aor				 	-> sip:<userName>@<sipServerAddress>
 *	@reg_uri			registrar 	-> sip:<sipServerAddress>
 *	@password			password
 *	@displayName	display name
 ******************************************************************/
int16_t bdSipUa_register(bdSipUa_mem_t *mem, bdAccount_t *account);

/******************************************************************
 * Unregister
 * This must be done in a task after the Task Scheduler is started.
 ******************************************************************/
void bdSipUa_unregister(bdSipUa_mem_t *mem);

/******************************************************************
 * Accept an incoming call
 * This must be done in a task after the Task Scheduler is started.
 ******************************************************************/
int16_t bdSipUa_accept(bdSipUa_mem_t *mem);

/******************************************************************
 * Hang up a call - Reject an incoming call
 * This must be done in a task after the Task Scheduler is started.
 ******************************************************************/
void bdSipUa_hangUp(bdSipUa_mem_t *mem);

/******************************************************************
 * Make a call
 * This must be done in a task after the Task Scheduler is started.
 *
 *	@mem				pointer to struct bdSipUa_mem_t
 *	@caller_aor			aor	-> sip:<userName>@<sipServerAddress>
 *
 *	if call type is BD_CALL_TYPE_SERVER
 *  	@callee_aor			aor	-> sip:<userName>@<sipServerAddress>	(Ex:	bdSipUa_call(bd_sipUa, "sip:1234@192.168.1.100", "sip:5678@192.168.1.100", BD_CALL_TYPE_SERVER);)
 *  else if call type is BD_CALL_TYPE_P2P
 *  	@callee_aor			aor	-> sip:<calleeIPaddress>				(Ex:	bdSipUa_call(bd_sipUa, "sip:1234@192.168.1.100", "sip:192.168.1.14", BD_CALL_TYPE_P2P);)
 *
 *	@callType			call types: BD_CALL_TYPE_SERVER or BD_CALL_TYPE_P2P
 *
 ******************************************************************/
int16_t bdSipUa_call(bdSipUa_mem_t *mem, char *caller_aor, char *callee_aor, bdCallType callType);

/******************************************************************
 * Enable/Disable Auto Answer
 ******************************************************************/
void bdSipUa_enableAutoAnswer(bdSipUa_mem_t *mem, uint8_t enable);

/******************************************************************
 * Get Enable/Disable Auto Answer
 ******************************************************************/
uint8_t bdSipUa_autoAnswerIsEnabled(bdSipUa_mem_t *mem);


/************************************************************************************************************************************
 * AUDIO FUNCTIONS
 ************************************************************************************************************************************/

/******************************************************************
 * Put Capture Audio Buffer to NET
 ******************************************************************/
void bdSipUa_putCaptureBuffer(bdSipUa_mem_t *mem, short *buffer);

/******************************************************************
 * Get Play Audio Buffer from NET
 ******************************************************************/
int16_t bdSipUa_getPlayBuffer(bdSipUa_mem_t *mem, short *buffer);

/******************************************************************
* Set RTP descriptor IP address for NAT
******************************************************************/
void bdSipUa_setRTPDescriptorIP(bdSipUa_mem_t *mem, const char *addr);

/***********************************************************************************
 * Get SIP Transport Protocol (returns 0 for UDP and 1 for TCP. -1 if error occurs)
 **********************************************************************************/
bdSIPTransportProtocol bdSipUa_getSIPTransportProtocol(bdSipUa_mem_t *mem);

/******************************************************************
 * Set RTP port
 ******************************************************************/
void bdSipUa_setPortRTP(bdSipUa_mem_t *mem, uint16_t port);

/******************************************************************
 * Get RTP port
 ******************************************************************/
uint16_t bdSipUa_getPortRTP(bdSipUa_mem_t *mem);

/******************************************************************
 * Get SIP port
 ******************************************************************/
uint16_t bdSipUa_getPortSIP(bdSipUa_mem_t *mem);

/******************************************************************
 * Get Register Status
 ******************************************************************/
bdRegisterStatus bdSipUa_getRegisterStatus(bdSipUa_mem_t *mem);

/******************************************************************
 * Get Account
 ******************************************************************/
bdAccount_t *bdSipUa_getAccount(bdSipUa_mem_t *mem);

/******************************************************************
 * Send a message
 * This must be done in a task after the Task Scheduler is started.
 *
 *	@mem				pointer to struct bdSipUa_mem_t
 *	@to_uri				the message recipient uri			-> sip:<RecipientUserName>@<sipServerAddress>
 *	@msg				string to be sent
 *	@from _uri			the message sender uri				-> sip:<SenderUserName>@<sipServerAddress>
 *	@msg_type			BD_SIP_MESSAGE_TEXT	or BD_SIP_MESSAGE_SCAIP or BD_SIP_MESSAGE_ESIVOIP
 *
 ******************************************************************/
int bdSipUa_message_send(bdSipUa_mem_t *mem, const char *to_uri, const char *msg, const char *from_uri, bdSIPMessageType msg_type);

/******************************************************************
 * Send DTMF key over RTP (RFC2833)
 *
 * @key			single DTMF char to be sent
 ******************************************************************/
void bdSipUa_send_RFC2833_DTMF(bdSipUa_mem_t *mem, char key);

/**************************************************************************
 * Send DTMF Signal (SIP Info)
 *
 * @dtmf			single DTMF char to be sent
 * @duration		duration of the single DTMF according to SIP-INFO [ms]
 ***************************************************************************/
void bdSipUa_send_SIPINFO_DTMF(bdSipUa_mem_t *mem, char dtmf, uint16_t duration);

/******************************************************************
 * CODEC FUNCTIONS
 ******************************************************************/
 /*******************************************************************************************
 * Sip Codec INIT function pointers
 * this function allows the user to set the init function for the codec he wants to import
 * @user_data	user codec memory pointer
 ******************************************************************************************/
typedef int (*bdSipUa_encoderInit)(void *user_data);
typedef int (*bdSipUa_decoderInit)(void *user_data);

/*******************************************************************************************
 * Sip Codec DEINIT function pointers
 * this function allows the user to set the deinit function for the codec he wants to import
 * @user_data	user codec memory pointer
 ******************************************************************************************/
typedef int (*bdSipUa_encoderDeinit)(void *user_data);
typedef int (*bdSipUa_decoderDeinit)(void *user_data);

/*******************************************************************************************
 * Sip Codec ENCODE/DECODE function pointers
 * this function allows the user to set the encode/decode function for the codec he wants to import
 * @user_data	user codec memory pointer
 * @input_buf	input buffer to encode/decode
 * @input_len	input buffer size in bytes
 * @output_buf	output encoded/decoded buffer
 * @output_len	output buffer size in bytes
 ******************************************************************************************/
typedef int (*bdSipUa_codecEncode)(void *user_data, int16_t *input_buf, uint32_t input_len, int8_t *output_buf, uint32_t *output_len, uint8_t fmt);
typedef int (*bdSipUa_codecDecode)(void *user_data, int8_t *input_buf, uint32_t input_len, int16_t *output_buf, uint32_t *output_len, uint8_t fmt);

#define BDSIPUA_MAX_ALLOWED_CODEC    10
typedef struct bdUsrCodec_t {

    bdSipUa_encoderInit     encoderInit;
    bdSipUa_decoderInit     decoderInit;
    bdSipUa_encoderDeinit   encoderDeinit;
    bdSipUa_decoderDeinit   decoderDeinit;
	bdSipUa_codecEncode     codecEncode;
	bdSipUa_codecDecode     codecDecode;

	int8_t   code;		//RTP payload type
    uint8_t  fmt;
    uint8_t  compressionRatio;
    uint16_t sample_rate;

	char    *name;

    void   *encDataMem;  // pointer to user data
    void   *decDataMem;

	int enabled;

} bdUsrCodec_t;

 /******************************************************************************************
 * Sip Codec add function
 * this function allows the user to add a custom codec to bdSound's SIP library block
 * @codec	user codec memory pointer
 ******************************************************************************************/
int bdSipUa_add_user_codec(bdSipUa_mem_t *mem, bdUsrCodec_t *codec);

  /******************************************************************************************
  * Sip Codec get if enabled/disabled
  * this function allows the user to check if a specific codec is enabled or disabled.
  * This function returns 0 if get is correct, -1 the specified codec is not found.
  * @codec_name	string associated to the codec name. Must match bdUsrCodec_t.name
  * @enabled can be 0 or 1, if disabled or enabled is returned.
  ******************************************************************************************/
 int bdSipUa_get_enable_codec(bdSipUa_mem_t *mem, char *codec_name, int *enabled);

  /******************************************************************************************
  * Sip Codec set enabled/disabled for a specific codec
  * this function allows the user to enable or disable a specific codec.
  * This function returns 0 if set is correct, -1 the specified codec is not found.
  * @codec_name	string associated to the codec name. Must match bdUsrCodec_t.name
  * @enabled can be 0 or 1, if you want disable or enable @codec_name.
  ******************************************************************************************/
 int bdSipUa_set_enable_codec(bdSipUa_mem_t *mem, char *codec_name, int enabled);

  /*******************************************************************************************************
  * Sip get codec list.
  * this function allows the user to retrieve the codec list.
  * This function returns 0 if get is correct, -1 if an error occurred.
  * @codec_list	is an array of bdUsrCodec_t. This array must be at least BDSIPUA_MAX_ALLOWED_CODEC long.
  *******************************************************************************************************/
 int bdSipUa_get_codec_list(bdSipUa_mem_t *mem, bdUsrCodec_t *codec_list);

#endif //_BD_SIP_UA_H__
