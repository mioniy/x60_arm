/*
 * (C) 2020, BDSOUND SRL. ALL RIGHTS RESERVED.
 *  THIS FILE IS RELEASED UNDER A VALID LICENSE ASSIGNED BY BDSOUND SRL ONLY.
 * IT IS FORBIDDEN ANY USE OF THIS CODE, WHOLE OR IN PART, IN ANY WAY, WITHOUT A VALID LICENSE FROM BDSOUND SRL.
 */
 
#ifndef BD_AUDIO_INTERFACE_AEC_H
#define BD_AUDIO_INTERFACE_AEC_H

#include <stdint.h>
#include "bdSipUa.h"
#include "bdS2C.h"

/******************************************************************
 * Audio Interface AEC Data Mem
 ******************************************************************/
typedef struct audioInterfaceAEC_t {  
  // Record Buffer 1 channel
  short  SinOut[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH];  /* AUDIO Buffer */
  short  RinOut[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH];  /* AUDIO Buffer */
	//float  *SinOutmic[1];

  // Play & Record Buffer 2 Channel not interleaved
  int16_t bufferAudioRecord[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH*2*2];  /* AUDIO Buffer */
  int16_t bufferAudioPlay[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH*2*2];  	/* AUDIO Buffer */
	
	uint16_t frame_size;
  
  // Pointers to Play & Record Buffer 2 Channel not interleaved
  int16_t *pAuxPlay;
  int16_t *pAuxRecord;
  
  // SipUa
  bdSipUa_mem_t *sipUa;
  
  // S2C
  S2C_mem_t *S2C_mem;
	
	// Semaphore wait for buffer from DMA
  void *aecProcSemaphore;
} audioInterfaceAEC_t;

/******************************************************************
 * Init Audio Interface
 ******************************************************************/
void audioInterfaceAEC_init(audioInterfaceAEC_t *mem_audioInterfaceAEC, bdSipUa_mem_t *sipUaMem, S2C_mem_t *S2C_mem);

/******************************************************************
 * ReInit Audio Interface
 ******************************************************************/
void audioInterfaceAEC_reInitAEC(void);


void audioInterfaceAEC_TransferRecordCallBack(short completeTransfer);
void audioInterfaceAEC_TransferPlayCallBack(short completeTransfer);

// Delay
//void Delay(__IO uint32_t nCount);

#endif // BD_AUDIO_INTERFACE_AEC_H
