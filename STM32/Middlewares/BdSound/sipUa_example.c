/**
 *
 *  Version 1.0
 *
 *  Copyright 2019 bdSound s.r.l.
 *  All rights reserved.
 *
 *  www.bdsound.com
 *
 *
 *  Use in binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *  1) this file must be used within a project that includes at least
 *  one fully licensed S2C product (or other products) from bdSound
 *  Redistribution in source form, with or without modification,
 *  is forbidden.
 *  THIS SOFTWARE IS PROVIDED BY BDSOUND ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "main.h"
#include "sipUa_example.h"
#include "audioInterfaceAEC.h"
#include <string.h>

/************************************************************************************************************************************
 * SIP HANDLERS
 ************************************************************************************************************************************/
bdSipUa_mem_t *sipUa_example_mem;

/******************************************************************
 * Register Status Handler
 ******************************************************************/
 void ex_register_handler(uint8_t registered, uint16_t scode, char* reason, void *user_data)
{
  Global_SipScode = scode ;
  if (Global_TftPrintDebug)
     {
       if(registered)
       {
               PRINT_TFT(TFT_DEV , ("  SIP registration success!\n"));
               PRINT_TFT(TFT_DEV , ("Reason: %s\n", reason));
               PRINT_TFT(TFT_DEV , ("Code: %d\n", scode));
               Global_IntercomSuccRegTm = 0 ;
       }
       else
       {
               PRINT_TFT(TFT_DEV , ("  SIP registration fail!\n"));
               PRINT_TFT(TFT_DEV , ("Reason: %s\n", reason));
               PRINT_TFT(TFT_DEV , ("Code: %d\n", scode));
       }
     }
}

/******************************************************************
 * Call Status Handler
 ******************************************************************/
void ex_call_status_handler(bdCallStatus status, char* incoming_aor, uint16_t scode, char* reason, uint16_t sample_rate, void *user_data)
{
	 	switch(status)
                {
			case BD_CALL_STATUS_ESTABLISHED:
			{
Global_audR = 'e' ;
				// Call Established
                                  if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV , ("  Call established\n"));
				break;
			}

			case BD_CALL_STATUS_RINGING:
			{
Global_audR = 'r' ;
				// Ringing
				if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV , ("  Call ringing\n"));
				break;
			}

			case BD_CALL_STATUS_TERMINATED:
			{
Global_audR = 't' ;
				// Call Terminated -> scode [reason]
				if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV , ("  Call terminated\n"));
				break;
			}

			case BD_CALL_STATUS_INCOMING_CALL:
			{
Global_audR = 'i' ;
				// Incoming call from "incoming_aor"
				if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV , ("  Call inprogress\n"));
				break;
			}
                        case BD_CALL_STATUS_IN_PROGRESS:
                        {
                                  Global_audR = 'p' ;
                                  break ;
                        }
		}
Global_audR3   =  Global_audR;
}

void KeepS2VolandMic(void)
{
  uint8_t   VolSpk , VolMic ;
  uint32_t  x               ;
   VolSpk = (uint8_t)(S2C_GetVolume_dB (&S2C_mem) + 40);
   VolMic = (uint8_t)(S2C_GetMicGain_dB(&S2C_mem) + 6 );
   x    = VolMic ;
   x <<= 8 ;
   x  |= VolSpk ;
   SetRtcMem( RTC_BKP_DR10 , x ) ;
}


ulong GetRtcMem(ulong Addr);
void AudioSetS2C(void);
void Print_S2Cflags(void)
{
  unsigned char Flag   ;
  float    f , fh ;
  uint32_t x      ;
  uint16_t i,j    ;
   AudioSetS2C();
                                       PRINT_TFT( TFT_DEV_C , ("*** INTERCOM SET UP ***\n"));
   Flag = S2C_GetEnable(&S2C_mem ,  0 );         PRINT_TFT( TFT_DEBUG_C , ("EchoCanceller   %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  1 );         PRINT_TFT( TFT_DEBUG_C , ("RescueDetector  %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  2 );         PRINT_TFT( TFT_DEBUG_C , ("DualToneDetector%7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  3 );         PRINT_TFT( TFT_DEBUG_C , ("Postfilter      %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  4 );         PRINT_TFT( TFT_DEBUG_C , ("Noise Reduction %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  5 );         PRINT_TFT( TFT_DEBUG_C , ("BuzzDetector    %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  6 );         PRINT_TFT( TFT_DEBUG_C , ("TxComfortNoise  %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  7 );         PRINT_TFT( TFT_DEBUG_C , ("TxEqualizer     %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  8 );         PRINT_TFT( TFT_DEBUG_C , ("SpeechDetector  %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem ,  9 );         PRINT_TFT( TFT_DEBUG_C , ("TxAGC           %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem , 10 );         PRINT_TFT( TFT_DEBUG_C , ("RxLimiter       %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem , 11 );         PRINT_TFT( TFT_DEBUG_C , ("RxParamEQ       %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem , 12 );         PRINT_TFT( TFT_DEBUG_C , ("TX Mute         %7d\n",Flag));
   Flag = S2C_GetEnable(&S2C_mem , 13 );         PRINT_TFT( TFT_DEBUG_C , ("RX Mute         %7d\n \n",Flag));
   x = GetRtcMem( RTC_BKP_DR10 ) ;
   f = S2C_GetTxAGC_Ref_dB(&S2C_mem);          PRINT_TFT( TFT_DEV_C , ("TxAGC Ref  dB        %4.2f\n",f));
   f = S2C_GetVolume_dB   (&S2C_mem);          PRINT_TFT( TFT_DEV_C , ("Spk Volume dB        %4.2f   (%d)\n",f , (x & 0xff)         ));
   f = S2C_GetMicGain_dB  (&S2C_mem);          PRINT_TFT( TFT_DEV_C , ("Mic Volume dB        %4.2f   (%d)\n",f , ((x >> 8) & 0xff)  ));
   f = S2C_GetLimiterRef_dB(&S2C_mem);         PRINT_TFT( TFT_DEV_C , ("LimiterRef dB        %4.2f\n",f));
   f = S2C_GetPostGain_dB  (&S2C_mem);         PRINT_TFT( TFT_DEV_C , ("PostGain   dB        %4.2f\n\n",f));
   j = S2C_GetMicEqNumberOfBands(&S2C_mem);
   if (j>21) j = 21 ;
   for (i=0; i<j; i++)
   {
     fh = S2C_GetMicEqLogBandFreq_Hz(&S2C_mem , i);
     f  = S2C_GetMicEqLogBandGain_dB(&S2C_mem , i);
     PRINT_TFT( TFT_DEV_C , ("Mic Eq Bands Hz %5.1f   dB %4.2f\n",fh,f));
   }
}


//void DoDeviceOpen(void);
void ProcessDTMF(char c)
{
  static float x ;
  if ((c>='0') && (c<='9'))
     {
       if (Global_Dtmf == 501)
          {
            if (c == '1')
               {
                 x = S2C_GetVolume_dB(&S2C_mem);
                 if (x<20) x++    ;
                 if (x>20) x = 20 ;
                 S2C_SetVolume_dB(&S2C_mem, x);
                 KeepS2VolandMic();
               }
            if (c == '2')
               {
                 x = S2C_GetVolume_dB(&S2C_mem);
                 if (x>-40) x--     ;
                 if (x<-40) x = -40 ;
                 S2C_SetVolume_dB(&S2C_mem, x);
                 KeepS2VolandMic();
               }
            if (c == '3')
               {
                 x = S2C_GetMicGain_dB(&S2C_mem);
                 if (x<20) x++    ;
                 if (x>20) x = 20 ;
                 S2C_SetMicGain_dB(&S2C_mem,x);
                 KeepS2VolandMic();
               }
            if (c == '4')
               {
                 x = S2C_GetMicGain_dB(&S2C_mem);
                 if (x>-6) x--    ;
                 if (x<-6) x = -6 ;
                 S2C_SetMicGain_dB(&S2C_mem,x);
                 KeepS2VolandMic();
               }
          }
         else
          {
            Global_Dtmf *= 10 ;
            Global_Dtmf += (c & 0x0f) ;
            if (Global_Dtmf)
               {
                 //if (Global_Dtmf == Global_SetUpIntrcomGateOpen) { DoDeviceOpen();       Global_Dtmf = 0 ; }
               }
          }
     }
   else
     {
       Global_Dtmf = 0 ;
     }
}


/******************************************************************
 * [HANDLER] Info DTMF
 ******************************************************************/
void ex_call_dtmf_info_handler(const char* sig, const char* duration, void *user_data)
{
	//sig[0] contains the single recieved DTMF
	//duration contains its lenght in ms
	//user data is a pointer to a user specified memory

	char str[50];
        char c ;
        c = sig[0] ;
	strcpy(str, "  DTMF received ");
	strncat(str, &sig[0], 1);
	strcat(str, ", duration[ms]: ");
	strncat(str, &duration[0], 3);
	strncat(str, "\n", 1);
         ProcessDTMF(c);
//	if(sig[0]=='9')
//	{
//		bdSipUa_hangUp(sipUa_example_mem);
//	}
        if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV , (str));
}

void ex_message_handler(char *sender, const char *ctype, char *body, uint32_t body_size, void *user_data)
{
   if (Global_TftPrintDebug)
      {
	PRINT_TFT(TFT_DEV , ("Sender: %s\n", sender));
	PRINT_TFT(TFT_DEV , ("C-TYPE: %s\n", ctype));
	PRINT_TFT(TFT_DEV , ("Message Body: %.*s\n", body_size, body));
      }
}


/******************************************************************
 * [HANDLER] RFC2833 DTMF
 ******************************************************************/
void ex_call_rfc2833_handler(const char sig, void *user_data)
{
	//sig[0] contains the single recieved DTMF
	//user data is a pointer to a user specified memory

	if (Global_TftPrintDebug)  PRINT_TFT(TFT_DEV ,  ((char *)"received RTP event (DTMF): '%c'\n", sig));
        ProcessDTMF(sig);
}
 /************************************************************************************************************************************
 * INITIALIZATION
 ************************************************************************************************************************************/

/******************************************************************
 * Init Sip Ua Example
 ******************************************************************/
void sipUaEx_init(bdSipUa_mem_t *mem_ua, void *userMem)
{
	/*
	 * Init bdSipUa
	 */
	int16_t err = bdSipUa_alloc(mem_ua);
	sipUa_example_mem = mem_ua;

	if(err)
	{
		PRINT_TFT(TFT_DEV , ("bdSipUa memory allocation FAIL.\n"));
		return;
	}

	/*
	 * Set the SIP Handlers
	 */
	bdSipUa_sipHandlers_t handlers;

	handlers.register_handler    = ex_register_handler;
	handlers.call_status_handler = ex_call_status_handler;
	handlers.call_status_user_data = userMem;		//fill this with whatever pointer you may need;

	handlers.sip_info_dtmf_handler = ex_call_dtmf_info_handler;
	handlers.sip_info_dtmf_user_data = 0;		//fill this with whatever pointer you may need;

	handlers.sip_message_handler = ex_message_handler;
	handlers.sip_message_user_data = 0;		//fill this with whatever pointer you may need;

  	handlers.sip_rfc2833_dtmf_handler = ex_call_rfc2833_handler;
  	handlers.sip_rfc2833_dtmf_user_data = 0;		//fill this with whatever pointer you may need;


	bdSipUa_setSipHandlers(mem_ua, handlers);
}
