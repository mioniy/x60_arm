/*
 * (C) 2020, BDSOUND SRL. ALL RIGHTS RESERVED.
 *  THIS FILE IS RELEASED UNDER A VALID LICENSE ASSIGNED BY BDSOUND SRL ONLY.
 * IT IS FORBIDDEN ANY USE OF THIS CODE, WHOLE OR IN PART, IN ANY WAY, WITHOUT A VALID LICENSE FROM BDSOUND SRL.
 */

#include "audioInterfaceAEC.h"
#include "mediaAudioLayer.h"
//#include "lcd_log.h"
#include <stdlib.h>
#include <string.h>
#include "cmsis_os.h"
#include "lwip/sys.h"
#include "math.h"
#include "main.h"

uint16_t bdAECprocessBYPASS=0;
// Aec Audio Device Ptr to be used in Interrupt
audioInterfaceAEC_t *memPtr_audioInterfaceAEC = NULL;

osSemaphoreId RxBfrSemaphore = NULL; /* Semaphore to signal incoming packets */


/******************************************************************
 * AEC Process Task
 * aec processing
 ******************************************************************/
void *aec_proc_task(void *arg)
{
	audioInterfaceAEC_t *audioInterfaceAEC = (audioInterfaceAEC_t *)arg;

	int16_t callEstablished = 0;
	int i=0;
 	int j=0;

#ifndef __CC_ARM
 	osDelay(5);
#endif

	while(1)
	{
           if (osSemaphoreWait( RxBfrSemaphore, osWaitForever)==osOK)
              {
//		sys_arch_sem_wait(audioInterfaceAEC->aecProcSemaphore,0);
		callEstablished = bdSipUa_getPlayBuffer(audioInterfaceAEC->sipUa,audioInterfaceAEC->RinOut);
		if(!callEstablished)
			memset(audioInterfaceAEC->RinOut, 0, S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH * sizeof(short));
		if(bdAECprocessBYPASS==0 && callEstablished)
                   {
                     S2C_ProcessMicSpkFrame(memPtr_audioInterfaceAEC->S2C_mem, audioInterfaceAEC->SinOut, audioInterfaceAEC->RinOut);
                   }
		j=1;
		for(i=0;i<audioInterfaceAEC->frame_size;i++)
		{

			if(callEstablished)
				audioInterfaceAEC->pAuxPlay[j] = audioInterfaceAEC->RinOut[i]/2;  // Play
			else
				audioInterfaceAEC->pAuxPlay[j] = (int16_t) 0;  // Play

			audioInterfaceAEC->pAuxPlay[j-1] = audioInterfaceAEC->pAuxPlay[j];

			j+=2;
		}
		bdSipUa_putCaptureBuffer(audioInterfaceAEC->sipUa,audioInterfaceAEC->SinOut);
              }
	}
}


/******************************************************************
 * Init AEC (Settings)
 * Echo cancel processing initialization
 ******************************************************************/
void audioInterfaceAEC_reInitAEC(void)
{
	S2C_Init(memPtr_audioInterfaceAEC->S2C_mem, S2C_MODE_ECNR);
}

/******************************************************************
 * Init Audio Interface
 * Audio Interface initialization, initialize the codec interface and all DMA and buffer required.
 ******************************************************************/
void audioInterfaceAEC_init(audioInterfaceAEC_t *mem_audioInterfaceAEC, bdSipUa_mem_t *sipUaMem, S2C_mem_t *S2C_mem)
{
	/*
	 * Store AEC & SipUa Pointers
	 */
    mem_audioInterfaceAEC->sipUa = sipUaMem;
	mem_audioInterfaceAEC->S2C_mem = S2C_mem;
	memPtr_audioInterfaceAEC = mem_audioInterfaceAEC;
        memPtr_audioInterfaceAEC->pAuxPlay   = &memPtr_audioInterfaceAEC->bufferAudioPlay[0]   ;
        memPtr_audioInterfaceAEC->pAuxRecord = &memPtr_audioInterfaceAEC->bufferAudioRecord[0] ;

	/*
	 * Init AEC (Settings)
	 */
	S2C_Init(S2C_mem, S2C_MODE_ECNR);

	mem_audioInterfaceAEC->aecProcSemaphore = NULL;
	sys_sem_new(&mem_audioInterfaceAEC->aecProcSemaphore,1);

        RxBfrSemaphore = xSemaphoreCreateBinary();


	mem_audioInterfaceAEC->frame_size = S2C_AUDIO_FRAME_SIZE_MS*S2C_AUDIO_SAMPLE_FREQUENCY/1000;
	mediaAudioLayer_Init(S2C_AUDIO_SAMPLE_FREQUENCY , (uint32_t *)mem_audioInterfaceAEC->bufferAudioPlay,
											(uint32_t *)mem_audioInterfaceAEC->bufferAudioRecord,mem_audioInterfaceAEC->frame_size*2*2);

	/*
	 * Create AEC Processing Task
	 */
	xTaskHandle *aecTask=NULL;
	xTaskCreate((pdTASK_CODE)aec_proc_task, "aecProc_th", 800, mem_audioInterfaceAEC, 2, aecTask);
}



/******************************************************************
 * Play Transfer Callback
 ******************************************************************/
 void audioInterfaceAEC_TransferPlayCallBack(short completeTransfer)
{
}

/******************************************************************
 * Record Transfer Callback
 ******************************************************************/
void audioInterfaceAEC_TransferRecordCallBack(short completeTransfer)
{
	int16_t i=0;
	int16_t j=0;
	int16_t *pBufferAudioRecord0 = &memPtr_audioInterfaceAEC->bufferAudioRecord[0];
	int16_t *pBufferAudioRecord1 = &memPtr_audioInterfaceAEC->bufferAudioRecord[memPtr_audioInterfaceAEC->frame_size*2];
	int16_t *pBufferAudioPlay0 = &memPtr_audioInterfaceAEC->bufferAudioPlay[0];
	int16_t *pBufferAudioPlay1 = &memPtr_audioInterfaceAEC->bufferAudioPlay[memPtr_audioInterfaceAEC->frame_size*2];

   if( (Global_audR3 != 'p') && (Global_audR3 != 'r') && (Global_audR3 != 'e'))
      return;

	if(completeTransfer)
	{
		memPtr_audioInterfaceAEC->pAuxPlay = pBufferAudioPlay1;
		memPtr_audioInterfaceAEC->pAuxRecord = pBufferAudioRecord1;
	}
	else
	{
		memPtr_audioInterfaceAEC->pAuxPlay = pBufferAudioPlay0;
		memPtr_audioInterfaceAEC->pAuxRecord = pBufferAudioRecord0;
	}


	j=0;
	for(i=0;i<memPtr_audioInterfaceAEC->frame_size;i++)
	{
			memPtr_audioInterfaceAEC->SinOut[i] =  (memPtr_audioInterfaceAEC->pAuxRecord[j]+memPtr_audioInterfaceAEC->pAuxRecord[j+1])/2;
			j+=2;
	}

	/*
	 * Post AEC proc semaphore
	 */
        osSemaphoreRelease(RxBfrSemaphore);

	portBASE_TYPE xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(memPtr_audioInterfaceAEC->aecProcSemaphore, &xHigherPriorityTaskWoken);
	/* Switch tasks if necessary. */
	if ( xHigherPriorityTaskWoken != pdFALSE ) portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

uint16_t mediaAudioLayer_GetSampleCallBack(void)
{
  return 0;
}

uint16_t mediaAudioLayer_GetSampleCallBackEXT(void)
{
  return 0;
}

void AudioSetS2C(void)
{
  float VolSpk , VolMic ;
  uint8_t  b ;
  uint32_t x ;
   S2C_Init(memPtr_audioInterfaceAEC->S2C_mem, S2C_MODE_ECNR);
   x = GetRtcMem( RTC_BKP_DR10 ) ;
   b =  x & 0x7f ; // 0..60
   VolSpk  = b  ;
   VolSpk -= 40 ;
   if ((VolSpk < -40) || (VolSpk > 20)) VolSpk = 8 ;
   b =  ((x >> 8) & 0x7f); // 0..26
   VolMic  = b ;
   VolMic -= 6 ;
   if ((VolMic <  -6) || (VolMic > 20)) VolMic = 8 ;
   S2C_SetVolume_dB (&S2C_mem, VolSpk);
   S2C_SetMicGain_dB(&S2C_mem, VolMic);
}
