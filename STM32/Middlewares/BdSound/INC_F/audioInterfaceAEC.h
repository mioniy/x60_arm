/**
 *
 *  Version 1.0
 *
 *  Copyright 2019 bdSound s.r.l.
 *  All rights reserved.
 *
 *  www.bdsound.com
 *
 *
 *  Use in binary forms, with or without modification, 
 *  are permitted provided that the following conditions are met:
 *  1) redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  2) this file must be used within a project that includes at least
 *  one fully licensed ICM4 product (or other products) from bdSound
 *  Redistribution in source form, with or without modification,
 *  is forbidden. 
 *  THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
#ifndef BD_AUDIO_INTERFACE_AEC_H
#define BD_AUDIO_INTERFACE_AEC_H

#include <stdint.h>
#include "bdSipUa.h"
#include "bdS2C.h"

/******************************************************************
 * Audio Interface AEC Data Mem
 ******************************************************************/
typedef struct audioInterfaceAEC_t {  
  // Record Buffer 1 channel
  short  SinOut[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH];  /* AUDIO Buffer */
  short  RinOut[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH];  /* AUDIO Buffer */
	//float  *SinOutmic[1];

  // Play & Record Buffer 2 Channel not interleaved
  int16_t bufferAudioRecord[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH*2*2];  /* AUDIO Buffer */
  int16_t bufferAudioPlay[S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH*2*2];  	/* AUDIO Buffer */
	
	uint16_t frame_size;
  
  // Pointers to Play & Record Buffer 2 Channel not interleaved
  int16_t *pAuxPlay;
  int16_t *pAuxRecord;
  
  // SipUa
	bdSipUa_mem_t *sipUa;
	
	// Semaphore wait for buffer from DMA
  void *aecProcSemaphore;
} audioInterfaceAEC_t;

/******************************************************************
 * Init Audio Interface
 ******************************************************************/
void audioInterfaceAEC_init(audioInterfaceAEC_t *mem_audioInterfaceAEC, bdSipUa_mem_t *sipUaMem);

/******************************************************************
 * ReInit Audio Interface
 ******************************************************************/
void audioInterfaceAEC_reInitAEC(void);


void audioInterfaceAEC_TransferRecordCallBack(short completeTransfer);
void audioInterfaceAEC_TransferPlayCallBack(short completeTransfer);

// Delay
//void Delay(__IO uint32_t nCount);

#endif // BD_AUDIO_INTERFACE_AEC_H
