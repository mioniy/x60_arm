/**
 *
 *  Version 1.0
 *
 *  Copyright 2019 bdSound s.r.l.
 *  All rights reserved.
 *
 *  www.bdsound.com
 *
 *
 *  Use in binary forms, with or without modification, 
 *  are permitted provided that the following conditions are met:
 *  1) this file must be used within a project that includes at least
 *  one fully licensed S2C product (or other products) from bdSound
 *  Redistribution in source form, with or without modification,
 *  is forbidden. 
 *  THIS SOFTWARE IS PROVIDED BY BDSOUND ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 
#ifndef SIP_UA_EXAMPLE_H
#define SIP_UA_EXAMPLE_H

#include "bdSipUa.h"

/******************************************************************
 * Init Sip Ua Example
 ******************************************************************/
void sipUaEx_init(bdSipUa_mem_t *mem_ua, void *userMem);


#endif // SIP_UA_EXAMPLE_H
