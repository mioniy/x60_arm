/**
 *
 *  Version 1.0
 *
 *  Copyright 2019 bdSound s.r.l.
 *  All rights reserved.
 *
 *  www.bdsound.com
 *
 *
 *  Use in binary forms, with or without modification, 
 *  are permitted provided that the following conditions are met:
 *  1) this file must be used within a project that includes at least
 *  one fully licensed ICM4 product (or other products) from bdSound
 *  Redistribution in source form, with or without modification,
 *  is forbidden. 
 *  THIS SOFTWARE IS PROVIDED BY BDSOUND ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BD_AUDIO_CODEC
#define __BD_AUDIO_CODEC

/* Includes ------------------------------------------------------------------*/
//#include "bdDMA.h"
//#include "bdI2S.h"
#include <stdint.h>

/* For the DMA modes select the interrupt that will be used */
#define MEDIA_AL_MODE_CIRCULAR

#define MEDIA_AL_DMA_IT_TC_EN
#define MEDIA_AL_DMA_IT_HT_EN

#define MEDIA_AL_IRQ_PREPRIO           11   /* Select the preemption priority level(0 is the highest) */
#define MEDIA_AL_IRQ_SUBRIO            0   /* Select the sub-priority level (0 is the highest) */


/* I2C clock speed configuration (in Hz)*/

#ifndef I2C_SPEED
 #define I2C_SPEED                        100000
#endif /* I2C_SPEED */


/* Audio interface : I2S or DAC */
#define AUDIO_INTERFACE_I2S           1
#define AUDIO_INTERFACE_DAC           2

/* Codec output DEVICE */
//#define OUTPUT_DEVICE_SPEAKER         1
//#define OUTPUT_DEVICE_HEADPHONE       2
//#define OUTPUT_DEVICE_BOTH            3
//#define OUTPUT_DEVICE_AUTO            4

/* Volume Levels values */
#define DEFAULT_VOLMIN                0x00
#define DEFAULT_VOLMAX                0xFF
#define DEFAULT_VOLSTEP               0x04

#define AUDIO_PAUSE                   0
#define AUDIO_RESUME                  1

/* Codec POWER DOWN modes */
#define CODEC_PDWN_HW                 1
#define CODEC_PDWN_SW                 2

/* MUTE commands */
#define AUDIO_MUTE_ON                 1
#define AUDIO_MUTE_OFF                0

#define DMA_MAX(x)           (((x) <= DMA_MAX_SZE)? (x):DMA_MAX_SZE)


uint32_t mediaAudioLayer_Init(uint32_t AudioFreq, uint32_t* pBufferPlay, uint32_t* pBufferRecord, uint32_t Size);
uint32_t mediaAudioLayer_DeInit(void);

void audioPlay(uint16_t* Addr, uint32_t Size);
void audioRecord(uint16_t* Addr, uint32_t Size);

/* User Callbacks: user has to implement these functions in his code if
  they are needed. -----------------------------------------------------------*/

uint16_t mediaAudioLayer_GetSampleCallBack(void);
uint16_t mediaAudioLayer_GetSampleCallBackEXT(void);

void mediaAudioLayer_TransferPlayCallBack(short completeTransfer);
void mediaAudioLayer_TransferRecordCallBack(short completeTransfer);

uint32_t Codec_TIMEOUT_UserCallback(void);


#endif /* __BD_AUDIO_CODEC */


