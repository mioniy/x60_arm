/**
 *
 *  Version 1.0
 *
 *  Copyright 2019 bdSound s.r.l.
 *  All rights reserved.
 *
 *  www.bdsound.com
 *
 *
 *  Use in binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *  1) this file must be used within a project that includes at least
 *  one fully licensed ICM4 product (or other products) from bdSound
 *  Redistribution in source form, with or without modification,
 *  is forbidden.
 *  THIS SOFTWARE IS PROVIDED BY BDSOUND ''AS IS'' AND ANY
 *  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/* Includes ------------------------------------------------------------------*/
#include "mediaAudioLayer.h"
#include "audioInterfaceAEC.h"
//#include "stm32746g_discovery_audio.h"
//#include "lcd_log.h"




#define ONLY_TEST








uint32_t AudioTotalSize = 0xFFFF; /* This variable holds the total size of the audio file */
uint32_t AudioRemSize   = 0xFFFF; /* This variable holds the remaining data in audio file */
uint16_t *CurrentPos ;             /* This variable holds the current position of audio pointer */

uint8_t dmaToggle = 0;
uint32_t dmaPlaySize = 0;
uint32_t dmaRecordSize = 0;

uint32_t CurrAudioInterface = AUDIO_INTERFACE_I2S; //AUDIO_INTERFACE_DAC

uint8_t BSP_AUDIO_OUT_Play(uint16_t* pBuffer, uint32_t Size);
uint8_t  BSP_AUDIO_IN_Record(uint16_t* pbuf, uint32_t size);
/**
  * @brief  Configure the audio peripherals and DMA
	* @brief  Starts play audio stream from a data buffer pBufferPlay for a determined size.
	* @brief  Starts record audio stream from a data buffer  pBufferRecord for a determined size.
  * @param  AudioFreq: Audio frequency used to play the audio stream.
  * @param  pBufferPlay: Pointer to the buffer Play
  * @param  pBufferRecord: Pointer to the buffer  Record
  * @param  Size: Number of audio data BYTES.
  * @retval 0 if correct communication, else wrong communication
*/
uint32_t mediaAudioLayer_Init(uint32_t AudioFreq, uint32_t* pBufferPlay, uint32_t* pBufferRecord, uint32_t size)
{

//  BSP_AUDIO_IN_OUT_Init(INPUT_DEVICE_DIGITAL_MICROPHONE_2, OUTPUT_DEVICE_HEADPHONE, AudioFreq, DEFAULT_AUDIO_IN_BIT_RESOLUTION, DEFAULT_AUDIO_IN_CHANNEL_NBR);
  BSP_AUDIO_IN_Record((uint16_t*)pBufferRecord, size);
//  BSP_AUDIO_OUT_SetAudioFrameSlot(CODEC_AUDIOFRAME_SLOT_02);
  BSP_AUDIO_OUT_Play((uint16_t*)pBufferPlay, size);
	return 0;
}


uint32_t mediaAudioLayer_DeInit(void)
{
#ifndef ONLY_TEST
	BSP_AUDIO_IN_DeInit();
	BSP_AUDIO_OUT_DeInit();
#endif
	return 0;
}


/* lasciare solo setplay e setrecord */
void audioPlay(uint16_t* addr, uint32_t size)
{
   if(size == 0)
	size = dmaPlaySize;
   else
	dmaPlaySize = size;

#ifndef ONLY_TEST
	 BSP_AUDIO_OUT_Play(addr, size);
#endif
}


void audioRecord(uint16_t* addr, uint32_t size)
{
	if(size == 0)
	 size = dmaRecordSize;
	else
	 dmaRecordSize = size;

#ifndef ONLY_TEST
	BSP_AUDIO_IN_Record(addr, size);
#endif
}


///////////////////////////////////////////////////////////////////////
/////////////////////DMA INTERRUPT/////////////////////////////////////
///////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------
       Callbacks implementation:
           the callbacks API are defined __weak in the stm32746g_discovery_audio.c file
           and their implementation should be done the user code if they are needed.
           Below some examples of callback implementations.
  ----------------------------------------------------------------------------*/
/**
  * @brief Manages the DMA Transfer complete interrupt.
  * @param None
  * @retval None
  */
void BSP_AUDIO_IN_TransferComplete_CallBack(void)
{
  audioInterfaceAEC_TransferRecordCallBack(1);
}

/**
  * @brief  Manages the DMA Half Transfer complete interrupt.
  * @param  None
  * @retval None
  */
void BSP_AUDIO_IN_HalfTransfer_CallBack(void)
{
  audioInterfaceAEC_TransferRecordCallBack(0);
}

