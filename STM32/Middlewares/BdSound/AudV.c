


/*
   SAI2_FS_A          PD12
   SAI2_MCLK_A        PI4
   SAI2_SCK_A         PD13
   SAI2_SD_A          PD11
   SAI2_SD_B          PA0
*/




#include "main.h"
#include "audioInterfaceAEC.h"


#define AUDIO_SAIx_CLK_ENABLE()              __HAL_RCC_SAI2_CLK_ENABLE()
#define AUDIO_SAIx_FS_GPIO_PORT              GPIOD
#define AUDIO_SAIx_FS_AF                     GPIO_AF10_SAI2
#define AUDIO_SAIx_FS_PIN                    GPIO_PIN_12
#define AUDIO_SAIx_SCK_GPIO_PORT             GPIOD
#define AUDIO_SAIx_SCK_AF                    GPIO_AF10_SAI2
#define AUDIO_SAIx_SCK_PIN                   GPIO_PIN_13
#define AUDIO_SAIx_SD_A_GPIO_PORT            GPIOD
#define AUDIO_SAIx_SD_A_AF                   GPIO_AF10_SAI2
#define AUDIO_SAIx_SD_A_PIN                  GPIO_PIN_11
#define AUDIO_SAIx_MCLK_GPIO_PORT            GPIOI
#define AUDIO_SAIx_MCLK_AF                   GPIO_AF10_SAI2
#define AUDIO_SAIx_MCLK_PIN                  GPIO_PIN_4
#define AUDIO_SAIx_SD_B_GPIO_PORT            GPIOA
#define AUDIO_SAIx_SD_B_AF                   GPIO_AF10_SAI2
#define AUDIO_SAIx_SD_B_PIN                  GPIO_PIN_0
#define AUDIO_SAIx_MCLK_ENABLE()             __HAL_RCC_GPIOI_CLK_ENABLE()
#define AUDIO_SAIx_SCK_ENABLE()              __HAL_RCC_GPIOD_CLK_ENABLE()
#define AUDIO_SAIx_FS_ENABLE()               __HAL_RCC_GPIOD_CLK_ENABLE()
#define AUDIO_SAIx_SD_A_ENABLE()             __HAL_RCC_GPIOD_CLK_ENABLE()
#define AUDIO_SAIx_SD_B_ENABLE()             __HAL_RCC_GPIOA_CLK_ENABLE()


extern audioInterfaceAEC_t  audioInterfaceAEC  ;

SAI_HandleTypeDef   SaiHandleTX = {0};
static SAI_HandleTypeDef   SaiHandleRX = {0};
static DMA_HandleTypeDef   hSaiDmaTX   = {0};
static DMA_HandleTypeDef   hSaiDmaRX   = {0};


static __IO int16_t        UpdatePointer = -1  ;


extern uint16_t PlayBuff[PLAY_BUFF_SIZE] ;

// ----------------------------------------------------------

void Aud_Init(uint32_t Speed , uint32_t Aud_Type)
{
  uint32_t           Sbit1 , Sbit2  ;
  uint32_t           Size           ;
  GPIO_InitTypeDef  GPIO_InitStruct ;
  /* Initialize SAI */
  Global_Aud_Type = Aud_Type ;

  __HAL_SAI_RESET_HANDLE_STATE(&SaiHandleTX);
  __HAL_SAI_DISABLE(&SaiHandleTX);
  __HAL_SAI_RESET_HANDLE_STATE(&SaiHandleTX);

  if (Aud_Type) Sbit1 = 64 ;
           else Sbit1 = 32 ;
  Sbit2 = 64 ;

  SaiHandleTX.Instance                    = SAI2_Block_A                  ;
  SaiHandleTX.Init.AudioMode              = SAI_MODEMASTER_TX             ;
  SaiHandleTX.Init.Synchro                = SAI_ASYNCHRONOUS              ;
  SaiHandleTX.Init.OutputDrive            = SAI_OUTPUTDRIVE_ENABLE        ;
  SaiHandleTX.Init.NoDivider              = SAI_MASTERDIVIDER_ENABLE      ;
  SaiHandleTX.Init.FIFOThreshold          = SAI_FIFOTHRESHOLD_1QF         ;
  SaiHandleTX.Init.AudioFrequency         = Speed                         ; // SAI_AUDIO_FREQUENCY_44K ; // SAI_AUDIO_FREQUENCY_22K;
  SaiHandleTX.Init.Protocol               = SAI_FREE_PROTOCOL             ;
  SaiHandleTX.Init.DataSize               = SAI_DATASIZE_16               ;
  SaiHandleTX.Init.FirstBit               = SAI_FIRSTBIT_MSB              ;
  SaiHandleTX.Init.ClockStrobing          = SAI_CLOCKSTROBING_FALLINGEDGE ; // SAI_CLOCKSTROBING_RISINGEDGE ;
  SaiHandleTX.Init.MonoStereoMode         = SAI_STEREOMODE               ;
  SaiHandleTX.FrameInit.FrameLength       = Sbit1                         ;
  SaiHandleTX.FrameInit.ActiveFrameLength = (Sbit1 / 2)                  ;
  SaiHandleTX.FrameInit.FSDefinition      = SAI_FS_CHANNEL_IDENTIFICATION ; // SAI_FS_CHANNEL_IDENTIFICATION ;
  SaiHandleTX.FrameInit.FSPolarity        = SAI_FS_ACTIVE_LOW             ;
  SaiHandleTX.FrameInit.FSOffset          = SAI_FS_BEFOREFIRSTBIT         ; // SAI_FS_BEFOREFIRSTBIT         ;
  SaiHandleTX.SlotInit.FirstBitOffset     = 0                             ;
  SaiHandleTX.SlotInit.SlotSize           = SAI_SLOTSIZE_DATASIZE         ; // SAI_SLOTSIZE_16B              ;  // SAI_SLOTSIZE_DATASIZE   //  SAI_SLOTSIZE_16B
  SaiHandleTX.SlotInit.SlotNumber         = 2                             ;
  SaiHandleTX.SlotInit.SlotActive         = ( SAI_SLOTACTIVE_0 | SAI_SLOTACTIVE_1 ) ;
  HAL_SAI_DeInit(&SaiHandleTX);
  if(HAL_OK != HAL_SAI_Init(&SaiHandleTX))  BR_Error_Handler(0x4000);
  __HAL_SAI_ENABLE(&SaiHandleTX);

  // ---------------------------
  __HAL_SAI_RESET_HANDLE_STATE(&SaiHandleRX);
  __HAL_SAI_DISABLE(&SaiHandleRX);
  __HAL_SAI_RESET_HANDLE_STATE(&SaiHandleRX);

  if (Aud_Type)
     {
       SaiHandleRX.Instance                    =  SAI2_Block_B ;
       SaiHandleRX.Init.AudioMode              =  SAI_MODESLAVE_RX ; // SAI_MODEMASTER_RX ; //SAI_MODESLAVE_RX ;
       SaiHandleRX.Init.Synchro                = SAI_SYNCHRONOUS ;
     //  SaiHandleRX.Init.SynchroExt             =  SAI_SYNCEXT_DISABLE;
       SaiHandleRX.Init.OutputDrive            =  SAI_OUTPUTDRIVE_DISABLED ;
       SaiHandleRX.Init.NoDivider              =  SAI_MASTERDIVIDER_ENABLED ; // SAI_MASTERDIVIDER_DISABLE ; // SAI_MASTERDIVIDER_ENABLED ;
       SaiHandleRX.Init.FIFOThreshold          = SAI_FIFOTHRESHOLD_1QF ; //  SAI_FIFOTHRESHOLD_HF ;
       SaiHandleRX.Init.AudioFrequency         =  Speed ;
       SaiHandleRX.Init.Mckdiv                 =  0 ;
     //  SaiHandleRX.Init.MckOverSampling        =  SAI_MCK_OVERSAMPLING_DISABLE;
       SaiHandleRX.Init.MonoStereoMode         =  SAI_STEREOMODE ; // SAI_MONOMODE ; // SAI_STEREOMODE;
     //  SaiHandleRX.Init.CompandingMode         =  SAI_NOCOMPANDING;
     //  SaiHandleRX.Init.TriState               =  SAI_OUTPUT_NOTRELEASED;
       SaiHandleRX.Init.PdmInit.Activation     =  DISABLE ;
       SaiHandleRX.Init.Protocol               =  SAI_FREE_PROTOCOL;
       SaiHandleRX.Init.DataSize               =  SAI_DATASIZE_16;
       SaiHandleRX.Init.FirstBit               =  SAI_FIRSTBIT_MSB;
       SaiHandleRX.Init.ClockStrobing          = SAI_CLOCKSTROBING_FALLINGEDGE; // SAI_CLOCKSTROBING_RISINGEDGE ;//  SAI_CLOCKSTROBING_FALLINGEDGE;

       SaiHandleRX.FrameInit.FrameLength       = Sbit2 ;
       SaiHandleRX.FrameInit.ActiveFrameLength = (Sbit2 / 2) ;
       SaiHandleRX.FrameInit.FSDefinition      = SAI_FS_CHANNEL_IDENTIFICATION ; // SAI_FS_STARTFRAME;
       SaiHandleRX.FrameInit.FSPolarity        = SAI_FS_ACTIVE_LOW;
       SaiHandleRX.FrameInit.FSOffset          = SAI_FS_BEFOREFIRSTBIT ; // SAI_FS_FIRSTBIT ; // SAI_FS_BEFOREFIRSTBIT;


       SaiHandleRX.SlotInit.FirstBitOffset     = 0 ;
       SaiHandleRX.SlotInit.SlotSize           = SAI_SLOTSIZE_DATASIZE;
       SaiHandleRX.SlotInit.SlotNumber         = 2 ;
       SaiHandleRX.SlotInit.SlotActive         = (SAI_SLOTACTIVE_0 | SAI_SLOTACTIVE_1) ;

       /* DeInit SAI PDM input */
       HAL_SAI_DeInit(&SaiHandleRX);

       /* Init SAI PDM input */
       if(HAL_OK != HAL_SAI_Init(&SaiHandleRX)) BR_Error_Handler(0x4000);

       __HAL_SAI_ENABLE(&SaiHandleRX);
     }
  Global_SAI_Tx1 = 0 ;
  Global_SAI_Tx2 = 0 ;
  Global_SAI_Rx1 = 0 ;
  Global_SAI_Rx2 = 0 ;

//    PG3     ------> Voice audio AMP
  GPIO_InitStruct.Pin  = GPIO_PIN_3           ;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP  ;
  GPIO_InitStruct.Pull = GPIO_PULLUP          ;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct)      ;

  if (Aud_Type)
     {
       Size = S2C_AUDIO_FRAME_SIZE_IN_SAMPLES_1CH*2*2 ;
       HAL_SAI_Transmit_DMA(&SaiHandleTX, (uint8_t*) &audioInterfaceAEC.bufferAudioPlay   , Size);
       HAL_SAI_Receive_DMA (&SaiHandleRX, (uint8_t*) &audioInterfaceAEC.bufferAudioRecord , Size);
     }
}

void HAL_SAI_MspInit(SAI_HandleTypeDef *hsai)
{
  GPIO_InitTypeDef  GPIO_Init ;
  static uint32_t   prioT     ;
  static uint32_t   prioR     ;

  if(hsai->Instance == SAI2_Block_A)
  {
    /* Enable SAI1 clock */
    AUDIO_SAIx_CLK_ENABLE();

    /* Configure GPIOs used for SAI1 */
    AUDIO_SAIx_MCLK_ENABLE();
    AUDIO_SAIx_SCK_ENABLE();
    AUDIO_SAIx_FS_ENABLE();
    AUDIO_SAIx_SD_A_ENABLE();

    GPIO_Init.Mode      = GPIO_MODE_AF_PP;
    GPIO_Init.Pull      = GPIO_NOPULL;
    GPIO_Init.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;

    GPIO_Init.Alternate = AUDIO_SAIx_FS_AF;
    GPIO_Init.Pin       = AUDIO_SAIx_FS_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_FS_GPIO_PORT, &GPIO_Init);
    GPIO_Init.Alternate = AUDIO_SAIx_SCK_AF;
    GPIO_Init.Pin       = AUDIO_SAIx_SCK_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_SCK_GPIO_PORT, &GPIO_Init);
    GPIO_Init.Alternate = AUDIO_SAIx_SD_A_AF;
    GPIO_Init.Pin       = AUDIO_SAIx_SD_A_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_SD_A_GPIO_PORT, &GPIO_Init);
    GPIO_Init.Alternate = AUDIO_SAIx_MCLK_AF;
    GPIO_Init.Pin       = AUDIO_SAIx_MCLK_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_MCLK_GPIO_PORT, &GPIO_Init);
    GPIO_Init.Alternate = AUDIO_SAIx_SD_B_AF;
    GPIO_Init.Pin       = AUDIO_SAIx_SD_B_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_SD_B_GPIO_PORT, &GPIO_Init);

    /* Configure DMA used for SAI1 */
    __HAL_RCC_DMA2_CLK_ENABLE();

    hSaiDmaTX.Init.Request             = DMA_REQUEST_SAI2_A;
    hSaiDmaTX.Init.Direction           = DMA_MEMORY_TO_PERIPH;
    hSaiDmaTX.Init.PeriphInc           = DMA_PINC_DISABLE;
    hSaiDmaTX.Init.MemInc              = DMA_MINC_ENABLE;
    hSaiDmaTX.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hSaiDmaTX.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;
    hSaiDmaTX.Init.Mode                = DMA_CIRCULAR;
    hSaiDmaTX.Init.Priority            = DMA_PRIORITY_HIGH;
    hSaiDmaTX.Init.FIFOMode            = DMA_FIFOMODE_ENABLE;
    hSaiDmaTX.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    hSaiDmaTX.Init.MemBurst            = DMA_MBURST_SINGLE;
    hSaiDmaTX.Init.PeriphBurst         = DMA_PBURST_SINGLE;
    /* Select the DMA instance to be used for the transfer : DMA2_Stream6 */
    hSaiDmaTX.Instance                 = DMA2_Stream6;
    /* Associate the DMA handle */
    __HAL_LINKDMA(hsai, hdmatx, hSaiDmaTX);
    /* Deinitialize the Stream for new transfer */
    HAL_DMA_DeInit(&hSaiDmaTX);
    /* Configure the DMA Stream */
    HAL_DMA_Init(&hSaiDmaTX);
    prioT = 0x0e ;
    HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, prioT , 0);    // 0x01
    HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
  }

  if(hsai->Instance == SAI2_Block_B)
  {
    /* Configure DMA used for SAI1 */
    __HAL_RCC_DMA2_CLK_ENABLE();

    hSaiDmaRX.Init.Request             = DMA_REQUEST_SAI2_B;
    hSaiDmaRX.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    hSaiDmaRX.Init.PeriphInc           = DMA_PINC_DISABLE;
    hSaiDmaRX.Init.MemInc              = DMA_MINC_ENABLE;
    hSaiDmaRX.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hSaiDmaRX.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;
    hSaiDmaRX.Init.Mode                = DMA_CIRCULAR;
    hSaiDmaRX.Init.Priority            = DMA_PRIORITY_HIGH;
    hSaiDmaRX.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    hSaiDmaRX.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    hSaiDmaRX.Init.MemBurst            = DMA_MBURST_SINGLE;
    hSaiDmaRX.Init.PeriphBurst         = DMA_PBURST_SINGLE;
    /* Select the DMA instance to be used for the transfer : DMA2_Stream6 */
    hSaiDmaRX.Instance                 = DMA2_Stream5;
    /* Associate the DMA handle */
    __HAL_LINKDMA(hsai, hdmarx, hSaiDmaRX);
    /* Deinitialize the Stream for new transfer */
    HAL_DMA_DeInit(&hSaiDmaRX);
    /* Configure the DMA Stream */
    HAL_DMA_Init(&hSaiDmaRX);
    prioR = 0x0f ;
    HAL_NVIC_SetPriority(DMA2_Stream5_IRQn, prioR , 0);    // 0x01
    HAL_NVIC_EnableIRQ(DMA2_Stream5_IRQn);
  }
}

// -------------------------------------------------------------------

void DMA2_Stream6_IRQHandler(void)
{
  HAL_DMA_IRQHandler(SaiHandleTX.hdmatx);
}
// -------------------------------------------------------------------
void BSP_AUDIO_IN_TransferComplete_CallBack(void);
void BSP_AUDIO_IN_HalfTransfer_CallBack(void);

void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai)
{
  Global_SAI_Rx1++;
  BSP_AUDIO_IN_TransferComplete_CallBack();
}

void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
  Global_SAI_Rx2++;
  BSP_AUDIO_IN_HalfTransfer_CallBack();
}

void DMA2_Stream5_IRQHandler(void)
{
  HAL_DMA_IRQHandler(SaiHandleRX.hdmarx);
}

// -------------------------------------------------------------------

uint8_t BSP_AUDIO_OUT_Play(uint16_t* pbuf, uint32_t Size)
{
//  HAL_SAI_Transmit_DMA(&SaiHandleTX, (uint8_t*) pbuf, Size);
  return 0 ;
}

uint8_t  BSP_AUDIO_IN_Record(uint16_t* pbuf, uint32_t size)
{
//  HAL_SAI_Receive_DMA(&SaiHandleRX, (uint8_t*)pbuf, size);
  return 0 ;
}

// -------------------------------------------------------------------



